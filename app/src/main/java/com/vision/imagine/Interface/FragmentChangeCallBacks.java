package com.vision.imagine.Interface;

import android.os.Bundle;

public interface FragmentChangeCallBacks {
    public void fragmentName(String fragment, Bundle bundle);
}
