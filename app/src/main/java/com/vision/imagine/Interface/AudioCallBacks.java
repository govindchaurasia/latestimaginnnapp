package com.vision.imagine.Interface;

import com.vision.imagine.pojos.Audio;

public interface AudioCallBacks {
    void startRecordingInter();

    void stopRecordingInter();

    void onStopRecord(Audio audio);
}
