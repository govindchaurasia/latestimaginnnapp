package com.vision.imagine.firebasenotifications.firebaseservices;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vision.imagine.MainActivity;
import com.vision.imagine.firebasenotifications.firebaseutils.NotificationUtils;
import com.vision.imagine.fragments.ExploreIdeaFragment;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by Ravi Dwivedi.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

//        NotificationUtils.setNotificationCountInSharedPrefs(false);
//        ExploreIdeaFragment.setupBadge();

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                handleDataMessage(remoteMessage.getData());
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        } else if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getIcon());
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        NotificationUtils.setPushTokenRegistered(false);
        sendRegistrationToServer(token);
        NotificationUtils.setFirebaseInstanceIdInSharedPreferences(token);
        NotificationUtils.setIsGlobalBroadcastTopicSubscribedInSharedPreferences(false);
        NotificationUtils.setIsIndianBroadcastTopicSubscribedInSharedPreferences(false);
        NotificationUtils.subscribeToTopic(getApplicationContext(), true);
    }

    private void handleNotification(String title, String message, String icon) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
//            Intent resultIntent;
//            if (NotificationUtils.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
//                resultIntent = new Intent(getApplicationContext(), MainActivity.class);
//            else
//            resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("title", title);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("icon", icon);

            // check for image attachment
            if (TextUtils.isEmpty(icon)) {
                showNotificationMessage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, icon);
            }
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(Map<String, String> resultDataObject) {
        Log.e(TAG, "push json: " + resultDataObject.toString());

        try {
            String title = resultDataObject.get("title");
            String message = resultDataObject.get("message");
            String messageJson = resultDataObject.get("m_data");
            String notificationType = resultDataObject.get("m_type");
            String imageUrl = resultDataObject.get("imageUrl");

            if (messageJson != null && !messageJson.isEmpty()) {
                Intent intent = new Intent();
                intent.setAction(Constants.MESSAGE_RECEIVED);
                intent.putExtra("messageJson", messageJson);
                sendBroadcast(intent);
            }

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "messageJson: " + messageJson);
            Log.e(TAG, "notificationType: " + notificationType);

            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
//            Intent resultIntent;
//            if (NotificationUtils.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
//                resultIntent = new Intent(getApplicationContext(), MainActivity.class);
//            else
//            resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("title", title);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("messageJson", messageJson);
            resultIntent.putExtra("notificationType", notificationType);
            resultIntent.putExtra("imageUrl", imageUrl);

            if (notificationType.equals(Constants.NOTIFICATION_TYPE_FRIEND_REQUEST)) {
                NotificationUtils.setFriendRequestCountInSharedPrefs(false);
                ExploreIdeaFragment.setupBadge();
            }else {
                NotificationUtils.setNotificationCountInSharedPrefs(false);
                ExploreIdeaFragment.setupNotificationBadge();
            }

            // check for image attachment
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, imageUrl);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, long timeStamp, Intent intent, boolean isActivityInForeground) {
        notificationUtils = new NotificationUtils(context);
        if (isActivityInForeground) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, long timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        if (!NotificationUtils.isPushTokenRegistered() && !token.isEmpty() && !SharedPrefUtils.getStringData(Constants.USER_ID).isEmpty()) {
            NotificationUtils.callRegisterPushTokenApi(token);
        }
    }
}