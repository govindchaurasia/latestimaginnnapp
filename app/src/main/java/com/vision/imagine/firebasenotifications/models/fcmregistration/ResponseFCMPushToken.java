package com.vision.imagine.firebasenotifications.models.fcmregistration;

import com.google.gson.annotations.SerializedName;

public class ResponseFCMPushToken {

    @SerializedName("remark")
    private String remark;

    @SerializedName("code")
    private String code;

    @SerializedName("userId")
    private String userId;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}