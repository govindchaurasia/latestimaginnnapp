package com.vision.imagine.firebasenotifications.models.fcmregistration;

import com.google.gson.annotations.SerializedName;

public class RequestFCMPushToken {

    @SerializedName("token")
    private String firebasePushToken;

    @SerializedName("userId")
    private String userId;

    public String getFirebasePushToken() {
        return firebasePushToken;
    }

    public void setFirebasePushToken(String firebasePushToken) {
        this.firebasePushToken = firebasePushToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}