package com.vision.imagine;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

import com.google.firebase.FirebaseApp;

public class ImagineApplication extends MultiDexApplication {

    private static Context appContext;

    public static Context getContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        FirebaseApp.initializeApp(this);
        /* If you has other classes that need context object to initialize when application is created,
         you can use the appContext here to process. */
    }
}