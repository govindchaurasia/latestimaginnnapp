package com.vision.imagine.utils;

import android.app.ProgressDialog;
import android.content.Context;

import com.vision.imagine.R;

import static android.os.Build.VERSION_CODES.R;

/**
 * Created by Studio-Pleximus on 21-09-2017.
 */

public class ProgressDialogBuilder {

    public static ProgressDialog build(Context context, String title, String message){
        ProgressDialog m_Dialog = new ProgressDialog(context, com.vision.imagine.R.style.AppCompatAlertDialogStyle);
        m_Dialog.setMessage(message);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(true);
        m_Dialog.setCanceledOnTouchOutside(true);
        m_Dialog.setTitle(title);
        return m_Dialog;
    }
}