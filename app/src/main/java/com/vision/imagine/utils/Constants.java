package com.vision.imagine.utils;

import com.vision.imagine.pojos.PostIdeaModel;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final int UPLOAD = 0;
    public static final int CAPTURE = 1;
    public static final int RECORD = 2;
    public static final int WRITE = 3;
    public static final int DRAW = 4;
    public static final String PHONE = "PHONE";
    public static final String EMAIL = "EMAIL";
    public static final String TOKEN = "TOKEN";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_ID = "USER_ID";
    public static final String USER_PIN = "USER_PIN";
    public static final String CURRENT_SCREEN = "CURRENT_SCREEN";
    public static final String APPLIED_FILTERS = "APPLIED_FILTERS";
    public static final String APPLIED_FILTERS_NAMES = "APPLIED_FILTERS_NAMES";
    public static final String DEFAULT_FILTER_ON = "DEFAULT_FILTER_ON";
    public static final String FIRST_TIME_FILTER = "FIRST_TIME_FILTER_SHOW";

    public static final String USERNAME_EXISTS = "Username Already Exists";
    public static final String USERNAME_NOT_EXISTS = "Username Available";
    public static final String EMAIL_REGEX = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
    public static final String MOBILE_REGEX = "(0/91)?[7-9][0-9]{9}";
    public static final String INVALID_MOBILE = "Invalid Mobile No.";
    public static final String INVALID_EMAIL = "Invalid Email Id";
    public static final String FIELDS_MISSING = "Mandatory fields are missing";
    public static final String TECHNICAL_FAILURE = "Technical Failure. Please contact Administration";
    public static final String INVALID_PIN = "Incorrect PIN. Please enter the valid PIN";
    public static final String PIN_MISSING = "Please enter 4 digit PIN";
    public static final String USERNAME_MISSING = "Please enter Username or Email Id";
    public static final String IDEA_UNPUBLISHED = "unpublished";

    //public static PostIdeaModel postIdeaModel=null;
    //public static int currentIdeaId = 0;

    //    FIREBASE
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String FIREBASE_TOPIC_GLOBAL_BROADCAST = "ImaginnnNotifications";
    public static final String FIREBASE_TOPIC_INDIAN_BROADCAST = "ImaginnnNotificationsIndia";
    public static final String FIREBASE_TOPIC_TEST_BROADCAST = "TestNotificationsImaginnn";
    public static final String CONTRY_CODE_INDIA = "in";
    public static final String GROUP_KEY_IMAGINNN_NOTIFICATIONS = "group_key_imaginnn_notifications";
    public static final String PREF_FIREBASE_INSTANCE_ID = "firebase_instance_id";
    public static final String PREF_FIREBASE_GLOBAL_BROADCAST_TOPIC_SUBSCRIPTION = "firebase_imaginnn_global_topic_subscription";
    public static final String PREF_FIREBASE_INDIAN_BROADCAST_TOPIC_SUBSCRIPTION = "firebase_imaginnn_indian_topic_subscription";
    public static final String PREF_REGISTER_PUSH_TOKEN_STATUS = "register_push_token_status";
    public static final String PREF_FRIEND_REQUEST_COUNT_VALUE = "notification_count_value";
    public static final String PREF_NOTIFICATION_COUNT_VALUE = "general_notification_count_value";
    public static final String MESSAGE_RECEIVED = "message_received";
    public static final String PREF_NOTIFICATION_ID = "notification_id";

    public static final int ACTION_TYPE_ACCEPT_REQUEST = 102;
    public static final int ACTION_TYPE_REJECT_REQUEST = 103;
    public static final int SEARCH_THRESHOLD = 2;

    public static  final String MESSAGE_FROM_ME = "1";
    public static  final String MESSAGE_FROM_YOU = "2";
    public static  final String IDEA_FROM_ME = "3";
    public static  final String IDEA_FROM_YOU = "4";

    public static  final String NOTIFICATION_TYPE_CHAT = "CHAT";
    public static  final String NOTIFICATION_TYPE_FRIEND_REQUEST = "FRIEND_REQUEST";
    public static  final String NOTIFICATION_TYPE_FRIEND_REQUEST_ACCEPTED = "FRIEND_REQUEST_ACCEPTED";

    public static String[] country = {"Afghanistan","Albania"
            ,"Algeria"
            ,"Andorra"
            ,"Angola"
            ,"Anguilla"
            ,"Antigua & Barbuda"
            ,"Argentina"
            ,"Armenia"
            ,"Australia"
            ,"Austria"
            ,"Azerbaijan"
            ,"Bahamas"
            ,"Bahrain"
            ,"Bangladesh"
            ,"Barbados"
            ,"Belarus"
            ,"Belgium"
            ,"Belize"
            ,"Benin"
            ,"Bermuda"
            ,"Bhutan"
            ,"Bolivia"
            ,"Bosnia & Herzegovina"
            ,"Botswana"
            ,"Brazil"
            ,"Brunei Darussalam"
            ,"Bulgaria"
            ,"Burkina Faso"
            ,"Myanmar/Burma"
            ,"Burundi"
            ,"Cambodia"
            ,"Cameroon"
            ,"Canada"
            ,"Cape Verde"
            ,"Cayman Islands"
            ,"Central African Republic"
            ,"Chad"
            ,"Chile"
            ,"China"
            ,"Colombia"
            ,"Comoros"
            ,"Congo"
            ,"Costa Rica"
            ,"Croatia"
            ,"Cuba"
            ,"Cyprus"
            ,"Czech Republic"
            ,"Democratic Republic of the Congo"
            ,"Denmark"
            ,"Djibouti"
            ,"Dominican Republic"
            ,"Dominica"
            ,"Ecuador"
            ,"Egypt"
            ,"El Salvador"
            ,"Equatorial Guinea"
            ,"Eritrea"
            ,"Estonia"
            ,"Ethiopia"
            ,"Fiji"
            ,"Finland"
            ,"France"
            ,"French Guiana"
            ,"Gabon"
            ,"Gambia"
            ,"Georgia"
            ,"Germany"
            ,"Ghana"
            ,"Great Britain"
            ,"Greece"
            ,"Grenada"
            ,"Guadeloupe"
            ,"Guatemala"
            ,"Guinea"
            ,"Guinea-Bissau"
            ,"Guyana"
            ,"Haiti"
            ,"Honduras"
            ,"Hungary"
            ,"Iceland"
            ,"India"
            ,"Indonesia"
            ,"Iran"
            ,"Iraq"
            ,"Israel and the Occupied Territories"
            ,"Italy"
            ,"Ivory Coast (Cote d'Ivoire)"
            ,"Jamaica"
            ,"Japan"
            ,"Jordan"
            ,"Kazakhstan"
            ,"Kenya"
            ,"Kosovo"
            ,"Kuwait"
            ,"Kyrgyz Republic (Kyrgyzstan)"
            ,"Laos"
            ,"Latvia"
            ,"Lebanon"
            ,"Lesotho"
            ,"Liberia"
            ,"Libya"
            ,"Liechtenstein"
            ,"Lithuania"
            ,"Luxembourg"
            ,"Republic of Macedonia"
            ,"Madagascar"
            ,"Malawi"
            ,"Malaysia"
            ,"Maldives"
            ,"Mali"
            ,"Malta"
            ,"Martinique"
            ,"Mauritania"
            ,"Mauritius"
            ,"Mayotte"
            ,"Mexico"
            ,"Moldova, Republic of Monaco"
            ,"Mongolia"
            ,"Montenegro"
            ,"Montserrat"
            ,"Morocco"
            ,"Mozambique"
            ,"Namibia"
            ,"Nepal"
            ,"Netherlands"
            ,"New Zealand"
            ,"Nicaragua"
            ,"Niger"
            ,"Nigeria"
            ,"Korea, Democratic Republic of (North Korea)"
            ,"Norway"
            ,"Oman"
            ,"Pacific Islands"
            ,"Pakistan"
            ,"Panama"
            ,"Papua New Guinea"
            ,"Paraguay"
            ,"Peru"
            ,"Philippines"
            ,"Poland"
            ,"Portugal"
            ,"Puerto Rico"
            ,"Qatar"
            ,"Reunion"
            ,"Romania"
            ,"Russian Federation"
            ,"Rwanda"
            ,"Saint Kitts and Nevis"
            ,"Saint Lucia"
            ,"Saint Vincent's & Grenadines"
            ,"Samoa"
            ,"Sao Tome and Principe"
            ,"Saudi Arabia"
            ,"Senegal"
            ,"Serbia"
            ,"Seychelles"
            ,"Sierra Leone"
            ,"Singapore"
            ,"Slovak Republic "
            ,"Slovenia"
            ,"Solomon Islands"
            ,"Somalia"
            ,"South Africa"
            ,"Korea, Republic of (South Korea)"
            ,"South Sudan"
            ,"Spain"
            ,"Sri Lanka"
            ,"Sudan"
            ,"Suriname"
            ,"Swaziland"
            ,"Sweden"
            ,"Switzerland"
            ,"Syria"
            ,"Tajikistan"
            ,"Tanzania"
            ,"Thailand"
            ,"Timor Leste"
            ,"Togo"
            ,"Trinidad & Tobago"
            ,"Tunisia"
            ,"Turkey"
            ,"Turkmenistan"
            ,"Turks & Caicos Islands"
            ,"Uganda"
            ,"Ukraine"
            ,"United Arab Emirates"
            ,"United States of America (USA)"
            ,"Uruguay"
            ,"Uzbekistan"
            ,"Venezuela"
            ,"Vietnam"
            ,"Virgin Islands (UK)"
            ,"Virgin Islands (US)"
            ,"Yemen"
            ,"Zambia"
            ,"Zimbabwe"};

    public static String[]  code = new String[]{"+93", "+355", "+213", "+1 684", "+376", "+244", "+1 264", "+672", "+1 268", "+54", "+374",

            "+297", "+61", "+43", "+994", "+1 242", "+973", "+880", "+1 246", "+375", "+32", "+501",

            "+229", "+1 441", "+975", "+591", "+387", "+267", "+55", "+246", "+1 284", "+673", "+359",

            "+226", "+95", "+257", "+855", "+237", "+1", "+238", "+1 345", "+236", "+235", "+56", "+86",

            "+61", "+891", "+57", "+269", "+682", "+506", "+385", "+53", "+357", "+420", "+243", "+45",

            "+253", "+1 767", "+1 849", "+1 829", "+1 809", "+593", "+20", "+503", "+240", "+291", "+372",

            "+251", "+500", "+298", "+679", "+358", "+33", "+689", "+241", "+220", "+970", "+995", "+49",

            "+233", "+350", "+30", "+299", "+1 473", "+1 671", "+502", "+224", "+245", "+592", "+509",

            "+379", "+504", "+852", "+36", "+354", "+91", "+62", "+98", "+964", "+353", "+44", "+972",

            "+39", "+225", "+1 876", "+81", "+44", "+962", "+7", "+254", "+686", "+381", "+965", "+996",

            "+856", "+371", "+961", "+266", "+231", "+218", "+423", "+370", "+352", "+853", "+389",

            "+261", "+265", "+60", "+960", "+223", "+356", "+692", "+222", "+230", "+262", "+52", "+691",

            "+373", "+377", "+976", "+382", "+1 664", "+212", "+258", "+264", "+674", "+977", "+31",

            "+599", "+687", "+64", "+505", "+227", "+234", "+683", "+672", "+850", "+1 670", "+47",

            "+968", "+92", "+680", "+507", "+675", "+595", "+51", "+63", "+870", "+48", "+351", "+1",

            "+974", "+242", "+40", "+7", "+250", "+590", "+290", "+1 869", "+1 758", "+1 599", "+508",

            "+1 784", "+685", "+378", "+239", "+966", "+221", "+381", "+248", "+232", "+65", "+421",

            "+386", "+677", "+252", "+27", "+82", "+34", "+94", "+249", "+597", "+268", "+46", "+41",

            "+963", "+886", "+992", "+255", "+66", "+670", "+228", "+690", "+676", "+1 868", "+216",

            "+90", "+993", "+1 649", "+688", "+256", "+380", "+971", "+44", "+1", "+598", "+1 340",

            "+998", "+678", "+58", "+84", "+681", "+970", "+967", "+260", "+263"};

    public static final Map<String, String> country2phone = new HashMap<String, String>();
    static {
        country2phone.put("AF", "+93");
        country2phone.put("AL", "+355");
        country2phone.put("DZ", "+213");
        country2phone.put("AD", "+376");
        country2phone.put("AO", "+244");
        country2phone.put("AG", "+1 268");
        country2phone.put("AR", "+54");
        country2phone.put("AM", "+374");
        country2phone.put("AU", "+61");
        country2phone.put("AT", "+43");
        country2phone.put("BS", "+1 242");
        country2phone.put("BH", "+973");
        country2phone.put("BD", "+880");
        country2phone.put("BB", "+1 246");
        country2phone.put("BY", "+375");
        country2phone.put("BE", "+32");
        country2phone.put("BZ", "+501");
        country2phone.put("BJ", "+229");
        country2phone.put("BT", "+975");
        country2phone.put("BO", "+591");
        country2phone.put("BA", "+387");
        country2phone.put("BW", "+267");
        country2phone.put("BR", "+55");
        country2phone.put("BN", "+673");
        country2phone.put("BG", "+359");
        country2phone.put("BF", "+226");
        country2phone.put("BI", "+257");
        country2phone.put("KH", "+855");
        country2phone.put("CM", "+237");
        country2phone.put("CA", "+1");
        country2phone.put("CV", "+238");
        country2phone.put("CF", "+236");
        country2phone.put("TD", "+235");
        country2phone.put("CL", "+56");
        country2phone.put("CN", "+86");
        country2phone.put("CO", "+57");
        country2phone.put("KM", "+269");
        country2phone.put("CD", "+243");
        country2phone.put("CG", "+242");
        country2phone.put("CR", "+506");
        country2phone.put("CI", "+225");
        country2phone.put("HR", "+385");
        country2phone.put("CU", "+53");
        country2phone.put("CY", "+357");
        country2phone.put("CZ", "+420");
        country2phone.put("DK", "+45");
        country2phone.put("DJ", "+253");
        country2phone.put("DM", "+1 767");
        country2phone.put("DO", "+1 809and1 829");
        country2phone.put("EC", "+593");
        country2phone.put("EG", "+20");
        country2phone.put("SV", "+503");
        country2phone.put("GQ", "+240");
        country2phone.put("ER", "+291");
        country2phone.put("EE", "+372");
        country2phone.put("ET", "+251");
        country2phone.put("FJ", "+679");
        country2phone.put("FI", "+358");
        country2phone.put("FR", "+33");
        country2phone.put("GA", "+241");
        country2phone.put("GM", "+220");
        country2phone.put("DE", "+49");
        country2phone.put("GH", "+233");
        country2phone.put("GR", "+30");
        country2phone.put("GD", "+1 473");
        country2phone.put("GT", "+502");
        country2phone.put("GN", "+224");
        country2phone.put("GW", "+245");
        country2phone.put("GY", "+592");
        country2phone.put("HT", "+509");
        country2phone.put("HN", "+504");
        country2phone.put("HU", "+36");
        country2phone.put("IS", "+354");
        country2phone.put("IN", "+91");
        country2phone.put("ID", "+62");
        country2phone.put("IR", "+98");
        country2phone.put("IQ", "+964");
        country2phone.put("IE", "+353");
        country2phone.put("IL", "+972");
        country2phone.put("IT", "+39");
        country2phone.put("JM", "+1 876");
        country2phone.put("JP", "+81");
        country2phone.put("JO", "+962");
        country2phone.put("KZ", "+7");
        country2phone.put("KE", "+254");
        country2phone.put("KI", "+686");
        country2phone.put("KP", "+850");
        country2phone.put("KR", "+82");
        country2phone.put("KW", "+965");
        country2phone.put("KG", "+996");
        country2phone.put("LA", "+856");
        country2phone.put("LV", "+371");
        country2phone.put("LB", "+961");
        country2phone.put("LS", "+266");
        country2phone.put("LR", "+231");
        country2phone.put("LY", "+218");
        country2phone.put("LI", "+423");
        country2phone.put("LT", "+370");
        country2phone.put("LU", "+352");
        country2phone.put("MK", "+389");
        country2phone.put("MG", "+261");
        country2phone.put("MW", "+265");
        country2phone.put("MY", "+60");
        country2phone.put("MV", "+960");
        country2phone.put("ML", "+223");
        country2phone.put("MT", "+356");
        country2phone.put("MH", "+692");
        country2phone.put("MR", "+222");
        country2phone.put("MU", "+230");
        country2phone.put("MX", "+52");
        country2phone.put("FM", "+691");
        country2phone.put("MD", "+373");
        country2phone.put("MC", "+377");
        country2phone.put("MN", "+976");
        country2phone.put("ME", "+382");
        country2phone.put("MA", "+212");
        country2phone.put("MZ", "+258");
        country2phone.put("MM", "+95");
        country2phone.put("NA", "+264");
        country2phone.put("NR", "+674");
        country2phone.put("NP", "+977");
        country2phone.put("NL", "+31");
        country2phone.put("NZ", "+64");
        country2phone.put("NI", "+505");
        country2phone.put("NE", "+227");
        country2phone.put("NG", "+234");
        country2phone.put("NO", "+47");
        country2phone.put("OM", "+968");
        country2phone.put("PK", "+92");
        country2phone.put("PW", "+680");
        country2phone.put("PA", "+507");
        country2phone.put("PG", "+675");
        country2phone.put("PY", "+595");
        country2phone.put("PE", "+51");
        country2phone.put("PH", "+63");
        country2phone.put("PL", "+48");
        country2phone.put("PT", "+351");
        country2phone.put("QA", "+974");
        country2phone.put("RO", "+40");
        country2phone.put("RU", "+7");
        country2phone.put("RW", "+250");
        country2phone.put("KN", "+1 869");
        country2phone.put("LC", "+1 758");
        country2phone.put("VC", "+1 784");
        country2phone.put("WS", "+685");
        country2phone.put("SM", "+378");
        country2phone.put("ST", "+239");
        country2phone.put("SA", "+966");
        country2phone.put("SN", "+221");
        country2phone.put("RS", "+381");
        country2phone.put("SC", "+248");
        country2phone.put("SL", "+232");
        country2phone.put("SG", "+65");
        country2phone.put("SK", "+421");
        country2phone.put("SI", "+386");
        country2phone.put("SB", "+677");
        country2phone.put("ZA", "+27");
        country2phone.put("ES", "+34");
        country2phone.put("LK", "+94");
        country2phone.put("SD", "+249");
        country2phone.put("SR", "+597");
        country2phone.put("SZ", "+268");
        country2phone.put("SE", "+46");
        country2phone.put("CH", "+41");
        country2phone.put("SY", "+963");
        country2phone.put("TJ", "+992");
        country2phone.put("TZ", "+255");
        country2phone.put("TH", "+66");
        country2phone.put("TL", "+670");
        country2phone.put("TG", "+228");
        country2phone.put("TO", "+676");
        country2phone.put("TT", "+1 868");
        country2phone.put("TN", "+216");
        country2phone.put("TR", "+90");
        country2phone.put("TM", "+993");
        country2phone.put("TV", "+688");
        country2phone.put("UG", "+256");
        country2phone.put("UA", "+380");
        country2phone.put("AE", "+971");
        country2phone.put("GB", "+44");
        country2phone.put("US", "+1");
        country2phone.put("UY", "+598");
        country2phone.put("UZ", "+998");
        country2phone.put("VU", "+678");
        country2phone.put("VA", "+379");
        country2phone.put("VE", "+58");
        country2phone.put("VN", "+84");
        country2phone.put("YE", "+967");
        country2phone.put("ZM", "+260");
        country2phone.put("ZW", "+263");
        country2phone.put("GE", "+995");
        country2phone.put("TW", "+886");
        country2phone.put("AZ", "+374 97");
        country2phone.put("SO", "+252");
        country2phone.put("CX", "+61");
        country2phone.put("CC", "+61");
        country2phone.put("NF", "+672");
        country2phone.put("NC", "+687");
        country2phone.put("PF", "+689");
        country2phone.put("YT", "+262");
        country2phone.put("GP", "+590");
        country2phone.put("PM", "+508");
        country2phone.put("WF", "+681");
        country2phone.put("CK", "+682");
        country2phone.put("NU", "+683");
        country2phone.put("TK", "+690");
        country2phone.put("GG", "+44");
        country2phone.put("IM", "+44");
        country2phone.put("JE", "+44");
        country2phone.put("AI", "+1 264");
        country2phone.put("BM", "+1 441");
        country2phone.put("IO", "+246");
        country2phone.put("", "+357");
        country2phone.put("VG", "+1 284");
        country2phone.put("KY", "+1 345");
        country2phone.put("FK", "+500");
        country2phone.put("GI", "+350");
        country2phone.put("MS", "+1 664");
        country2phone.put("SH", "+290");
        country2phone.put("TC", "+1 649");
        country2phone.put("MP", "+1 670");
        country2phone.put("PR", "+1 787");
        country2phone.put("AS", "+1 684");
        country2phone.put("GU", "+1 671");
        country2phone.put("VI", "+1 340");
        country2phone.put("HK", "+852");
        country2phone.put("MO", "+853");
        country2phone.put("FO", "+298");
        country2phone.put("GL", "+299");
        country2phone.put("GF", "+594");
        country2phone.put("MQ", "+596");
        country2phone.put("RE", "+262");
        country2phone.put("AX", "+358 18");
        country2phone.put("AW", "+297");
        country2phone.put("AN", "+599");
        country2phone.put("SJ", "+47");
        country2phone.put("AC", "+247");
        country2phone.put("TA", "+290");
        country2phone.put("CS", "+381");
        country2phone.put("PS", "+970");
        country2phone.put("EH", "+212");
    }

}
