//package com.vision.imagine.utils;
//
//import android.os.Environment;
//
//import java.io.File;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//
//public class BitmapUtils {
//    File getOutputMediaFile() {
//        File mediaStorageDir = new File(
//                Environment.getExternalStorageDirectory(),
//                "easyTouchPro");
//        if (mediaStorageDir.isDirectory()) {
//
//            // Create a media file name
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
//                    .format(Calendar.getInstance().getTime());
//            String mCurrentPath = mediaStorageDir.getPath() + File.separator
//                    + "IMG_" + timeStamp + ".jpg";
//            File mediaFile = new File(mCurrentPath);
//            return mediaFile;
//        } else { /// error handling for PIE devices..
//            mediaStorageDir.delete();
//            mediaStorageDir.mkdirs();
//            galleryAddPic(mediaStorageDir);
//
//            return (getOutputMediaFile());
//        }
//    }
//
//    public static int getQualityNumber(Bitmap bitmap) {
//        int size = bitmap.getByteCount();
//        int percentage = 0;
//
//        if (size > 500000 && size <= 800000) {
//            percentage = 15;
//        } else if (size > 800000 && size <= 1000000) {
//            percentage = 20;
//        } else if (size > 1000000 && size <= 1500000) {
//            percentage = 25;
//        } else if (size > 1500000 && size <= 2500000) {
//            percentage = 27;
//        } else if (size > 2500000 && size <= 3500000) {
//            percentage = 30;
//        } else if (size > 3500000 && size <= 4000000) {
//            percentage = 40;
//        } else if (size > 4000000 && size <= 5000000) {
//            percentage = 50;
//        } else if (size > 5000000) {
//            percentage = 75;
//        }
//
//        return percentage;
//    }
//
//    void galleryAddPic(File f) {
//        Intent mediaScanIntent = new Intent(
//                "android.intent.action.MEDIA_SCANNER_SCAN_FILE");
//        Uri contentUri = Uri.fromFile(f);
//        mediaScanIntent.setData(contentUri);
//
//        this.sendBroadcast(mediaScanIntent);
//    }
//}
