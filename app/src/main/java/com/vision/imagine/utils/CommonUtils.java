package com.vision.imagine.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.core.graphics.drawable.DrawableCompat;

import com.vision.imagine.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {

    public static boolean emailValidation(String emailId) {
        if (emailId.matches(Constants.EMAIL_REGEX))
            return true;
        else
            return false;
    }

    public static boolean mobileVaildation(String mobile) {
        if (mobile.matches(Constants.MOBILE_REGEX))
            return true;
        else
            return false;
    }

    public static void hideKeyboard(Context context, IBinder token) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(token, 0);
    }

    public static Drawable tintMyDrawable(Context context, Drawable imgDrawableToTint) {
        try {
            Drawable wrapDrawable = DrawableCompat.wrap(imgDrawableToTint);
            DrawableCompat.setTint(wrapDrawable, context.getResources().getColor(MainActivity.colorSelected));
            return wrapDrawable;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFormattedTimeEvent(Long time) {
        SimpleDateFormat newFormat = new SimpleDateFormat("h:mm a");
        return newFormat.format(new Date(time));
    }
}
