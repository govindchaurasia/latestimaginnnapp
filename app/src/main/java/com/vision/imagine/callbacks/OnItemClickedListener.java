package com.vision.imagine.callbacks;

import android.os.Bundle;

public interface OnItemClickedListener {
    public void clickedItem(Bundle data);
}
