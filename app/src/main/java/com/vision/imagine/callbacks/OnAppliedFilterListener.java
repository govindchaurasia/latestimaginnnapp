package com.vision.imagine.callbacks;

import android.os.Bundle;

public interface OnAppliedFilterListener {
    public void appliedFilter(Bundle data);
}
