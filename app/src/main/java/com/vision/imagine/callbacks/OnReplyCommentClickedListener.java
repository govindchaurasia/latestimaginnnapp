package com.vision.imagine.callbacks;

import android.os.Bundle;

public interface OnReplyCommentClickedListener {
    public void clickedReply(Bundle data);
}
