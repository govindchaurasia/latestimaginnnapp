package com.vision.imagine.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.vision.imagine.ImagineApplication;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pagination.PaginationListener;
import com.vision.imagine.pojos.FriendsItem;
import com.vision.imagine.pojos.Message;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.RequestFilterUser;
import com.vision.imagine.pojos.RequestGetConversation;
import com.vision.imagine.pojos.RequestSendMessage;
import com.vision.imagine.pojos.ResponseDeleteFriend;
import com.vision.imagine.pojos.ResponseFilterUser;
import com.vision.imagine.pojos.ResponseGetConversation;
import com.vision.imagine.rvAdapters.AdapterChatSharedIdea;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.ProgressDialogBuilder;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactChatFragment extends Fragment implements OnItemClickedListener {

    private RecyclerView rvConversation;
    private SwipeRefreshLayout srlMessages;
    private ImageView ivSend, ivAttach;
    private ProgressBar progressBarSentMessage, progressBarAttachement;
    private TextView txtNoConversation;
    private ImageView btnSearch;
    private EditText etSearch, etMessage;
    private List<Message> messageArrayList;
    private AdapterChatSharedIdea myConversationAdapter;
    private APIService service = RetrofitClient.getClient().create(APIService.class);
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private boolean isSearchPerformed = false;

    private ProgressDialog progressDialog;
    private Menu menu;
    private SelectionTracker<Long> selectionTracker;
    private MyIdeasModel objMyIdeasModel;
    ArrayList<MyIdeasModel> ideasModels = new ArrayList<>();
    private FriendsItem friendsItem;
    private int currentPage = 1;
    private static final int RECORD_PER_PAGE = 30;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            friendsItem = (FriendsItem) getArguments().getSerializable("friendsItem");
        }

        try {
            if (savedInstanceState != null) {
                selectionTracker.onRestoreInstanceState(savedInstanceState);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setActionBarTitle() {
        try {
            if (friendsItem != null && friendsItem.getUsername() != null) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(friendsItem.getUsername());
            } else {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.lbl_chat));
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_chat, container, false);
        rvConversation = view.findViewById(R.id.rvConversation);
        srlMessages = view.findViewById(R.id.srlMessages);
        txtNoConversation = view.findViewById(R.id.txtNoChat);
        btnSearch = view.findViewById(R.id.icon);
        etSearch = view.findViewById(R.id.et);

        etMessage = view.findViewById(R.id.etMessage);
        ivSend = view.findViewById(R.id.ivSend);
        ivAttach = view.findViewById(R.id.ivAttach);
        progressBarSentMessage = view.findViewById(R.id.progressBarSentMessage);
        progressBarAttachement = view.findViewById(R.id.progressBarAttachement);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN );

        CommonUtils.tintMyDrawable(getActivity(), btnSearch.getDrawable());
        CommonUtils.tintMyDrawable(getActivity(), ivSend.getDrawable());
        CommonUtils.tintMyDrawable(getActivity(), ivAttach.getDrawable());

        setActionBarTitle();

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String msg = etMessage.getText().toString().trim();
                if (msg.isEmpty())
                    return;
                callSendMessageApi(msg, "", null);
            }
        });

        ivAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context wrapper = new ContextThemeWrapper(getActivity(), R.style.MyPopupMenu);
                PopupMenu popupMenu = new PopupMenu(wrapper, view);
                popupMenu.getMenuInflater().inflate(R.menu.menu_attach, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_private_ideas: {
                                toggleAttachmentProgressBar(true);
                                fragmentChangeCallBacks.fragmentName("goToMyIdeasFragmentFromChat", new Bundle());
                            }
                            break;
                            case R.id.item_public_ideas: {
                                toggleAttachmentProgressBar(true);
                                fragmentChangeCallBacks.fragmentName("goToExploreIdeaFragment", new Bundle());
                            }
                            break;
//                            case R.id.item_public_ideas: {
//                                myChatAdapter.insertItem(new Message(myChatAdapter.getItemCount(), getIdeaToShare(), false, myChatAdapter.getItemCount() % 5 == 0, CommonUtils.getFormattedTimeEvent(System.currentTimeMillis())));
//                                rvConversation.scrollToPosition(myChatAdapter.getItemCount() - 1);
//                            }
//                            break;
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearch.getText().toString().trim().length() > 0 && !isSearchPerformed) {
                    isSearchPerformed = true;
                    callFilterUserApi(etSearch.getText().toString().trim());
                } else {
                    isSearchPerformed = false;
                }
            }
        });

//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (s.length() == Constants.SEARCH_THRESHOLD && !isSearchPerformed) {
//                    isSearchPerformed = true;
//                    callFilterUserApi(s.toString());
//                } else if (s.length() < Constants.SEARCH_THRESHOLD) {
//                    isSearchPerformed = false;
//                }
//            }
//        });

        setHasOptionsMenu(true);
        setMyConversationAdapter();
        callGetMyConversionApi(true);

        srlMessages.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage++;
                callGetMyConversionApi(false);
            }
        });

        return view;
    }

    private void callGetMyConversionApi(boolean showProgress) {
        if (showProgress) {
            progressDialog = ProgressDialogBuilder.build(getActivity(), "", getResources().getString(R.string.getting_conversation));
            progressDialog.show();
        }

        RequestGetConversation objRequestGetConversation = new RequestGetConversation();
        objRequestGetConversation.setFromUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        if (friendsItem != null && friendsItem.getUserId() != null)
            objRequestGetConversation.setToUserId(friendsItem.getUserId());
        else
            objRequestGetConversation.setToUserId("");

        int totalMessages = currentPage * RECORD_PER_PAGE;
        objRequestGetConversation.setStartCount("0");
        objRequestGetConversation.setOffsetCount(totalMessages + "");

        Call<ResponseGetConversation> call = service.getConversationApi(objRequestGetConversation);
        call.enqueue(new Callback<ResponseGetConversation>() {
            @Override
            public void onResponse(Call<ResponseGetConversation> call, Response<ResponseGetConversation> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        messageArrayList.clear();
                        messageArrayList = response.body().getData();
                        myConversationAdapter.setMessageArrayList(messageArrayList, response.body().getBaseUrl());
                        myConversationAdapter.notifyDataSetChanged();

//                        if (rvConversation != null && rvConversation.getAdapter() != null) {
//                            selectionTracker.clearSelection();
//                            rvConversation.getAdapter().notifyDataSetChanged();
//                        }

                        srlMessages.setRefreshing(false);
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                            rvConversation.scrollToPosition(messageArrayList.size() - 1);
                        }

                        if (myConversationAdapter.getItemCount() > 0)
                            txtNoConversation.setVisibility(View.GONE);
                        else
                            txtNoConversation.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseGetConversation> call, Throwable t) {
                srlMessages.setRefreshing(false);
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                call.cancel();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    @Override
    public void clickedItem(Bundle bundle) {
        if (!bundle.getString("userId", "").isEmpty()) {

        } else if (bundle.getSerializable("friendsItem") != null) {

        }
    }

    private void callFilterUserApi(String textToSearch) {
        RequestFilterUser objRequestFilterUser = new RequestFilterUser();
        objRequestFilterUser.setUsername(textToSearch);
        objRequestFilterUser.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        Call<ResponseFilterUser> call = service.filterUserApi(objRequestFilterUser);
        call.enqueue(new Callback<ResponseFilterUser>() {
            @Override
            public void onResponse(Call<ResponseFilterUser> call, Response<ResponseFilterUser> response) {
                try {
                    if (response != null && response.body() != null && response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("Success")) {
//                            autoCompleteItemsArrayList.clear();
//                            for (int i = 0; i < response.body().getFilterUser().size(); i++) {
//                                autoCompleteItemsArrayList.add(response.body().getFilterUser().get(i));
//                            }
//
//                            autoCompleteTextViewAdapter = new CustomArrayAdapter(getActivity(), R.layout.search_auto_complete_single_row, autoCompleteItemsArrayList, ContactChatFragment.this, response.body().getUserProfilePath());
//                            etSearch.setAdapter(autoCompleteTextViewAdapter);
                        } else {
                            Toast.makeText(getContext(), response.body().getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseFilterUser> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void setMyConversationAdapter() {
        messageArrayList = new ArrayList<>();
        myConversationAdapter = new AdapterChatSharedIdea(getActivity(), messageArrayList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
        rvConversation.setLayoutManager(mLayoutManager);
        rvConversation.setItemAnimator(new DefaultItemAnimator());
        rvConversation.setAdapter(myConversationAdapter);

//        MyConnectionsAdapter.KeyProvider objKeyProvider = new MyConnectionsAdapter.KeyProvider(rvConversation.getAdapter());
//        selectionTracker = new SelectionTracker.Builder<>("my_selection",
//                rvConversation,
//                objKeyProvider,
//                new MyConnectionsAdapter.DetailsLookup(rvConversation),
//                StorageStrategy.createLongStorage())
//                .withSelectionPredicate(new MyConnectionsAdapter.Predicate())
//                .build();
//
//        selectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
//            @Override
//            public void onSelectionChanged() {
//                super.onSelectionChanged();
//                try {
////                    if (selectionTracker.hasSelection()) {
////                        toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list) + "  " + selectionTracker.getSelection().size() + " Connection(s)");
////                    } else if (!selectionTracker.hasSelection()) {
////                        toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list));
////                    } else {
////                        toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list) + "  " + selectionTracker.getSelection().size() + " Connection(s)");
////                    }
//
//                    if (selectionTracker.getSelection().size() > 0) {
//                        menu.findItem(R.id.item_delete_contact).setVisible(true);
//                    } else {
//                        menu.findItem(R.id.item_delete_contact).setVisible(true);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        myChatAdapter.setSelectionTracker(selectionTracker);

        rvConversation.addOnScrollListener(new PaginationListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
//                showMyConversion();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        myConversationAdapter.setOnItemClickListener(new AdapterChatSharedIdea.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Message obj, int position) {
                if (obj.getSharedIdea() != null && obj.getSharedIdea().getIdeaId() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ideaObj", obj.getSharedIdea());
                    fragmentChangeCallBacks.fragmentName("goToOthersIdeaPreviewFragment", bundle);
                } else {
//                    Toast.makeText(getActivity(), "Item Clicked", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onButtonClick(View view, Message obj, int position, TextView text_content) {
            }
        });
    }

    private void callSendMessageApi(String messageText, String sharedIdeaId, MyIdeasModel model) {
        if (!sharedIdeaId.isEmpty())
            toggleAttachmentProgressBar(true);
        else
            toggleSendMessageProgressBar(true);

        RequestSendMessage objRequestSendMessage = new RequestSendMessage();
        objRequestSendMessage.setFromUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        if (friendsItem != null && friendsItem.getUserId() != null)
            objRequestSendMessage.setToUserId(friendsItem.getUserId());
        else
            objRequestSendMessage.setToUserId("");
        objRequestSendMessage.setMessageText(messageText);
        objRequestSendMessage.setSharedIdeaId(sharedIdeaId);

        Call<ResponseDeleteFriend> call = service.sendMessageApi(objRequestSendMessage);
        call.enqueue(new Callback<ResponseDeleteFriend>() {
            @Override
            public void onResponse(Call<ResponseDeleteFriend> call, Response<ResponseDeleteFriend> response) {
                try {
                    if (!sharedIdeaId.isEmpty())
                        toggleAttachmentProgressBar(false);
                    else
                        toggleSendMessageProgressBar(false);

                    if (response != null && response.body() != null && response.isSuccessful()) {
                        if (response.body().getRemark().equalsIgnoreCase("Success")) {
                            sendChat(messageText, sharedIdeaId, model, true);
                        } else {
                            Toast.makeText(getContext(), response.body().getRemark(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseDeleteFriend> call, Throwable t) {
                if (!sharedIdeaId.isEmpty())
                    toggleAttachmentProgressBar(false);
                else
                    toggleSendMessageProgressBar(false);
                call.cancel();
                if (t != null && t.getMessage() != null)
                    Toast.makeText(getContext(), "Sending Failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null)
            selectionTracker.onSaveInstanceState(outState);
    }

    private void sendChat(String msg, String sharedIdeaId, MyIdeasModel objMyIdeasModel, boolean isMessageFromMe) {
        try {
            if (!sharedIdeaId.isEmpty()) {
                myConversationAdapter.insertItem(new Message(Calendar.getInstance().getTimeInMillis() + "", objMyIdeasModel, isMessageFromMe, false, CommonUtils.getFormattedTimeEvent(System.currentTimeMillis())));
            } else {
                myConversationAdapter.insertItem(new Message(Calendar.getInstance().getTimeInMillis() + "", msg, isMessageFromMe, false, CommonUtils.getFormattedTimeEvent(System.currentTimeMillis())));
                if (isMessageFromMe)
                    etMessage.setText("");
            }
            rvConversation.scrollToPosition(myConversationAdapter.getItemCount() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void toggleSendMessageProgressBar(boolean isLoading) {
        if (isLoading) {
            ivSend.setEnabled(false);
            ivAttach.setEnabled(false);
            progressBarSentMessage.setVisibility(View.VISIBLE);
        } else {
            ivSend.setEnabled(true);
            ivAttach.setEnabled(true);
            progressBarSentMessage.setVisibility(View.GONE);
        }
    }

    private void toggleAttachmentProgressBar(boolean isLoading) {
        if (isLoading) {
            ivAttach.setEnabled(false);
            ivSend.setEnabled(false);
            progressBarAttachement.setVisibility(View.VISIBLE);
        } else {
            ivAttach.setEnabled(true);
            ivSend.setEnabled(true);
            progressBarAttachement.setVisibility(View.GONE);
        }
    }

    public void setIdea(Bundle bundle) {
        if(bundle!=null && bundle.getSerializable("models")!=null) {
            ideasModels = (ArrayList<MyIdeasModel>) bundle.getSerializable("models");
            if(ideasModels!=null && ideasModels.size()>0) {
                for (MyIdeasModel model : ideasModels) {
                    objMyIdeasModel = model;
                    setActionBarTitle();
                    Log.d("Track", "Selected idea ids : "+objMyIdeasModel.getIdeaId() + "");
                    callSendMessageApi("", objMyIdeasModel.getIdeaId() + "", model);
                }
            }else {toggleAttachmentProgressBar(false);}
        }else {
            toggleAttachmentProgressBar(false);
        }
    }

    public void setReceivedMessage(ResponseGetConversation objResponseGetConversation) {
        String currentUserId = SharedPrefUtils.getStringData(Constants.USER_ID);
        String chatUserId = friendsItem != null && friendsItem.getUserId() != null ? friendsItem.getUserId() : "";

//        if (objResponseGetConversation.getFromUserId().equals(fromUserId) && objResponseGetConversation.getToUserId().equals(toUserId) && objResponseGetConversation.getData() != null && objResponseGetConversation.getData().size() > 0) {
        if (objResponseGetConversation.getFromUserId().equals(chatUserId) && objResponseGetConversation.getToUserId().equals(currentUserId) && objResponseGetConversation.getData() != null && objResponseGetConversation.getData().size() > 0) {
            if (objResponseGetConversation.getData().get(0).getSharedIdea() != null) {
                sendChat(objResponseGetConversation.getData().get(0).getMessageText(), objResponseGetConversation.getData().get(0).getSharedIdea().getIdeaId() + "", objResponseGetConversation.getData().get(0).getSharedIdea(), false);
            } else {
                sendChat(objResponseGetConversation.getData().get(0).getMessageText(), "", objResponseGetConversation.getData().get(0).getSharedIdea(), false);
            }
        }
    }

    //    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        menu.clear();
//        inflater.inflate(R.menu.menu_my_connection, menu);
//        this.menu = menu;
//
//        CommonUtils.tintMyDrawable(getActivity(), menu.findItem(R.id.item_delete_contact).getIcon());
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.item_delete_contact:
//                if (selectionTracker != null && selectionTracker.getSelection().size() > 0)
//                    dialogDeleteContacts();
//                else
//                    Toast.makeText(getActivity(), "Please select any contact", Toast.LENGTH_LONG).show();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

//    private void dialogDeleteContacts() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(getResources().getString(R.string.title_delete_contact_list));
//        builder.setMessage(getResources().getString(R.string.msg_delete_contact_list));
//        builder.setCancelable(true);
//
//        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                callDeleteContactApi();
//                dialog.dismiss();
//            }
//        });
//
//        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//    private void callDeleteContactApi() {
//        progressDialog = ProgressDialogBuilder.build(getActivity(), "", getResources().getString(R.string.sending_connection));
//        progressDialog.show();
//
//        String friendsToDelete = "";
//        if (myConnectionsAdapter != null) {
//            Selection<Long> selectionList = myConnectionsAdapter.getAllSelectionTrackerItems();
//            Iterator<Long> itemIterable = selectionList.iterator();
//            while (itemIterable.hasNext()) {
//                FriendsItem objUserContactsItem = friendsItemArrayList.get(itemIterable.next().intValue());
//                friendsToDelete += objUserContactsItem.getUserId() + ",";
//            }
//            if (friendsToDelete.length() > 0) {
//                friendsToDelete = friendsToDelete.substring(0, friendsToDelete.length() - 1);
//            }
//        }
//
//        RequestDeleteFriend objRequestDeleteFriend = new RequestDeleteFriend();
//        objRequestDeleteFriend.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
//        objRequestDeleteFriend.setFriendId(friendsToDelete);
//
//        Call<ResponseDeleteFriend> call = service.deleteFriendApi(objRequestDeleteFriend);
//        call.enqueue(new Callback<ResponseDeleteFriend>() {
//            @Override
//            public void onResponse(Call<ResponseDeleteFriend> call, Response<ResponseDeleteFriend> response) {
//                progressDialog.cancel();
//                try {
//                    if (response != null && response.body() != null) {
//                        if (response.isSuccessful()) {
//                            Toast.makeText(getActivity(), response.body().getRemark(), Toast.LENGTH_LONG).show();
//
//                            if (response.body().getRemark() != null && response.body().getRemark().equalsIgnoreCase("Success")) {
//                                String[] userIds = objRequestDeleteFriend.getFriendId().split(",");
//                                for (int i = 0; i < userIds.length; i++) {
//                                    for (int j = 0; j < friendsItemArrayList.size(); j++) {
//                                        if (userIds[i].equals(friendsItemArrayList.get(j).getUserId())) {
//                                            friendsItemArrayList.remove(j);
//                                            break;
//                                        }
//                                    }
//                                }
//                                selectionTracker.clearSelection();
//                                myConnectionsAdapter.notifyDataSetChanged();
//                            }
//                        }
//                    } else {
//                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseDeleteFriend> call, Throwable t) {
//                progressDialog.cancel();
//                call.cancel();
//            }
//        });
//    }
}
