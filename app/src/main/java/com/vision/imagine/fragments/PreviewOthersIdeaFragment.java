package com.vision.imagine.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.AddViewResponse;
import com.vision.imagine.pojos.CommentListResponse;
import com.vision.imagine.pojos.DisableIdeaResponse;
import com.vision.imagine.pojos.IdeaDetailsDataSet;
import com.vision.imagine.pojos.IdeaListResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vision.imagine.MainActivity.colorSelectedLight;
import static com.vision.imagine.fragments.ExploreIdeaFragment.tintMenuIcon;

public class PreviewOthersIdeaFragment extends Fragment implements Runnable {

    APIService service = RetrofitClient.getClient().create(APIService.class);
    MyIdeasModel ideasModel;
    View view;
    LinearLayout lm;
    IdeaDetailsDataSet ideaDetailsDataSet;
    String baseUrl = RetrofitClient.BASE_URL + "getImage/";
    String ideaPath = "";
    MediaPlayer mediaPlayer = new MediaPlayer();
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private AppCompatImageView comment, thumb, heart, viewsIv, fullScreenIv;
    TextView title, description, authorTv, viewCountTv, catTv, likeCountTv, commentCount;
    Dialog audioPlayerDialog;
    TextView playBtn, seekBarHint;
    SeekBar seekBar;
    LinearLayout sideBar;

    public PreviewOthersIdeaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ideasModel = (MyIdeasModel) getArguments().getSerializable("ideaObj");
        }
        setupAudioPlayer();
    }

    private void setupAudioPlayer() {
        audioPlayerDialog = new Dialog(getActivity());
        audioPlayerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        audioPlayerDialog.setCancelable(true);
        audioPlayerDialog.setContentView(R.layout.custom_dialog_audio_player);
        audioPlayerDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        audioPlayerDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_bg));
        playBtn = (TextView) audioPlayerDialog.findViewById(R.id.play_btn);
        seekBar = (SeekBar) audioPlayerDialog.findViewById(R.id.seek_bar);
        seekBarHint = (TextView) audioPlayerDialog.findViewById(R.id.textView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_idea_preview_mein, container, false);
        setHasOptionsMenu(true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Idea Details");
        showSelectedIdea();
        likeShareCommentOptionToolbar();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Idea Details");
        initView();
        updateViews();
        getComments();
        return view;
    }

    // Show Selected Idea
    private void showSelectedIdea() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("idea_id", ideasModel.getIdeaId());
        Call<IdeaListResponse> call = service.showSelectedIdea(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        ideasModel = response.body().getIdeas().get(0);
                        if (lm != null)
                            lm.removeAllViews();
                        initView();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_preview:
                Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToIdeaCommentReplyFragment", bundle);
                return true;

            case R.id.item_favorite:
                if (ideaDetailsDataSet.getFavFlag()) {
                    item.setIcon(R.drawable.ic_favorite_white_border_24px);
                    CommonApiUtils.updateFav(false, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                } else {
                    item.setIcon(R.drawable.ic_favorite_white_24dp);
                    CommonApiUtils.updateFav(true, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                }
                showSelectedIdea();
                return true;

            case R.id.item_like:
                if (ideaDetailsDataSet.getLikeFlag()) {
                    item.setIcon(R.drawable.ic_outline_thumb_up_alt_24);
                    CommonApiUtils.updateLike(false, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                } else {
                    item.setIcon(R.drawable.ic_baseline_thumb_up_alt_24);
                    CommonApiUtils.updateLike(true, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                }
                showSelectedIdea();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

/*    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_preview_others_idea, menu);

        MenuItem settingsItemLike = menu.findItem(R.id.item_like);
        MenuItem settingsItemFav = menu.findItem(R.id.item_favorite);

        try {
            if (ideaDetailsDataSet.getFavFlag()) {
                settingsItemFav.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_favorite_white_24dp));
            } else {
                settingsItemFav.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_favorite_white_border_24px));
            }

            if (ideaDetailsDataSet.getLikeFlag()) {
                settingsItemLike.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_baseline_thumb_up_alt_24));
            } else {
                settingsItemLike.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_outline_thumb_up_alt_24));
            }
        } catch (Exception e) {
        }

        super.onCreateOptionsMenu(menu, inflater);
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }


    private void initView() {
        title = view.findViewById(R.id.title);
        commentCount = view.findViewById(R.id.comments_tv);
        description = view.findViewById(R.id.description);
        authorTv = view.findViewById(R.id.tv_author);
        viewCountTv = view.findViewById(R.id.view_count_tv);
        catTv = view.findViewById(R.id.tv_categories);
        likeCountTv = view.findViewById(R.id.fav_tv);
        title.setText(ideasModel.getTitle());
        sideBar = view.findViewById(R.id.card_view);
        fullScreenIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideBar.animate()
                        .translationX(sideBar.getWidth())
                        .alpha(0.0f)
                        .setDuration(500)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                sideBar.setVisibility(View.GONE);
                            }
                        });
                ((MainActivity) getActivity()).setFullScreen();
            }
        });
        setData();
    }

    private void setData() {
        description.setText(Html.fromHtml("<b>Description : </b>"+ideasModel.getCaption()));
        authorTv.setText(Html.fromHtml("<b>Author : </b>"+ideasModel.getAuthor()));
        if (ideasModel.getViews() != null) {
            viewCountTv.setText("" + ideasModel.getViews());
        } else {
            viewCountTv.setText("0");
        }
        catTv.setText(Html.fromHtml("<b>Related Industry : </b>"+ ideasModel.getCategory()));
        likeCountTv.setText("" + ideasModel.getLikeCount());
        //ideaPath = ideasModel.getBaseUrl() + "/";
        ideaPath = ideasModel.getUserId() + "/" + ideasModel.getIdeaId() + "/";
        try {
            if (ideasModel.getDescription() != null && ideasModel.getDescription().contains("~")) {
                String[] ele1 = ideasModel.getDescription().split("~");
                lm = (LinearLayout) view.findViewById(R.id.ll_dynamic);
                for (int j = 0; j < ele1.length; j++) {
                    String[] ele2 = ele1[j].split(":");
                    if (ele2[0].equalsIgnoreCase("photo")) {
                        inflateImage(ele2[1]);
                    } else if (ele2[0].equalsIgnoreCase("audio")) {
                        inflateRecord(ele2[1]);
                    } else {
                        inflateText(ele2[1]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        authorTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle data = new Bundle();
                data.putString("authorId", ideasModel.getUserId());
                fragmentChangeCallBacks.fragmentName("goToAuthorProfileFragment", data);
            }
        });
        viewCountTv.setTextColor(getResources().getColor(MainActivity.colorSelected));
        likeCountTv.setTextColor(getResources().getColor(MainActivity.colorSelected));
        commentCount.setTextColor(getResources().getColor(MainActivity.colorSelected));
    }

    private void inflateImage(String createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.image_single_row, lm, false);
        ImageView imageView = layout.findViewById(R.id.image);
        String localUrl = baseUrl + ideaPath + createIdeaDataSet;
        Glide.with(this)
                .load(localUrl)
                .into(imageView);

        lm.addView(layout);
    }

    private void inflateText(String createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.text_single_row, lm, false);
        TextView textView = layout.findViewById(R.id.tv_text);
        textView.setTextSize(18);
        textView.setTextColor(getResources().getColor(R.color.black_color));
        textView.setText(createIdeaDataSet);
        lm.addView(layout);
    }

    private void inflateRecord(String createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.audio_single_view, lm, false);
        CardView img_play = layout.findViewById(R.id.write_product_card_view);
        String localUrl = baseUrl + ideaPath + createIdeaDataSet;
        img_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Uri uri = Uri.parse(localUrl);
                    playAudio(uri);
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        });

        lm.addView(layout);
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (mediaPlayer != null)
                mediaPlayer.stop();
        } catch (Exception e) {
        }
    }

    private void likeShareCommentOptionToolbar() {
        comment = view.findViewById(R.id.comment);
        heart = view.findViewById(R.id.heart);
        viewsIv = view.findViewById(R.id.views_iv);
        fullScreenIv = view.findViewById(R.id.full_screen_iv);

        ImageViewCompat.setImageTintList(comment, ColorStateList.valueOf(getResources().getColor(MainActivity.colorSelected)));
        ImageViewCompat.setImageTintList(heart, ColorStateList.valueOf(getResources().getColor(MainActivity.colorSelected)));
        ImageViewCompat.setImageTintList(viewsIv, ColorStateList.valueOf(getResources().getColor(MainActivity.colorSelected)));
        ImageViewCompat.setImageTintList(fullScreenIv, ColorStateList.valueOf(getResources().getColor(MainActivity.colorSelected)));

        try {
            Drawable unwrappedDrawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_favorite_white_24dp);
            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
            DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelectedLight));


            if (ideasModel.getIsFav() != null && ideasModel.getIsFav().equals("1")) {
                heart.setImageDrawable(unwrappedDrawable);
            } else if (ideasModel.getIsFav() == null) {
                //heart.setImageResource(R.drawable.ic_favorite_white_24dp);
                heart.setImageDrawable(unwrappedDrawable);
            } else {
                Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getContext(), R.drawable.ic_heart_tool);
                Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
                DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(colorSelectedLight));
                heart.setImageDrawable(unwrappedDrawable1);
            }

        } catch (Exception e) {
        }




       /* draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle2 = new Bundle();
                bundle2.putSerializable("ideaObj", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToUpdateIdeasFragment", bundle2);
            }
        });*/


        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToIdeaCommentReplyFragment", bundle);
            }
        });

       /* thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ideaDetailsDataSet.getLikeFlag()) {
                    thumb.setImageResource(R.drawable.ic_outline_thumb_up_alt_24);
                    CommonApiUtils.updateLike(false, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                } else {
                    thumb.setImageResource(R.drawable.ic_thumb_up_tool);
                    CommonApiUtils.updateLike(true, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                }
                showSelectedIdea();

            }
        });*/

        heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ideasModel.getIsFav().equals("1")) {
                    heart.setImageResource(R.drawable.ic_heart_tool);
                    CommonApiUtils.updateFav(false, ideasModel.getUserId(), ideasModel.getIdeaId());
                    String tmp = likeCountTv.getText().toString();int i = 0;
                    try {
                        i = Integer.parseInt(tmp);
                    }catch (Exception ignored){}
                    if(i>0){
                        i--;ideasModel.setLikeCount(i);likeCountTv.setText(String.valueOf(i));
                        ideasModel.setIsFav("0");
                    }
                } else {
                    heart.setImageResource(R.drawable.ic_favorite_white_24dp);
                    CommonApiUtils.updateFav(true, ideasModel.getUserId(), ideasModel.getIdeaId());
                    String tmp = likeCountTv.getText().toString();int i = 0;
                    try {
                        i = Integer.parseInt(tmp);
                    }catch (Exception ignored){}
                    if(i>=0){
                        i++;ideasModel.setLikeCount(i);likeCountTv.setText(String.valueOf(i));
                        ideasModel.setIsFav("1");
                    }
                }
               // showSelectedIdea();
            }
        });


    }

    private void updateViews() {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("idea_id", String.valueOf(ideasModel.getIdeaId()));
        Call<AddViewResponse> call = service.addViews(jsonObject);
        call.enqueue(new Callback<AddViewResponse>() {
            @Override
            public void onResponse(Call<AddViewResponse> call, Response<AddViewResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        showSelectedIdea();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddViewResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void getComments() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("idea_id", String.valueOf(ideasModel.getIdeaId()));
        Call<CommentListResponse> call = service.getComments(jsonObject);
        call.enqueue(new Callback<CommentListResponse>() {
            @Override
            public void onResponse(Call<CommentListResponse> call, Response<CommentListResponse> response) {
                try {
                    if (response.body().getCommentList() != null) {
                        commentCount.setText("" + response.body().getCommentList().size());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CommentListResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (ideasModel != null && ideasModel.getUserId().equals(SharedPrefUtils.getStringData(Constants.USER_ID))) {
            inflater.inflate(R.menu.edit_menu, menu);

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_preview:
                onMenuClick();
                return true;

            default:
                return false;
        }
    }

    public void onMenuClick(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_edit_idea_options);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView editBtn = (TextView) dialog.findViewById(R.id.edit_btn);
        TextView deleteBtn = (TextView) dialog.findViewById(R.id.delete_btn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle data = new Bundle();
                data.putSerializable("idea", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToEditIdeaFragment", data);
                dialog.dismiss();
            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.isDeleteIdea = true;
                deleteIdea();
                dialog.dismiss();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void playAudio(Uri localUri) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(PreviewOthersIdeaFragment.this.getActivity(), localUri);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        seekBar.setMax(mediaPlayer.getDuration());

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                        mediaPlayer.seekTo(0);
                        playBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.play_ic));
                        seekBar.setProgress(0);
                    } else {
                        try {
                            mediaPlayer.start();
                            playBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.stop_ic));
                            new Thread(PreviewOthersIdeaFragment.this).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });

        audioPlayerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarHint.setVisibility(View.VISIBLE);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
               /* seekBarHint.setVisibility(View.VISIBLE);
                int x = (int) Math.ceil(progress / 1000f);

                if (x == 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    //clearMediaPlayer();
                    //playBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), android.R.drawable.ic_media_play));
                    seekBar.setProgress(0);
                }else if(mediaPlayer!=null && mediaPlayer.isPlaying()){
                    seekBar.setProgress(progress);
                }*/
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                }
            }
        });
        long millis = mediaPlayer.getDuration();
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        seekBarHint.setText("" + hms);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                playBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.play_ic));
                seekBar.setProgress(0);
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        audioPlayerDialog.getWindow().setBackgroundDrawable(draw);
        audioPlayerDialog.show();
    }

    public void run() {

        int currentPosition = 0;
        int total = mediaPlayer.getDuration();
        Log.d("Audio", "First run");

        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = mediaPlayer.getCurrentPosition();
                Log.d("Audio", "In while loop:" + currentPosition);
            } catch (InterruptedException e) {
                Log.d("Audio", "Error in run ");
                return;
            } catch (Exception e) {
                Log.d("Audio", "Error in run" + e.getMessage());
                return;
            }

            seekBar.setProgress(currentPosition);
            Log.d("Audio", "Current position:" + currentPosition);
        }
        if (!mediaPlayer.isPlaying()) {
            seekBar.setProgress(0);
        }
    }

    public void setNormalMode() {
        sideBar.setVisibility(View.VISIBLE);
        sideBar.animate()
                .translationX(0)
                .alpha(1.0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                });
    }

    private void disableIdeaAPI() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("idea_id", String.valueOf(ideasModel.getIdeaId()));
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        Call<DisableIdeaResponse> call = service.disableIdea(jsonObject);
        call.enqueue(new Callback<DisableIdeaResponse>() {
            @Override
            public void onResponse(Call<DisableIdeaResponse> call, Response<DisableIdeaResponse> response) {
                try {
                    getActivity().onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DisableIdeaResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void deleteIdea() {
/*        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                    case DialogInterface.BUTTON_POSITIVE:
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("idea_id", String.valueOf(ideasModel.getIdeaId()));
                        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
                        Call<DisableIdeaResponse> call = service.disableIdea(jsonObject);
                        call.enqueue(new Callback<DisableIdeaResponse>() {
                            @Override
                            public void onResponse(Call<DisableIdeaResponse> call, Response<DisableIdeaResponse> response) {
                                try {

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<DisableIdeaResponse> call, Throwable t) {
                                call.cancel();
                            }
                        });
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete idea?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();*/


        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_delete_idea_confirmation);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        TextView NoBtn = (TextView) dialog.findViewById(R.id.no_btn);
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("idea_id", String.valueOf(ideasModel.getIdeaId()));
                jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
                Call<DisableIdeaResponse> call = service.disableIdea(jsonObject);
                call.enqueue(new Callback<DisableIdeaResponse>() {
                    @Override
                    public void onResponse(Call<DisableIdeaResponse> call, Response<DisableIdeaResponse> response) {
                        try {
                            getActivity().onBackPressed();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<DisableIdeaResponse> call, Throwable t) {
                        call.cancel();
                    }
                });
                dialog.cancel();
            }
        });
        NoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }
}
