package com.vision.imagine.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.vision.imagine.ImagineApplication;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pagination.PaginationListener;
import com.vision.imagine.pojos.IdeaListResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.recyclerViewUtils.MyDividerItemDecoration;
import com.vision.imagine.rvAdapters.FavouriteIdeasAdapter;
import com.vision.imagine.rvAdapters.IdeasAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritesFragment extends Fragment implements OnItemClickedListener {

    RecyclerView rv_ideas;
    ArrayList<MyIdeasModel> ideasModelArrayList = new ArrayList<>();
    ArrayList<MyIdeasModel> filterIdeasModelArrayList = new ArrayList<>();
    IdeasAdapter ideasAdapter;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    ImageView searchIv;
    TextView noDataTv;
    EditText searchEt;
    Drawable dialogBg;
    boolean resumeCalled = false;
    boolean isSearchIdea = false;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING );
        rv_ideas = view.findViewById(R.id.rv_ideas);
        ideasAdapter = new IdeasAdapter(ideasModelArrayList, this,getContext());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
        rv_ideas.setLayoutManager(mLayoutManager);
        rv_ideas.setItemAnimator(new DefaultItemAnimator());
        rv_ideas.setAdapter(ideasAdapter);

        rv_ideas.addOnScrollListener(new PaginationListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                if(isSearchIdea) {
                    searchIdea();
                }else {
                    showFavIdea();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Favorite Ideas");

        searchIv = (ImageView) view.findViewById(R.id.icon);
        noDataTv = (TextView) view.findViewById(R.id.no_data_tv);
        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getContext(), R.drawable.ic_search_icon);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(MainActivity.colorSelected));
        searchIv.setImageDrawable(unwrappedDrawable1);

        searchEt = (EditText) view.findViewById(R.id.et);
        Drawable unwrappedDrawable2 = AppCompatResources.getDrawable(getContext(), R.drawable.search_corner_gray);
        Drawable wrappedDrawable2 = DrawableCompat.wrap(unwrappedDrawable2);
        DrawableCompat.setTint(wrappedDrawable2, getResources().getColor(MainActivity.colorSelected));
        searchEt.setBackground(unwrappedDrawable2);
        initViews();
        MainActivity.fevIdeas.clear();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showFavIdea();
                }
            }, 200);
            if(MainActivity.fevIdeas != null && !MainActivity.isDeleteIdea && MainActivity.fevIdeas.size()>0){
                resumeCalled = true;
                ideasAdapter.swapFilterData(MainActivity.fevIdeas);
            } else {
                if(MainActivity.isDeleteIdea)MainActivity.fevIdeas.clear();
                MainActivity.isDeleteIdea = false;
            }
        }catch (Exception e){}
    }

    private void initViews() {
        searchIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!searchEt.getText().toString().trim().equals("")){
                    setSearchData(searchEt.getText().toString());
                }
            }
        });
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(searchEt.getText().toString().trim().equals("")){
                    isSearchIdea = false;
                    ideasAdapter.swapFilterData(ideasModelArrayList);
                    if(ideasAdapter.IdeasModelsList==null || ideasAdapter.IdeasModelsList.isEmpty()){
                        rv_ideas.setVisibility(View.GONE);
                        noDataTv.setVisibility(View.VISIBLE);
                        showFavIdea();
                    }else {
                        rv_ideas.setVisibility(View.VISIBLE);
                        noDataTv.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void setSearchData(String keyWords){
        if(ideasModelArrayList!=null && ideasModelArrayList.size()>0){
            for(MyIdeasModel model : ideasModelArrayList){
                String str1 = model.getTitle().toLowerCase().trim();
                String str2 = keyWords.toLowerCase().trim();
                if(str1.contains(str2)){
                    filterIdeasModelArrayList.add(model);
                }
            }
            ideasAdapter.swapFilterData(filterIdeasModelArrayList);
        }else {
            ideasAdapter.swapFilterData(ideasModelArrayList);
        }
    }


    // Show Idea
    private void showFavIdea() {
        ((MainActivity) requireActivity()).showCustomProgressDialog(dialogBg);
        JsonObject jsonObject = new JsonObject();
        int count_ = ideasAdapter.getItemCount();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        /*jsonObject.addProperty("start_count", count_ + "");
        jsonObject.addProperty("offset_count", "20");
        jsonObject.addProperty("access_to", "public");*/
        Call<IdeaListResponse> call = service.showFavIdea(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {
                isLoading = false;
                try {
                    if (response.isSuccessful()) {
//                        ideasModelArrayList.addAll(response.body());
                        ideasModelArrayList = (ArrayList<MyIdeasModel>) response.body().getIdeas();

                        if(!resumeCalled) {
                            ideasAdapter.swapData(response.body().getIdeas());
                        }else {
                            resumeCalled = false;
                        }
                        if(ideasAdapter.IdeasModelsList==null || ideasAdapter.IdeasModelsList.isEmpty()){
                            rv_ideas.setVisibility(View.GONE);
                            noDataTv.setVisibility(View.VISIBLE);
                        }else {
                            rv_ideas.setVisibility(View.VISIBLE);
                            noDataTv.setVisibility(View.GONE);
                        }
                        if(((MainActivity) getActivity()) != null ) {
                            MainActivity.fevIdeas.addAll(ideasAdapter.getIdeas());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }
        });
    }

    private void searchIdea() {
        JsonObject jsonObject = new JsonObject();
        int count_ = 0;
        if(isSearchIdea) count_ = ideasAdapter.getItemCount();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("start_count", count_ + "");
        jsonObject.addProperty("offset_count", "20");
        jsonObject.addProperty("searchValue", searchEt.getText().toString());
        jsonObject.addProperty("flag", "favorite");
        ((MainActivity) requireActivity()).showCustomProgressDialog(dialogBg);
        Call<IdeaListResponse> call = service.searchIdeas(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        isSearchIdea = true;
                        if (response.body() != null) {
                            ideasAdapter.swapFilterData(response.body().getIdeas());
                        }else {
                            ideasAdapter.swapFilterData(new ArrayList<MyIdeasModel>());
                        }
                        if(ideasAdapter.IdeasModelsList==null || ideasAdapter.IdeasModelsList.isEmpty()){
                            rv_ideas.setVisibility(View.GONE);
                            noDataTv.setVisibility(View.VISIBLE);
                        }else {
                            rv_ideas.setVisibility(View.VISIBLE);
                            noDataTv.setVisibility(View.GONE);
                        }
                    }
                    ((MainActivity) requireActivity()).cancelProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    @Override
    public void clickedItem(Bundle bundle) {
        fragmentChangeCallBacks.fragmentName("goToOthersIdeaPreviewFragment", bundle);
    }

    private void captureBg() {
        try {
            Bitmap map = CommonApiUtils.takeScreenShot(getActivity());
            Bitmap fast = CommonApiUtils.fastblur(map, 30);
            dialogBg = new BitmapDrawable(getResources(), fast);
        }catch (Exception ignored){}

    }
}
