package com.vision.imagine.fragments;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.vision.imagine.Interface.AudioCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.pojos.Audio;
import com.vision.imagine.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.vision.imagine.fragments.CreateIdeasFragment.RequestPermissionCode;

public class AudioDialogFragment extends DialogFragment {
    Chronometer chronometer;
    //    List<Audio> audioList;
    ImageButton btnFloatRecord;
    ConstraintLayout mainView;
    RelativeLayout rlOpenRecording;
    //    LinearLayout llButtonLayout;
    AudioCallBacks audioCallBacks;
    AppCompatImageButton bt_stop_recording;
    MediaRecorder mediaRecorder;
    Random random;
    String AudioSavePathInDevice = null;
    String RandomAudioFileName = "Imagine";
    Drawable dialogBg;

    public AudioDialogFragment(AudioCallBacks audioCallBacks, Drawable dialogBg) {
        this.audioCallBacks = audioCallBacks;
        random = new Random();
        this.dialogBg = dialogBg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View customLayout = inflater.inflate(R.layout.create_idea_record_layout, container, false);

        mainView = customLayout.findViewById(R.id.main_view);
        chronometer = customLayout.findViewById(R.id.chronometer_audio);
        btnFloatRecord = customLayout.findViewById(R.id.fab_record);
        bt_stop_recording = customLayout.findViewById(R.id.bt_stop_recording);
        bt_stop_recording = customLayout.findViewById(R.id.bt_stop_recording);
        rlOpenRecording = customLayout.findViewById(R.id.rl_recording);
        btnFloatRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlOpenRecording.setVisibility(View.VISIBLE);
//                llButtonLayout.setVisibility(View.GONE);
                btnFloatRecord.setVisibility(View.GONE);
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                startRecording();
            }
        });
        bt_stop_recording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStopRecord();
                dismiss();
            }
        });
        if(getDialog()!=null & getDialog().getWindow()!=null) {
            mainView.setBackground(dialogBg);
        }
        return customLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getDialog()!=null & getDialog().getWindow()!=null) {
                    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    getDialog().getWindow().setBackgroundDrawable(dialogBg);

                }
            }
        },200);*/

    }

    @Override
    public void onStart() {
        super.onStart();
        /*if(getDialog()!=null & getDialog().getWindow()!=null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }*/
    }

    public void startRecording() {
        if (checkPermission()) {
            AudioSavePathInDevice =
                    getActivity().getExternalFilesDir("").getAbsolutePath() + "/" +
                            "ImagineAudio_" + random.nextInt(1000) + ".3gp";
            MediaRecorderReady();
            try {
                mediaRecorder.prepare();
                mediaRecorder.start();
            } catch (IllegalStateException | IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(getActivity(), "Recording started",
                    Toast.LENGTH_SHORT).show();
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new
                String[]{WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(AudioDialogFragment.this.getActivity(),
                WRITE_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(AudioDialogFragment.this.getActivity(),
                READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(AudioDialogFragment.this.getActivity(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private void stopRecording() {
       // try {
            FileUtils.showProgress(getContext(), "please wait");

            mediaRecorder.stop();
            chronometer.stop();
            Toast.makeText(getActivity(), "Recording Completed",
                    Toast.LENGTH_SHORT).show();

            rlOpenRecording.setVisibility(View.GONE);
//        llButtonLayout.setVisibility(View.VISIBLE);
            btnFloatRecord.setVisibility(View.VISIBLE);
            long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
            long minutes = TimeUnit.MILLISECONDS.toMinutes(elapsedMillis);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(elapsedMillis);

            Audio audio = new Audio();
            audio.setDuration(minutes + ":" + seconds);
            audio.setFileName(AudioSavePathInDevice);
            MediaPlayer mp = MediaPlayer.create(getActivity(), Uri.parse(AudioSavePathInDevice));
            int duration = mp.getDuration();
            audio.setIntDuration(duration);
            audio.setAudioUri(Uri.fromFile(new File(AudioSavePathInDevice)));
//        if (audioList != null) {
//            this.audioList.add(audio);
//            adapter.setData(audioList);
            audioCallBacks.onStopRecord(audio);
//        }
            FileUtils.stopProgress();
        //}catch (Exception e){
        //    Log.e("Error",""+e.getMessage());
        //}
    }

    public void onStopRecord() {
        Thread timer = new Thread() {
            @Override
            public void run() {
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stopRecording();
                    }
                });
            }
        };
        timer.start();
    }

    private String CreateRandomAudioFileName(int string) {
        random = new Random();
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));
            i++;
        }
        return stringBuilder.toString();
    }
}
