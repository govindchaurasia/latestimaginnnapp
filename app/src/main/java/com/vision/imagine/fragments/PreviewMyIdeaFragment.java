package com.vision.imagine.fragments;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.IdeaDetailsDataSet;
import com.vision.imagine.pojos.IdeaListResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PreviewMyIdeaFragment extends Fragment {

    APIService service = RetrofitClient.getClient().create(APIService.class);
    MyIdeasModel ideasModel;
    View view;
    LinearLayout lm;
    IdeaDetailsDataSet ideaDetailsDataSet;
    public static String baseUrl = RetrofitClient.BASE_URL + "getImage/";
    String ideaPath = "";
    MediaPlayer player = new MediaPlayer();
    private FragmentChangeCallBacks fragmentChangeCallBacks;

    private AppCompatImageView comment,thumb,heart,draw;
    public PreviewMyIdeaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ideasModel = (MyIdeasModel) getArguments().getSerializable("ideaObj");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        view = inflater.inflate(R.layout.fragment_idea_preview_dyn, container, false);
        view = inflater.inflate(R.layout.fragment_idea_preview_mein, container, false);
        setHasOptionsMenu(true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("");

        showSelectedIdea();
        likeShareCommentOptionToolbar();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Preview");
        return view;
    }

    private void likeShareCommentOptionToolbar() {
         comment = view.findViewById(R.id.comment);
         heart = view.findViewById(R.id.heart);
        draw = view.findViewById(R.id.draw);


        try {
            if (ideaDetailsDataSet.getFavFlag()) {
                heart.setImageResource(R.drawable.ic_favorite_white_24dp);
            } else {
                heart.setImageResource(R.drawable.ic_heart_tool);
            }
        } catch (Exception e) {
        }


        /*draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle2 = new Bundle();
                bundle2.putSerializable("ideaObj", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToUpdateIdeasFragment", bundle2);
            }
        });*/



         comment.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Bundle bundle = new Bundle();
                 bundle.putSerializable("ideaObj", ideasModel);
                 fragmentChangeCallBacks.fragmentName("goToIdeaCommentReplyFragment", bundle);
             }
         });

        /*thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ideaDetailsDataSet.getLikeFlag()) {
                    CommonApiUtils.updateLike(false, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    thumb.setImageResource(R.drawable.ic_outline_thumb_up_alt_24);
                    ideaDetailsDataSet.setLikeFlag(false);
                } else {
                    CommonApiUtils.updateLike(true, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    thumb.setImageResource(R.drawable.ic_thumb_up_tool);
                    ideaDetailsDataSet.setLikeFlag(true);
                }


            }
        });*/

        heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ideaDetailsDataSet.getFavFlag()) {
                    CommonApiUtils.updateFav(false, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    heart.setImageResource(R.drawable.ic_heart_tool);
                    ideaDetailsDataSet.setFavFlag(false);
                } else {
                    CommonApiUtils.updateFav(true, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    heart.setImageResource(R.drawable.ic_favorite_white_24dp);
                    ideaDetailsDataSet.setFavFlag(true);
                }


            }
        });


    }

    // Show Selected Idea
    private void showSelectedIdea() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("idea_id", ideasModel.getIdeaId());
        Call<IdeaListResponse> call = service.showSelectedIdea(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        ideasModel = response.body().getIdeas().get(0);

                        if (lm != null)
                            lm.removeAllViews();
                        initView();

                        getActivity().invalidateOptionsMenu();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_preview:
                Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToIdeaCommentReplyFragment", bundle);
                return true;

            case R.id.item_favorite:
                if (ideaDetailsDataSet.getFavFlag()) {
                    CommonApiUtils.updateFav(false, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    item.setIcon(R.drawable.ic_favorite_white_border_24px);
                    ideaDetailsDataSet.setFavFlag(false);
                } else {
                    CommonApiUtils.updateFav(true, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    item.setIcon(R.drawable.ic_favorite_white_24dp);
                    ideaDetailsDataSet.setFavFlag(true);
                }
                return true;

            case R.id.item_like:
                if (ideaDetailsDataSet.getLikeFlag()) {
                    CommonApiUtils.updateLike(false, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    item.setIcon(R.drawable.ic_outline_thumb_up_alt_24);
                    ideaDetailsDataSet.setLikeFlag(true);
                } else {
                    CommonApiUtils.updateLike(true, ideaDetailsDataSet.getCreatedBy(), ideaDetailsDataSet.getIdeaId());
                    item.setIcon(R.drawable.ic_baseline_thumb_up_alt_24);
                    ideaDetailsDataSet.setLikeFlag(false);
                }
                return true;

            case R.id.item_update:
                Bundle bundle2 = new Bundle();
                bundle2.putSerializable("ideaObj", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToUpdateIdeasFragment", bundle2);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_preview_my_idea, menu);

        MenuItem settingsItemLike = menu.findItem(R.id.item_like);
        MenuItem settingsItemFav = menu.findItem(R.id.item_favorite);

        try {
            if (ideaDetailsDataSet.getFavFlag()) {
                settingsItemFav.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_favorite_white_24dp));
            } else {
                settingsItemFav.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_favorite_white_border_24px));
            }

            if (ideaDetailsDataSet.getLikeFlag()) {
                settingsItemLike.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_baseline_thumb_up_alt_24));
            } else {
                settingsItemLike.setIcon(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_outline_thumb_up_alt_24));
            }
        } catch (Exception e) {
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    private void initView() {
        TextView title = view.findViewById(R.id.title);
        title.setText(ideaDetailsDataSet.getTitle());

        TextView description = view.findViewById(R.id.description);
        description.setText(ideaDetailsDataSet.getCaption());
       // TextView author = view.findViewById(R.id.tv_author);
       // author.setText(ideaDetailsDataSet.get());

        ideaPath = ideasModel.getUserId() + "/" + ideasModel.getIdeaId() + "/";
        String[] ele1 = ideaDetailsDataSet.getDescription().split("~");
        lm = (LinearLayout) view.findViewById(R.id.ll_dynamic);
        for (int j = 0; j < ele1.length; j++) {
            String[] ele2 = ele1[j].split(":");
            if (ele2[0].equalsIgnoreCase("photo")) {
                inflateImage(ele2[1]);
            } else if (ele2[0].equalsIgnoreCase("audio")) {
                inflateRecord(ele2[1]);
            } else {
                inflateText(ele2[1]);
            }
        }
    }

    private void inflateImage(String createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.image_single_row, lm, false);
        ImageView imageView = layout.findViewById(R.id.image);
        String localUrl = baseUrl + ideaPath + createIdeaDataSet;
        Glide.with(this)
//                .load(createIdeaDataSet.getFileUri())
                .load(localUrl)
//                .apply(new RequestOptions().override(100, 100))
                .into(imageView);
        lm.addView(layout);
    }

    private void inflateText(String createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.text_single_row, lm, false);
        TextView textView = layout.findViewById(R.id.tv_text);
        textView.setText(createIdeaDataSet);
        lm.addView(layout);
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (player != null)
                player.stop();
        } catch (Exception e) {
        }
    }

    private void inflateRecord(String createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.audio_single_view, lm, false);
        ImageView img_play = layout.findViewById(R.id.img_play);
        String localUrl =  baseUrl + ideaPath + createIdeaDataSet;
        img_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Uri uri = Uri.parse(localUrl);
                    player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    player.setDataSource(PreviewMyIdeaFragment.this.getActivity(), uri);
                    player.prepare();
                    player.start();
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        });

        lm.addView(layout);
    }





}