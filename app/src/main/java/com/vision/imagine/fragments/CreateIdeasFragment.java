package com.vision.imagine.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.bumptech.glide.Glide;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vision.imagine.BuildConfig;
import com.vision.imagine.Interface.AudioCallBacks;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.activities.DrawingActivity;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.callbacks.StartDragListener;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.Audio;
import com.vision.imagine.pojos.CategoryDataSet;
import com.vision.imagine.pojos.CategorySelectionData;
import com.vision.imagine.pojos.CreateIdeaDataSet;
import com.vision.imagine.pojos.CreateIdeaResponse;
import com.vision.imagine.pojos.DisableIdeaResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.PostIdeaModel;
import com.vision.imagine.rvAdapters.CategoryAdapter;
import com.vision.imagine.rvAdapters.CreateIdeaAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.FileCompressor;
import com.vision.imagine.utils.FileUtils;
import com.vision.imagine.utils.ItemMoveCallback;
import com.vision.imagine.utils.ProgressDialogBuilder;
import com.vision.imagine.utils.SharedPrefUtils;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class CreateIdeasFragment extends Fragment implements StartDragListener, AudioCallBacks, OnItemClickedListener {

    public static final int RequestPermissionCode = 1;
    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";
    private static final int FILE_SELECT_CODE = 113;
    private static final String TAG = CreateIdeasFragment.class.getName();
    private static final int REQUEST_TAKE_PHOTO = 111;
    private static final int REQUEST_GALLERY_PHOTO = 222;
    private static final int REQUEST_TAKE_PHOTO_IDEA = 110;
    private static final int REQUEST_GALLERY_PHOTO_IDEA = 220;
    private static final int REQUEST_DRAW = 115;
    FileCompressor mCompressor;
    PostIdeaModel postIdeaModel = new PostIdeaModel();
    //    AnimatedEditText et_title;
    EditText et_title, et_description;
    TextView doneCatBtn, addImgLbl, etFields;
    ChipGroup cpCategories;
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    int themeSelected;
    Dialog progressCustomDialog;
    ProgressDialog progressDialog;
    private RelativeLayout upload, capture, write, record, draw;
    private AppCompatImageView uploadIv, captureIv, writeIv, recordIv, drawIv;
    private ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList = new ArrayList<>();
    private CreateIdeaAdapter createIdeaAdapter;
    private ItemTouchHelper touchHelper;
    private RecyclerView rv_items;
    private CreateIdeaDataSet ideaProfileImage;
    private String mPhotoPath;
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private String title, des;
    String category = "", categoryName = "";
    private boolean isCreate = false;
    List<CategoryDataSet> createIdeaResponseList;
    //List<KeyPairBoolData> categories = new ArrayList<>();
    List<CategorySelectionData> categories = new ArrayList<>();
    Dialog createDialog;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    Drawable dialogBg;
    CategoryAdapter catSelectionAdapter;
    //Edit idea properties
    MyIdeasModel ideasModel = null;
    public boolean isEdit = false;
    String baseUrl = "http://imaginnn.com:443/images/";
    String ideaPath = "";
    MultiSpinnerSearch spFields;
    TextView fieldsTv;
    //Recreate idea
    boolean isReCreate = false;
    boolean isDialogShow = false;
    ActivityResultLauncher<Intent> launchSomeActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = ProgressDialogBuilder.build(getActivity(), getResources().getString(R.string.please_wait),"");
        progressDialog.setCancelable(false);
        launchSomeActivity = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK && result.getData()!=null) {
                            Bundle data = result.getData().getExtras();
                                try {
                                    File TempPhotoFile = new File(mPhotoPath);
                                    //Convert bitmap to byte array
                                    Bitmap bitmap = (Bitmap) data.get("data");
                                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                                    byte[] bitmapData = bos.toByteArray();
                                    FileOutputStream fos = new FileOutputStream(TempPhotoFile);
                                    fos.write(bitmapData);
                                    fos.flush();
                                    fos.close();

                                    TempPhotoFile = mCompressor.compressToFile(TempPhotoFile);
                                    String path = TempPhotoFile.getPath();
                                    String fileName = path.substring(path.lastIndexOf("/") + 1);

                                    CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
                                    createIdeaDataSet.setFileName(fileName);
                                    createIdeaDataSet.setPath(path);
                                    createIdeaDataSet.setFileUri(Uri.fromFile(TempPhotoFile));
                                    createIdeaDataSet.setFile(TempPhotoFile);
                                    createIdeaDataSetArrayList.add(createIdeaDataSet);
                                    createIdeaAdapter.setData(createIdeaDataSetArrayList);
                                    rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                        }
                    }
                });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_ideas, container, false);
        if (getArguments() != null && getArguments().getSerializable("idea") != null) {
            ideasModel = (MyIdeasModel) getArguments().getSerializable("idea");
            isEdit = true;
            MainActivity.isCreateIdeaOpen = true;
        }
        initView(view);
        askCheckPermission();
        mCompressor = new FileCompressor(getActivity());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        displayIdeaCategory();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isEdit) {
                    captureBg();
                    if(!isReCreate){
                        showDialog(getActivity(), true);
                        isDialogShow = true;
                    }
                }
            }
        }, 400);

        /*if (Constants.postIdeaModel != null) {
            isReCreate = true;
            setReCreateData();
        }*/
        return view;
    }

    @Override
    public void onResume() {
        MainActivity.isCreateIdeaOpen = true;
        if(!isCreate && !isEdit && isDialogShow){
            //getActivity().onBackPressed();
        }
        super.onResume();
    }

    /*private void setReCreateData() {
        createIdeaDataSetArrayList = Constants.postIdeaModel.getCreateIdeaDataSetArrayList();
        postIdeaModel = Constants.postIdeaModel;
        title = postIdeaModel.getTitle();
        des = postIdeaModel.getCaption();
        category = postIdeaModel.getCategory();
        categoryName = postIdeaModel.getCategoryName();
        ideaProfileImage = postIdeaModel.getIdeaImage();
        createIdeaAdapter.reSetData();
        createIdeaAdapter.setData(createIdeaDataSetArrayList);
        rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
        showDialog(getActivity(), false);
        setReCreateViewData();
    }*/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putSerializable("ideaModel", postIdeaModel);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       /* if(savedInstanceState!=null) {
            postIdeaModel = (PostIdeaModel) savedInstanceState.getSerializable("ideaModel");
            title=postIdeaModel.getTitle();
            des=postIdeaModel.getDescription();
            ideaProfileImage=postIdeaModel.getIdeaImage();
            category=postIdeaModel.getCategory();
            createIdeaDataSetArrayList=postIdeaModel.getCreateIdeaDataSetArrayList();
            createIdeaAdapter.notifyDataSetChanged();
        }*/
    }

    private void initView(View itemView) {
        createIdeaDataSetArrayList = new ArrayList<CreateIdeaDataSet>();

        rv_items = itemView.findViewById(R.id.rv_items);
        createIdeaAdapter = new CreateIdeaAdapter(createIdeaDataSetArrayList, getContext(), this, this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(CreateIdeasFragment.this.getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_items.setLayoutManager(mLayoutManager);
        rv_items.setHasFixedSize(true);

        ItemTouchHelper.Callback callback =
                new ItemMoveCallback(createIdeaAdapter);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(rv_items);

        rv_items.setAdapter(createIdeaAdapter);

//        et_title = itemView.findViewById(R.id.et_title);
//        et_description = itemView.findViewById(R.id.et_description);

        upload = itemView.findViewById(R.id.upload_container);
        capture = itemView.findViewById(R.id.capture_container);
        write = itemView.findViewById(R.id.write_container);
        record = itemView.findViewById(R.id.record_container);
        draw = itemView.findViewById(R.id.draw_container);
        uploadIv = itemView.findViewById(R.id.upload);
        captureIv = itemView.findViewById(R.id.capture);
        writeIv = itemView.findViewById(R.id.write);
        recordIv = itemView.findViewById(R.id.record);
        drawIv = itemView.findViewById(R.id.draw);
        setDrawableWithTint(uploadIv, R.drawable.ic_upload_tool);
        setDrawableWithTint(captureIv, R.drawable.ic_camera_tool);
        setDrawableWithTint(writeIv, R.drawable.ic_write_tool);
        setDrawableWithTint(recordIv, R.drawable.ic_mic_tool);
        setDrawableWithTint(drawIv, R.drawable.ic_draw_tool);

        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent drawIntent = new Intent(getContext(), DrawingActivity.class);
                getActivity().startActivityForResult(drawIntent, REQUEST_DRAW);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser(Constants.UPLOAD);
            }
        });
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                selectImage();
                requestStoragePermission(true);
            }
        });
        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogInputText_(Constants.WRITE);
            }
        });
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRecording();
            }
        });

        setHasOptionsMenu(true);
        if (isEdit) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Edit");
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Create Idea");
        }
        //((AppCompatActivity) getActivity()).getSupportActionBar().set

    }

    private void setDrawableWithTint(ImageView imageView, Integer drawable) {
        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getContext(), drawable);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(MainActivity.colorSelected));
        imageView.setImageDrawable(unwrappedDrawable1);
    }

    @Override
    public void requestDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_create_idea, menu);
    }

    private void askCheckPermission() {
        Dexter.withActivity(CreateIdeasFragment.this.getActivity())
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showFileChooser(int record) {
        Intent intent = getFileChooserIntent();
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    record);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(CreateIdeasFragment.this.getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private Intent getFileChooserIntent() {
        String[] mimeTypes = {"image/*", "application/pdf", "audio/mpeg"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";

            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }

            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        return intent;
    }

    private void dialogInputText_(int type) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_input_text);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        EditText textEt = dialog.findViewById(R.id.text_et);
        TextView cancelBtn = dialog.findViewById(R.id.cancel_button);
        TextView okBtn = dialog.findViewById(R.id.ok_button);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredText = textEt.getText().toString();

                CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(type);
                createIdeaDataSet.setFileName(enteredText);
                createIdeaDataSetArrayList.add(createIdeaDataSet);
                createIdeaAdapter.setData(createIdeaDataSetArrayList);
                rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(dialogBg);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    private void showCustomProgressDialog() {
        progressCustomDialog = new Dialog(getActivity());
        progressCustomDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressCustomDialog.setCancelable(false);
        progressCustomDialog.setContentView(R.layout.custom_progress_dialog);

        progressCustomDialog.show();
        progressCustomDialog.getWindow().setBackgroundDrawable(dialogBg);
        Window window = progressCustomDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    private void cancelProgressDialog(){
        if(progressCustomDialog!=null){
            progressCustomDialog.dismiss();
        }
    }

    private void dialogInputEditText_(int type, Bundle data) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_input_text);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        EditText textEt = dialog.findViewById(R.id.text_et);
        TextView cancelBtn = dialog.findViewById(R.id.cancel_button);
        TextView okBtn = dialog.findViewById(R.id.ok_button);

        CreateIdeaDataSet dataSet = (CreateIdeaDataSet) data.getSerializable("textData");
        int position = data.getInt("position");
        if (dataSet != null) {
            textEt.setText(dataSet.getFileName());
        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredText = textEt.getText().toString();
                createIdeaDataSetArrayList.get(position).setFileName(enteredText);
                createIdeaAdapter.setData(createIdeaDataSetArrayList);
                rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(dialogBg);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void saveDrawnBitmap(Bitmap pictureBitmap) {
        try {
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
//            File myDir = new File(root + "/ImagineImages");
//            myDir.mkdirs();

//            String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOut = null;
            Random random = new Random();
            File file = new File(path, "ImagineDrawing_" + random.nextInt(100) + ".jpg"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
            fOut = new FileOutputStream(file);

//            Bitmap pictureBitmap = getImageBitmap(myurl); // obtaining the Bitmap
            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream

            MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            // Tell the media scanner about the new file so that it is
// immediately available to the user.
            MediaScannerConnection.scanFile(getActivity(), new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });

            Log.d(TAG, "Bitmap Drawing File Path: " + file.getAbsolutePath());

            CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.DRAW);
            createIdeaDataSet.setFileName(file.getName());
            createIdeaDataSet.setPath(file.getAbsolutePath());
            createIdeaDataSet.setFileUri(Uri.fromFile(file));
            createIdeaDataSet.setFile(file);
            createIdeaDataSetArrayList.add(createIdeaDataSet);
            createIdeaAdapter.setData(createIdeaDataSetArrayList);
            rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_DRAW:
                if (resultCode == RESULT_OK) {
                    byte[] result = data.getByteArrayExtra("bitmap");
                    Bitmap bitmap = BitmapFactory.decodeByteArray(result, 0, result.length);
                    saveDrawnBitmap(bitmap);
                }
                break;
            case REQUEST_GALLERY_PHOTO:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri selectedImage = data.getData();
                        File filePhoto = mCompressor.compressToFile(new File(FileUtils.getRealPathFromUri(selectedImage, CreateIdeasFragment.this.getActivity())));

                        String path = filePhoto.getPath();
                        Log.d(TAG, "File Path: " + path);
                        String fileName = path.substring(path.lastIndexOf("/") + 1);

                        CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
                        createIdeaDataSet.setFileName(fileName);
                        createIdeaDataSet.setPath(path);
                        createIdeaDataSet.setFileUri(Uri.fromFile(filePhoto));
                        createIdeaDataSet.setFile(filePhoto);
                        createIdeaDataSetArrayList.add(createIdeaDataSet);
                        createIdeaAdapter.setData(createIdeaDataSetArrayList);
                        rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case REQUEST_TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    try {
                        File TempPhotoFile = new File(mPhotoPath);
                        TempPhotoFile = mCompressor.compressToFile(TempPhotoFile);
                        String path = TempPhotoFile.getPath();
                        String fileName = path.substring(path.lastIndexOf("/") + 1);

                        CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
                        createIdeaDataSet.setFileName(fileName);
                        createIdeaDataSet.setPath(path);
                        createIdeaDataSet.setFileUri(Uri.fromFile(TempPhotoFile));
                        createIdeaDataSet.setFile(TempPhotoFile);
                        createIdeaDataSetArrayList.add(createIdeaDataSet);
                        createIdeaAdapter.setData(createIdeaDataSetArrayList);
                        rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case REQUEST_TAKE_PHOTO_IDEA:
                if (resultCode == RESULT_OK) {
                    try {
                        File TempPhotoFile = new File(mPhotoPath);
                        TempPhotoFile = mCompressor.compressToFile(TempPhotoFile);
                        //ideaProfileImage = TempPhotoFile;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case REQUEST_GALLERY_PHOTO_IDEA:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri selectedImage = data.getData();
                        File filePhoto = mCompressor.compressToFile(new File(FileUtils.getRealPathFromUri(selectedImage, CreateIdeasFragment.this.getActivity())));
                        String path = filePhoto.getPath();
                        String fileName = path.substring(path.lastIndexOf("/") + 1);
                        CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
                        createIdeaDataSet.setFileName(fileName);
                        createIdeaDataSet.setPath(path);
                        createIdeaDataSet.setFileUri(Uri.fromFile(filePhoto));
                        createIdeaDataSet.setFile(filePhoto);
                        ideaProfileImage = createIdeaDataSet;
                        postIdeaModel.setIdeaImage(ideaProfileImage);
                        if (iv_idea_image != null && createDialog != null) {
                            Glide.with(getContext())
                                    .load(new File(filePhoto.getPath())) // Uri of the picture
                                    .into(iv_idea_image);
                            addImgLbl.setVisibility(View.INVISIBLE);
                        } else {
                            addImgLbl.setVisibility(View.VISIBLE);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Constants.RECORD:
            case Constants.UPLOAD:
            case Constants.WRITE:
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        // Get the Uri of the selected file
                        Uri uri = data.getData();
                        Log.d(TAG, "File Uri: " + uri.toString());
                        // Get the path
                        String path = FileUtils.getPath(CreateIdeasFragment.this.getActivity(), uri);
                        Log.d(TAG, "File Path: " + path);

                        String fileName = path.substring(path.lastIndexOf("/") + 1);

                        CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(0);
                        if (path.contains(".jpg") || path.contains(".jpeg") || path.contains(".png"))
                            createIdeaDataSet.setType(Constants.CAPTURE);
                        else if (path.contains(".3gp") || path.contains(".mp3"))
                            createIdeaDataSet.setType(Constants.RECORD);
                        else
                            createIdeaDataSet.setType(requestCode);

                        createIdeaDataSet.setFileName(fileName);
                        createIdeaDataSet.setFileUri(uri);
                        createIdeaDataSet.setFile(new File(uri.getPath()));
                        createIdeaDataSet.setPath(path);
                        createIdeaDataSetArrayList.add(createIdeaDataSet);
                        createIdeaAdapter.setData(createIdeaDataSetArrayList);
                        rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        captureBg();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateIdeasFragment.this.getActivity());
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                requestStoragePermission(true);
            } else if (items[item].equals("Choose from Library")) {
                requestStoragePermission(false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Capture image from camera
     */
    private void dispatchTakePictureIntent(Integer requestFor) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile(CreateIdeasFragment.this.getActivity());
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(CreateIdeasFragment.this.getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);

                mPhotoPath = photoFile.getPath();
                Bundle bundle = new Bundle();
                bundle.putString("uri", String.valueOf(photoURI));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                // mStartForResult.launch(takePictureIntent);
                getActivity().startActivityForResult(takePictureIntent, requestFor, bundle);
                //launchSomeActivity.launch(takePictureIntent);
                //registerForActivityResult(takePictureIntent,)
            }
        }
    }

    /**
     * Select image fro gallery
     */
    private void dispatchGalleryIntent(Integer requestfor) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, requestfor);
    }

    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent(REQUEST_TAKE_PHOTO);
                            } else {
                                dispatchGalleryIntent(REQUEST_GALLERY_PHOTO);
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
//                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> Toast.makeText(CreateIdeasFragment.this.getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }


    private void openRecording() {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment prev = getChildFragmentManager().findFragmentByTag("audioDialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        AudioDialogFragment dialogFragment = new AudioDialogFragment(CreateIdeasFragment.this, dialogBg);
        dialogFragment.setCancelable(true);
        dialogFragment.show(ft, "audioDialog");
       /* Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(dialogFragment.getDialog()!=null & dialogFragment.getDialog().getWindow()!=null) {
                    dialogFragment.getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    dialogFragment.getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                }
            }
        },100);*/

    }

    @Override
    public void startRecordingInter() {
//        startRecording();
    }

    @Override
    public void stopRecordingInter() {
    }

    @Override
    public void onStopRecord(Audio audio) {
        String path = audio.getFileName();
        String fileName = path.substring(path.lastIndexOf("/") + 1);

        CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.RECORD);
        createIdeaDataSet.setFileName(fileName);
        createIdeaDataSet.setPath(path);
        createIdeaDataSet.setFileUri(audio.getAudioUri());
        createIdeaDataSet.setFile(new File(audio.getAudioUri().getPath()));
        createIdeaDataSetArrayList.add(createIdeaDataSet);
        createIdeaAdapter.setData(createIdeaDataSetArrayList);
        rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.item_option:
                onOptionClick();

                return true;
        }
        return false;
    }

    public void onOptionClick() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_create_options2);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView editBtn = (TextView) dialog.findViewById(R.id.edit_btn);
        TextView previewBtn = (TextView) dialog.findViewById(R.id.preview_btn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) fieldsTv.setText(categoryName);
                createDialog.getWindow().setBackgroundDrawable(dialogBg);
                createDialog.show();
                dialog.dismiss();
            }
        });
        previewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isEdit) {
                    postIdeaModel.setEdit(1);
                    postIdeaModel.setIdea_id(String.valueOf(ideasModel.getIdeaId()));
                    postIdeaModel.setAccess_to(ideasModel.getAccessTo());
                } else {
                    postIdeaModel.setEdit(0);
                }
                postIdeaModel.setTitle(title);
                postIdeaModel.setCaption(des);
                postIdeaModel.setCategory(category);
                postIdeaModel.setCategoryName(categoryName);
                postIdeaModel.setIdeaImage(ideaProfileImage);
                postIdeaModel.setCreateIdeaDataSetArrayList(createIdeaDataSetArrayList);

                if (title.isEmpty()) {
                    Toast.makeText(getContext(), "Please enter title", Toast.LENGTH_LONG).show();
                } else if (des.isEmpty()) {
                    Toast.makeText(getContext(), "Please enter description", Toast.LENGTH_LONG).show();
                } else if (category.isEmpty()) {
                    Toast.makeText(getContext(), "Please select fields", Toast.LENGTH_LONG).show();
                } else if(createIdeaDataSetArrayList.size()<=0){
                    Toast.makeText(getContext(), "Please add something in your idea", Toast.LENGTH_LONG).show();
                } else {
                    postIdeaModel.setCreateIdeaDataSetArrayList(createIdeaAdapter.getOrderedList());
                    //Constants.postIdeaModel = postIdeaModel;
                    //DatabaseManager.getInstance(getContext()).addIdea(postIdeaModel);
                    //ideaUpdate(true);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ideaObj", postIdeaModel);
                    fragmentChangeCallBacks.fragmentName("goToMyIdeaPublishPreviewFragment", bundle);

                }
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(dialogBg);
        dialog.show();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    AppCompatImageView iv_idea_image;

    public void showDialog(Activity activity, boolean isShow) {

        createDialog = new Dialog(activity);
        createDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        createDialog.setCancelable(true);
        createDialog.setCanceledOnTouchOutside(false);
        createDialog.setContentView(R.layout.custom_dialog_create_idea);
        //createDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView gotIt = createDialog.findViewById(R.id.btn_submit);
        LinearLayout catPanel = createDialog.findViewById(R.id.cat_panel);
        //spFields = createDialog.findViewById(R.id.sp_fields);
        fieldsTv = createDialog.findViewById(R.id.tv_fields);
        /*spFields.setSearchEnabled(true);
        spFields.setSearchHint("Select your categories");
        spFields.setEmptyTitle("Data Not Found!");
        spFields.setLimit(3, new MultiSpinnerSearch.LimitExceedListener() {
            @Override
            public void onLimitListener(KeyPairBoolData data) {
                Log.e("MaxLimit","You can choose maximum 2 categories");
            }
        });*/
        /*spFields.setItems(categories, new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        if (category.isEmpty()) {
                            category = items.get(i).getId() + "";
                            categoryName = items.get(i).getName() + "";
                            etFields.setText(items.get(i).getName());
                        } else {
                            category = category + "," + items.get(i).getId();
                            categoryName = categoryName + "," + items.get(i).getName();
                            //etFields.setText(metFields.getText()+", "+ items.get(i).getText());
                        }
                        Log.i(TAG, i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
                    }
                }
            }
        });*/
        fieldsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSelectCategory(true);
            }
        });

        et_title = createDialog.findViewById(R.id.et_title);
        et_description = createDialog.findViewById(R.id.et_description);
        iv_idea_image = createDialog.findViewById(R.id.iv_idea_image);
        etFields = createDialog.findViewById(R.id.tv_fields);
        cpCategories = createDialog.findViewById(R.id.cp_category);
        doneCatBtn = createDialog.findViewById(R.id.done_cat_btn);
        addImgLbl = createDialog.findViewById(R.id.add_img_lbl);
        catPanel.setVisibility(View.GONE);

        mSharedPreferences = getActivity().getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);

        //ChangeBtnColor changeBtnColor = new ChangeBtnColor();
        //changeBtnColor.changecolor(getActivity(), gotIt, colorSelected);
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelected));
        gotIt.setBackgroundDrawable(unwrappedDrawable);

       /* doneCatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catPanel.setVisibility(View.GONE);
                etFields.setText("");
                List<Integer> ids = cpCategories.getCheckedChipIds();
                if (ids.size() > 0) {
                    for (Integer id : ids) {
                        Chip chip = cpCategories.findViewById(id);
                        if (category.isEmpty()) {
                            category = chip.getId() + "";
                            etFields.setText(chip.getText());
                        }else {
                            category = category + "," + chip.getId();
                            etFields.setText(etFields.getText()+", "+ chip.getText());
                        }
                    }

                }
            }
        });*/

        iv_idea_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchGalleryIntent(REQUEST_GALLERY_PHOTO_IDEA);
            }
        });
        gotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title = et_title.getText().toString();
                des = et_description.getText().toString();

                if (title.isEmpty()) {
                    Toast.makeText(getContext(), "Please enter title", Toast.LENGTH_LONG).show();
                } else if (des.isEmpty()) {
                    Toast.makeText(getContext(), "Please enter description", Toast.LENGTH_LONG).show();
                } else if (fieldsTv.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "Please select fields", Toast.LENGTH_LONG).show();
                } else {
                    isCreate = true;
                    createDialog.hide();
                    if (catSelectionAdapter.categories != null) {
                        List<CategorySelectionData> catList = catSelectionAdapter.categories;
                        category = "";
                        for (int i = 0; i < catList.size(); i++) {
                            if (catList.get(i).getIsSelected() == 1) {
                                if (category.isEmpty()) {
                                    category = catList.get(i).getCatId() + "";
                                    categoryName = catList.get(i).getCatName() + "";
                                } else {
                                    category = category + "," + catList.get(i).getCatId();
                                    categoryName = categoryName + "," + catList.get(i).getCatName();
                                }
                            }
                        }
                    }
                    postIdeaModel.setTitle(title);
                    postIdeaModel.setCaption(des);
                    postIdeaModel.setCategory(category);
                    postIdeaModel.setCategoryName(categoryName);

                    if(!isEdit) {
                        postIdeaModel.setAccess_to(Constants.IDEA_UNPUBLISHED);
                        if (postIdeaModel.getIdea_id() != null && !postIdeaModel.getIdea_id().equals("")) {
                            ideaUpdate(false);
                        } else {
                            ideaCreation();
                        }
                    }
                }
            }
        });
        createDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (!isCreate) {

                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(createDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;

        if (isEdit) {
            fieldsTv.setText(categoryName);
            String path = baseUrl + ideasModel.getUserId() + "/" + ideasModel.getIdeaId() + "/" + ideasModel.getIdeaImage();
            Glide.with(getContext()).load(path)
                    .placeholder(R.drawable.ic_default).into(iv_idea_image);
        }
        if (isReCreate) {
            if(postIdeaModel.getIdeaImage()!=null) {
                Glide.with(getContext()).load(postIdeaModel.getIdeaImage().getFileUri())
                        .placeholder(R.drawable.ic_default).into(iv_idea_image);
            }
        }
        createDialog.getWindow().setAttributes(lp);
        createDialog.getWindow().setBackgroundDrawable(dialogBg);
        if (isShow)
            createDialog.show();
    }

    private void displayIdeaCategory() {
        Call<List<CategoryDataSet>> call = service.displayIdeaCategory();
        call.enqueue(new Callback<List<CategoryDataSet>>() {
            @Override
            public void onResponse(Call<List<CategoryDataSet>> call, Response<List<CategoryDataSet>> response) {
                try {
                    if (response.isSuccessful()) {
                        createIdeaResponseList = response.body();
                        category = "";
                        for (CategoryDataSet cat : createIdeaResponseList) {
                            /*KeyPairBoolData data = new KeyPairBoolData(cat.getCategoryName(), false);
                            data.setId(cat.getCategoryId());
                            data.setName(cat.getCategoryName());*/
                            CategorySelectionData data = new CategorySelectionData();
                            data.setCatId(cat.getCategoryId());
                            data.setCatName(cat.getCategoryName());
                            data.setIsSelected(0);
                            categories.add(data);
                            if (isEdit) {
                                if (ideasModel.getCategory().toString() != null) {
                                    if (ideasModel.getCategory().toString().contains(",")) {
                                        String[] catList = ideasModel.getCategory().toString().split(",");
                                        for (String item : catList) {
                                            if (item.equals(cat.getCategoryName())) {
                                                if (category.isEmpty()) {
                                                    category = cat.getCategoryId() + "";
                                                } else {
                                                    category = category + "," + cat.getCategoryId();
                                                }
                                            }
                                        }
                                    } else {
                                        if (ideasModel.getCategory().toString().equals(cat.getCategoryName())) {
                                            if (category.isEmpty()) {
                                                category = cat.getCategoryId() + "";
                                            } else {
                                                category = category + "," + cat.getCategoryId();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (isEdit) setEditCatData();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryDataSet>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void setEditCatData() {
        categoryName = ideasModel.getCategory().toString();
        if (categoryName != null) {
            String[] cat;
            if (categoryName.contains(",")) {
                cat = categoryName.split(",");
            } else {
                cat = new String[1];
                cat[0] = categoryName;
            }
            for (int i = 0; i < categories.size(); i++) {
                for (String s : cat) {
                    if (s.equals(categories.get(i).getCatName())) {
                        categories.get(i).setIsSelected(1);
                    }
                }
            }
        }
        showSelectCategory(false);
        setEditData();
    }

    /*
    Set Edit data
    */
    private void setEditData() {
        if (ideasModel != null) {
            categoryName = ideasModel.getCategory().toString();
            createIdeaAdapter.reSetData();
            ideaPath = ideasModel.getUserId() + "/" + ideasModel.getIdeaId() + "/";
            List<String> ele1 = new ArrayList<>();
            if(ideasModel.getDescription()!=null && ideasModel.getDescription().contains("~")) {
                ele1 = Arrays.asList(ideasModel.getDescription().split("~"));
            }
            for (int j = 0; j < ele1.size(); j++) {
                CreateIdeaDataSet createIdeaDataSet;
                String[] ele2 = ele1.get(j).split(":");
                if (ele2[0].equalsIgnoreCase("photo")) {
                    createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
                    createIdeaDataSet.setFileName(ele2[1]);
                    String localUrl = baseUrl + ideaPath + ele2[1];
                    createIdeaDataSet.setFileUrl(localUrl);
                    createIdeaDataSetArrayList.add(createIdeaDataSet);
                } else if (ele2[0].equalsIgnoreCase("audio")) {
                    createIdeaDataSet = new CreateIdeaDataSet(Constants.RECORD);
                    createIdeaDataSet.setFileName(ele2[1]);
                    String localUrl = baseUrl + ideaPath + ele2[1];
                    createIdeaDataSet.setFileUrl(localUrl);
                    createIdeaDataSetArrayList.add(createIdeaDataSet);
                } else {
                    createIdeaDataSet = new CreateIdeaDataSet(Constants.WRITE);
                    createIdeaDataSet.setFileName(ele2[1]);
                    createIdeaDataSetArrayList.add(createIdeaDataSet);
                }
            }
            CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
            createIdeaDataSet.setFileName(ideasModel.getIdeaImage());
            createIdeaDataSet.setPath(baseUrl + ideasModel.getUserId() + "/" + ideasModel.getIdeaId() + "/" + ideasModel.getIdeaImage());
            createIdeaDataSet.setFileUri(null);
            createIdeaDataSet.setFile(null);
            ideaProfileImage = createIdeaDataSet;
            createIdeaAdapter.setData(createIdeaDataSetArrayList);
            rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
            //Set attributes
            showDialog(getActivity(), false);
            title = ideasModel.getTitle();
            des = ideasModel.getCaption();
            et_title.setText(title);
            et_description.setText(des);
            fieldsTv.setText(categoryName);
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    captureBg();
                }
            }, 400);
            postIdeaModel.setIdea_id(String.valueOf(ideasModel.getIdeaId()));
            postIdeaModel.setEdit(1);
            postIdeaModel.setTitle(title);
            postIdeaModel.setCaption(des);
            postIdeaModel.setCategory(category);
            postIdeaModel.setCategoryName(categoryName);
            postIdeaModel.setIdeaImage(ideaProfileImage);
            postIdeaModel.setAccess_to(ideasModel.getAccessTo()+"");
            MainActivity.startElementCount = ele1.size();
        } else {
            isEdit = false;
        }
        captureBg();
        createDialog.getWindow().setBackgroundDrawable(dialogBg);
    }

    /*
    Set Re Create data
    */
    private void setReCreateViewData() {
        //Set attributes
        showDialog(getActivity(), false);
        et_title.setText(title);
        et_description.setText(des);
        fieldsTv.setText(categoryName);
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 400);

    }

    private void captureBg() {
        Bitmap map = CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast = CommonApiUtils.fastblur(map, 30);
        dialogBg = new BitmapDrawable(getResources(), fast);
    }

    @Override
    public void clickedItem(Bundle data) {
        dialogInputEditText_(Constants.WRITE, data);
    }

    private void showSelectCategory(boolean isShow) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_select_category);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView cancelBtn = dialog.findViewById(R.id.cancel_btn);
        TextView doneBtn = dialog.findViewById(R.id.done_btn);
        RecyclerView catRecyclerView = dialog.findViewById(R.id.category_recycler_view);

        if (categories != null && categories.size() > 0) {
            catRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            catRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            catSelectionAdapter = new CategoryAdapter(getContext(), categories, false);
            catRecyclerView.setAdapter(catSelectionAdapter);
        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categories = catSelectionAdapter.categories;
                List<CategorySelectionData> catList = catSelectionAdapter.categories;
                fieldsTv.setText("");
                fieldsTv.setHint("Fields...");
                for (int i = 0; i < catList.size(); i++) {
                    if (catList.get(i).getIsSelected() == 1) {
                        if (fieldsTv.getText().toString().isEmpty()) {
                            fieldsTv.setText(catList.get(i).getCatName() + "");
                        } else {
                            fieldsTv.setText(fieldsTv.getText() + "," + catList.get(i).getCatName());
                        }
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(dialogBg);
        if (isShow) dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

   /* public void saveToDraft(){

        postIdeaModel.setEdit(0);
        postIdeaModel.setTitle(title);
        postIdeaModel.setCaption(des);
        postIdeaModel.setCategory(category);
        postIdeaModel.setCategoryName(categoryName);
        postIdeaModel.setIdeaImage(ideaProfileImage);
        postIdeaModel.setCreateIdeaDataSetArrayList(createIdeaDataSetArrayList);

        if (title.isEmpty()) {
            Toast.makeText(getContext(), "Please enter title", Toast.LENGTH_LONG).show();
        } else if (des.isEmpty()) {
            Toast.makeText(getContext(), "Please enter description", Toast.LENGTH_LONG).show();
        } else if (category.isEmpty()) {
            Toast.makeText(getContext(), "Please select fields", Toast.LENGTH_LONG).show();
        } else if(createIdeaDataSetArrayList.size()<=0){
            Toast.makeText(getContext(), "Please add something in your idea", Toast.LENGTH_LONG).show();
        } else {
            postIdeaModel.setCreateIdeaDataSetArrayList(createIdeaAdapter.getOrderedList());
            Constants.postIdeaModel = postIdeaModel;
            if(Constants.currentIdeaId==0){
                Random random = new Random();
                int x = random.nextInt(900) + 100;
                Constants.currentIdeaId = x;
                postIdeaModel.setIdea_id(String.valueOf(x));
            }
            //DatabaseManager.getInstance(getContext()).addIdea(postIdeaModel);
        }
    }*/

    private void ideaCreation() {
        showCustomProgressDialog();
        JsonObject jsonObject = new JsonObject();
        //jsonObject.addProperty("created_by", SharedPrefUtils.getStringData(Constants.USER_NAME));
        jsonObject.addProperty("title", postIdeaModel.getTitle());
        jsonObject.addProperty("caption", postIdeaModel.getCaption());
        jsonObject.addProperty("category", postIdeaModel.getCategory());
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));

        Call<CreateIdeaResponse> call = service.createIdea(jsonObject);
        call.enqueue(new Callback<CreateIdeaResponse>() {
            @Override
            public void onResponse(Call<CreateIdeaResponse> call, Response<CreateIdeaResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        CreateIdeaResponse createIdeaResponse = response.body();
                        postIdeaModel.setIdea_id(createIdeaResponse.getId() + "");
                        //ideaUpdate();
                        MainActivity.isCreateIdeaOpen = true;
                    }
                    cancelProgressDialog();
                } catch (Exception e) {
                    cancelProgressDialog();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CreateIdeaResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public void ideaUpdate(boolean gotoPreview) {
        showCustomProgressDialog();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", postIdeaModel.getIdea_id());
        jsonObject.addProperty("category", postIdeaModel.getCategory());
        jsonObject.addProperty("published",postIdeaModel.getAccess_to()+"");
        jsonObject.addProperty("title", postIdeaModel.getTitle());
        jsonObject.addProperty("caption", postIdeaModel.getCaption());
        jsonObject.addProperty("patent", "No IPR");
        jsonObject.addProperty("sharedTo", "");
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));

        List<MultipartBody.Part> parts = new ArrayList<>();

        String description = "";
        for (CreateIdeaDataSet createIdeaDataSet : createIdeaDataSetArrayList) {
            if (createIdeaDataSet.getType() != Constants.WRITE && createIdeaDataSet.getFileUri()!=null)
                parts.add(prepareFilePart("file", createIdeaDataSet));
            String prefix = "";
            switch (createIdeaDataSet.getType()) {
                case Constants.RECORD:
                    prefix = "audio:";
                    break;
                case Constants.WRITE:
                    prefix = "text:";
                    break;
                case Constants.UPLOAD:
                case Constants.DRAW:
                case Constants.CAPTURE:
                    prefix = "photo:";
                    break;
            }
            if (description.isEmpty())
                description = prefix + createIdeaDataSet.getFileName();
            else
                description = description + "~" + prefix + createIdeaDataSet.getFileName();
        }

        if(postIdeaModel.getIdeaImage() != null && postIdeaModel.getIdeaImage().getFileUri()!=null)
            parts.add(prepareFilePart("ideaProfilePic", postIdeaModel.getIdeaImage()));
        jsonObject.addProperty("description", description);

        Call<CreateIdeaResponse> call = service.updateIdea(parts, jsonObject);
        call.enqueue(new Callback<CreateIdeaResponse>() {
            @Override
            public void onResponse(Call<CreateIdeaResponse> call, Response<CreateIdeaResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (MainActivity.exitWithDraft){
                            MainActivity.exitWithDraft = false;
                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                                fm.popBackStack();
                            }
                            fragmentChangeCallBacks.fragmentName("goToMyIdeasFragment", new Bundle());
                        }
                    }
                    cancelProgressDialog();
                    if(gotoPreview){
                        //Constants.postIdeaModel = postIdeaModel;
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("ideaObj", postIdeaModel);
                        fragmentChangeCallBacks.fragmentName("goToMyIdeaPublishPreviewFragment", bundle);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CreateIdeaResponse> call, Throwable t) {
                try {
                    cancelProgressDialog();
                    call.cancel();
                }catch (Exception ignored){}
            }
        });
        if(gotoPreview){
            MyIdeasModel tempIdeaModel = new MyIdeasModel();
            tempIdeaModel.setIdeaImage(postIdeaModel.getIdeaImage().getFileName()+"");
            tempIdeaModel.setTitle(postIdeaModel.getTitle());
            tempIdeaModel.setIdeaId(Integer.parseInt(postIdeaModel.getIdea_id()));
            tempIdeaModel.setDescription(description);
            tempIdeaModel.setCategoryId(postIdeaModel.getCategory()+"");
            tempIdeaModel.setCategory(postIdeaModel.getCategoryName());
            tempIdeaModel.setCaption(postIdeaModel.getCaption());
            tempIdeaModel.setAuthor(SharedPrefUtils.getStringData(Constants.USER_NAME));
            tempIdeaModel.setActive(1);
            tempIdeaModel.setAccessTo(postIdeaModel.getAccess_to()+"");
            if(getArguments()==null){
                Bundle bundle = new Bundle();
                bundle.putSerializable("idea", tempIdeaModel);
                setArguments(bundle);
            }else {
                getArguments().putSerializable("idea", tempIdeaModel);
            }
        }
    }

    public void deleteIdeaAPI() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", String.valueOf(postIdeaModel.getIdea_id()));
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        Call<DisableIdeaResponse> call = service.deleteIdea(jsonObject);
        call.enqueue(new Callback<DisableIdeaResponse>() {
            @Override
            public void onResponse(Call<DisableIdeaResponse> call, Response<DisableIdeaResponse> response) {
                try {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }
                    fragmentChangeCallBacks.fragmentName("goToMyIdeasFragment", new Bundle());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DisableIdeaResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, CreateIdeaDataSet createIdeaDataSet) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new File(createIdeaDataSet.getPath()));
        return MultipartBody.Part.createFormData(partName, createIdeaDataSet.getFileName(), requestFile);
    }


}
