package com.vision.imagine.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.vision.imagine.ImagineApplication;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.callbacks.OnReplyCommentClickedListener;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.networking.response.CommentsResponse;
import com.vision.imagine.pojos.AddComment;
import com.vision.imagine.pojos.CommentList;
import com.vision.imagine.pojos.CommentListResponse;
import com.vision.imagine.pojos.CommentsModel;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.rvAdapters.CommentReplyAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.KeyboardUtils;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CommentReplyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CommentReplyFragment extends Fragment implements OnReplyCommentClickedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    CommentReplyAdapter commentReplyAdapter;
    RecyclerView CommentsRecyclerView;
    String idea_id = "66";
    ArrayList<CommentList> commentsModelArrayList = new ArrayList<>();
    ImageView iv_send;
    TextView et_comment, replyCommentMsg, noDataTv;
    Boolean isReply = false;
    LinearLayout replyPanel;
    ImageView closeReplyPanel;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    MyIdeasModel ideasModel;
    String replyCommentId = "";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public CommentReplyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CommentReplyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CommentReplyFragment newInstance(String param1, String param2) {
        CommentReplyFragment fragment = new CommentReplyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ideasModel = (MyIdeasModel) getArguments().getSerializable("ideaObj");
            idea_id = ideasModel.getIdeaId() + "";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_comment_reply, container, false);

        CommentsRecyclerView = view.findViewById(R.id.recycler_view_chat_activity_main);
        iv_send = view.findViewById(R.id.iv_send);
        et_comment = view.findViewById(R.id.et_comment);
        replyPanel = view.findViewById(R.id.reply_panel);
        closeReplyPanel = view.findViewById(R.id.close_reply_panel);
        replyCommentMsg = view.findViewById(R.id.comment_msg);
        noDataTv = view.findViewById(R.id.no_data_tv);

        iv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = et_comment.getText().toString();
                if (!comment.isEmpty()) {
                    if (isReply){
                        postReply(comment);
                    }else {
                        postComments(comment);
                    }
                    et_comment.setText("");
                }
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_comment.getWindowToken(), 0);
            }
        });

        closeReplyPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               clearReply();
            }
        });

        CommentsRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if ( bottom < oldBottom) {
                    CommentsRecyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                CommentsRecyclerView.scrollToPosition(commentReplyAdapter.getItemCount());
                            }catch (Exception ignored){}
                        }
                    }, 100);
                }
            }
        });

        setHasOptionsMenu(true);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Comments");
        getComments();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN );
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_blank, menu);
    }

    private void getComments() {
        JsonObject jsonObject = new JsonObject();

        //jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("idea_id", idea_id);
        Call<CommentListResponse> call = service.getComments(jsonObject);
        call.enqueue(new Callback<CommentListResponse>() {
            @Override
            public void onResponse(Call<CommentListResponse> call, Response<CommentListResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        setData(response.body().getCommentList(),response.body().getProfileImagePath());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CommentListResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void setData(List<CommentList> commentLists, String profileImagePath) {
        if(commentLists!=null && commentLists.size()>0){
            noDataTv.setVisibility(View.GONE);
            CommentsRecyclerView.setVisibility(View.VISIBLE);
            List<CommentList> actualComments = new ArrayList<>();
            List<CommentList> allReplies = new ArrayList<>();
            for (CommentList item : commentLists){
                if (item.getParentId().equals("0")){
                    actualComments.add(item);
                }else {
                    allReplies.add(item);
                }
            }
            for (int i = 0; i<actualComments.size();i++){
                List<CommentList> commentReplies = new ArrayList<>();
                for (CommentList reply : allReplies){
                    if(actualComments.get(i).getId().equals(reply.getParentId())){
                        commentReplies.add(reply);
                    }
                }
                actualComments.get(i).setReplyList(commentReplies);
            }
            commentReplyAdapter = new CommentReplyAdapter(commentsModelArrayList, this, getContext(),profileImagePath);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
            CommentsRecyclerView.setLayoutManager(mLayoutManager);
            CommentsRecyclerView.setItemAnimator(new DefaultItemAnimator());
            CommentsRecyclerView.setAdapter(commentReplyAdapter);
            commentReplyAdapter.resetData();
            commentReplyAdapter.swapData(actualComments);
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    CommentsRecyclerView.scrollToPosition(actualComments.size()-1);
                }
            }, 100);

        }else {
            noDataTv.setVisibility(View.VISIBLE);
            CommentsRecyclerView.setVisibility(View.GONE);
        }
    }

    private void postComments(String comments) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("action_value", "comment");
        jsonObject.addProperty("idea_id", idea_id);
        jsonObject.addProperty("comment_message", comments);
        Call<AddComment> call = service.addComments(jsonObject);
        call.enqueue(new Callback<AddComment>() {
            @Override
            public void onResponse(Call<AddComment> call, Response<AddComment> response) {

                try {
                    if (response.isSuccessful()) {
                        getComments();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddComment> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void postReply(String comments) {
        clearReply();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("action_value", "reply");
        jsonObject.addProperty("idea_id", idea_id);
        jsonObject.addProperty("comment_id", replyCommentId);
        jsonObject.addProperty("reply_message", comments);
        Call<AddComment> call = service.addComments(jsonObject);
        call.enqueue(new Callback<AddComment>() {
            @Override
            public void onResponse(Call<AddComment> call, Response<AddComment> response) {

                try {
                    if (response.isSuccessful()) {
                        getComments();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddComment> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void clickedReply(Bundle data) {
        isReply = true;
        replyPanel.setVisibility(View.VISIBLE);
        replyCommentMsg.setText(""+data.getString("comment"));
        replyCommentId = data.getString("comment_id");
        et_comment.setHint("Add reply...");
        et_comment.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et_comment, InputMethodManager.SHOW_IMPLICIT);
    }

    private void clearReply(){
        isReply = false;
        replyPanel.setVisibility(View.GONE);
        et_comment.setHint("Add comment...");
    }
}