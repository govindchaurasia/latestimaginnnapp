package com.vision.imagine.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.JsonObject;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.CategoryDataSet;
import com.vision.imagine.pojos.CreateIdeaDataSet;
import com.vision.imagine.pojos.CreateIdeaResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.PostIdeaModel;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class IdeaCategoryUpdateFragment extends Fragment {

    APIService service = RetrofitClient.getClient().create(APIService.class);
    PostIdeaModel postIdeaModel = new PostIdeaModel();
    ChipGroup chipGroup;
    String privacy = "";
    LinearLayout ll_private_button, ll_public_button, ll_apply_button;
    View view;
    List<CategoryDataSet> createIdeaResponseList;
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList = new ArrayList<>();

    public IdeaCategoryUpdateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            postIdeaModel = (PostIdeaModel) getArguments().getSerializable("ideaObj");
            createIdeaDataSetArrayList = postIdeaModel.getCreateIdeaDataSetArrayList();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_category_selector, container, false);

        chipGroup = view.findViewById(R.id.cp_category);
        ll_public_button = view.findViewById(R.id.ll_public_button);
        ll_private_button = view.findViewById(R.id.ll_private_button);
        ll_apply_button = view.findViewById(R.id.ll_apply_button);

        ll_public_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_public_button.setBackgroundResource(R.drawable.public_private_button_blue);
                ll_private_button.setBackgroundResource(R.drawable.public_private_button);
                privacy = "public";
            }
        });
        ll_private_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_public_button.setBackgroundResource(R.drawable.public_private_button);
                ll_private_button.setBackgroundResource(R.drawable.public_private_button_blue);
                privacy = "private";
            }
        });
        ll_apply_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<Integer> ids = chipGroup.getCheckedChipIds();
                String category = "";
                for (Integer id : ids) {
                    Chip chip = chipGroup.findViewById(id);
                    if (category.isEmpty())
                        category = chip.getId() + "";
                    else
                        category = category + "," + chip.getId();
                }
                postIdeaModel.setCategory(category);
                postIdeaModel.setAccess_to(privacy);
                if (category.isEmpty() && privacy.isEmpty())
                    Toast.makeText(getActivity(), "Please select Privacy and Category", Toast.LENGTH_SHORT).show();
                else
                    ideaCreation();
            }
        });
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Publish");
        displayIdeaCategory();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_blank, menu);
    }

    private void displayIdeaCategory() {
        Call<List<CategoryDataSet>> call = service.displayIdeaCategory();
        call.enqueue(new Callback<List<CategoryDataSet>>() {
            @Override
            public void onResponse(Call<List<CategoryDataSet>> call, Response<List<CategoryDataSet>> response) {
                try {
                    if (response.isSuccessful()) {
                        createIdeaResponseList = response.body();
                        setCategoryView();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryDataSet>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void setCategoryView() {

        for (CategoryDataSet genre : createIdeaResponseList) {
            Chip chip = new Chip(view.getContext(), null, R.attr.CustomChipChoice);
            chip.setText(genre.getCategoryName());
            chip.setId(genre.getCategoryId());
            chipGroup.addView(chip);
        }
    }

    private void ideaCreation() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("title", postIdeaModel.getTitle());
        jsonObject.addProperty("caption", postIdeaModel.getCaption());
        jsonObject.addProperty("category", postIdeaModel.getCategory());
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));

        Call<CreateIdeaResponse> call = service.createIdea(jsonObject);
        call.enqueue(new Callback<CreateIdeaResponse>() {
            @Override
            public void onResponse(Call<CreateIdeaResponse> call, Response<CreateIdeaResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        CreateIdeaResponse createIdeaResponse = response.body();
                        postIdeaModel.setIdea_id(createIdeaResponse.getId() + "");
                        ideaUpdate();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CreateIdeaResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void ideaUpdate() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", postIdeaModel.getIdea_id());
        jsonObject.addProperty("userId",SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("category", postIdeaModel.getCategory());
        jsonObject.addProperty("published", postIdeaModel.getAccess_to());
        //jsonObject.addProperty("created_by", SharedPrefUtils.getStringData(Constants.USER_NAME));

        List<MultipartBody.Part> parts = new ArrayList<>();

        String description = "";
        for (CreateIdeaDataSet createIdeaDataSet : createIdeaDataSetArrayList) {
            if (createIdeaDataSet.getType() != Constants.WRITE)
                parts.add(prepareFilePart("file", createIdeaDataSet));
            String prefix = "";
            switch (createIdeaDataSet.getType()) {
                case Constants.RECORD:
                    prefix = "audio:";
                    break;
                case Constants.WRITE:
                    prefix = "text:";
                    break;
                case Constants.UPLOAD:
                case Constants.DRAW:
                case Constants.CAPTURE:
                    prefix = "photo:";
                    break;
            }
            if (description.isEmpty())
                description = prefix + createIdeaDataSet.getFileName();
            else
                description = description + "~" + prefix + createIdeaDataSet.getFileName();
        }
        jsonObject.addProperty("description", description);

        Call<CreateIdeaResponse> call = service.updateIdea(parts, jsonObject);
        call.enqueue(new Callback<CreateIdeaResponse>() {
            @Override
            public void onResponse(Call<CreateIdeaResponse> call, Response<CreateIdeaResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        fragmentChangeCallBacks.fragmentName("goToMyIdeasFragment", new Bundle());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CreateIdeaResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, CreateIdeaDataSet createIdeaDataSet) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new File(createIdeaDataSet.getPath()));
        return MultipartBody.Part.createFormData(partName, createIdeaDataSet.getFileName(), requestFile);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }
}
