package com.vision.imagine.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.vision.imagine.ImagineApplication;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pagination.PaginationListener;
import com.vision.imagine.pojos.AuthorProfileResponse;
import com.vision.imagine.pojos.IdeaListResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.RequestAddConnection;
import com.vision.imagine.pojos.ResponseAddConnection;
import com.vision.imagine.pojos.UserProfileDataSet;
import com.vision.imagine.rvAdapters.IdeasAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.ProgressDialogBuilder;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthorProfileFragment extends Fragment implements OnItemClickedListener {

    UserProfileDataSet userProfileDataSet;
    ArrayList<MyIdeasModel> ideasModelArrayList = new ArrayList<>();
    APIService service = RetrofitClient.getClient().create(APIService.class);
    View view;
    TextView authorNameTv, ideaCountTv, statusTv;
    ImageView authorProfileIv;
    IdeasAdapter ideasAdapter;
    RecyclerView authorIdeaRecyclerView;
    String authorID = SharedPrefUtils.getStringData(Constants.USER_ID);
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            authorID = getArguments().getString("authorId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_author_profile, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Author Profile");
        setHasOptionsMenu(true);

        initView();
        getAuthorProfile();
        showIdea();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    private void getAuthorProfile() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", authorID);
        Call<AuthorProfileResponse> call = service.userProfile(jsonObject);
        call.enqueue(new Callback<AuthorProfileResponse>() {
            @Override
            public void onResponse(Call<AuthorProfileResponse> call, Response<AuthorProfileResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        userProfileDataSet = response.body().getAuthorData();
                        setData();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AuthorProfileResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void showIdea() {

        JsonObject jsonObject = new JsonObject();
        int count_ = ideasAdapter.getItemCount();

        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("start_count", count_ + "");
        jsonObject.addProperty("offset_count", "20");
        jsonObject.addProperty("filter", "author#"+authorID);
        //if (!category_id_filter.isEmpty() && SharedPrefUtils.getStringData(Constants.DEFAULT_FILTER_ON).equals("1"))
        //jsonObject.addProperty("category_id", category_id_filter);

        Call<IdeaListResponse> call = service.showIdeas(jsonObject);
        //Call<IdeaListResponse> call = service.showIdeasForUser(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {

                try {
                    if (response.isSuccessful() && response.body().getIdeas() != null && response.body().getIdeas().size() > 0) {
                        ideasAdapter.addData(response.body().getIdeas());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void initView() {
        authorNameTv = (TextView) view.findViewById(R.id.tv_author_name);
        ideaCountTv = (TextView) view.findViewById(R.id.tv_idea_count);
        statusTv = (TextView) view.findViewById(R.id.tv_status);
        authorProfileIv = (ImageView) view.findViewById(R.id.iv_display_picture);
        authorIdeaRecyclerView = (RecyclerView) view.findViewById(R.id.author_idea_recycler_view);

        ideasAdapter = new IdeasAdapter(ideasModelArrayList, this, getContext());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
        authorIdeaRecyclerView.setLayoutManager(mLayoutManager);
        authorIdeaRecyclerView.setItemAnimator(new DefaultItemAnimator());
        authorIdeaRecyclerView.setAdapter(ideasAdapter);

        authorIdeaRecyclerView.addOnScrollListener(new PaginationListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                showIdea();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    void setData() {
        if (userProfileDataSet != null) {
            authorNameTv.setText(userProfileDataSet.getUserName());
            ideaCountTv.setText(userProfileDataSet.getNoOfideas() + "");
            if(userProfileDataSet.getCaption()!=null && !userProfileDataSet.getCaption().equals("")) {
                statusTv.setVisibility(View.VISIBLE);
                statusTv.setText(userProfileDataSet.getCaption() + "");
            }else {
                statusTv.setVisibility(View.GONE);
            }
            if (userProfileDataSet.getProfilePicName() != null) {
                Glide.with(getContext())
                        .load("http://imaginnn.com:443/images/profilepic/" + userProfileDataSet.getProfilePicName())
                        .placeholder(getResources().getDrawable(R.drawable.ic_user))
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .circleCrop()
                        .into(authorProfileIv);
            }
        }
    }

    @Override
    public void clickedItem(Bundle data) {
        fragmentChangeCallBacks.fragmentName("goToOthersIdeaPreviewFragment", data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_profile, menu);

        if (!authorID.equals(SharedPrefUtils.getStringData(Constants.USER_ID)))
            menu.findItem(R.id.item_add_contact).setVisible(true);
        else
            menu.findItem(R.id.item_add_contact).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_add_contact:
                dialogAddContact();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dialogAddContact() {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.title_add_to_contact_list));
        builder.setMessage(getResources().getString(R.string.msg_add_to_contact_list));
        builder.setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callAddContactApi();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();*/

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_delete_people_confirmation);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView deleteMsgTv = (TextView) dialog.findViewById(R.id.delete_msg_tv);
        TextView saveBtn = (TextView) dialog.findViewById(R.id.save_btn);
        TextView discardBtn = (TextView) dialog.findViewById(R.id.discard_btn);
        deleteMsgTv.setText(getResources().getString(R.string.msg_add_to_contact_list));
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAddContactApi();
                dialog.cancel();
            }
        });
        discardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void callAddContactApi() {
        progressDialog = ProgressDialogBuilder.build(getActivity(), "", getResources().getString(R.string.sending_connection));
        progressDialog.show();

        RequestAddConnection objRequestAddConnection = new RequestAddConnection();
        objRequestAddConnection.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        objRequestAddConnection.setFriendId(authorID);

        Call<ResponseAddConnection> call = service.addConnection(objRequestAddConnection);
        call.enqueue(new Callback<ResponseAddConnection>() {
            @Override
            public void onResponse(Call<ResponseAddConnection> call, Response<ResponseAddConnection> response) {
                progressDialog.cancel();
                try {
                    if (response != null && response.body() != null) {
                        if (response.isSuccessful()) {
                            Toast.makeText(getContext(), response.body().getRemarks(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseAddConnection> call, Throwable t) {
                progressDialog.cancel();
                call.cancel();
            }
        });
    }

    public String getAuthorId() {
        return authorID;
    }

    public void setAuthorId(String authorID) {
        this.authorID = authorID;
    }
}
