package com.vision.imagine.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.vision.imagine.R;

public class PeopleFragment extends Fragment {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_people, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("People");
        setHasOptionsMenu(true);

        return view;
    }
}
