package com.vision.imagine.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.vision.imagine.R;

public class FaqsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faqs, container, false);
//        WebView webView = view.findViewById(R.id.wb_faqs);
//        webView.loadUrl("file:///android_asset/faq_sample.html");
//        webView.loadUrl("https://www.samplestore.com/site/faq");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("FAQs");
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_blank, menu);
    }
}
