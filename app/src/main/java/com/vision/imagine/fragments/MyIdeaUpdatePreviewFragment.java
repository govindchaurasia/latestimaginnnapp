package com.vision.imagine.fragments;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.pojos.CreateIdeaDataSet;
import com.vision.imagine.pojos.PostIdeaModel;
import com.vision.imagine.utils.Constants;

import java.io.File;
import java.util.ArrayList;


public class MyIdeaUpdatePreviewFragment extends Fragment {

    PostIdeaModel postIdeaModel = new PostIdeaModel();
    View view;
    LinearLayout lm;
    MediaPlayer player = new MediaPlayer();
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList = new ArrayList<>();

    public MyIdeaUpdatePreviewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            postIdeaModel = (PostIdeaModel) getArguments().getSerializable("ideaObj");
            createIdeaDataSetArrayList = postIdeaModel.getCreateIdeaDataSetArrayList();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_idea_preview_dyn, container, false);
        setHasOptionsMenu(true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Preview");

        initView();
        return view;
    }

    private void initView() {
        TextView title = view.findViewById(R.id.title);
        title.setText(postIdeaModel.getTitle());

        TextView description = view.findViewById(R.id.description);
        description.setText(postIdeaModel.getCaption());

        lm = (LinearLayout) view.findViewById(R.id.ll_dynamic);
        for (int j = 0; j < createIdeaDataSetArrayList.size(); j++) {
            if (createIdeaDataSetArrayList.get(j).getType() == Constants.CAPTURE || createIdeaDataSetArrayList.get(j).getType() == Constants.DRAW) {
                inflateImage(createIdeaDataSetArrayList.get(j));
            } else if (createIdeaDataSetArrayList.get(j).getType() == Constants.RECORD) {
                inflateRecord(createIdeaDataSetArrayList.get(j));
            } else {
                inflateText(createIdeaDataSetArrayList.get(j));
            }
        }
    }

    private void inflateImage(CreateIdeaDataSet createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.image_single_row, lm, false);
        ImageView imageView = layout.findViewById(R.id.image);
        Glide.with(this)
//                .load(createIdeaDataSet.getFileUri())
                .load(Uri.fromFile(new File(createIdeaDataSet.getPath())))
//                .apply(new RequestOptions().override(100, 100))
                .into(imageView);
        lm.addView(layout);
    }

    private void inflateText(CreateIdeaDataSet createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.text_single_row, lm, false);
        TextView textView = layout.findViewById(R.id.tv_text);
        textView.setText(createIdeaDataSet.getFileName());
        lm.addView(layout);
    }

    private void inflateRecord(CreateIdeaDataSet createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.audio_single_view, lm, false);

        ImageView img_play = layout.findViewById(R.id.img_play);
        Uri localUri = createIdeaDataSet.getFileUri();
        img_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    player.setDataSource(MyIdeaUpdatePreviewFragment.this.getActivity(), localUri);
                    player.prepare();
                    player.start();
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        });
        lm.addView(layout);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_idea_preview_publish, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
           /* case R.id.item_publish:
                Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", postIdeaModel);
                fragmentChangeCallBacks.fragmentName("goToIdeaCategoryUpdateFragment", bundle);
                return true;*/
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (player != null)
                player.stop();
        } catch (Exception e) {
        }
    }
}
