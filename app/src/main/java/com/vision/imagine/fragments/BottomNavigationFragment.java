package com.vision.imagine.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class BottomNavigationFragment extends Fragment {
    BottomNavigationView bottomNavigationView;
    LinearLayout search;
    MainActivity mainActivity;

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()) {
                        case R.id.nav_explore:
                            selectedFragment = new MyIdeasFragment();
                            //search.setVisibility(View.VISIBLE);
                            break;
                        case R.id.nav_ideas:
                            selectedFragment = new ExploreIdeaFragment();
                            //search.setVisibility(View.VISIBLE);
                            break;
                        case R.id.nav_my_connections:
                            selectedFragment = new MyConnectionsFragment();
                            //search.setVisibility(View.VISIBLE);
                            break;
                        case R.id.nav_favorites:
                            selectedFragment = new FavoritesFragment();
                            //search.setVisibility(View.VISIBLE);
                            break;
                        case R.id.nav_faqs:
                            selectedFragment = new FaqsFragment();
                            break;
                    }

                    getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment, getName(selectedFragment)).commit();
                    onResume();
                    return true;
                }
            };

    public BottomNavigationFragment(MainActivity mainActivity) {
        this.mainActivity=mainActivity;
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bottom_navigation, container, false);

        bottomNavigationView = view.findViewById(R.id.bottom_navigation);
        //search = view.findViewById(R.id.search);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        mainActivity.getSupportActionBar().setHomeButtonEnabled(true);
        mainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mainActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);

        bottomNavigationView.setItemIconTintList(null);
        bottomNavigationView.setBackgroundResource(R.drawable.bottom_bg);

        getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, new ExploreIdeaFragment(), getName(new ExploreIdeaFragment())).commit();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mainActivity.getSupportActionBar().setHomeButtonEnabled(true);
        mainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mainActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Fragment fragment = getChildFragmentManager().findFragmentByTag(getName(new CreateIdeasFragment()));
        //fragment.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected String getName(final Fragment fragment) {
        return fragment.getClass().getSimpleName();
    }

}