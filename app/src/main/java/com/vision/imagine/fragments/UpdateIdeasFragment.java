package com.vision.imagine.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alimuzaffar.lib.widgets.AnimatedEditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vision.imagine.BuildConfig;
import com.vision.imagine.Interface.AudioCallBacks;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.activities.DrawingActivity;
import com.vision.imagine.callbacks.StartDragListener;
import com.vision.imagine.pojos.Audio;
import com.vision.imagine.pojos.CreateIdeaDataSet;
import com.vision.imagine.pojos.PostIdeaModel;
import com.vision.imagine.rvAdapters.CreateIdeaAdapter;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.FileCompressor;
import com.vision.imagine.utils.FileUtils;
import com.vision.imagine.utils.ItemMoveCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.app.Activity.RESULT_OK;

public class UpdateIdeasFragment extends Fragment implements StartDragListener, AudioCallBacks {

    public static final int RequestPermissionCode = 1;
    private static final int FILE_SELECT_CODE = 113;
    private static final String TAG = UpdateIdeasFragment.class.getName();
    private static final int REQUEST_TAKE_PHOTO = 111;
    private static final int REQUEST_GALLERY_PHOTO = 222;
    private static final int REQUEST_DRAW = 115;
    FileCompressor mCompressor;
    PostIdeaModel postIdeaModel = new PostIdeaModel();
    AnimatedEditText et_title;
    EditText et_description;
    private RelativeLayout upload, capture, write, record, draw;
    private ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList = new ArrayList<>();
    private CreateIdeaAdapter createIdeaAdapter;
    private ItemTouchHelper touchHelper;
    private RecyclerView rv_items;
    private String mPhotoPath;
    private FragmentChangeCallBacks fragmentChangeCallBacks;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_ideas, container, false);
        initView(view);
        askCheckPermission();
        mCompressor = new FileCompressor(UpdateIdeasFragment.this.getActivity());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        return view;
    }

    private void initView(View itemView) {
        createIdeaDataSetArrayList = new ArrayList<CreateIdeaDataSet>();

        rv_items = itemView.findViewById(R.id.rv_items);
        createIdeaAdapter = new CreateIdeaAdapter(createIdeaDataSetArrayList, getContext(), this,null);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(UpdateIdeasFragment.this.getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_items.setLayoutManager(mLayoutManager);
        rv_items.setHasFixedSize(true);

        ItemTouchHelper.Callback callback =
                new ItemMoveCallback(createIdeaAdapter);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(rv_items);

        rv_items.setAdapter(createIdeaAdapter);

        et_title = itemView.findViewById(R.id.et_title);
        et_description = itemView.findViewById(R.id.et_description);

        upload = itemView.findViewById(R.id.upload_container);
        capture = itemView.findViewById(R.id.capture_container);
        write = itemView.findViewById(R.id.write_container);
        record = itemView.findViewById(R.id.record_container);
        draw = itemView.findViewById(R.id.draw_container);

        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent drawIntent = new Intent(getContext(), DrawingActivity.class);
                getActivity().startActivityForResult(drawIntent, REQUEST_DRAW);
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser(Constants.UPLOAD);
            }
        });
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                selectImage();
                requestStoragePermission(true);
            }
        });
        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogInputText(Constants.WRITE);
            }
        });
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRecording();
            }
        });

        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Create Idea");
    }

    @Override
    public void requestDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_create_idea, menu);
    }

    private void askCheckPermission() {
        Dexter.withActivity(UpdateIdeasFragment.this.getActivity())
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showFileChooser(int record) {
        Intent intent = getFileChooserIntent();
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    record);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(UpdateIdeasFragment.this.getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private Intent getFileChooserIntent() {
        String[] mimeTypes = {"image/*", "application/pdf", "audio/mpeg"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";

            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }

            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        return intent;
    }

    private void dialogInputText(int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(UpdateIdeasFragment.this.getActivity());
        builder.setTitle("Enter Text");

        FrameLayout container = new FrameLayout(UpdateIdeasFragment.this.getActivity());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 66);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.bottomMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.height = 333;

        final EditText input = new EditText(UpdateIdeasFragment.this.getActivity());
//        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        input.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
        Drawable drawable = getResources().getDrawable(R.drawable.round_corner_lightblue_stroke);
        input.setBackground(drawable);
        input.setLayoutParams(params);
//        input.setHeight(UpdateIdeasFragment.this.getResources().getDimensionPixelSize(R.dimen.dialog_height));
        container.addView(input);
        builder.setView(container);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String enteredText = input.getText().toString();

                CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(type);
                createIdeaDataSet.setFileName(enteredText);
                createIdeaDataSetArrayList.add(createIdeaDataSet);
                createIdeaAdapter.setData(createIdeaDataSetArrayList);
                rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void saveDrawnBitmap(Bitmap pictureBitmap) {
        try {
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
//            File myDir = new File(root + "/ImagineImages");
//            myDir.mkdirs();

//            String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOut = null;
            Random random = new Random();
            File file = new File(path, "ImagineDrawing_" + random.nextInt(100) + ".jpg"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
            fOut = new FileOutputStream(file);

//            Bitmap pictureBitmap = getImageBitmap(myurl); // obtaining the Bitmap
            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream

            MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            // Tell the media scanner about the new file so that it is
// immediately available to the user.
            MediaScannerConnection.scanFile(getActivity(), new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });

            Log.d(TAG, "Bitmap Drawing File Path: " + file.getAbsolutePath());

            CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.DRAW);
            createIdeaDataSet.setFileName(file.getName());
            createIdeaDataSet.setPath(file.getAbsolutePath());
            createIdeaDataSet.setFileUri(Uri.fromFile(file));
            createIdeaDataSet.setFile(file);
            createIdeaDataSetArrayList.add(createIdeaDataSet);
            createIdeaAdapter.setData(createIdeaDataSetArrayList);
            rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_DRAW:
                if (resultCode == RESULT_OK) {
                    byte[] result = data.getByteArrayExtra("bitmap");
                    Bitmap bitmap = BitmapFactory.decodeByteArray(result, 0, result.length);
                    saveDrawnBitmap(bitmap);
                }
                break;
            case REQUEST_GALLERY_PHOTO:
                try {
                    Uri selectedImage = data.getData();
                    File filePhoto = mCompressor.compressToFile(new File(FileUtils.getRealPathFromUri(selectedImage, UpdateIdeasFragment.this.getActivity())));

                    String path = filePhoto.getPath();
                    Log.d(TAG, "File Path: " + path);
                    String fileName = path.substring(path.lastIndexOf("/") + 1);

                    CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
                    createIdeaDataSet.setFileName(fileName);
                    createIdeaDataSet.setPath(path);
                    createIdeaDataSet.setFileUri(Uri.fromFile(filePhoto));
                    createIdeaDataSet.setFile(filePhoto);
                    createIdeaDataSetArrayList.add(createIdeaDataSet);
                    createIdeaAdapter.setData(createIdeaDataSetArrayList);
                    rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_TAKE_PHOTO:
                try {
                    File TempPhotoFile = new File(mPhotoPath);
                    TempPhotoFile = mCompressor.compressToFile(TempPhotoFile);
                    String path = TempPhotoFile.getPath();
                    String fileName = path.substring(path.lastIndexOf("/") + 1);

                    CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.CAPTURE);
                    createIdeaDataSet.setFileName(fileName);
                    createIdeaDataSet.setPath(path);
                    createIdeaDataSet.setFileUri(Uri.fromFile(TempPhotoFile));
                    createIdeaDataSet.setFile(TempPhotoFile);
                    createIdeaDataSetArrayList.add(createIdeaDataSet);
                    createIdeaAdapter.setData(createIdeaDataSetArrayList);
                    rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.RECORD:
            case Constants.UPLOAD:
            case Constants.WRITE:
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        // Get the Uri of the selected file
                        Uri uri = data.getData();
                        Log.d(TAG, "File Uri: " + uri.toString());
                        // Get the path
                        String path = FileUtils.getPath(UpdateIdeasFragment.this.getActivity(), uri);
                        Log.d(TAG, "File Path: " + path);

                        String fileName = path.substring(path.lastIndexOf("/") + 1);

                        CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(0);
                        if (path.contains(".jpg") || path.contains(".jpeg") || path.contains(".png"))
                            createIdeaDataSet.setType(Constants.CAPTURE);
                        else if (path.contains(".3gp") || path.contains(".mp3"))
                            createIdeaDataSet.setType(Constants.RECORD);
                        else
                            createIdeaDataSet.setType(requestCode);

                        createIdeaDataSet.setFileName(fileName);
                        createIdeaDataSet.setFileUri(uri);
                        createIdeaDataSet.setFile(new File(uri.getPath()));
                        createIdeaDataSet.setPath(path);
                        createIdeaDataSetArrayList.add(createIdeaDataSet);
                        createIdeaAdapter.setData(createIdeaDataSetArrayList);
                        rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(UpdateIdeasFragment.this.getActivity());
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                requestStoragePermission(true);
            } else if (items[item].equals("Choose from Library")) {
                requestStoragePermission(false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Capture image from camera
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(UpdateIdeasFragment.this.getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile(UpdateIdeasFragment.this.getActivity());
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(UpdateIdeasFragment.this.getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);

                mPhotoPath = photoFile.getPath();
                Bundle bundle = new Bundle();
                bundle.putString("uri", String.valueOf(photoURI));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO, bundle);

            }
        }
    }

    /**
     * Select image fro gallery
     */
    private void dispatchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(UpdateIdeasFragment.this.getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent();
                            } else {
                                dispatchGalleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
//                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> Toast.makeText(UpdateIdeasFragment.this.getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }


    private void openRecording() {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment prev = getChildFragmentManager().findFragmentByTag("audioDialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        AudioDialogFragment dialogFragment = new AudioDialogFragment(UpdateIdeasFragment.this,null);
        dialogFragment.setCancelable(false);
        dialogFragment.show(ft, "audioDialog");
    }

    @Override
    public void startRecordingInter() {
//        startRecording();
    }

    @Override
    public void stopRecordingInter() {
    }

    @Override
    public void onStopRecord(Audio audio) {
        String path = audio.getFileName();
        String fileName = path.substring(path.lastIndexOf("/") + 1);

        CreateIdeaDataSet createIdeaDataSet = new CreateIdeaDataSet(Constants.RECORD);
        createIdeaDataSet.setFileName(fileName);
        createIdeaDataSet.setPath(path);
        createIdeaDataSet.setFileUri(audio.getAudioUri());
        createIdeaDataSet.setFile(new File(audio.getAudioUri().getPath()));
        createIdeaDataSetArrayList.add(createIdeaDataSet);
        createIdeaAdapter.setData(createIdeaDataSetArrayList);
        rv_items.getLayoutManager().scrollToPosition(createIdeaDataSetArrayList.size() - 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.item_preview:

                String title = et_title.getText().toString();
                String des = et_description.getText().toString();
                postIdeaModel.setTitle(title);
                postIdeaModel.setCaption(des);
                postIdeaModel.setCreateIdeaDataSetArrayList(createIdeaDataSetArrayList);

                if (!title.isEmpty() && !des.isEmpty() && createIdeaDataSetArrayList.size() > 0) {
                    postIdeaModel.setCreateIdeaDataSetArrayList(createIdeaAdapter.getOrderedList());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ideaObj", postIdeaModel);
                    fragmentChangeCallBacks.fragmentName("goToMyIdeaUpdatePreviewFragment", bundle);
                }

                return true;
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }
}
