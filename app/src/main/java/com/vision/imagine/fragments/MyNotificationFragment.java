package com.vision.imagine.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vision.imagine.ImagineApplication;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.firebasenotifications.firebaseutils.NotificationUtils;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pagination.PaginationListener;
import com.vision.imagine.pojos.FriendNotifyListItem;
import com.vision.imagine.pojos.RequestAddConnection;
import com.vision.imagine.pojos.RequestNotificationFriendRequest;
import com.vision.imagine.pojos.ResponseAddConnection;
import com.vision.imagine.pojos.ResponseNotificationFriendRequest;
import com.vision.imagine.rvAdapters.NotificationAdapter;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyNotificationFragment extends Fragment implements OnItemClickedListener {

    RecyclerView rv_notifications;
    TextView txtNoNotifications;
    ArrayList<FriendNotifyListItem> friendsItemArrayList = new ArrayList<>();
    NotificationAdapter notificationAdapter;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifcation, container, false);
        rv_notifications = view.findViewById(R.id.rv_notifications);
        txtNoNotifications = view.findViewById(R.id.txtNoNotifications);

        notificationAdapter = new NotificationAdapter(friendsItemArrayList, this, getContext());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
        rv_notifications.setLayoutManager(mLayoutManager);
        rv_notifications.setItemAnimator(new DefaultItemAnimator());
        rv_notifications.setAdapter(notificationAdapter);

//        if (notificationAdapter.getItemCount() > 0)
//            txtNoContacts.setVisibility(View.GONE);
//        else
//            txtNoContacts.setVisibility(View.VISIBLE);

        rv_notifications.addOnScrollListener(new PaginationListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                showNotifications();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Friend Requests");
        showNotifications();
        clearAllNotifications();
        return view;
    }

    private void showNotifications() {
        int count_ = notificationAdapter.getItemCount();

        RequestNotificationFriendRequest objRequestNotificationFriendRequest = new RequestNotificationFriendRequest();
        objRequestNotificationFriendRequest.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        objRequestNotificationFriendRequest.setStartCount(count_ + "");
        objRequestNotificationFriendRequest.setOffsetCount("20");

        Call<ResponseNotificationFriendRequest> call = service.getFriendRequestsApi(objRequestNotificationFriendRequest);
        call.enqueue(new Callback<ResponseNotificationFriendRequest>() {
            @Override
            public void onResponse(Call<ResponseNotificationFriendRequest> call, Response<ResponseNotificationFriendRequest> response) {
                isLoading = false;
                try {
                    if (response.isSuccessful()) {
//                        friendsItemArrayList.clear();
//                        notificationAdapter.swapData(response.body().getFriendNotifyList(), response.body().getUserProfileURL());
                        notificationAdapter.swapData(response.body().getFriendNotifyList(), response.body().getProfileImagePath());

                        if (notificationAdapter.getItemCount() > 0)
                            txtNoNotifications.setVisibility(View.GONE);
                        else
                            txtNoNotifications.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseNotificationFriendRequest> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    @Override
    public void clickedItem(Bundle bundle) {
        if (bundle.getInt("actionType", 0) == Constants.ACTION_TYPE_ACCEPT_REQUEST) {
            callAcceptRequestApi((FriendNotifyListItem) bundle.getSerializable("friendNotifyListItem"), true, bundle.getInt("position", -1));
        } else if (bundle.getInt("actionType", 0) == Constants.ACTION_TYPE_REJECT_REQUEST) {
            callAcceptRequestApi((FriendNotifyListItem) bundle.getSerializable("friendNotifyListItem"), false, bundle.getInt("position", -1));
        }
    }

    private void callAcceptRequestApi(FriendNotifyListItem friendNotifyListItem, boolean isAccept, int position) {
        RequestAddConnection objRequestAddConnection = new RequestAddConnection();
        objRequestAddConnection.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        objRequestAddConnection.setFriendId(friendNotifyListItem.getUserId());

        Call<ResponseAddConnection> call;
        if (isAccept) {
            call = service.acceptConnectionsRequest(objRequestAddConnection);
        } else {
            call = service.rejectConnectionsRequest(objRequestAddConnection);
        }
        call.enqueue(new Callback<ResponseAddConnection>() {
            @Override
            public void onResponse(Call<ResponseAddConnection> call, Response<ResponseAddConnection> response) {
                try {
                    if (response.isSuccessful()) {
                        Toast.makeText(getActivity(), response.body().getRemarks(), Toast.LENGTH_LONG).show();

                        if (response.body().getRemarks() != null && response.body().getRemarks().equalsIgnoreCase("Success")) {
                            if (position != -1) {
                                friendsItemArrayList.remove(position);
                                notificationAdapter.notifyDataSetChanged();
                            }
                        }
                        if (notificationAdapter.getItemCount() > 0)
                            txtNoNotifications.setVisibility(View.GONE);
                        else
                            txtNoNotifications.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseAddConnection> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed to perform this action", Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    private void clearAllNotifications() {
        NotificationUtils.setFriendRequestCountInSharedPrefs(true);
        NotificationUtils.setNotificationCountInSharedPrefs(true);
        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
}
