package com.vision.imagine.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.db.DatabaseManager;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.networking.response.MyFriendsResponse;
import com.vision.imagine.pojos.CategorySelectionData;
import com.vision.imagine.pojos.CreateIdeaDataSet;
import com.vision.imagine.pojos.CreateIdeaResponse;
import com.vision.imagine.pojos.MyFriend;
import com.vision.imagine.pojos.MyFriendListRequest;
import com.vision.imagine.pojos.PostIdeaModel;
import com.vision.imagine.pojos.RequestFilterUser;
import com.vision.imagine.pojos.ResponseFilterUser;
import com.vision.imagine.rvAdapters.CategoryAdapter;
import com.vision.imagine.rvAdapters.CustomArrayAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.ProgressDialogBuilder;
import com.vision.imagine.utils.SharedPrefUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyIdeaPublishPreviewFragment extends Fragment implements Runnable{

    APIService service = RetrofitClient.getClient().create(APIService.class);
    PostIdeaModel postIdeaModel = new PostIdeaModel();
    View view;
    LinearLayout lm;
    MediaPlayer mediaPlayer = new MediaPlayer();
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList = new ArrayList<>();
    ProgressDialog progressDialog;
    Dialog progressCustomDialog;
    ArrayList<MyFriend> friends = new ArrayList<>();
    String selectedFriendsIds = "";
    Dialog audioPlayerDialog;
    TextView playBtn, seekBarHint;
    SeekBar seekBar;
    Drawable dialogBg;

    public MyIdeaPublishPreviewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            postIdeaModel = (PostIdeaModel) getArguments().getSerializable("ideaObj");
            createIdeaDataSetArrayList = postIdeaModel.getCreateIdeaDataSetArrayList();
        }
        progressDialog = ProgressDialogBuilder.build(getActivity(), getResources().getString(R.string.please_wait),"");
        setupAudioPlayer();
        MainActivity.isDeleteIdea = true;
    }

    private void setupAudioPlayer() {
        audioPlayerDialog = new Dialog(getActivity());
        audioPlayerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        audioPlayerDialog.setCancelable(true);
        audioPlayerDialog.setContentView(R.layout.custom_dialog_audio_player);
        audioPlayerDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        audioPlayerDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_bg));
        playBtn = (TextView) audioPlayerDialog.findViewById(R.id.play_btn);
        seekBar = (SeekBar) audioPlayerDialog.findViewById(R.id.seek_bar);
        seekBarHint = (TextView) audioPlayerDialog.findViewById(R.id.textView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_idea_preview_dyn, container, false);
        setHasOptionsMenu(true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Preview");

        initView();
        getFriendList();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               captureBg();
            }
        }, 400);
    }

    private void initView() {
        TextView title = view.findViewById(R.id.title);
        title.setText(postIdeaModel.getTitle());

        TextView description = view.findViewById(R.id.description);
        TextView category = view.findViewById(R.id.category);
        description.setText(Html.fromHtml("<b>Description : </b>"+postIdeaModel.getCaption()));
        category.setText((Html.fromHtml("<b>Related Industry : </b>"+postIdeaModel.getCategoryName())));

        lm = (LinearLayout) view.findViewById(R.id.ll_dynamic);
        for (int j = 0; j < createIdeaDataSetArrayList.size(); j++) {
            if (createIdeaDataSetArrayList.get(j).getType() == Constants.CAPTURE || createIdeaDataSetArrayList.get(j).getType() == Constants.DRAW) {
                inflateImage(createIdeaDataSetArrayList.get(j));
            } else if (createIdeaDataSetArrayList.get(j).getType() == Constants.RECORD) {
                inflateRecord(createIdeaDataSetArrayList.get(j));
            } else {
                inflateText(createIdeaDataSetArrayList.get(j));
            }
        }
    }

    private void inflateImage(CreateIdeaDataSet createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.image_single_row, lm, false);
        ImageView imageView = layout.findViewById(R.id.image);
        /*LinearLayout.LayoutParams parameter =  (LinearLayout.LayoutParams) imageView.getLayoutParams();
        parameter.setMargins(20, 10, 20, 10); // left, top, right, bottom
        imageView.setLayoutParams(parameter);*/
        if(createIdeaDataSet.getFile()!=null) {
            Glide.with(this)
//                .load(createIdeaDataSet.getFileUri())
                    .load(Uri.fromFile(new File(createIdeaDataSet.getPath())))
//                .apply(new RequestOptions().override(100, 100))
                    .into(imageView);
        }else {
            Log.d("Img","Image path : "+createIdeaDataSet.getPath());
            Glide.with(this)
                .load(createIdeaDataSet.getFileUrl()).into(imageView);
        }
        lm.addView(layout);

    }

    private void inflateText(CreateIdeaDataSet createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.text_single_row, lm, false);
        TextView textView = layout.findViewById(R.id.tv_text);
        textView.setTextColor(getResources().getColor(R.color.black_color));
        textView.setTextSize(16);
        textView.setText(createIdeaDataSet.getFileName());
        LinearLayout.LayoutParams parameter =  (LinearLayout.LayoutParams) textView.getLayoutParams();
        parameter.setMargins(20, 10, 20, 10); // left, top, right, bottom
        textView.setLayoutParams(parameter);
        lm.addView(layout);
    }

    private void inflateRecord(CreateIdeaDataSet createIdeaDataSet) {
        View layout = getLayoutInflater().inflate(R.layout.audio_single_view, lm, false);
        CardView audioView = layout.findViewById(R.id.write_product_card_view);
        ImageView img_play = layout.findViewById(R.id.img_play);
        ImageView playBtn = layout.findViewById(R.id.play_btn);
        Uri localUri = createIdeaDataSet.getFileUri();
        audioView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                   /* player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    player.setDataSource(MyIdeaPublishPreviewFragment.this.getActivity(), localUri);
                    player.prepare();
                    player.start();*/
                    playAudio(localUri);
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        });
        LinearLayout.LayoutParams parameter =  (LinearLayout.LayoutParams) audioView.getLayoutParams();
        parameter.setMargins(0, 40, 0, 5); // left, top, right, bottom
        audioView.setLayoutParams(parameter);
        lm.addView(layout);
    }

    public void onMenuClick(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_preview_options1);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView iprText = (TextView) dialog.findViewById(R.id.ipr_btn);
        TextView postText = (TextView) dialog.findViewById(R.id.post_btn);
        iprText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        postText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPostClick();
                dialog.dismiss();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }
    public void onPostClick(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_preview_options2);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView publicBtn = (TextView) dialog.findViewById(R.id.public_btn);
        TextView privateBtn = (TextView) dialog.findViewById(R.id.private_btn);
        publicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPublicPostClick();
                dialog.dismiss();
            }
        });
        privateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Share idea as a private
                showFriendList();
                dialog.dismiss();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void showFriendList() {
        postIdeaModel.setAccess_to("private");
        CategoryAdapter adapter = null;
        List<CategorySelectionData> friends = new ArrayList<>();
        for(MyFriend myFriend : this.friends){
            CategorySelectionData data = new CategorySelectionData();
            data.setIsSelected(0);
            data.setCatName(myFriend.getUsername());
            data.setCatId(Integer.parseInt(myFriend.getUserId()));
            friends.add(data);
        }

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_friends_list);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView shareBtn = (TextView) dialog.findViewById(R.id.share_btn);
        TextView skipBtn = (TextView) dialog.findViewById(R.id.skip_btn);
        RecyclerView friendsRecyclerView = (RecyclerView) dialog.findViewById(R.id.friends_recycler_view);
        if(friends!=null && friends.size()>0){
            friendsRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            friendsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new CategoryAdapter(getContext(),friends, false);
            friendsRecyclerView.setAdapter(adapter);
        }
        CategoryAdapter finalAdapter = adapter;
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(finalAdapter !=null){
                    List<CategorySelectionData> selectedFriends = finalAdapter.getSelectedCategories();
                    for (CategorySelectionData data : selectedFriends){
                        if(selectedFriendsIds.isEmpty()){
                            selectedFriendsIds = ""+data.getCatId();
                        }else {
                            selectedFriendsIds = selectedFriendsIds+","+data.getCatId();
                        }
                    }
                }
               // if(postIdeaModel.isEdit() == 1) {
                    ideaUpdate();
               // }else {
                //    ideaCreation();
               // }
                dialog.dismiss();
            }
        });
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedFriendsIds = "";
                //if(postIdeaModel.isEdit() == 1) {
                    ideaUpdate();
               // }else {
                   // ideaCreation();
              //  }
                dialog.dismiss();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void getFriendList() {
        MyFriendListRequest request = new MyFriendListRequest();
        request.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        Call<MyFriendsResponse> call = service.getFriendsApi(request);
        call.enqueue(new Callback<MyFriendsResponse>() {
            @Override
            public void onResponse(Call<MyFriendsResponse> call, Response<MyFriendsResponse> response) {
                try {
                    if (response != null && response.body() != null && response.isSuccessful()) {
                        if (response.body().getFriendList()!=null) {
                            friends = response.body().getFriendList();
                        } else {
                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyFriendsResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public void onPublicPostClick(){
        postIdeaModel.setAccess_to("public");
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_preview_confirmation);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView confirmBtn = (TextView) dialog.findViewById(R.id.confirm_btn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // if(postIdeaModel.isEdit() == 1){
                    ideaUpdate();
               // }else {
                    //ideaCreation();
               // }
                dialog.dismiss();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void ideaCreation() {
        progressDialog.show();
        JsonObject jsonObject = new JsonObject();
        //jsonObject.addProperty("created_by", SharedPrefUtils.getStringData(Constants.USER_NAME));
        jsonObject.addProperty("title", postIdeaModel.getTitle());
        jsonObject.addProperty("caption", postIdeaModel.getCaption());
        jsonObject.addProperty("category", postIdeaModel.getCategory());
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));

        Call<CreateIdeaResponse> call = service.createIdea(jsonObject);
        call.enqueue(new Callback<CreateIdeaResponse>() {
            @Override
            public void onResponse(Call<CreateIdeaResponse> call, Response<CreateIdeaResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        CreateIdeaResponse createIdeaResponse = response.body();
                        postIdeaModel.setIdea_id(createIdeaResponse.getId() + "");
                        ideaUpdate();
                    }else {
                        progressDialog.cancel();
                    }
                } catch (Exception e) {
                    progressDialog.cancel();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CreateIdeaResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void ideaUpdate() {
        showCustomProgressDialog();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", postIdeaModel.getIdea_id());
        jsonObject.addProperty("category", postIdeaModel.getCategory());
        jsonObject.addProperty("published", postIdeaModel.getAccess_to());
        jsonObject.addProperty("title", postIdeaModel.getTitle());
        jsonObject.addProperty("caption", postIdeaModel.getCaption());
        jsonObject.addProperty("patent", "No IPR");
        jsonObject.addProperty("sharedTo", selectedFriendsIds);
        //jsonObject.addProperty("created_by", SharedPrefUtils.getStringData(Constants.USER_NAME));
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));

        List<MultipartBody.Part> parts = new ArrayList<>();

        String description = "";
        for (CreateIdeaDataSet createIdeaDataSet : createIdeaDataSetArrayList) {
            if (createIdeaDataSet.getType() != Constants.WRITE && createIdeaDataSet.getFile()!=null)
                parts.add(prepareFilePart("file", createIdeaDataSet));
            String prefix = "";
            switch (createIdeaDataSet.getType()) {
                case Constants.RECORD:
                    prefix = "audio:";
                    break;
                case Constants.WRITE:
                    prefix = "text:";
                    break;
                case Constants.UPLOAD:
                case Constants.DRAW:
                case Constants.CAPTURE:
                    prefix = "photo:";
                    break;
            }
            if (description.isEmpty())
                description = prefix + createIdeaDataSet.getFileName();
            else
                description = description + "~" + prefix + createIdeaDataSet.getFileName();
        }
        if(postIdeaModel.getIdeaImage() != null && postIdeaModel.getIdeaImage().getFile()!=null)
            parts.add(prepareFilePart("ideaProfilePic", postIdeaModel.getIdeaImage()));
        jsonObject.addProperty("description", description);

        Call<CreateIdeaResponse> call = service.updateIdea(parts, jsonObject);
        call.enqueue(new Callback<CreateIdeaResponse>() {
            @Override
            public void onResponse(Call<CreateIdeaResponse> call, Response<CreateIdeaResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();
                        }
                        /*Constants.postIdeaModel = null;
                        if(Constants.currentIdeaId!=0){
                            DatabaseManager.getInstance(getContext()).deleteIdea(Constants.currentIdeaId);
                            Constants.currentIdeaId = 0;
                        }*/
                        fragmentChangeCallBacks.fragmentName("goToMyIdeasFragment", new Bundle());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cancelProgressDialog();
            }

            @Override
            public void onFailure(Call<CreateIdeaResponse> call, Throwable t) {
                cancelProgressDialog();
                call.cancel();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if(postIdeaModel.isEdit() == 1) {
            inflater.inflate(R.menu.edit_idea_done, menu);
        }else {
            inflater.inflate(R.menu.menu_idea_preview_publish, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(postIdeaModel.isEdit() == 1) {
            switch (item.getItemId()) {
                case R.id.idea_edit:
                    //ideaUpdate();
                    onMenuClick();
                    return true;
            }
        }else {
            switch (item.getItemId()) {
                case R.id.item_preview:
               /* Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", postIdeaModel);
                fragmentChangeCallBacks.fragmentName("goToIdeaCategorySelectorFragment", bundle);*/
                    onMenuClick();
                    return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (mediaPlayer != null)
                mediaPlayer.stop();
        } catch (Exception e) {
        }
    }
    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, CreateIdeaDataSet createIdeaDataSet) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new File(createIdeaDataSet.getPath()));
        return MultipartBody.Part.createFormData(partName, createIdeaDataSet.getFileName(), requestFile);
    }

    private void playAudio(Uri localUri){
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(MyIdeaPublishPreviewFragment.this.getActivity(),localUri);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        seekBar.setMax(mediaPlayer.getDuration());

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer!=null){
                    if(mediaPlayer.isPlaying()){
                        mediaPlayer.pause();
                        mediaPlayer.seekTo(0);
                            playBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.play_ic));
                            seekBar.setProgress(0);
                    }else {
                        try {
                            mediaPlayer.start();
                            playBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.stop_ic));
                            new Thread(MyIdeaPublishPreviewFragment.this).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });

        audioPlayerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarHint.setVisibility(View.VISIBLE);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
               /* seekBarHint.setVisibility(View.VISIBLE);
                int x = (int) Math.ceil(progress / 1000f);

                if (x == 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    //clearMediaPlayer();
                    //playBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), android.R.drawable.ic_media_play));
                    seekBar.setProgress(0);
                }else if(mediaPlayer!=null && mediaPlayer.isPlaying()){
                    seekBar.setProgress(progress);
                }*/
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                }
            }
        });
        long millis = mediaPlayer.getDuration();
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        seekBarHint.setText(""+hms);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                playBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.play_ic));
                seekBar.setProgress(0);
            }
        });
        audioPlayerDialog.show();
    }

    public void run() {

        int currentPosition = 0;
        int total = mediaPlayer.getDuration();
        Log.d("Audio","First run");

        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = mediaPlayer.getCurrentPosition();
                Log.d("Audio","In while loop:"+currentPosition);
            } catch (InterruptedException e) {
                Log.d("Audio","Error in run ");
                return;
            } catch (Exception e) {
                Log.d("Audio","Error in run"+e.getMessage());
                return;
            }

            seekBar.setProgress(currentPosition);
            Log.d("Audio","Current position:"+currentPosition);
        }
        if(!mediaPlayer.isPlaying()){
            seekBar.setProgress(0);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearMediaPlayer();
    }

    private void clearMediaPlayer() {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }catch (Exception ignored){}
    }

    private void showCustomProgressDialog() {
        progressCustomDialog = new Dialog(getActivity());
        progressCustomDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressCustomDialog.setCancelable(false);
        progressCustomDialog.setContentView(R.layout.custom_progress_dialog);

        progressCustomDialog.show();
        progressCustomDialog.getWindow().setBackgroundDrawable(dialogBg);
        Window window = progressCustomDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    private void cancelProgressDialog(){
        if(progressCustomDialog!=null){
            progressCustomDialog.dismiss();
        }
    }
    private void captureBg() {
        Bitmap map = CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast = CommonApiUtils.fastblur(map, 30);
        dialogBg = new BitmapDrawable(getResources(), fast);
    }
}
