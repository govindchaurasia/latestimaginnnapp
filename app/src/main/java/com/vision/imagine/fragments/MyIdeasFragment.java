package com.vision.imagine.fragments;

import static com.vision.imagine.fragments.ExploreIdeaFragment.tintMenuIcon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.vision.imagine.ImagineApplication;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.db.DatabaseManager;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pagination.PaginationListener;
import com.vision.imagine.pojos.IdeaListResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.PostIdeaModel;
import com.vision.imagine.rvAdapters.MyIdeaAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.MovableFloatingActionButton;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyIdeasFragment extends Fragment implements OnItemClickedListener {

    RecyclerView rv_ideas;
    MovableFloatingActionButton addIdeaBtn;
    ArrayList<MyIdeasModel> ideasModelArrayList = new ArrayList<>();
    ArrayList<MyIdeasModel> filterIdeasModelArrayList = new ArrayList<>();
    MyIdeaAdapter ideasAdapter;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    CreateIdeasFragment createFragment;
    ImageView searchIv;
    public static boolean isCalledFromSharingActivity = false;
    ContactChatFragment objContactChatFragment;
    TextView noDataTv;
    EditText searchEt;
    Drawable dialogBg;
    boolean resumeCalled = false;
    boolean isSearchIdea = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_ideas, container, false);
        rv_ideas = view.findViewById(R.id.rv_ideas);
        addIdeaBtn = view.findViewById(R.id.create_idea_btn);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING );

        ideasAdapter = new MyIdeaAdapter(ideasModelArrayList, this, getContext());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
        rv_ideas.setLayoutManager(mLayoutManager);
        rv_ideas.setItemAnimator(new DefaultItemAnimator());
        rv_ideas.setAdapter(ideasAdapter);

        rv_ideas.addOnScrollListener(new PaginationListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                if(isSearchIdea) {
                    searchIdea();
                }else {
                    showIdea();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        addIdeaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentChangeCallBacks.fragmentName("goToCreateIdeaFragment", null);
            }
        });
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_add_idea);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(MainActivity.colorSelected));
        addIdeaBtn.setImageDrawable(unwrappedDrawable);

        searchIv = (ImageView) view.findViewById(R.id.icon);
        noDataTv = (TextView) view.findViewById(R.id.no_data_tv);
        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getContext(), R.drawable.ic_search_icon);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(MainActivity.colorSelected));
        searchIv.setImageDrawable(unwrappedDrawable1);

        searchEt = (EditText) view.findViewById(R.id.et);
        Drawable unwrappedDrawable2 = AppCompatResources.getDrawable(getContext(), R.drawable.search_corner_gray);
        Drawable wrappedDrawable2 = DrawableCompat.wrap(unwrappedDrawable2);
        DrawableCompat.setTint(wrappedDrawable2, getResources().getColor(MainActivity.colorSelected));
        searchEt.setBackground(unwrappedDrawable2);
        initViews();
        if (!isCalledFromSharingActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("My Vault");
            addIdeaBtn.setVisibility(View.VISIBLE);
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Select Ideas");
            addIdeaBtn.setVisibility(View.GONE);
        }
        setHasOptionsMenu(true);
        // showIdea();
        //getDraftList();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showIdea();
                }
            }, 200);
            if (MainActivity.myIdeas != null && !MainActivity.isDeleteIdea && MainActivity.myIdeas.size()>0) {
                resumeCalled = true;
                ideasAdapter.swapFilterData(MainActivity.myIdeas);
            } else {
                if(MainActivity.isDeleteIdea)MainActivity.myIdeas.clear();
                MainActivity.isDeleteIdea = false;
            }
        }catch (Exception e){}
    }

    private void initViews() {
        searchIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!searchEt.getText().toString().trim().equals("")) {
                    //setSearchData(searchEt.getText().toString());
                    isSearchIdea = false;
                    searchIdea();
                }
            }
        });
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (searchEt.getText().toString().trim().equals("")) {
                    isSearchIdea = false;
                    ideasAdapter.swapFilterData(ideasModelArrayList);
                    if(ideasAdapter.IdeasModelsList==null || ideasAdapter.IdeasModelsList.isEmpty()){
                        rv_ideas.setVisibility(View.GONE);
                        noDataTv.setVisibility(View.VISIBLE);
                        showIdea();
                    }else {
                        rv_ideas.setVisibility(View.VISIBLE);
                        noDataTv.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void setSearchData(String keyWords) {
        if (ideasModelArrayList != null && ideasModelArrayList.size() > 0) {
            for (MyIdeasModel model : ideasModelArrayList) {
                String str1 = model.getTitle().toLowerCase().trim();
                String str2 = keyWords.toLowerCase().trim();
                if (str1.contains(str2)) {
                    filterIdeasModelArrayList.add(model);
                }
            }
            ideasAdapter.swapFilterData(filterIdeasModelArrayList);
        } else {
            ideasAdapter.swapFilterData(ideasModelArrayList);
        }
    }


    private void getDraftList() {
        List<PostIdeaModel> ideas = DatabaseManager.getInstance(getContext()).getIdea();
        List<MyIdeasModel> draftList = new ArrayList<>();
        if (ideas != null && ideas.size() > 0) {
            for (PostIdeaModel model : ideas) {
                MyIdeasModel ideasModel = new MyIdeasModel();
                ideasModel.setAccessTo(model.getAccess_to());
                ideasModel.setAuthor(SharedPrefUtils.getStringData(Constants.USER_NAME));
                ideasModel.setCaption(model.getCaption());
                ideasModel.setCategory(model.getCategoryName());
                ideasModel.setCategoryId(model.getCategory());
                ideasModel.setDescription(model.getDescription());
                ideasModel.setDraft(true);
                ideasModel.setIdeaId(Integer.valueOf(model.getIdea_id()));
                ideasModel.setTitle(model.getTitle());
                ideasModel.setActive(1);
                ideasModel.setIdeaImage(String.valueOf(model.getIdeaImage()));
                draftList.add(ideasModel);
            }
            ideasAdapter.swapData(draftList);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (createFragment != null) {
            createFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    // Show Idea
    private void showIdea() {
        if(ideasAdapter.getIdeas()==null || ideasAdapter.getIdeas().size()<=0)((MainActivity) requireActivity()).showCustomProgressDialog(dialogBg);
        JsonObject jsonObject = new JsonObject();
        int count_ = 0;
        if (!resumeCalled)
            count_ = ideasAdapter.getItemCount();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("start_count", count_ + "");
        jsonObject.addProperty("offset_count", "20");
        jsonObject.addProperty("filter", "off");
        //jsonObject.addProperty("access_to", "public");
        Call<IdeaListResponse> call = service.showIdeasForUser(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {

                try {
                    if (response.isSuccessful()) {
//                        ideasModelArrayList.addAll(response.body());
                        ideasModelArrayList = (ArrayList<MyIdeasModel>) response.body().getIdeas();
                        if(!resumeCalled) {
                            ideasAdapter.swapData(response.body().getIdeas());
                        }else {
                            resumeCalled = false;
                            ideasAdapter.swapFilterData(response.body().getIdeas());
                        }
                        if (ideasAdapter.IdeasModelsList == null || ideasAdapter.IdeasModelsList.isEmpty()) {
                            rv_ideas.setVisibility(View.GONE);
                            noDataTv.setVisibility(View.VISIBLE);
                        } else {
                            rv_ideas.setVisibility(View.VISIBLE);
                            noDataTv.setVisibility(View.GONE);
                        }
                        if(((MainActivity) getActivity()) != null) {
                            MainActivity.myIdeas.clear();
                            MainActivity.myIdeas.addAll(ideasAdapter.getIdeas());
                        }
                    }
                    ((MainActivity) requireActivity()).cancelProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }
        });
    }

    private void searchIdea() {
        JsonObject jsonObject = new JsonObject();
        int count_ = 0;
        if(isSearchIdea) count_ = ideasAdapter.getItemCount();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("start_count", count_ + "");
        jsonObject.addProperty("offset_count", "20");
        jsonObject.addProperty("searchValue", searchEt.getText().toString());
        jsonObject.addProperty("flag", "vault");

        Call<IdeaListResponse> call = service.searchIdeas(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        isSearchIdea = true;
                        if (response.body() != null) {
                            ideasAdapter.swapFilterData(response.body().getIdeas());
                        }else {
                            ideasAdapter.swapFilterData(new ArrayList<MyIdeasModel>());
                        }
                        if(ideasAdapter.IdeasModelsList==null || ideasAdapter.IdeasModelsList.isEmpty()){
                            rv_ideas.setVisibility(View.GONE);
                            noDataTv.setVisibility(View.VISIBLE);
                        }else {
                            rv_ideas.setVisibility(View.VISIBLE);
                            noDataTv.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
        if (ideasAdapter != null && ideasAdapter.IdeasModelsList != null && ideasAdapter.IdeasModelsList.size() > 0) {
            ideasAdapter.IdeasModelsList.clear();
            showIdea();
        }
    }

    @Override
    public void clickedItem(Bundle bundle) {
        try {
            int position = bundle.getInt("position");
            Log.d("Click_item","Item click position = "+position);
            if (isCalledFromSharingActivity) {
                MyIdeasModel tmpModel = ideasAdapter.getIdeas().get(position);
                if (tmpModel.isSelected()) {
                    ideasModelArrayList.get(position).setSelected(false);
                    ideasAdapter.getIdeas().get(position).setSelected(false);
                } else {
                    ideasModelArrayList.get(position).setSelected(true);
                    ideasAdapter.getIdeas().get(position).setSelected(true);
                }
                if(getSelectedCount()>3) {
                    if (tmpModel.isSelected()) {
                        ideasModelArrayList.get(position).setSelected(false);
                        ideasAdapter.getIdeas().get(position).setSelected(false);
                    }
                    Toast.makeText(getContext(),"You can select maximum 3 ideas", Toast.LENGTH_LONG).show();
                }
                ideasAdapter.notifyItemChanged(position);
            } else {
                MyIdeasModel ideasModel = (MyIdeasModel) bundle.getSerializable("ideaObj");
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("idea", ideasModel);
                fragmentChangeCallBacks.fragmentName("goToOthersIdeaPreviewFragment", bundle);
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if(isCalledFromSharingActivity){
            inflater.inflate(R.menu.menu_multiselect_idea, menu);
            MenuItem menuItem1 = menu.findItem(R.id.item_cancel);
            MenuItem menuItem2 = menu.findItem(R.id.item_done);
            if (menuItem1 != null) {
                tintMenuIcon(getContext(), menuItem1, MainActivity.colorSelected);
            }
            if (menuItem2 != null) {
                tintMenuIcon(getContext(), menuItem2, MainActivity.colorSelected);
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_cancel: {
                ExploreIdeaFragment.isCalledFromSharingActivity = false;
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                objContactChatFragment.setIdea(null);
                return true;
            }
            case R.id.item_done: {
                if (objContactChatFragment != null) {
                    if (ideasModelArrayList != null) {
                        ArrayList<MyIdeasModel> ideasModels = new ArrayList<>();
                        int i = 0;
                        for (MyIdeasModel model : ideasModelArrayList) {
                            if (model.isSelected()) {
                                ideasModels.add(model);
                                ideasModelArrayList.get(i).setSelected(false);
                            }
                            i++;
                        }
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("models", ideasModels);
                        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                        objContactChatFragment.setIdea(bundle);
                    }
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public ContactChatFragment getObjContactChatFragment() {
        return objContactChatFragment;
    }

    public void setObjContactChatFragment(ContactChatFragment objContactChatFragment) {
        this.objContactChatFragment = objContactChatFragment;
    }

    private void captureBg() {
        try {
            Bitmap map = CommonApiUtils.takeScreenShot(getActivity());
            Bitmap fast = CommonApiUtils.fastblur(map, 30);
            dialogBg = new BitmapDrawable(getResources(), fast);
        }catch (Exception ignored){}

    }

    private int getSelectedCount(){
        int count = 0;
        for(MyIdeasModel model : ideasModelArrayList){
            if(model.isSelected()){
                count++;
            }
        }
        return count;
    }

}
