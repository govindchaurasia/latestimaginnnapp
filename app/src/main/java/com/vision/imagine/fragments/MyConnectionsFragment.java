package com.vision.imagine.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vision.imagine.ImagineApplication;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pagination.PaginationListener;
import com.vision.imagine.pojos.FilterUserItem;
import com.vision.imagine.pojos.FriendsItem;
import com.vision.imagine.pojos.RequestDeleteFriend;
import com.vision.imagine.pojos.RequestFilterUser;
import com.vision.imagine.pojos.RequestShowMyConnections;
import com.vision.imagine.pojos.ResponseDeleteFriend;
import com.vision.imagine.pojos.ResponseFilterUser;
import com.vision.imagine.pojos.ResponseShowMyConnections;
import com.vision.imagine.rvAdapters.CustomArrayAdapter;
import com.vision.imagine.rvAdapters.MyConnectionsAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.ProgressDialogBuilder;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyConnectionsFragment extends Fragment implements OnItemClickedListener {

    RecyclerView rv_contacts;
    ImageView addContactBtn;
    TextView txtNoContacts;
    ImageView btnSearch;
    AutoCompleteTextView etSearch;
    ArrayList<FriendsItem> friendsItemArrayList = new ArrayList<>();
    MyConnectionsAdapter myConnectionsAdapter;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    CreateIdeasFragment createFragment;
    private boolean isSearchPerformed = false;

    CustomArrayAdapter autoCompleteTextViewAdapter;
    ArrayList<FilterUserItem> autoCompleteItemsArrayList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private Menu menu;
    private SelectionTracker<Long> selectionTracker;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (savedInstanceState != null) {
                selectionTracker.onRestoreInstanceState(savedInstanceState);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_connections, container, false);
        rv_contacts = view.findViewById(R.id.rv_contacts);
        addContactBtn = view.findViewById(R.id.add_contact_button);
        txtNoContacts = view.findViewById(R.id.txtNoContacts);
        btnSearch = view.findViewById(R.id.btnSearch);
        etSearch = view.findViewById(R.id.etSearch);
        etSearch.setThreshold(Constants.SEARCH_THRESHOLD);
        etSearch.requestFocus();

        CommonUtils.tintMyDrawable(getActivity(), btnSearch.getDrawable());

        setMyConnectionsAdapter();

        if (autoCompleteTextViewAdapter != null)
            etSearch.setAdapter(autoCompleteTextViewAdapter);

//        if (myConnectionsAdapter.getItemCount() > 0)
//            txtNoContacts.setVisibility(View.GONE);
//        else
//            txtNoContacts.setVisibility(View.VISIBLE);

        addContactBtn.setVisibility(View.GONE);
        addContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                fragmentChangeCallBacks.fragmentName("goToCreateIdeaFragment", null);
            }
        });

//        btnSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity(),"Search Clicked",Toast.LENGTH_LONG).show();
//            }
//        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == Constants.SEARCH_THRESHOLD && !isSearchPerformed) {
                    isSearchPerformed = true;
                    callFilterUserApi(s.toString());
                } else if (s.length() < Constants.SEARCH_THRESHOLD) {
                    isSearchPerformed = false;
                }
            }
        });

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("People");
        setHasOptionsMenu(true);
        showMyConnections();

        Drawable unwrappedDrawable2 = AppCompatResources.getDrawable(getContext(), R.drawable.search_corner_gray);
        Drawable wrappedDrawable2 = DrawableCompat.wrap(unwrappedDrawable2);
        DrawableCompat.setTint(wrappedDrawable2, getResources().getColor(MainActivity.colorSelected));
        etSearch.setBackground(unwrappedDrawable2);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (createFragment != null) {
            createFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showMyConnections() {
        RequestShowMyConnections objRequestShowMyConnections = new RequestShowMyConnections();
        objRequestShowMyConnections.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));

        Call<ResponseShowMyConnections> call = service.showMyConnections(objRequestShowMyConnections);
        call.enqueue(new Callback<ResponseShowMyConnections>() {
            @Override
            public void onResponse(Call<ResponseShowMyConnections> call, Response<ResponseShowMyConnections> response) {
                try {
                    if (response.isSuccessful()) {
                        friendsItemArrayList.clear();
                        if (rv_contacts != null && rv_contacts.getAdapter() != null) {
                            selectionTracker.clearSelection();
                            rv_contacts.getAdapter().notifyDataSetChanged();
                        }
                        myConnectionsAdapter.swapData(response.body().getFriends(), response.body().getUserProfileURL());

                        if (myConnectionsAdapter.getItemCount() > 0)
                            txtNoContacts.setVisibility(View.GONE);
                        else
                            txtNoContacts.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseShowMyConnections> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    @Override
    public void clickedItem(Bundle bundle) {
//        MyIdeasModel ideasModel = (MyIdeasModel) data.getSerializable("idea_obj");
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("ideaObj", ideasModel);

//       IF APPROVED FRIEND THEN USER CAN CHAT WITH HIS CONTACT OTHERWISE USER'S PROFILE PAGE WILL BE DISPLAYED
        if (!bundle.getString("userId", "").isEmpty()) {
            CommonUtils.hideKeyboard(getActivity(), etSearch.getWindowToken());
            bundle.putString("authorId", bundle.getString("userId", ""));
            fragmentChangeCallBacks.fragmentName("goToAuthorProfileFragment", bundle);
        } else if (bundle.getSerializable("friendsItem") != null) {
            bundle.putString("userId", bundle.getString("userId", ""));
            fragmentChangeCallBacks.fragmentName("goToContactChatFragment", bundle);
        }
    }

    private void callFilterUserApi(String textToSearch) {
        RequestFilterUser objRequestFilterUser = new RequestFilterUser();
        objRequestFilterUser.setUsername(textToSearch);
        objRequestFilterUser.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        Call<ResponseFilterUser> call = service.filterUserApi(objRequestFilterUser);
        call.enqueue(new Callback<ResponseFilterUser>() {
            @Override
            public void onResponse(Call<ResponseFilterUser> call, Response<ResponseFilterUser> response) {
                try {
                    if (response != null && response.body() != null && response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("Success")) {

                            autoCompleteItemsArrayList.clear();
                            for (int i = 0; i < response.body().getFilterUser().size(); i++) {
                                autoCompleteItemsArrayList.add(response.body().getFilterUser().get(i));
                            }

                            autoCompleteTextViewAdapter = new CustomArrayAdapter(getActivity(), R.layout.search_auto_complete_single_row, autoCompleteItemsArrayList, MyConnectionsFragment.this, response.body().getUserProfilePath());
                            etSearch.setAdapter(autoCompleteTextViewAdapter);
                        } else {
                            Toast.makeText(getContext(), response.body().getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseFilterUser> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_my_connection, menu);
        this.menu = menu;

//        if (!authorID.equals(SharedPrefUtils.getStringData(Constants.USER_ID)))
//            menu.findItem(R.id.item_delete_contact).setVisible(true);
//        else
//            menu.findItem(R.id.item_delete_contact).setVisible(false);

        CommonUtils.tintMyDrawable(getActivity(), menu.findItem(R.id.item_delete_contact).getIcon());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_delete_contact:
                if (selectionTracker != null && selectionTracker.getSelection().size() > 0)
                    dialogDeleteContacts();
                else
                    Toast.makeText(getActivity(), "Please select any contact", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dialogDeleteContacts() {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.title_delete_contact_list));
        builder.setMessage(getResources().getString(R.string.msg_delete_contact_list));
        builder.setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callDeleteContactApi();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();*/

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_delete_people_confirmation);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView deleteMsgTv = (TextView) dialog.findViewById(R.id.delete_msg_tv);
        TextView saveBtn = (TextView) dialog.findViewById(R.id.save_btn);
        TextView discardBtn = (TextView) dialog.findViewById(R.id.discard_btn);
        if(selectionTracker.getSelection().size()==1){
            deleteMsgTv.setText(getResources().getString(R.string.msg_delete_single_contact_list));
        }else { deleteMsgTv.setText(getResources().getString(R.string.msg_delete_contact_list));}
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDeleteContactApi();
                dialog.cancel();
            }
        });
        discardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(getActivity());
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void callDeleteContactApi() {
        progressDialog = ProgressDialogBuilder.build(getActivity(), "", getResources().getString(R.string.sending_connection));
        progressDialog.show();

        String friendsToDelete = "";
        if (myConnectionsAdapter != null) {
            Selection<Long> selectionList = myConnectionsAdapter.getAllSelectionTrackerItems();
            Iterator<Long> itemIterable = selectionList.iterator();
            while (itemIterable.hasNext()) {
                FriendsItem objUserContactsItem = friendsItemArrayList.get(itemIterable.next().intValue());
                friendsToDelete += objUserContactsItem.getUserId() + ",";
            }
            if (friendsToDelete.length() > 0) {
                friendsToDelete = friendsToDelete.substring(0, friendsToDelete.length() - 1);
            }
        }

        RequestDeleteFriend objRequestDeleteFriend = new RequestDeleteFriend();
        objRequestDeleteFriend.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        objRequestDeleteFriend.setFriendId(friendsToDelete);

        Call<ResponseDeleteFriend> call = service.deleteFriendApi(objRequestDeleteFriend);
        call.enqueue(new Callback<ResponseDeleteFriend>() {
            @Override
            public void onResponse(Call<ResponseDeleteFriend> call, Response<ResponseDeleteFriend> response) {
                progressDialog.cancel();
                try {
                    if (response != null && response.body() != null) {
                        if (response.isSuccessful()) {
                            Toast.makeText(getActivity(), response.body().getRemark(), Toast.LENGTH_LONG).show();

                            if (response.body().getRemark() != null && response.body().getRemark().equalsIgnoreCase("Success")) {
                                String[] userIds = objRequestDeleteFriend.getFriendId().split(",");
                                for (int i = 0; i < userIds.length; i++) {
                                    for (int j = 0; j < friendsItemArrayList.size(); j++) {
                                        if (userIds[i].equals(friendsItemArrayList.get(j).getUserId())) {
                                            friendsItemArrayList.remove(j);
                                            break;
                                        }
                                    }
                                }
                                selectionTracker.clearSelection();
                                myConnectionsAdapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseDeleteFriend> call, Throwable t) {
                progressDialog.cancel();
                call.cancel();
            }
        });
    }

    private void setMyConnectionsAdapter() {
        myConnectionsAdapter = new MyConnectionsAdapter(friendsItemArrayList, this, getContext());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
        rv_contacts.setLayoutManager(mLayoutManager);
        rv_contacts.setItemAnimator(new DefaultItemAnimator());
        rv_contacts.setAdapter(myConnectionsAdapter);

        MyConnectionsAdapter.KeyProvider objKeyProvider = new MyConnectionsAdapter.KeyProvider(rv_contacts.getAdapter());
        selectionTracker = new SelectionTracker.Builder<>("my_selection",
                rv_contacts,
                objKeyProvider,
                new MyConnectionsAdapter.DetailsLookup(rv_contacts),
                StorageStrategy.createLongStorage())
                .withSelectionPredicate(new MyConnectionsAdapter.Predicate())
                .build();

        selectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
            @Override
            public void onSelectionChanged() {
                super.onSelectionChanged();
                try {
//                    if (selectionTracker.hasSelection()) {
//                        toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list) + "  " + selectionTracker.getSelection().size() + " Connection(s)");
//                    } else if (!selectionTracker.hasSelection()) {
//                        toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list));
//                    } else {
//                        toolbar.setTitle(getResources().getString(R.string.title_activity_contact_list) + "  " + selectionTracker.getSelection().size() + " Connection(s)");
//                    }

                    if (selectionTracker.getSelection().size() > 0) {
                        menu.findItem(R.id.item_delete_contact).setVisible(true);
                    } else {
                        menu.findItem(R.id.item_delete_contact).setVisible(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        myConnectionsAdapter.setSelectionTracker(selectionTracker);

        rv_contacts.addOnScrollListener(new PaginationListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                showMyConnections();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null)
            selectionTracker.onSaveInstanceState(outState);
    }
}
