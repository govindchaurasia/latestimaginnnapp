package com.vision.imagine.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vision.imagine.BuildConfig;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.RequestAddConnection;
import com.vision.imagine.pojos.ResponseAddConnection;
import com.vision.imagine.pojos.AuthorProfileResponse;
import com.vision.imagine.pojos.UserProfileDataSet;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.FileCompressor;
import com.vision.imagine.utils.FileUtils;
import com.vision.imagine.utils.ProgressDialogBuilder;
import com.vision.imagine.utils.SharedPrefUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getName();
    private static final int REQUEST_TAKE_PHOTO = 111;
    private static final int REQUEST_GALLERY_PHOTO = 222;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    View view;
    TextView tv_email, tv_number, tv_idea_count, tv_name;
    CircularImageView iv_display_picture;
    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
    UserProfileDataSet userProfileDataSet;
    FileCompressor mCompressor;
    String localUrl = "";
    int color = generator.getRandomColor();
    private String mPhotoPath;
    private String userId = SharedPrefUtils.getStringData(Constants.USER_ID);
    private ProgressDialog progressDialog;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Profile");
        setHasOptionsMenu(true);

        mCompressor = new FileCompressor(ProfileFragment.this.getActivity());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        initView();
        getUserProfile();
        return view;
    }

    private void initView() {
//        tv_email = view.findViewById(R.id.tv_email);
//        tv_number = view.findViewById(R.id.tv_number);
        tv_idea_count = view.findViewById(R.id.tv_idea_count);
        tv_name = view.findViewById(R.id.tv_name);
        iv_display_picture = view.findViewById(R.id.iv_display_picture);
//        showImage();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_profile, menu);

        if (!userId.equals(SharedPrefUtils.getStringData(Constants.USER_ID)))
            menu.findItem(R.id.item_add_contact).setVisible(true);
        else
            menu.findItem(R.id.item_add_contact).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_upload_pic:
                selectImage();
                return true;
            case R.id.item_add_contact:
                dialogAddContact();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dialogAddContact() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.title_add_to_contact_list));
        builder.setMessage(getResources().getString(R.string.msg_add_to_contact_list));
        builder.setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callAddContactApi();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    private void callAddContactApi() {
        progressDialog = ProgressDialogBuilder.build(getActivity(), "",getResources().getString(R.string.sending_connection));
        progressDialog.show();

        RequestAddConnection objRequestAddConnection = new RequestAddConnection();
        objRequestAddConnection.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        objRequestAddConnection.setFriendId(userId);

        Call<ResponseAddConnection> call = service.addConnection(objRequestAddConnection);
        call.enqueue(new Callback<ResponseAddConnection>() {
            @Override
            public void onResponse(Call<ResponseAddConnection> call, Response<ResponseAddConnection> response) {
                progressDialog.cancel();
                try {
                    if (response != null && response.body() != null) {
                        if (response.isSuccessful()) {
                            Toast.makeText(getContext(), response.body().getRemarks(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseAddConnection> call, Throwable t) {
                progressDialog.cancel();
                call.cancel();
            }
        });
    }

    private void getUserProfile() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", userId);

        Call<AuthorProfileResponse> call = service.userProfile(jsonObject);

        call.enqueue(new Callback<AuthorProfileResponse>() {
            @Override
            public void onResponse(Call<AuthorProfileResponse> call, Response<AuthorProfileResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        userProfileDataSet = response.body().getAuthorData();
//                        tv_email.setText(userProfileDataSet.getEmail());
//                        tv_number.setText(userProfileDataSet.getMobileNumber());
                        tv_idea_count.setText(userProfileDataSet.getNoOfideas() + "");
                        tv_name.setText(userProfileDataSet.getUserName());

                        localUrl = "http://imaginnn.com:443/images/profilepic/" + userProfileDataSet.getUserName() + ".jpg";

                        showImage();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AuthorProfileResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void showImage() {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .withBorder(4) /* thickness in px */
                .endConfig()
                .buildRoundRect(String.valueOf(userProfileDataSet.getUserName().charAt(0)).toUpperCase(), color, 10);

        Glide.with(ProfileFragment.this)
                .load(localUrl)
                .apply(RequestOptions.placeholderOf(drawable).error(drawable))
//                              .apply(new RequestOptions().override(100, 100))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(iv_display_picture);
    }

    private void updateProfile(String filePath, String filename) {

        MultipartBody.Part part = prepareFilePart("file", filePath, userProfileDataSet.getUserName() + ".jpg");
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("nameOfuser", userProfileDataSet.getNameOfuser());
        jsonObject.addProperty("address1", userProfileDataSet.getAddress1());
        jsonObject.addProperty("address2", userProfileDataSet.getAddress2());
        jsonObject.addProperty("email", userProfileDataSet.getEmail());
        jsonObject.addProperty("mobileNumber", userProfileDataSet.getMobileNumber());
        jsonObject.addProperty("username", userProfileDataSet.getUserName());
        jsonObject.addProperty("userId", userProfileDataSet.getUserId());

        Call<UserProfileDataSet> call;
        if (part == null)
            call = service.updateProfile(jsonObject);
        else
            call = service.updateProfile(part, jsonObject);

        call.enqueue(new Callback<UserProfileDataSet>() {
            @Override
            public void onResponse(Call<UserProfileDataSet> call, Response<UserProfileDataSet> response) {

                try {
                    if (response.isSuccessful()) {
                        getUserProfile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserProfileDataSet> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String filePath, String fileName) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new File(filePath));
        return MultipartBody.Part.createFormData(partName, fileName, requestFile);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFragment.this.getActivity());
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                requestStoragePermission(true);
            } else if (items[item].equals("Choose from Library")) {
                requestStoragePermission(false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(ProfileFragment.this.getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent();
                            } else {
                                dispatchGalleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
//                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> Toast.makeText(ProfileFragment.this.getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    /**
     * Capture image from camera
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(ProfileFragment.this.getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile(ProfileFragment.this.getActivity());
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(ProfileFragment.this.getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);

                mPhotoPath = photoFile.getPath();
                Bundle bundle = new Bundle();
                bundle.putString("uri", String.valueOf(photoURI));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO, bundle);

            }
        }
    }

    /**
     * Select image fro gallery
     */
    private void dispatchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_GALLERY_PHOTO:
                try {
                    Uri selectedImage = data.getData();
                    File filePhoto = mCompressor.compressToFile(new File(FileUtils.getRealPathFromUri(selectedImage, ProfileFragment.this.getActivity())));

                    String path = filePhoto.getPath();
                    Log.d(TAG, "File Path: " + path);
                    String fileName = path.substring(path.lastIndexOf("/") + 1);

                    updateProfile(path, fileName);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_TAKE_PHOTO:
                try {
                    File TempPhotoFile = new File(mPhotoPath);
                    TempPhotoFile = mCompressor.compressToFile(TempPhotoFile);
                    String path = TempPhotoFile.getPath();
                    String fileName = path.substring(path.lastIndexOf("/") + 1);

                    updateProfile(path, fileName);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
