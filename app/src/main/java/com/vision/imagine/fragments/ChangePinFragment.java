package com.vision.imagine.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.JsonObject;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.R;
import com.vision.imagine.activities.ForgotPinActivity;
import com.vision.imagine.activities.LoginActivity;
import com.vision.imagine.activities.OTPEntryActivity;
import com.vision.imagine.activities.PinEntryActivity;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.RegistrationDataSet;
import com.vision.imagine.pojos.SliderItem;
import com.vision.imagine.rvAdapters.SliderAdapter;
import com.vision.imagine.utils.ChangeBtnColor;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChangePinFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePinFragment extends Fragment {

    APIService service = RetrofitClient.getClient().create(APIService.class);
    View view;
    PinEntryEditText txt_pin_entry_old, txt_pin_entry_new;
    TextView btn_submit;
    private FragmentChangeCallBacks fragmentChangeCallBacks;

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    int themeSelected;
    boolean showOldPin = false,showNewPin = false;

    public ChangePinFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChangePinFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangePinFragment newInstance(String param1, String param2) {
        ChangePinFragment fragment = new ChangePinFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_change_pin, container, false);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Change Pin");
        initView();
        slider();
        return view;
    }

    private void initView() {
        txt_pin_entry_old = view.findViewById(R.id.txt_pin_entry_old);
        txt_pin_entry_new = view.findViewById(R.id.txt_pin_entry_new);
        btn_submit = view.findViewById(R.id.btn_submit);
        ImageView viewOldPin = (ImageView) view.findViewById(R.id.view_old_pin_btn);
        ImageView viewNewPin = (ImageView) view.findViewById(R.id.view_new_pin_btn);
        TextView forgotPinTv = (TextView) view.findViewById(R.id.tv_forgot_pin);

        mSharedPreferences = getActivity().getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);

        ChangeBtnColor changeBtnColor = new ChangeBtnColor();
        changeBtnColor.changecolor(getActivity(),btn_submit,colorSelected);

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getActivity(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelected));
        btn_submit.setBackground(unwrappedDrawable);

        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getActivity(), R.drawable.ic_enter_otp);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(colorSelectedLight));
        txt_pin_entry_new.setPinBackground(unwrappedDrawable1);

        Drawable unwrappedDrawable2 = AppCompatResources.getDrawable(getActivity(), R.drawable.ic_enter_otp);
        Drawable wrappedDrawable2 = DrawableCompat.wrap(unwrappedDrawable2);
        DrawableCompat.setTint(wrappedDrawable2, getResources().getColor(colorSelectedLight));
        txt_pin_entry_old.setPinBackground(unwrappedDrawable2);

        forgotPinTv.setTextColor(getResources().getColor(colorSelected));

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String codeOld = txt_pin_entry_old.getText().toString();
                String codeNew = txt_pin_entry_new.getText().toString();
                if (!codeNew.isEmpty() || codeNew.length() == 4) {
                    if (SharedPrefUtils.getStringData(Constants.USER_PIN).equalsIgnoreCase(codeOld)) {
                        updatePin();
                    }
                }
            }
        });
        forgotPinTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragmentChangeCallBacks.fragmentName("goToResetPinFragment", new Bundle());
                Intent intent = new Intent(getActivity(), OTPEntryActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });
        viewOldPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOldPin = !showOldPin;
                if (showOldPin) {
                    txt_pin_entry_old.setMask("");
                }else {
                    txt_pin_entry_old.setMask("*");
                }
                txt_pin_entry_old.setText(txt_pin_entry_old.getText());
            }
        });
        viewNewPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNewPin = !showNewPin;
                if (showNewPin) {
                    txt_pin_entry_new.setMask("");
                }else {
                    txt_pin_entry_new.setMask("*");
                }
                txt_pin_entry_new.setText(txt_pin_entry_new.getText());
            }
        });
    }

    // Update Pin
    private void updatePin() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("pin", txt_pin_entry_new.getText().toString());
        jsonObject.addProperty("action", "update");

        Call<RegistrationDataSet> call = service.updatePin(jsonObject);
        call.enqueue(new Callback<RegistrationDataSet>() {
            @Override
            public void onResponse(Call<RegistrationDataSet> call, Response<RegistrationDataSet> response) {

                try {
                    if (response.isSuccessful()) {
                        SharedPrefUtils.saveData(Constants.USER_PIN, txt_pin_entry_new.getText().toString());
                        Toast.makeText(ChangePinFragment.this.getActivity(), "Pin Updated Successfully", Toast.LENGTH_LONG).show();
                        ChangePinFragment.this.getActivity().onBackPressed();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDataSet> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_blank, menu);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    private void slider() {

        SliderView sliderView = view.findViewById(R.id.imageSlider);
        SliderAdapter sliderAdapter = new SliderAdapter(getActivity());
        sliderView.setSliderAdapter(sliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();

        for (int i = 0; i < 2; i++) {
            SliderItem sliderItem = new SliderItem();
            if (i==0){
                sliderItem.setDescription("Life is what happens when you're busy making other plans.\" - John Lennon");
            }else {
                sliderItem.setDescription("The greatest glory in living lies not in never falling, but in rising every time we fall.\" -Nelson Mandela");
            }
//            sliderItem.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
            sliderAdapter.addItem(sliderItem);

        }

    }


}
