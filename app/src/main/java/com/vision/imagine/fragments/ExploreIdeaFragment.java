package com.vision.imagine.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.vision.imagine.ImagineApplication;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnAppliedFilterListener;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.firebasenotifications.firebaseutils.NotificationUtils;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pagination.PaginationListener;
import com.vision.imagine.pojos.CategoryDataSet;
import com.vision.imagine.pojos.CategorySelectionData;
import com.vision.imagine.pojos.IdeaListResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.SuccessFilterResponse;
import com.vision.imagine.rvAdapters.CategoryAdapter;
import com.vision.imagine.rvAdapters.IdeasAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExploreIdeaFragment extends Fragment implements OnItemClickedListener, OnAppliedFilterListener {

    RecyclerView rv_ideas;
    ArrayList<MyIdeasModel> ideasModelArrayList = new ArrayList<>();
    ArrayList<MyIdeasModel> filterIdeasModelArrayList = new ArrayList<>();
    IdeasAdapter ideasAdapter;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    String category_id_filter = "", category_name_filter = "";
    String currentFilter = "default";
    boolean isFilterApplied = true, dateSortAsc = false, nameSortAsc = false, idDefaultFilterApplied = false;
    private FragmentChangeCallBacks fragmentChangeCallBacks;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    boolean resumeCalled = false;
    //ArrayList<MultiSelectModel> categories = new ArrayList<>();
    List<CategorySelectionData> categories = new ArrayList<>();
    Dialog dialog;
    LinearLayout aToZPanel, datePanel, fieldPanel;
    TextView viewBtn, likeBtn, defaultFilter;
    Switch defaultSwitch;
    Drawable dialogBg;
    ImageView searchIv, nameSortIv, dateSortIv, fieldsIv;
    CategoryAdapter catSelectionAdapter;
    Dialog catFilterDialog;
    TextView cancelBtn, doneBtn, noDataTv, selectAllBtn, unSelectAllBtn;
    RecyclerView catRecyclerView;
    EditText searchEt;
    boolean isSearchIdea = false;
    boolean manualSwitchFilter = false;
    private List<Integer> selectedIdeas = new ArrayList<>();

    public static TextView textCartItemCount, textNotificationItemCount;
    private ImageView imgNotification,imgNotification1;
    public static boolean isCalledFromSharingActivity = false;
    ContactChatFragment objContactChatFragment;
    boolean resume;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        rv_ideas = view.findViewById(R.id.rv_ideas);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        ideasAdapter = new IdeasAdapter(ideasModelArrayList, this, getContext());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ImagineApplication.getContext());
        rv_ideas.setLayoutManager(mLayoutManager);
        rv_ideas.setItemAnimator(new DefaultItemAnimator());
        rv_ideas.setAdapter(ideasAdapter);

        rv_ideas.addOnScrollListener(new PaginationListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                isFilterApplied = false;
                if (isSearchIdea) {
                    searchIdea();
                } else {
                    showIdea();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        searchIv = (ImageView) view.findViewById(R.id.icon);
        noDataTv = (TextView) view.findViewById(R.id.no_data_tv);

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getContext(), R.drawable.ic_search_icon);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(MainActivity.colorSelected));
        searchIv.setImageDrawable(unwrappedDrawable);

        searchEt = (EditText) view.findViewById(R.id.et);
        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getContext(), R.drawable.search_corner_gray);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(MainActivity.colorSelected));
        searchEt.setBackground(unwrappedDrawable1);
        resume = true;
        initViews();
        //showIdea();
        setHasOptionsMenu(true);
        if (!isCalledFromSharingActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Explore Ideas");
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Select Ideas");
        }
        getDefaultFilter();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupBadge();
        if (SharedPrefUtils.getStringData(Constants.DEFAULT_FILTER_ON).equals("0")) {
            manualSwitchFilter = false;
            currentFilter = "off";
            defaultSwitch.setChecked(false);
        }
        try {
            final Handler handler = new Handler(Looper.getMainLooper());

            if (MainActivity.exploreIdeas != null && !MainActivity.isDeleteIdea && MainActivity.exploreIdeas.size() > 0) {
                resumeCalled = true;
                ideasAdapter.swapFilterData(MainActivity.exploreIdeas);
            } else {
                if (MainActivity.isDeleteIdea) MainActivity.exploreIdeas.clear();
                MainActivity.isDeleteIdea = false;
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showIdea();
                }
            }, 200);
        } catch (Exception ignored) {
        }
    }

    private void initViews() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_filter_explore_idea);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        aToZPanel = (LinearLayout) dialog.findViewById(R.id.aToZPanel);
        datePanel = (LinearLayout) dialog.findViewById(R.id.date_panel);
        viewBtn = (TextView) dialog.findViewById(R.id.view_btn);
        likeBtn = (TextView) dialog.findViewById(R.id.like_btn);
        defaultFilter = (TextView) dialog.findViewById(R.id.default_filter_tv);
        fieldPanel = (LinearLayout) dialog.findViewById(R.id.field_panel);
        defaultSwitch = (Switch) dialog.findViewById(R.id.default_filter_switch);
        nameSortIv = (ImageView) dialog.findViewById(R.id.name_up_down_iv);
        dateSortIv = (ImageView) dialog.findViewById(R.id.date_up_down_iv);
        fieldsIv = (ImageView) dialog.findViewById(R.id.field_iv);

        catFilterDialog = new Dialog(getActivity());
        catFilterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        catFilterDialog.setCancelable(true);
        catFilterDialog.setContentView(R.layout.custom_dialog_select_category);
        catFilterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        cancelBtn = catFilterDialog.findViewById(R.id.cancel_btn);
        doneBtn = catFilterDialog.findViewById(R.id.done_btn);
        selectAllBtn = catFilterDialog.findViewById(R.id.select_all_btn);
        unSelectAllBtn = catFilterDialog.findViewById(R.id.un_select_all_btn);
        catRecyclerView = catFilterDialog.findViewById(R.id.category_recycler_view);

        nameSortIv.setImageDrawable(getDrawableTint(R.drawable.ic_sort));
        dateSortIv.setImageDrawable(getDrawableTint(R.drawable.ic_sort));
        fieldsIv.setImageDrawable(getDrawableTint(R.drawable.ic_fields));

        searchIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!searchEt.getText().toString().trim().equals("")) {
                    //setSearchData(searchEt.getText().toString());
                    isSearchIdea = false;
                    searchIdea();
                }
            }
        });
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (searchEt.getText().toString().trim().equals("") && !resume) {
                    isSearchIdea = false;
                    ideasAdapter.swapFilterData(ideasModelArrayList);
                    if (ideasAdapter.IdeasModelsList == null || ideasAdapter.IdeasModelsList.isEmpty()) {
                        rv_ideas.setVisibility(View.GONE);
                        noDataTv.setVisibility(View.VISIBLE);
                        showIdea();
                    } else {
                        rv_ideas.setVisibility(View.VISIBLE);
                        noDataTv.setVisibility(View.GONE);
                    }
                    resume = false;
                }
            }
        });
    }

    private void setSearchData(String keyWords) {
        if (ideasModelArrayList != null && ideasModelArrayList.size() > 0) {
            for (MyIdeasModel model : ideasModelArrayList) {
                String str1 = model.getTitle().toLowerCase().trim();
                String str2 = keyWords.toLowerCase().trim();
                if (str1.contains(str2)) {
                    filterIdeasModelArrayList.add(model);
                }
            }
            ideasAdapter.swapFilterData(filterIdeasModelArrayList);
        } else {
            ideasAdapter.swapFilterData(ideasModelArrayList);
        }
    }

    private Drawable getDrawableTint(int drawable) {
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getContext(), drawable);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(MainActivity.colorSelected));
        return unwrappedDrawable;
    }

    // Show Idea
    private void showIdea() {
        if(ideasAdapter.getIdeas()==null || ideasAdapter.getIdeas().size()<=0)((MainActivity) requireActivity()).showCustomProgressDialog(dialogBg);
        JsonObject jsonObject = new JsonObject();
        int count_ = 0;
        if (!isFilterApplied && !resumeCalled)
            count_ = ideasAdapter.getItemCount();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("start_count", count_ + "");
        jsonObject.addProperty("offset_count", "20");
        jsonObject.addProperty("filter", currentFilter);


        Call<IdeaListResponse> call = service.showIdeas(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {
                isLoading = false;
                try {
                    if (response.isSuccessful()) {
//                        ideasModelArrayList.addAll(response.body());
                        //ideasAdapter.profilePath=response.body().getIdeaImageURL();
                        ideasModelArrayList = (ArrayList<MyIdeasModel>) response.body().getIdeas();
                        if (!resumeCalled) {
                            if (isFilterApplied) {
                                ideasAdapter.swapFilterData(response.body().getIdeas());
                            } else {
                                ideasAdapter.swapData(response.body().getIdeas());
                            }
                        } else {
                            resumeCalled = false;
                            if (!isFilterApplied) {
                                ideasAdapter.swapFilterData(response.body().getIdeas());
                            } else {
                                ideasAdapter.swapData(response.body().getIdeas());
                            }
                        }
                        isFilterApplied = false;
                        if (ideasAdapter.IdeasModelsList == null || ideasAdapter.IdeasModelsList.isEmpty()) {
                            rv_ideas.setVisibility(View.GONE);
                            noDataTv.setVisibility(View.VISIBLE);
                        } else {
                            rv_ideas.setVisibility(View.VISIBLE);
                            noDataTv.setVisibility(View.GONE);
                        }
                        if (((MainActivity) getActivity()) != null) {
                            MainActivity.exploreIdeas.clear();
                            MainActivity.exploreIdeas.addAll(ideasAdapter.getIdeas());
                        }
                    }
                    captureBg();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }
        });
    }

    private void searchIdea() {
        JsonObject jsonObject = new JsonObject();
        int count_ = 0;
        if (isSearchIdea) count_ = ideasAdapter.getItemCount();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("start_count", count_ + "");
        jsonObject.addProperty("offset_count", "20");
        jsonObject.addProperty("searchValue", searchEt.getText().toString());
        jsonObject.addProperty("flag", "explorer");

        ((MainActivity) requireActivity()).showCustomProgressDialog(dialogBg);
        Call<IdeaListResponse> call = service.searchIdeas(jsonObject);
        call.enqueue(new Callback<IdeaListResponse>() {
            @Override
            public void onResponse(Call<IdeaListResponse> call, Response<IdeaListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        isSearchIdea = true;
                        if (response.body() != null) {
                            ideasAdapter.swapFilterData(response.body().getIdeas());
                        } else {
                            ideasAdapter.swapFilterData(new ArrayList<MyIdeasModel>());
                        }
                        if (ideasAdapter.IdeasModelsList == null || ideasAdapter.IdeasModelsList.isEmpty()) {
                            rv_ideas.setVisibility(View.GONE);
                            noDataTv.setVisibility(View.VISIBLE);
                        } else {
                            rv_ideas.setVisibility(View.VISIBLE);
                            noDataTv.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }

            @Override
            public void onFailure(Call<IdeaListResponse> call, Throwable t) {
                call.cancel();
                ((MainActivity) requireActivity()).cancelProgressDialog();
            }
        });
    }

    private void checkDefaultFilter() {
        if (SharedPrefUtils.getStringData(Constants.FIRST_TIME_FILTER).equals("")) {
            showDefaultFilter(true);
            SharedPrefUtils.saveData(Constants.FIRST_TIME_FILTER, "1");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentChangeCallBacks = (FragmentChangeCallBacks) context;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (!isCalledFromSharingActivity) {
            inflater.inflate(R.menu.menu_main, menu);
            MenuItem menuItem1 = menu.findItem(R.id.item_filter);
            //MenuItem menuItem2 = menu.findItem(R.id.item_notification);
            if (menuItem1 != null) {
                tintMenuIcon(getContext(), menuItem1, MainActivity.colorSelected);
            }
            /*if (menuItem2 != null) {
                tintMenuIcon(getContext(), menuItem2, MainActivity.colorSelected);
            }*/
            textCartItemCount = null;
            textNotificationItemCount = null;

            final MenuItem menuItem = menu.findItem(R.id.action_friend_notification);
            View actionView = MenuItemCompat.getActionView(menuItem);
            textCartItemCount = actionView.findViewById(R.id.notification_badge);
            imgNotification = actionView.findViewById(R.id.imgNotification);
            textCartItemCount.setVisibility(View.GONE);
            CommonUtils.tintMyDrawable(getActivity(), imgNotification.getDrawable());
            setupBadge();
            actionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onOptionsItemSelected(menuItem);
                }
            });

            final MenuItem notificationMenuItem = menu.findItem(R.id.item_notification);
            View notificationAactionView = MenuItemCompat.getActionView(notificationMenuItem);
            textNotificationItemCount = (TextView) notificationAactionView.findViewById(R.id.notification_count_badge);
            imgNotification1 = notificationAactionView.findViewById(R.id.imgNotification);
            textNotificationItemCount.setVisibility(View.GONE);
            CommonUtils.tintMyDrawable(getActivity(), imgNotification1.getDrawable());
            setupBadge();
            notificationAactionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onOptionsItemSelected(notificationMenuItem);
                }
            });
        } else {
            inflater.inflate(R.menu.menu_multiselect_idea, menu);
            MenuItem menuItem1 = menu.findItem(R.id.item_cancel);
            MenuItem menuItem2 = menu.findItem(R.id.item_done);
            if (menuItem1 != null) {
                tintMenuIcon(getContext(), menuItem1, MainActivity.colorSelected);
            }
            if (menuItem2 != null) {
                tintMenuIcon(getContext(), menuItem2, MainActivity.colorSelected);
            }

        }
    }

    public static void setupBadge() {
        try {
            if (textCartItemCount != null) {
                textCartItemCount.post(() -> {
                    textCartItemCount.setText(NotificationUtils.getFriendRequestCountFromSharedPrefs() + "");
                    if (NotificationUtils.getFriendRequestCountFromSharedPrefs() == 0) {
                        textCartItemCount.setVisibility(View.GONE);
                    } else {
                        textCartItemCount.setVisibility(View.VISIBLE);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setupNotificationBadge() {
        try {
            if (textNotificationItemCount != null) {
                textNotificationItemCount.post(() -> {
                    textNotificationItemCount.setText(NotificationUtils.getFriendRequestCountFromSharedPrefs() + "");
                    if (NotificationUtils.getFriendRequestCountFromSharedPrefs() == 0) {
                        textNotificationItemCount.setVisibility(View.GONE);
                    } else {
                        textNotificationItemCount.setVisibility(View.VISIBLE);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        textCartItemCount = null;
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_notification: {
//                Bundle bundle = new Bundle();
////                bundle.putString("userId", "");
//                fragmentChangeCallBacks.fragmentName("goToNotificationFragment", bundle);
                return true;
            }
            case R.id.action_friend_notification: {
                Bundle bundle = new Bundle();
//                bundle.putString("userId", "");
                fragmentChangeCallBacks.fragmentName("goToNotificationFragment", bundle);
                return true;
            }
            case R.id.item_filter: {
                // BottomSheetFilterFragment addPhotoBottomDialogFragment = BottomSheetFilterFragment.newInstance(this);
                // addPhotoBottomDialogFragment.show(getChildFragmentManager(), "BottomSheetFilterFragment");
                onFilterClick();
                return true;
            }
            case R.id.item_cancel: {
                ExploreIdeaFragment.isCalledFromSharingActivity = false;
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                objContactChatFragment.setIdea(null);
                return true;
            }
            case R.id.item_done: {
                if (objContactChatFragment != null) {
                    if (ideasModelArrayList != null) {
                        ArrayList<MyIdeasModel> ideasModels = new ArrayList<>();
                        int i = 0;
                        for (MyIdeasModel model : ideasModelArrayList) {
                            if (model.isSelected()) {
                                ideasModels.add(model);
                                ideasModelArrayList.get(i).setSelected(false);
                            }
                            i++;
                        }
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("models", ideasModels);
                        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                        objContactChatFragment.setIdea(bundle);
                    }
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void clickedItem(Bundle bundle) {
        try {
            int position = bundle.getInt("position");
            Log.d("Click_item","Item click position = "+position);
            if (isCalledFromSharingActivity) {
                MyIdeasModel tmpModel = ideasAdapter.getIdeas().get(position);
                if (tmpModel.isSelected()) {
                    ideasModelArrayList.get(position).setSelected(false);
                    ideasAdapter.getIdeas().get(position).setSelected(false);
                } else {
                    ideasModelArrayList.get(position).setSelected(true);
                    ideasAdapter.getIdeas().get(position).setSelected(true);
                }
                if(getSelectedCount()>3) {
                    if (tmpModel.isSelected()) {
                        ideasModelArrayList.get(position).setSelected(false);
                        ideasAdapter.getIdeas().get(position).setSelected(false);
                    }
                    Toast.makeText(getContext(),"You can select maximum 3 ideas", Toast.LENGTH_LONG).show();
                }
                ideasAdapter.notifyItemChanged(position);
            } else
                fragmentChangeCallBacks.fragmentName("goToOthersIdeaPreviewFragment", bundle);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void appliedFilter(Bundle data) {
        category_id_filter = SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS);
        ideasAdapter.resetData();
        if (ideasAdapter.IdeasModelsList == null || ideasAdapter.IdeasModelsList.isEmpty()) {
            rv_ideas.setVisibility(View.GONE);
            noDataTv.setVisibility(View.VISIBLE);
        } else {
            rv_ideas.setVisibility(View.VISIBLE);
            noDataTv.setVisibility(View.GONE);
        }
        showIdea();
    }

    public void onFilterClick() {
        aToZPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameSortAsc = !nameSortAsc;
                ideasAdapter.sortByName(nameSortAsc);
                dialog.hide();
            }
        });
        datePanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dateSortAsc = !dateSortAsc;
                currentFilter = "time";
                isFilterApplied = true;
                showIdea();
                dialog.hide();

            }
        });
        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentFilter = "views";
                isFilterApplied = true;
                showIdea();
                dialog.hide();
            }
        });
        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentFilter = "favorites";
                isFilterApplied = true;
                showIdea();
                dialog.hide();
            }
        });
        fieldPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDefaultFilter(false);
                dialog.hide();
            }
        });
        defaultFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDefaultFilter(true);
                dialog.dismiss();
            }
        });
        defaultSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    currentFilter = "off";
                    SharedPrefUtils.saveData(Constants.DEFAULT_FILTER_ON, "0");
                    //isFilterApplied = false;
                } else if(!SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS).equals("")){
                    SharedPrefUtils.saveData(Constants.DEFAULT_FILTER_ON, "1");
                    currentFilter = "default";

                }
                if(!SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS).equals("")) {
                    isFilterApplied = true;
                    if (!manualSwitchFilter) {
                        final Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showIdea();
                                dialog.hide();
                            }
                        }, 500);
                    } else {
                        manualSwitchFilter = false;
                        dialog.hide();
                    }
                }else {
                    dialog.hide();
                    showSelectCategory(true);
                    manualSwitchFilter = true;
                    defaultSwitch.setChecked(false);
                }
            }
        });

        dialog.getWindow().setBackgroundDrawable(dialogBg);
        dialog.show();
    }

    private void getDefaultFilter() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        Call<List<String>> call = service.getDefaultFilters(jsonObject);
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() != null && !response.body().isEmpty())
                            SharedPrefUtils.saveData(Constants.APPLIED_FILTERS, android.text.TextUtils.join(",", response.body()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getAllCategory();
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                call.cancel();
                getAllCategory();
            }
        });
    }

    private void getAllCategory() {
        Call<List<CategoryDataSet>> call = service.displayIdeaCategory();
        call.enqueue(new Callback<List<CategoryDataSet>>() {
            @Override
            public void onResponse(Call<List<CategoryDataSet>> call, Response<List<CategoryDataSet>> response) {
                try {
                    if (response.isSuccessful()) {
                        categories.clear();
                        String appFilters = SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS);
                        for (CategoryDataSet cat : response.body()) {
                            //MultiSelectModel data = new MultiSelectModel(cat.getCategoryId(),cat.getCategoryName());
                            CategorySelectionData data = new CategorySelectionData();
                            data.setCatId(cat.getCategoryId());
                            data.setCatName(cat.getCategoryName());
                            data.setIsSelected(0);
                            categories.add(data);
                        }
                        checkDefaultFilter();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryDataSet>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void showDefaultFilter(boolean isDefault) {
        ArrayList<Integer> selectedFilters = new ArrayList<>();
        if (!SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS).equals("") && isDefault) {
            String filters = SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS);
            if (filters.contains(",")) {
                List<String> list = new ArrayList<String>(Arrays.asList(filters.split(",")));
                for (int i = 0; i < list.size(); i++) {
                    selectedFilters.add(Integer.parseInt(list.get(i).trim()));
                    for (int j = 0; j < categories.size(); j++) {
                        if (list.get(i).trim().equals(String.valueOf(categories.get(j).getCatId()))) {
                            categories.get(j).setIsSelected(1);
                        }
                    }
                }
            }
        } else {
            for (int j = 0; j < categories.size(); j++) {
                categories.get(j).setIsSelected(0);
            }
        }
        /*MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                .title("Select your topics of interest") //setting title for dialog
                .titleSize(18)
                .positiveText("Done")
                .negativeText("Cancel")
                .preSelectIDsList(selectedFilters) //List of ids that you need to be selected
                .multiSelectList(categories) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                        //will return list of selected IDS
                        for (int i = 0; i < selectedIds.size(); i++) {
                            if(i==0){
                                category_id_filter = String.valueOf(selectedIds.get(i));
                                category_name_filter = selectedNames.get(i);
                            }else {
                                category_id_filter = category_id_filter + "," + String.valueOf(selectedIds.get(i));
                                category_name_filter = category_name_filter + "," + selectedNames.get(i);
                            }
                        }
                        SharedPrefUtils.saveData(Constants.APPLIED_FILTERS_NAMES, category_name_filter);
                        if(isDefault) {
                            setDefaultFilter(category_id_filter);
                        }else {
                            currentFilter = "category#" + category_id_filter;
                            isFilterApplied = true;
                            showIdea();
                        }
                    }
                    @Override
                    public void onCancel() {
                        Log.d("TAG","Dialog cancelled");
                    }
                });
         multiSelectDialog.show(getChildFragmentManager(), "multiSelectDialog");
        */
        showSelectCategory(isDefault);
    }

    private void setDefaultFilter(String categories) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("category", categories);
        Call<SuccessFilterResponse> call = service.addDefaultFilters(jsonObject);
        call.enqueue(new Callback<SuccessFilterResponse>() {
            @Override
            public void onResponse(Call<SuccessFilterResponse> call, Response<SuccessFilterResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        SharedPrefUtils.saveData(Constants.DEFAULT_FILTER_ON, "1");
                        currentFilter = "default";
                        showIdea();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SuccessFilterResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public static void tintMenuIcon(Context context, MenuItem item, @ColorRes int color) {
        try {
            Drawable normalDrawable = item.getIcon();
            Drawable wrapDrawable = DrawableCompat.wrap(normalDrawable);
            DrawableCompat.setTint(wrapDrawable, context.getResources().getColor(color));

            item.setIcon(wrapDrawable);
        } catch (Exception e) {
        }
    }

    private void showSelectCategory(boolean isDefault) {
        selectAllBtn.setVisibility(View.VISIBLE);
        unSelectAllBtn.setVisibility(View.VISIBLE);
        if (categories != null && categories.size() > 0) {
            catRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            catRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            catSelectionAdapter = new CategoryAdapter(getContext(), categories, true);
            catRecyclerView.setAdapter(catSelectionAdapter);
        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catFilterDialog.hide();
            }
        });

        selectAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int j = 0; j < categories.size(); j++) {
                    categories.get(j).setIsSelected(1);
                }
                catSelectionAdapter = new CategoryAdapter(getContext(), categories, true);
                catRecyclerView.setAdapter(catSelectionAdapter);
            }
        });

        unSelectAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int j = 0; j < categories.size(); j++) {
                    categories.get(j).setIsSelected(0);
                }
                catSelectionAdapter = new CategoryAdapter(getContext(), categories, true);
                catRecyclerView.setAdapter(catSelectionAdapter);
            }
        });


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //will return list of selected IDS
                for (int i = 0; i < catSelectionAdapter.getSelectedCategories().size(); i++) {
                    if (i == 0) {
                        category_id_filter = String.valueOf(catSelectionAdapter.getSelectedCategories().get(i).getCatId());
                        category_name_filter = catSelectionAdapter.getSelectedCategories().get(i).getCatName();
                    } else {
                        category_id_filter = category_id_filter + "," + String.valueOf(catSelectionAdapter.getSelectedCategories().get(i).getCatId());
                        category_name_filter = category_name_filter + "," + catSelectionAdapter.getSelectedCategories().get(i).getCatName();
                    }
                }
                SharedPrefUtils.saveData(Constants.APPLIED_FILTERS_NAMES, category_name_filter);
                if (isDefault) {
                    SharedPrefUtils.saveData(Constants.APPLIED_FILTERS, category_id_filter);
                    setDefaultFilter(category_id_filter);
                } else {
                    currentFilter = "category#" + category_id_filter;
                    isFilterApplied = true;
                    showIdea();
                }
                category_id_filter = "";category_name_filter="";
                catFilterDialog.hide();
            }
        });
        catFilterDialog.getWindow().setBackgroundDrawable(dialogBg);
        catFilterDialog.show();
        Window window = catFilterDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void captureBg() {
        try {
            Bitmap map = CommonApiUtils.takeScreenShot(getActivity());
            Bitmap fast = CommonApiUtils.fastblur(map, 30);
            dialogBg = new BitmapDrawable(getResources(), fast);
        } catch (Exception ignored) {
        }

    }

    public void setCalledFromSharingActivity(boolean calledFromSharingActivity) {
        isCalledFromSharingActivity = calledFromSharingActivity;
    }

    public ContactChatFragment getObjContactChatFragment() {
        return objContactChatFragment;
    }

    public void setObjContactChatFragment(ContactChatFragment objContactChatFragment) {
        this.objContactChatFragment = objContactChatFragment;
    }

    private int getSelectedCount(){
        int count = 0;
        for(MyIdeasModel model : ideasModelArrayList){
            if(model.isSelected()){
                count++;
            }
        }
        return count;
    }
}
