package com.vision.imagine.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnAppliedFilterListener;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.CategoryDataSet;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottomSheetFilterFragment extends BottomSheetDialogFragment {

    static OnAppliedFilterListener onAppliedFilterListener;
    List<CategoryDataSet> createIdeaResponseList;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    View view;
    ChipGroup chipGroup;
    LinearLayout ll_clear_button, ll_apply_button;

    public static BottomSheetFilterFragment newInstance(OnAppliedFilterListener onAppliedFilterListener_) {
        onAppliedFilterListener = onAppliedFilterListener_;
        return new BottomSheetFilterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.filter_bottom_sheet_dialog, container, false);
        initView();
        displayIdeaCategory();

        return view;
    }

    private void initView() {
        chipGroup = view.findViewById(R.id.cp_category);
        ll_clear_button = view.findViewById(R.id.ll_clear_button);
        ll_apply_button = view.findViewById(R.id.ll_apply_button);

        ll_apply_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<Integer> ids = chipGroup.getCheckedChipIds();

                if (ids.size() > 0) {
                    String category = "";
                    for (Integer id : ids) {
                        Chip chip = chipGroup.findViewById(id);
                        if (category.isEmpty())
                            category = chip.getId() + "";
                        else
                            category = category + "," + chip.getId();
                    }

                    SharedPrefUtils.saveData(Constants.APPLIED_FILTERS, category);
                    onAppliedFilterListener.appliedFilter(new Bundle());
                }
                dismiss();
            }
        });
        ll_clear_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPrefUtils.saveData(Constants.APPLIED_FILTERS, "");
                onAppliedFilterListener.appliedFilter(new Bundle());
                dismiss();
            }
        });
    }

    private void displayIdeaCategory() {
        Call<List<CategoryDataSet>> call = service.displayIdeaCategory();
        call.enqueue(new Callback<List<CategoryDataSet>>() {
            @Override
            public void onResponse(Call<List<CategoryDataSet>> call, Response<List<CategoryDataSet>> response) {
                try {
                    if (response.isSuccessful()) {
                        createIdeaResponseList = response.body();
                        setCategoryView();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryDataSet>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void setCategoryView() {

        String[] strings = SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS).split(",");
        for (CategoryDataSet genre : createIdeaResponseList) {
            Chip chip = new Chip(view.getContext(), null, R.attr.CustomChipChoice);
            chip.setText(genre.getCategoryName());
            chip.setId(genre.getCategoryId());

            try {
                for (String str : strings) {
                    if (Integer.parseInt(str) == genre.getCategoryId()) {
                        chip.setChecked(true);
                        break;
                    }
                }
            } catch (Exception e) {
            }
            chipGroup.addView(chip);
        }
    }
}