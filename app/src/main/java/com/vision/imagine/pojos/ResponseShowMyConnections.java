package com.vision.imagine.pojos;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseShowMyConnections {

	@SerializedName("userId")
	private String userId;

	@SerializedName("userProfileURL")
	private String userProfileURL;

	@SerializedName("friendList")
	private List<FriendsItem> friends;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setFriends(List<FriendsItem> friends){
		this.friends = friends;
	}

	public List<FriendsItem> getFriends(){
		return friends;
	}

	public String getUserProfileURL() {
		return userProfileURL;
	}

	public void setUserProfileURL(String userProfileURL) {
		this.userProfileURL = userProfileURL;
	}
}