package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class RequestFilterUser{

	@SerializedName("username")
	private String username;

	@SerializedName("userId")
	private String userId;

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserId() {
		return userId;
	}
}