package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class FilterUserItem{

	@SerializedName("userId")
	private String userId;

	@SerializedName("username")
	private String username;

	@SerializedName("userProfileImage")
	private String userProfileImage;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public String getUserProfileImage() {
		return userProfileImage;
	}

	public void setUserProfileImage(String userProfileImage) {
		this.userProfileImage = userProfileImage;
	}
}
