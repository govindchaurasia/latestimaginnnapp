package com.vision.imagine.pojos;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class Audio implements Parcelable {
    public static final Creator<Audio> CREATOR = new Creator<Audio>() {
        @Override
        public Audio createFromParcel(Parcel in) {
            return new Audio(in);
        }

        @Override
        public Audio[] newArray(int size) {
            return new Audio[size];
        }
    };
    protected String id;
    protected String name;
    private String fileName;
    private String duration;
    private Uri audioUri;
    private int intDuration;
    private boolean isPlaying = false;

    public Audio() {
    }

    protected Audio(Parcel in) {
        id = in.readString();
        name = in.readString();
        fileName = in.readString();
        duration = in.readString();
        isPlaying = in.readByte() != 0;
        intDuration = in.readInt();
    }

    public static Creator<Audio> getCREATOR() {
        return CREATOR;
    }

    public Uri getAudioUri() {
        return audioUri;
    }

    public void setAudioUri(Uri audioUri) {
        this.audioUri = audioUri;
    }

    public int getIntDuration() {
        return intDuration;
    }

    public void setIntDuration(int intDuration) {
        this.intDuration = intDuration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(fileName);
        dest.writeString(duration);
        dest.writeByte((byte) (isPlaying ? 1 : 0));
        dest.writeInt(intDuration);
    }
}
