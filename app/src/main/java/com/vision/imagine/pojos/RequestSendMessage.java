package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class RequestSendMessage{

	@SerializedName("messageText")
	private String messageText;

	@SerializedName("fromUserId")
	private String fromUserId;

	@SerializedName("sharedIdeaId")
	private String sharedIdeaId;

	@SerializedName("toUserId")
	private String toUserId;

	public void setMessageText(String messageText){
		this.messageText = messageText;
	}

	public String getMessageText(){
		return messageText;
	}

	public void setFromUserId(String fromUserId){
		this.fromUserId = fromUserId;
	}

	public String getFromUserId(){
		return fromUserId;
	}

	public void setSharedIdeaId(String sharedIdeaId){
		this.sharedIdeaId = sharedIdeaId;
	}

	public String getSharedIdeaId(){
		return sharedIdeaId;
	}

	public void setToUserId(String toUserId){
		this.toUserId = toUserId;
	}

	public String getToUserId(){
		return toUserId;
	}
}