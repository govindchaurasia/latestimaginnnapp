package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentsModel {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("action_value")
    @Expose
    private Object actionValue;
    @SerializedName("comment_message")
    @Expose
    private String commentMessage;
    @SerializedName("reply_message")
    @Expose
    private Object replyMessage;
    @SerializedName("comment_id")
    @Expose
    private Integer commentId;
    @SerializedName("reply_id")
    @Expose
    private Integer replyId;
    @SerializedName("idea_id")
    @Expose
    private Integer ideaId;
    @SerializedName("creation_time")
    @Expose
    private String creation_time;

    public String getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(String creation_time) {
        this.creation_time = creation_time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getActionValue() {
        return actionValue;
    }

    public void setActionValue(Object actionValue) {
        this.actionValue = actionValue;
    }

    public String getCommentMessage() {
        return commentMessage;
    }

    public void setCommentMessage(String commentMessage) {
        this.commentMessage = commentMessage;
    }

    public Object getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(Object replyMessage) {
        this.replyMessage = replyMessage;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public Integer getIdeaId() {
        return ideaId;
    }

    public void setIdeaId(Integer ideaId) {
        this.ideaId = ideaId;
    }
}
