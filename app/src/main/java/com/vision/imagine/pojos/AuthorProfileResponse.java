package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AuthorProfileResponse implements Serializable {
    @SerializedName("profilePicUrl")
    @Expose
    private String profilePicURI;
    @SerializedName("userDetails")
    @Expose
    private UserProfileDataSet authorData;

    public String getProfilePicURI() {
        return profilePicURI;
    }

    public void setProfilePicURI(String profilePicURI) {
        this.profilePicURI = profilePicURI;
    }

    public UserProfileDataSet getAuthorData() {
        return authorData;
    }

    public void setAuthorData(UserProfileDataSet authorData) {
        this.authorData = authorData;
    }
}
