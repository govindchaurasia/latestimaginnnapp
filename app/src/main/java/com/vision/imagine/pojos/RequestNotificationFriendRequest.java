package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class RequestNotificationFriendRequest{

	@SerializedName("offset_count")
	private String offsetCount;

	@SerializedName("start_count")
	private String startCount;

	@SerializedName("userId")
	private String userId;

	public void setOffsetCount(String offsetCount){
		this.offsetCount = offsetCount;
	}

	public String getOffsetCount(){
		return offsetCount;
	}

	public void setStartCount(String startCount){
		this.startCount = startCount;
	}

	public String getStartCount(){
		return startCount;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}
}