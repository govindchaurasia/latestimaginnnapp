package com.vision.imagine.pojos;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseGetConversation{

	@SerializedName("baseUrl")
	private String baseUrl;

	@SerializedName("data")
	private List<Message> data;

	@SerializedName("fromUserId")
	private String fromUserId;

	@SerializedName("toUserId")
	private String toUserId;

	public void setBaseUrl(String baseUrl){
		this.baseUrl = baseUrl;
	}

	public String getBaseUrl(){
		return baseUrl;
	}

	public void setData(List<Message> data){
		this.data = data;
	}

	public List<Message> getData(){
		return data;
	}

	public void setFromUserId(String fromUserId){
		this.fromUserId = fromUserId;
	}

	public String getFromUserId(){
		return fromUserId;
	}

	public void setToUserId(String toUserId){
		this.toUserId = toUserId;
	}

	public String getToUserId(){
		return toUserId;
	}
}