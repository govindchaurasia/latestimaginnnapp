package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FriendNotifyListItem implements Serializable {

	@SerializedName("sender_creation_time")
	private String senderCreationTime;

	@SerializedName("senderName")
	private String senderName;

	@SerializedName("sender_profile_image")
	private String senderProfileImage;

	@SerializedName("userId")
	private String userId;

	public void setSenderCreationTime(String senderCreationTime){
		this.senderCreationTime = senderCreationTime;
	}

	public String getSenderCreationTime(){
		return senderCreationTime;
	}

	public void setSenderName(String senderName){
		this.senderName = senderName;
	}

	public String getSenderName(){
		return senderName;
	}

	public void setSenderProfileImage(String senderProfileImage){
		this.senderProfileImage = senderProfileImage;
	}

	public String getSenderProfileImage(){
		return senderProfileImage;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}
}