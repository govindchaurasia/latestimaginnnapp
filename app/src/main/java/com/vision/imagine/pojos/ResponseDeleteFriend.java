package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class ResponseDeleteFriend{

	@SerializedName("code")
	private String code;

	@SerializedName("remark")
	private String remark;

	@SerializedName("userId")
	private String userId;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}
}