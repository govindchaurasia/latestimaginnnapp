package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IdeaListResponse {
    @SerializedName("baseURL")
    @Expose
    private String baseURL;
    @SerializedName("ideaList")
    @Expose
    private List<MyIdeasModel> ideas = null;
    @SerializedName("userId")
    @Expose
    private String userId;

    public String getBaseURL() {
        return baseURL;
    }
    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<MyIdeasModel> getIdeas() {
        return ideas;
    }

    public void setIdeas(List<MyIdeasModel> ideas) {
        this.ideas = ideas;
    }
}
