package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class MyFriendListRequest {
    @SerializedName("userId")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
