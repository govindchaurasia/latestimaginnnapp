package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddComment {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("Idea_id")
    @Expose
    private Integer ideaId;
    @SerializedName("comment_id")
    @Expose
    private String commentId;
    @SerializedName("userId")
    @Expose
    private String userId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIdeaId() {
        return ideaId;
    }

    public void setIdeaId(Integer ideaId) {
        this.ideaId = ideaId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
