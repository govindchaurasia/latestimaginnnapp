package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoryDataSet implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer categoryId;
    @SerializedName("name")
    @Expose
    private String categoryName;
    @SerializedName("categoryDesc")
    @Expose
    private Object categoryDesc;
    @SerializedName("categoryStatus")
    @Expose
    private Object categoryStatus;
    @SerializedName("orderNo")
    @Expose
    private String orderNo;
    @SerializedName("categoryImage")
    @Expose
    private Object categoryImage;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Object getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(Object categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public Object getCategoryStatus() {
        return categoryStatus;
    }

    public void setCategoryStatus(Object categoryStatus) {
        this.categoryStatus = categoryStatus;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Object getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(Object categoryImage) {
        this.categoryImage = categoryImage;
    }
}