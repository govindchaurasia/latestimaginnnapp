package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class RequestShowMyConnections{

	@SerializedName("userId")
	private String userId;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}
}