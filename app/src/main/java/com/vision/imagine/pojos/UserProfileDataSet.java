package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileDataSet {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobileNo")
    @Expose
    private String mobileNumber;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("appVersion")
    @Expose
    private String appVersion;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("pin")
    @Expose
    private Integer pin;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("tokenId")
    @Expose
    private String tokenId;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("noOfIdeas")
    @Expose
    private Integer noOfideas;
    @SerializedName("name")
    @Expose
    private String nameOfuser;
    @SerializedName("createDate")
    @Expose
    private String createDate;
    @SerializedName("otpFlag")
    @Expose
    private Boolean otpFlag;
    @SerializedName("baseUrl")
    @Expose
    private String baseUrl;
    @SerializedName("profilePicName")
    @Expose
    private String profilePicName;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("caption")
    @Expose
    private String caption;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Integer getNoOfideas() {
        return noOfideas;
    }

    public void setNoOfideas(Integer noOfideas) {
        this.noOfideas = noOfideas;
    }

    public String getNameOfuser() {
        return nameOfuser;
    }

    public void setNameOfuser(String nameOfuser) {
        this.nameOfuser = nameOfuser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Boolean getOtpFlag() {
        return otpFlag;
    }

    public void setOtpFlag(Boolean otpFlag) {
        this.otpFlag = otpFlag;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getProfilePicName() {
        return profilePicName;
    }

    public void setProfilePicName(String profilePicName) {
        this.profilePicName = profilePicName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
