package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class RequestAddConnection{

	@SerializedName("friendId")
	private String friendId;

	@SerializedName("userId")
	private String userId;

	public void setFriendId(String friendId){
		this.friendId = friendId;
	}

	public String getFriendId(){
		return friendId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}
}