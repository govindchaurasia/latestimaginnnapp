package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FriendsItem implements Serializable {

	@SerializedName("userId")
	private String userId;

	@SerializedName("username")
	private String username;

	@SerializedName("profileImageName")
	private String profileImageName;

	@SerializedName("status")
	private String status;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public String getProfileImageName() {
		return profileImageName;
	}

	public void setProfileImageName(String profileImageName) {
		this.profileImageName = profileImageName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}