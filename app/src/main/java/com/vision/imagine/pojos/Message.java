package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;
import com.vision.imagine.utils.Constants;

import java.io.Serializable;

public class Message implements Serializable {

    @SerializedName("creation_time")
    private String creationTime;

    @SerializedName("messageText")
    private String messageText;

    @SerializedName("messageType")
    private String messageType;

    @SerializedName("viewType")
    private String viewType;

    @SerializedName("id")
    private String id;

//    @SerializedName("sharedIdea")
//    private SharedIdea sharedIdea;

    @SerializedName("sharedIdea")
    private MyIdeasModel sharedIdea;

    private boolean showTime;
    private boolean messageFromMe;


    public Message(String id, String messageText, boolean messageFromMe, boolean showTime, String date) {
        this.id = id;
        this.creationTime = date;
        this.messageText = messageText;
        this.messageFromMe = messageFromMe;
        this.showTime = showTime;
        if (messageFromMe)
            this.viewType = Constants.MESSAGE_FROM_ME;
        else
            this.viewType = Constants.MESSAGE_FROM_YOU;
    }

    public Message(String id, MyIdeasModel sharedIdea, boolean messageFromMe, boolean showTime, String date) {
        this.id = id;
        this.creationTime = date;
        this.messageFromMe = messageFromMe;
        this.sharedIdea = sharedIdea;
        this.showTime = showTime;
        if (messageFromMe)
            this.viewType = Constants.IDEA_FROM_ME;
        else
            this.viewType = Constants.IDEA_FROM_YOU;
    }


    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getViewType() {
        return viewType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

//    public void setSharedIdea(SharedIdea sharedIdea) {
//        this.sharedIdea = sharedIdea;
//    }
//
//    public SharedIdea getSharedIdea() {
//        return sharedIdea;
//    }

    public void setSharedIdea(MyIdeasModel sharedIdea) {
        this.sharedIdea = sharedIdea;
    }

    public MyIdeasModel getSharedIdea() {
        return sharedIdea;
    }

    public boolean isShowTime() {
        return showTime;
    }

    public void setShowTime(boolean showTime) {
        this.showTime = showTime;
    }

    public boolean isMessageFromMe() {
        return messageFromMe;
    }

    public void setMessageFromMe(boolean messageFromMe) {
        this.messageFromMe = messageFromMe;
    }
}