package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class SharedIdea{

	@SerializedName("like_count")
	private String likeCount;

	@SerializedName("author")
	private String author;

	@SerializedName("caption")
	private String caption;

	@SerializedName("access_to")
	private String accessTo;

	@SerializedName("idea_id")
	private String ideaId;

	@SerializedName("title")
	private String title;

	@SerializedName("category")
	private String category;

	@SerializedName("ideaImage")
	private String ideaImage;

	@SerializedName("views")
	private String views;

	public void setLikeCount(String likeCount){
		this.likeCount = likeCount;
	}

	public String getLikeCount(){
		return likeCount;
	}

	public void setAuthor(String author){
		this.author = author;
	}

	public String getAuthor(){
		return author;
	}

	public void setCaption(String caption){
		this.caption = caption;
	}

	public String getCaption(){
		return caption;
	}

	public void setAccessTo(String accessTo){
		this.accessTo = accessTo;
	}

	public String getAccessTo(){
		return accessTo;
	}

	public void setIdeaId(String ideaId){
		this.ideaId = ideaId;
	}

	public String getIdeaId(){
		return ideaId;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setIdeaImage(String ideaImage){
		this.ideaImage = ideaImage;
	}

	public String getIdeaImage(){
		return ideaImage;
	}

	public void setViews(String views){
		this.views = views;
	}

	public String getViews(){
		return views;
	}
}