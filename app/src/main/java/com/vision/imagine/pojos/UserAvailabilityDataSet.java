package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAvailabilityDataSet {

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("username")
    @Expose
    private String username;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
