package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
@DatabaseTable(tableName = "PostIdeaModel")
public class PostIdeaModel implements Serializable {
    @DatabaseField(id = true)
    String idea_id;
    @DatabaseField
    String title;
    @DatabaseField
    String caption;
    @DatabaseField
    String category;
    @DatabaseField
    String description;
    @DatabaseField
    String access_to="";
    @DatabaseField
    String created_by;

    public int isEdit() {
        return isEdit;
    }

    public void setEdit(int edit) {
        isEdit = edit;
    }

    int isEdit = 0;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @DatabaseField
    String categoryName;

    public CreateIdeaDataSet getIdeaImage() {
        return ideaImage;
    }

    public void setIdeaImage(CreateIdeaDataSet ideaImage) {
        this.ideaImage = ideaImage;
    }

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    CreateIdeaDataSet ideaImage;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList = new ArrayList<>();

    public ArrayList<CreateIdeaDataSet> getCreateIdeaDataSetArrayList() {
        return createIdeaDataSetArrayList;
    }

    public void setCreateIdeaDataSetArrayList(ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList) {
        this.createIdeaDataSetArrayList = createIdeaDataSetArrayList;
    }

    public String getIdea_id() {
        return idea_id;
    }

    public void setIdea_id(String idea_id) {
        this.idea_id = idea_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAccess_to() {
        return access_to;
    }

    public void setAccess_to(String access_to) {
        this.access_to = access_to;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
}
