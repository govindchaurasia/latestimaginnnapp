package com.vision.imagine.pojos;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseNotificationFriendRequest{

	@SerializedName("friendNotifyList")
	private List<FriendNotifyListItem> friendNotifyList;

	@SerializedName("userId")
	private String userId;

	@SerializedName("profileImagePath")
	private String profileImagePath;

	public void setFriendNotifyList(List<FriendNotifyListItem> friendNotifyList){
		this.friendNotifyList = friendNotifyList;
	}

	public List<FriendNotifyListItem> getFriendNotifyList(){
		return friendNotifyList;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public String getProfileImagePath() {
		return profileImagePath;
	}

	public void setProfileImagePath(String profileImagePath) {
		this.profileImagePath = profileImagePath;
	}
}