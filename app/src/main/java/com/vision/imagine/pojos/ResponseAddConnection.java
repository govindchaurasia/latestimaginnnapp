package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class ResponseAddConnection{

	@SerializedName("result")
	private String result;

	@SerializedName("code")
	private String code;

	@SerializedName("userId")
	private String userId;

	@SerializedName("remark")
	private String remarks;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setRemarks(String remarks){
		this.remarks = remarks;
	}

	public String getRemarks(){
		return remarks;
	}
}