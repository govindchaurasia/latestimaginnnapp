//package com.vision.imagine.pojos;
//
//import com.google.gson.annotations.Expose;
//import com.google.gson.annotations.SerializedName;
//
//public class IdeasModel {
//    @SerializedName("idea_id")
//    @Expose
//    private Integer ideaId;
//    @SerializedName("title")
//    @Expose
//    private String title;
//    @SerializedName("caption")
//    @Expose
//    private String caption;
//    @SerializedName("category")
//    @Expose
//    private String category;
//    @SerializedName("description")
//    @Expose
//    private Object description;
//    @SerializedName("likes")
//    @Expose
//    private Integer likes;
//    @SerializedName("views")
//    @Expose
//    private Integer views;
//    @SerializedName("status")
//    @Expose
//    private Object status;
//    @SerializedName("access_to")
//    @Expose
//    private Object accessTo;
//    @SerializedName("created_by")
//    @Expose
//    private String createdBy;
//    @SerializedName("active")
//    @Expose
//    private Integer active;
//    @SerializedName("username")
//    @Expose
//    private String username;
//    @SerializedName("offset_count")
//    @Expose
//    private Integer offsetCount;
//    @SerializedName("start_count")
//    @Expose
//    private Integer startCount;
//    @SerializedName("category_id")
//    @Expose
//    private Object categoryId;
//
//    public Integer getIdeaId() {
//        return ideaId;
//    }
//
//    public void setIdeaId(Integer ideaId) {
//        this.ideaId = ideaId;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getCaption() {
//        return caption;
//    }
//
//    public void setCaption(String caption) {
//        this.caption = caption;
//    }
//
//    public String getCategory() {
//        return category;
//    }
//
//    public void setCategory(String category) {
//        this.category = category;
//    }
//
//    public Object getDescription() {
//        return description;
//    }
//
//    public void setDescription(Object description) {
//        this.description = description;
//    }
//
//    public Integer getLikes() {
//        return likes;
//    }
//
//    public void setLikes(Integer likes) {
//        this.likes = likes;
//    }
//
//    public Integer getViews() {
//        return views;
//    }
//
//    public void setViews(Integer views) {
//        this.views = views;
//    }
//
//    public Object getStatus() {
//        return status;
//    }
//
//    public void setStatus(Object status) {
//        this.status = status;
//    }
//
//    public Object getAccessTo() {
//        return accessTo;
//    }
//
//    public void setAccessTo(Object accessTo) {
//        this.accessTo = accessTo;
//    }
//
//    public String getCreatedBy() {
//        return createdBy;
//    }
//
//    public void setCreatedBy(String createdBy) {
//        this.createdBy = createdBy;
//    }
//
//    public Integer getActive() {
//        return active;
//    }
//
//    public void setActive(Integer active) {
//        this.active = active;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public Integer getOffsetCount() {
//        return offsetCount;
//    }
//
//    public void setOffsetCount(Integer offsetCount) {
//        this.offsetCount = offsetCount;
//    }
//
//    public Integer getStartCount() {
//        return startCount;
//    }
//
//    public void setStartCount(Integer startCount) {
//        this.startCount = startCount;
//    }
//
//    public Object getCategoryId() {
//        return categoryId;
//    }
//
//    public void setCategoryId(Object categoryId) {
//        this.categoryId = categoryId;
//    }
//
//}