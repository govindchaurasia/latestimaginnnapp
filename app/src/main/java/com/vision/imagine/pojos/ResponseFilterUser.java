package com.vision.imagine.pojos;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseFilterUser{

	@SerializedName("filterUser")
	private List<FilterUserItem> filterUser;

	@SerializedName("status")
	private String status;

	@SerializedName("userProfilePath")
	private String userProfilePath;

	public void setFilterUser(List<FilterUserItem> filterUser){
		this.filterUser = filterUser;
	}

	public List<FilterUserItem> getFilterUser(){
		return filterUser;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public String getUserProfilePath() {
		return userProfilePath;
	}

	public void setUserProfilePath(String userProfilePath) {
		this.userProfilePath = userProfilePath;
	}
}