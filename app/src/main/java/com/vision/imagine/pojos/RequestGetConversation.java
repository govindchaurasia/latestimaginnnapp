package com.vision.imagine.pojos;

import com.google.gson.annotations.SerializedName;

public class RequestGetConversation{

	@SerializedName("fromUserId")
	private String fromUserId;

	@SerializedName("offset_count")
	private String offsetCount;

	@SerializedName("start_count")
	private String startCount;

	@SerializedName("toUserId")
	private String toUserId;

	public void setFromUserId(String fromUserId){
		this.fromUserId = fromUserId;
	}

	public String getFromUserId(){
		return fromUserId;
	}

	public void setOffsetCount(String offsetCount){
		this.offsetCount = offsetCount;
	}

	public String getOffsetCount(){
		return offsetCount;
	}

	public void setStartCount(String startCount){
		this.startCount = startCount;
	}

	public String getStartCount(){
		return startCount;
	}

	public void setToUserId(String toUserId){
		this.toUserId = toUserId;
	}

	public String getToUserId(){
		return toUserId;
	}
}