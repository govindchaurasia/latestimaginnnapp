package com.vision.imagine.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyIdeasModel implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer ideaId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("category")
    @Expose
    private Object category;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("likes")
    @Expose
    private Integer likes;
    @SerializedName("views")
    @Expose
    private Integer views;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("published")
    @Expose
    private String accessTo;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("offset_count")
    @Expose
    private Integer offsetCount;
    @SerializedName("start_count")
    @Expose
    private Integer startCount;
    @SerializedName("category_id")
    @Expose
    private Object categoryId;
    @SerializedName("fav_flag")
    @Expose
    private Boolean favFlag;
    @SerializedName("baseUrl")
    @Expose
    private Object baseUrl;
    @SerializedName("like_flag")
    @Expose
    private Boolean likeFlag;
    @SerializedName("favorite")
    @Expose
    private Integer likeCount;
    @SerializedName("favCount")
    @Expose
    private Integer favCount;
    @SerializedName("ideaCreationDate")
    @Expose
    private String ideaCreationDate;
    @SerializedName("images")
    @Expose
    private String ideaImage;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("isFavorite")
    @Expose
    private String isFav;
    @SerializedName("isDraft")
    @Expose
    private boolean isDraft = false;
    private boolean isSelected = false;

    public Integer getIdeaId() {
        return ideaId;
    }

    public void setIdeaId(Integer ideaId) {
        this.ideaId = ideaId;
    }

    public String getTitle() {
        return ""+title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Object getCategory() {
        return category;
    }

    public void setCategory(Object category) {
        this.category = category;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getAccessTo() {
        return accessTo;
    }

    public void setAccessTo(String accessTo) {
        this.accessTo = accessTo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getOffsetCount() {
        return offsetCount;
    }

    public void setOffsetCount(Integer offsetCount) {
        this.offsetCount = offsetCount;
    }

    public Integer getStartCount() {
        return startCount;
    }

    public void setStartCount(Integer startCount) {
        this.startCount = startCount;
    }

    public Object getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Object categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getFavFlag() {
        return favFlag;
    }

    public void setFavFlag(Boolean favFlag) {
        this.favFlag = favFlag;
    }

    public Object getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(Object baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Boolean getLikeFlag() {
        return likeFlag;
    }

    public void setLikeFlag(Boolean likeFlag) {
        this.likeFlag = likeFlag;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getFavCount() {
        return favCount;
    }

    public void setFavCount(Integer favCount) {
        this.favCount = favCount;
    }

    public String getIdeaCreationDate() {
        return ideaCreationDate;
    }

    public void setIdeaCreationDate(String ideaCreationDate) {
        this.ideaCreationDate = ideaCreationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdeaImage() {
        return ideaImage;
    }

    public void setIdeaImage(String ideaImage) {
        this.ideaImage = ideaImage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public boolean isDraft() {
        return isDraft;
    }

    public void setDraft(boolean draft) {
        isDraft = draft;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}