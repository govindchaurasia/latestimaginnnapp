package com.vision.imagine.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.vision.imagine.pojos.PostIdeaModel;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "Imaginn.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = DatabaseHelper.class.getName();
    private Dao<PostIdeaModel, Integer> ideaDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.v(TAG, "Creating Tables..");
            TableUtils.createTable(connectionSource, PostIdeaModel.class);


        } catch (SQLException e) {
            Log.e(TAG, "Can't create database\n" + e.getMessage());
        } catch (java.sql.SQLException e2) {
            e2.printStackTrace();
        }
    }

    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.v(TAG, "Old Version: " + oldVersion + ", New Version: " + newVersion);
            switch (newVersion) {
                case 1:
                    return;
                default:
                    //database.execSQL("DROP TABLE DBStudent");
                    break;
            }
            onCreate(database, connectionSource);
        } catch (SQLException sqlException) {
            Log.e(TAG, sqlException.getMessage());
        }
    }

    public void close() {
        super.close();
        ideaDao = null;
    }

    public Dao<PostIdeaModel, Integer> getIdeaDao() {
        if (ideaDao == null) {
            try {
                ideaDao = getDao(PostIdeaModel.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return ideaDao;
    }
}
