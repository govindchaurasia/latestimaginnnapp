package com.vision.imagine.db;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.table.TableUtils;
import com.vision.imagine.pojos.PostIdeaModel;

import java.sql.SQLException;
import java.util.List;

public class DatabaseManager {
    private static final String TAG = DatabaseManager.class.getSimpleName();
    private static DatabaseManager instance;
    private DatabaseHelper databaseHelper;

    public DatabaseManager(Context context) {
        this.databaseHelper = new DatabaseHelper(context);
    }

    public static void init(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
    }

    public static DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
        return instance;
    }

    public DatabaseHelper getDatabaseHelper() {
        return this.databaseHelper;
    }

    public boolean addIdea(PostIdeaModel postIdeaModel) {
        if (postIdeaModel == null) {
            return false;
        }
        try {
            getDatabaseHelper().getIdeaDao().createOrUpdate(postIdeaModel);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<PostIdeaModel> getIdea() {
        List<PostIdeaModel> postIdeaModels = null;
        try {
            postIdeaModels = getDatabaseHelper().getIdeaDao().queryForAll();
        } catch (SQLException e) {
            Log.d(TAG, "Error");
            e.printStackTrace();
        }
        if (postIdeaModels == null || postIdeaModels.size() == 0) {
            return null;
        }
        return postIdeaModels;
    }

    public PostIdeaModel getIdeaById(String ideaId) {
        List<PostIdeaModel> postIdeaModels = null;
        try {
            postIdeaModels = getDatabaseHelper().getIdeaDao().queryBuilder().where().eq("idea_id", ideaId).query();
        } catch (SQLException e) {
            Log.d(TAG, "Error");
            e.printStackTrace();
        }
        if (postIdeaModels == null || postIdeaModels.size() == 0) {
            return null;
        }
        return postIdeaModels.get(0);
    }

    public int deleteIdea(int ideaId) {
        int result = 0;
        try {
            result = getDatabaseHelper().getIdeaDao().deleteById(ideaId);
        } catch (SQLException e) {
            Log.d(TAG, "Error");
            e.printStackTrace();
        }
        return result;
    }

    public void clearDatabase() {
        try {
            TableUtils.clearTable(getDatabaseHelper().getConnectionSource(), PostIdeaModel.class);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
