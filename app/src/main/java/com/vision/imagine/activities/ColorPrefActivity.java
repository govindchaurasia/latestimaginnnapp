package com.vision.imagine.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

import com.vision.imagine.R;

import akndmr.github.io.colorprefutil.ColorPrefUtil;

public class ColorPrefActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";

    int colorSelected, colorSelectedLight;

    SharedPreferences mSharedPreferences;
    SharedPreferences.Editor editor;

    TextView tvImaginnn;
    TextView tv_red, tv_blue, tv_orange, tv_blue2, tv_yellow, tv_blue3, tv_green, tv_purple;


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set theme before setContentView
        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        editor = mSharedPreferences.edit();
//        int themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);
//        ColorPrefUtil.changeThemeStyle(this, themeSelected);
        setContentView(R.layout.color_pref_activity);

        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);

        bindViews();
        bindListeners();

        String first = "<font color='#096E9E'>Imagi</font>";
        String second = "<font color='#5795C1'>nn</font>";
        String next = "<font color='#82C7E8'>n</font>";
        tvImaginnn.setText(Html.fromHtml(first + second + next));

    }

    public void bindViews() {
        tvImaginnn = findViewById(R.id.tv_imaginnn);
        tv_red = findViewById(R.id.tv_red);
        tv_blue = findViewById(R.id.tv_blue);
        tv_orange = findViewById(R.id.tv_orange);
        tv_blue2 = findViewById(R.id.tv_blue2);
        tv_yellow = findViewById(R.id.tv_yellow);
        tv_blue3 = findViewById(R.id.tv_blue3);
        tv_green = findViewById(R.id.tv_green);
        tv_purple = findViewById(R.id.tv_purple);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_red.setBackground(getColoredDrawable(R.color.red_pref));
            }
        }, 100);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_blue.setBackground(getColoredDrawable(R.color.blue_pref));
            }
        }, 100);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_orange.setBackground(getColoredDrawable(R.color.orange_pref));
            }
        }, 100);
       handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_blue2.setBackground(getColoredDrawable(R.color.blue2_pref));
            }
        }, 100);
       handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_yellow.setBackground(getColoredDrawable(R.color.yellow_pref));
            }
        }, 100);
       handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_blue3.setBackground(getColoredDrawable(R.color.blue3_pref));
            }
        }, 100);
       handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_green.setBackground(getColoredDrawable(R.color.green_pref));
            }
        }, 100);
       handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_purple.setBackground(getColoredDrawable(R.color.purple_pref));
            }
        }, 100);









    }

    public void bindListeners() {
        tv_red.setOnClickListener(this);
        tv_blue.setOnClickListener(this);
        tv_orange.setOnClickListener(this);
        tv_blue2.setOnClickListener(this);
        tv_yellow.setOnClickListener(this);
        tv_blue3.setOnClickListener(this);
        tv_green.setOnClickListener(this);
        tv_purple.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_red:
                editor.putInt(COLOR_SELECTED, R.color.red_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.red_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemeRed);
                editor.commit();

                recreateColor();
//                //showMessage("btnAsphalt");
//                recreate();
                intent();
                break;
            case R.id.tv_blue:
                editor.putInt(COLOR_SELECTED, R.color.blue_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.blue_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemeBlue);
                editor.commit();


//                editor.putInt(THEME_SELECTED, R.style.AppTheme1);
//                editor.commit();

                recreateColor();

//                //showMessage("btnBlue");
//                recreate();
                intent();
                break;
            case R.id.tv_orange:
                editor.putInt(COLOR_SELECTED, R.color.orange_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.orange_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemeOrange);
                editor.commit();
                recreateColor();
//                //showMessage("btnRed");
//                recreate();
                intent();
                break;
            case R.id.tv_blue2:

                editor.putInt(COLOR_SELECTED, R.color.blue2_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.blue2_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemeBlue2);
                editor.commit();
                recreateColor();
//                editor.putInt(THEME_SELECTED, R.style.AppTheme);
//                editor.commit();
//                //showMessage("Green theme selected");
                recreate();
                intent();
                break;
            case R.id.tv_yellow:

                editor.putInt(COLOR_SELECTED, R.color.yellow_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.yellow_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemeYellow);
                editor.commit();
                recreateColor();
                intent();
//                editor.putInt(THEME_SELECTED, R.style.AppThemePurple);
//                editor.commit();
//                //showMessage("Purple theme selected");
//                recreate();
                break;

            case R.id.tv_blue3:

                editor.putInt(COLOR_SELECTED, R.color.blue3_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.blue3_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemeBlue3);
                editor.commit();
                recreateColor();
//                editor.putInt(THEME_SELECTED, R.style.AppThemePurple);
//                editor.commit();
//                //showMessage("Purple theme selected");
//                recreate();
                intent();
                break;

            case R.id.tv_green:

                editor.putInt(COLOR_SELECTED, R.color.green_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.green_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemeGreen);
                editor.commit();
                recreateColor();
//                editor.putInt(THEME_SELECTED, R.style.AppThemePurple);
//                editor.commit();
//                //showMessage("Purple theme selected");
//                recreate();
                intent();
                break;

            case R.id.tv_purple:

                editor.putInt(COLOR_SELECTED, R.color.purple_pref);
                editor.putInt(COLOR_LIGHT_SELECTED, R.color.purple_pref_light);
                editor.putInt(THEME_SELECTED, R.style.AppThemePurple);
                editor.commit();



                //showMessage("Purple theme selected");
//                recreate();


//                editor.putInt(COLOR_SELECTED, R.color.purple_pref);
//                editor.putInt(COLOR_LIGHT_SELECTED, R.color.purple_pref);


//                editor.commit();
                recreateColor();
//                editor.putInt(THEME_SELECTED, R.style.AppThemePurple);
//                editor.commit();
//                //showMessage("Purple theme selected");
//                recreate();
                intent();
                break;
        }
    }

    private void recreateColor() {

        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);

        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        int themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);
        ColorPrefUtil.changeThemeStyle(this, themeSelected);


        //        ColorPrefUtil.changeColorOfTabLayout(this, mTabLayout, colorSelected, colorSelectedLight, R.color.textColor, R.color.textColor);
//          ColorPrefUtil.changeTextColorOfChildViews(this, mConstraintLayout, colorSelected, colorSelectedLight);
//        ColorPrefUtil.changeTextColorOfSingleView(this, mTextView, colorSelected, colorSelectedLight);
//        ColorPrefUtil.changeTextColorOfSingleView(this, mEditText, colorSelected, colorSelectedLight);
//        ColorPrefUtil.changeTintColorOfIcon(this, mImageViewIcon, colorSelected);
//        ColorPrefUtil.changeBackgroundColorOfSingleView(this, tvImaginnn, colorSelected);

    }

    private void intent() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private Drawable getColoredDrawable(int color){

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.color_picker_ic);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(color));
        return unwrappedDrawable;
    }
}
