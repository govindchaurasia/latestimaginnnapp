package com.vision.imagine.activities;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

import com.vision.imagine.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends FragmentActivity {

    private final TimerTask spashScreenFinished = new TimerTask() {
        @Override
        public void run() {
            Intent splash = new Intent(SplashActivity.this, ColorPrefActivity.class);
            // We set these flags so the user cannot return to the SplashScreen
            splash.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(splash);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slpash_screen);

//        final ImageView img_animation = (ImageView) findViewById(R.id.splashImageView);

        //        // We use a Timer to schedule a TimerTask for 3 seconds in the future!
        Timer timer = new Timer();
        timer.schedule(this.spashScreenFinished, 1000);


    }

}
