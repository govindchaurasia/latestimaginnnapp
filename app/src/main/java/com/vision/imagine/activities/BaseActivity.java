package com.vision.imagine.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.vision.imagine.R;
import com.vision.imagine.utils.CommonApiUtils;

public class BaseActivity extends AppCompatActivity {
    Dialog progressCustomDialog;
    Drawable dialogBg;

    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            int heightDiff = rootLayout.getRootView().getHeight() - rootLayout.getHeight();
            int contentViewTop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(BaseActivity.this);

            if(heightDiff <= contentViewTop){
                onHideKeyboard();

                Intent intent = new Intent("KeyboardWillHide");
                broadcastManager.sendBroadcast(intent);
            } else {
                int keyboardHeight = heightDiff - contentViewTop;
                onShowKeyboard(keyboardHeight);

                Intent intent = new Intent("KeyboardWillShow");
                intent.putExtra("KeyboardHeight", keyboardHeight);
                broadcastManager.sendBroadcast(intent);
            }
        }
    };

    private boolean keyboardListenersAttached = false;
    private ViewGroup rootLayout;

    protected void onShowKeyboard(int keyboardHeight) {}
    protected void onHideKeyboard() {}

    protected void attachKeyboardListeners() {
        if (keyboardListenersAttached) {
            return;
        }

        rootLayout = (ViewGroup) findViewById(R.id.drawer_layout);
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);

        keyboardListenersAttached = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (keyboardListenersAttached) {
            rootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(keyboardLayoutListener);
        }
    }
    public void showCustomProgressDialog(Drawable dialogBg) {
        progressCustomDialog = new Dialog(this);
        progressCustomDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressCustomDialog.setCancelable(true);
        progressCustomDialog.setCanceledOnTouchOutside(true);
        progressCustomDialog.setContentView(R.layout.custom_progress_dialog);

        progressCustomDialog.show();
        progressCustomDialog.getWindow().setBackgroundDrawable(dialogBg);
        Window window = progressCustomDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    public void cancelProgressDialog(){
        try {
            if (progressCustomDialog != null) {
                progressCustomDialog.dismiss();
            }
        }catch (Exception ignored){}
    }

    private void captureBg() {
        try {
            Bitmap map = CommonApiUtils.takeScreenShot(this);
            Bitmap fast = CommonApiUtils.fastblur(map, 30);
            dialogBg = new BitmapDrawable(getResources(), fast);
        }catch (Exception ignored){}
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 200);
    }
}