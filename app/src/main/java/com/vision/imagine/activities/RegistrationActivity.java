package com.vision.imagine.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.vision.imagine.BuildConfig;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.RegistrationDataSet;
import com.vision.imagine.pojos.UserAvailabilityDataSet;
import com.vision.imagine.utils.ChangeBtnColor;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity {

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";
    EditText et_name, txtEmail, txtNumber, txtCountry;
    TextView tv_message,btLogin,tvLogin;
    Spinner codeSp, countrySp;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    int themeSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String screen = SharedPrefUtils.getStringData(Constants.CURRENT_SCREEN);


        if (!screen.isEmpty()) {
            Intent intent = new Intent();
            if (screen.equalsIgnoreCase(MainActivity.class.getCanonicalName())) {
                intent = new Intent(RegistrationActivity.this, PinEntryActivity.class);
            } else if (screen.equalsIgnoreCase(OTPEntryActivity.class.getCanonicalName())) {
                intent = new Intent(RegistrationActivity.this, OTPEntryActivity.class);
            } else if (screen.equalsIgnoreCase(GeneratePinActivity.class.getCanonicalName())) {
                intent = new Intent(RegistrationActivity.this, GeneratePinActivity.class);
            } else if (screen.equalsIgnoreCase(PinEntryActivity.class.getCanonicalName())) {
                intent = new Intent(RegistrationActivity.this, PinEntryActivity.class);
            } else if (screen.equalsIgnoreCase(LoginActivity.class.getCanonicalName())) {
                intent = new Intent(RegistrationActivity.this, LoginActivity.class);
            }

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_registration);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Registration" + "</font>")));
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showDialog(RegistrationActivity.this);
            }
        }, 500);



        tvLogin = (TextView) findViewById(R.id.lnkLogin);
        btLogin = (TextView) findViewById(R.id.btnLogin);
        tvLogin.setMovementMethod(LinkMovementMethod.getInstance());

        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);
        setButtonColor();
        ChangeBtnColor changeBtnColor = new ChangeBtnColor();
        changeBtnColor.changecolor(this, btLogin, colorSelected);

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = et_name.getText() != null ? et_name.getText().toString() : "";
                String emailId = txtEmail.getText() != null ? txtEmail.getText().toString() : "";
                String mobile = txtNumber.getText() != null ? txtNumber.getText().toString() : "";
//               String mobile = "9853563664";

                if (!"".equalsIgnoreCase(username) && !"".equalsIgnoreCase(emailId) && !"".equalsIgnoreCase(mobile)) {
                    //if (tv_message.getText().toString().equals(Constants.USERNAME_NOT_EXISTS)) {
                        if (CommonUtils.emailValidation(emailId)) {
                            if (CommonUtils.mobileVaildation(mobile)) {
                                registerUser();
                            } else {
                                Toast.makeText(getApplicationContext(), Constants.INVALID_MOBILE, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), Constants.INVALID_EMAIL, Toast.LENGTH_SHORT).show();
                        }
                    //} else {
                    //    Toast.makeText(getApplicationContext(), Constants.USERNAME_EXISTS, Toast.LENGTH_SHORT).show();
                    //}
                } else {
                    Toast.makeText(getApplicationContext(), Constants.FIELDS_MISSING, Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_message = findViewById(R.id.tv_message);
        et_name = findViewById(R.id.et_name);
        txtEmail = findViewById(R.id.txtEmail);
        txtNumber = findViewById(R.id.txtNumber);
        txtCountry = findViewById(R.id.txtCountry);
        codeSp = findViewById(R.id.country_code_sp);
        countrySp = findViewById(R.id.country_name_sp);
        et_name.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence.length() > 4) {
                    checkUserAvailability(charSequence.toString());
                } else {
                    tv_message.setVisibility(View.INVISIBLE);
                }
            }
        });
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.code_item, Constants.code);
        codeSp.setAdapter(dataAdapter);
        codeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                String selectedCountry = (String)codeSp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, R.layout.country_item, Constants.country);
        countrySp.setAdapter(dataAdapter2);
        countrySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                String selectedCountry = (String)countrySp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        try {
            String locale = getResources().getConfiguration().locale.getDisplayCountry();
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String countryCode = tm.getSimCountryIso();
            countryCode.toUpperCase();
            String code = "+91";
            if(Constants.country2phone.containsKey(countryCode)) {
                code = Constants.country2phone.get(countryCode);
            }
            int cPosition = 0;
            for (int i = 0; i < Constants.code.length; i++) {
                if (code != null && code.equals(Constants.code[i])) {
                    cPosition = i;
                    break;
                }
            }
            codeSp.setSelection(cPosition);

            int position = 0;
            for (int i = 0; i < Constants.country.length; i++) {
                if (locale.equals(Constants.country[i])) {
                    position = i;
                    break;
                }
            }
            countrySp.setSelection(position);
        }catch (Exception ignored){}
    }

    private void checkUserAvailability(String username) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", username);

        Call<UserAvailabilityDataSet> call = service.checkUserNameAvailability(jsonObject);

        call.enqueue(new Callback<UserAvailabilityDataSet>() {
            @Override
            public void onResponse(Call<UserAvailabilityDataSet> call, Response<UserAvailabilityDataSet> response) {

                try {
                    if (response.isSuccessful()) {
                        UserAvailabilityDataSet userAvailabilityDataSet = response.body();
                        tv_message.setVisibility(View.VISIBLE);
                        if (userAvailabilityDataSet.getResult()) {
                            tv_message.setText(Constants.USERNAME_EXISTS);
                            tv_message.setTextColor(getResources().getColor(R.color.red));
                        } else {
                            tv_message.setText(Constants.USERNAME_NOT_EXISTS);
                            tv_message.setTextColor(getResources().getColor(R.color.primaryColor));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserAvailabilityDataSet> call, Throwable t) {

                call.cancel();
            }
        });
    }

    private void registerUser() {
        showCustomProgressDialog(dialogBg);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", et_name.getText().toString());
        jsonObject.addProperty("email", txtEmail.getText().toString());
//        jsonObject.addProperty("mobileNumber", "+919853563664");
        jsonObject.addProperty("mobile", codeSp.getSelectedItem().toString() + txtNumber.getText().toString());
        jsonObject.addProperty("country", countrySp.getSelectedItem().toString());
        jsonObject.addProperty("deviceId", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        jsonObject.addProperty("appVersion", BuildConfig.VERSION_NAME);
//        jsonObject.addProperty("action", "create");

        Call<RegistrationDataSet> call = service.userRegistration(jsonObject);

        call.enqueue(new Callback<RegistrationDataSet>() {
            @Override
            public void onResponse(Call<RegistrationDataSet> call, Response<RegistrationDataSet> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().getResult()) {
                            RegistrationDataSet userAvailabilityDataSet = response.body();
                            SharedPrefUtils.saveData(Constants.PHONE, codeSp.getSelectedItem().toString() + txtNumber.getText().toString());
                            SharedPrefUtils.saveData(Constants.EMAIL, txtEmail.getText().toString());
                            SharedPrefUtils.saveData(Constants.USER_NAME, et_name.getText().toString());
                            SharedPrefUtils.saveData(Constants.USER_ID, userAvailabilityDataSet.getUserId());

                            Intent intent = new Intent(RegistrationActivity.this, OTPEntryActivity.class);
//                        intent.putExtra("phonenumber", "+91" + txtNumber.getText().toString());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), response.body().getRemark(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    cancelProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDataSet> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + Constants.TECHNICAL_FAILURE, Toast.LENGTH_SHORT).show();
                call.cancel();
                cancelProgressDialog();
            }
        });
    }

    public void showDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_layout_welcome);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView gotIt = dialog.findViewById(R.id.btn_submit);

        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);

        //ChangeBtnColor changeBtnColor = new ChangeBtnColor();
        //changeBtnColor.changecolor(this, gotIt, colorSelected);
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelectedLight));
        gotIt.setBackgroundDrawable(unwrappedDrawable);

        gotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(this);
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void setButtonColor() {
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelectedLight));
        btLogin.setBackgroundDrawable(unwrappedDrawable);
    }
}
