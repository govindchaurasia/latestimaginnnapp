package com.vision.imagine.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.JsonObject;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.LoginResponse;
import com.vision.imagine.pojos.SliderItem;
import com.vision.imagine.rvAdapters.SliderAdapter;
import com.vision.imagine.utils.ChangeBtnColor;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    APIService service = RetrofitClient.getClient().create(APIService.class);
    TextView txtEmail;
    PinEntryEditText txt_pin_entry_new;

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    int themeSelected;
    boolean showPin = false;
    Drawable dialogBg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Login" + "</font>")));
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));
        slider();
        txtEmail = findViewById(R.id.txtEmail);
        txt_pin_entry_new = findViewById(R.id.txt_pin_entry_new);
        txt_pin_entry_new.setMask("*");

        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);

        TextView register = (TextView) findViewById(R.id.lnkRegister);
        TextView btLogin = (TextView) findViewById(R.id.btnLogin);
        TextView forgot_pin = (TextView) findViewById(R.id.tv_forgot_pin);
        ImageView viewPin = (ImageView) findViewById(R.id.view_pin_btn);

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelected));
        btLogin.setBackground(unwrappedDrawable);

        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_enter_otp);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(colorSelectedLight));
        txt_pin_entry_new.setPinBackground(unwrappedDrawable1);

        forgot_pin.setTextColor(getResources().getColor(colorSelected));

        register.setMovementMethod(LinkMovementMethod.getInstance());
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SharedPrefUtils.saveData(Constants.CURRENT_SCREEN,"");
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        viewPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPin = !showPin;
                if (showPin) {
                    txt_pin_entry_new.setMask("");
                } else {
                    txt_pin_entry_new.setMask("*");
                }
                txt_pin_entry_new.setText(txt_pin_entry_new.getText());
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = txt_pin_entry_new.getText().toString();
                String username = txtEmail.getText().toString();
                if (username != null && !username.isEmpty()) {
                    if (!code.isEmpty() || code.length() == 4) {
                        validatePin();
                    } else {
                        Toast.makeText(getApplicationContext(), Constants.PIN_MISSING, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Constants.USERNAME_MISSING, Toast.LENGTH_SHORT).show();
                }

            }
        });

        forgot_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPinActivity.class);
                startActivity(intent);
            }
        });
    }

    private void validatePin() {
        showCustomProgressDialog(dialogBg);
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("username", txtEmail.getText().toString());
        jsonObject.addProperty("pin", txt_pin_entry_new.getText().toString());
        Call<LoginResponse> call = service.loginUser(jsonObject);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().getToken() != null && !response.body().getToken().isEmpty()) {
                            SharedPrefUtils.saveData(Constants.USER_PIN, txt_pin_entry_new.getText().toString());
                            SharedPrefUtils.saveData(Constants.TOKEN, response.body().getToken());
                            SharedPrefUtils.saveData(Constants.USER_NAME, response.body().getUsername());
                            SharedPrefUtils.saveData(Constants.EMAIL, response.body().getUsername());
                            SharedPrefUtils.saveData(Constants.USER_ID, String.valueOf(response.body().getUserId()));
                            SharedPrefUtils.saveData(Constants.PHONE, String.valueOf(response.body().getMobNo()));

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else
                            Toast.makeText(getApplicationContext(), Constants.TECHNICAL_FAILURE, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cancelProgressDialog();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                call.cancel();
                Toast.makeText(getApplicationContext(), t.getMessage() + Constants.TECHNICAL_FAILURE, Toast.LENGTH_SHORT).show();
                cancelProgressDialog();
            }
        });
    }


    private void slider() {

        SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapter sliderAdapter = new SliderAdapter(this);
        sliderView.setSliderAdapter(sliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();


        for (int i = 0; i < 2; i++) {
            SliderItem sliderItem = new SliderItem();
            if (i == 0) {
                sliderItem.setDescription("Life is what happens when you're busy making other plans.\n - John Lennon");
            } else {
                sliderItem.setDescription("The greatest glory in living lies not in never falling, but in rising every time we fall.\n -Nelson Mandela");
            }
//            sliderItem.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
            sliderAdapter.addItem(sliderItem);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Login" + "</font>")));
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 200);
    }

    private void captureBg() {
        try {
            Bitmap map = CommonApiUtils.takeScreenShot(this);
            Bitmap fast = CommonApiUtils.fastblur(map, 30);
            dialogBg = new BitmapDrawable(getResources(), fast);
        }catch (Exception ignored){}
    }


}
