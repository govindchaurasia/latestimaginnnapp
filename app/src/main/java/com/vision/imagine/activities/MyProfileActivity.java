package com.vision.imagine.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.DrawableCompat;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vision.imagine.BuildConfig;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.AuthorProfileResponse;
import com.vision.imagine.pojos.UserProfileDataSet;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.FileCompressor;
import com.vision.imagine.utils.FileUtils;
import com.vision.imagine.utils.ProgressDialogBuilder;
import com.vision.imagine.utils.SharedPrefUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends AppCompatActivity {
    private static final String TAG = MyProfileActivity.class.getName();
    private static final int REQUEST_TAKE_PHOTO = 111;
    private static final int REQUEST_GALLERY_PHOTO = 222;

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";

    Context context;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    UserProfileDataSet userProfileDataSet;
    private String mPhotoPath;
    FileCompressor mCompressor;
    ProgressDialog progressDialog;
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    Dialog progressCustomDialog;
    Drawable dialogBg;
    Spinner countrySp;

    ImageView profileImage, logoIv;
    EditText nameEt, usernameEt, emailEt, mobileNoEt, countryEt, address1Et, address2Et, pinEt, captionEt;
    TextView submitBtn;
    FrameLayout addImageBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

       /* getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Profile" + "</font>")));
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));*/
        context = this;

        progressDialog = ProgressDialogBuilder.build(this, getResources().getString(R.string.please_wait),"");
        progressDialog.setCancelable(false);
        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(colorSelected));
        }
        setViews();
        getUserProfile();
    }

    private void setViews() {
        profileImage = (ImageView) findViewById(R.id.profile_iv);
        addImageBtn = (FrameLayout) findViewById(R.id.add_image_btn);
        nameEt = (EditText) findViewById(R.id.name_et);
        usernameEt = (EditText) findViewById(R.id.user_name_et);
        emailEt = (EditText) findViewById(R.id.email_et);
        mobileNoEt = (EditText) findViewById(R.id.mobile_et);
        countryEt = (EditText) findViewById(R.id.country_et);
        address1Et = (EditText) findViewById(R.id.address1_et);
        address2Et = (EditText) findViewById(R.id.address2_et);
        pinEt = (EditText) findViewById(R.id.pin_et);
        captionEt = (EditText) findViewById(R.id.caption_et);
        submitBtn = (TextView) findViewById(R.id.btn_submit);
        logoIv = (ImageView) findViewById(R.id.logo_iv);
        countrySp = findViewById(R.id.country_name_sp);

        mCompressor = new FileCompressor(context);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        addImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile(null,null);
            }
        });

        logoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

       /* SortedSet<String> countries = new TreeSet<>();
        for (Locale locale : Locale.getAvailableLocales()) {
            if (!TextUtils.isEmpty(locale.getDisplayCountry())) {
                countries.add(locale.getDisplayCountry());
            }
        }

        Spinner citizenship = (Spinner)findViewById(R.id.input_citizenship);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, getCountryListByLocale().toArray(new String[0]));
        citizenship.setAdapter(adapter);*/
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelected));
        submitBtn.setBackground(unwrappedDrawable);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, R.layout.country_item, Constants.country);
        countrySp.setAdapter(dataAdapter2);
        countrySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                String selectedCountry = (String)countrySp.getSelectedItem();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        setCountry("");

    }

    private void setCountry(String country) {
        try {
            String locale = getResources().getConfiguration().locale.getDisplayCountry();
            if(country!=null && !country.isEmpty()){
                locale = country;
            }
            int position = 0;
            for (int i = 0; i < Constants.country.length; i++) {
                if (locale.equals(Constants.country[i])) {
                    position = i;
                    break;
                }
            }
            countrySp.setSelection(position);
        }catch (Exception ignored){}
    }

    private void getUserProfile() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));

        Call<AuthorProfileResponse> call = service.userProfile(jsonObject);

        call.enqueue(new Callback<AuthorProfileResponse>() {
            @Override
            public void onResponse(Call<AuthorProfileResponse> call, Response<AuthorProfileResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        userProfileDataSet = response.body().getAuthorData();
                        String localUrl = "http://imaginnn.com:443/images/profilepic/" + userProfileDataSet.getProfilePicName();
                        Glide.with(context)
                                .load(localUrl)
                                .placeholder(getResources().getDrawable(R.drawable.ic_user))
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .circleCrop()
                                .into(profileImage);
                        setData();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AuthorProfileResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void setData() {
        if(userProfileDataSet!=null){
            nameEt.setText(userProfileDataSet.getNameOfuser());
                usernameEt.setText(userProfileDataSet.getUserName());usernameEt.setEnabled(false);
            emailEt.setText(userProfileDataSet.getEmail());emailEt.setEnabled(false);
            mobileNoEt.setText(userProfileDataSet.getMobileNumber());
            address1Et.setText(userProfileDataSet.getAddress1());
            address2Et.setText(userProfileDataSet.getAddress2());
            countryEt.setText(userProfileDataSet.getCountry());
            if(userProfileDataSet.getPin()!=null) {
                pinEt.setText(userProfileDataSet.getPin() + "");
            }
            captionEt.setText(userProfileDataSet.getCaption()+"");
        }
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 400);
        setCountry(userProfileDataSet.getCountry());
    }
    private void updateProfile(String filePath, String filename) {
        MultipartBody.Part part = null;
        if(filePath != null) {
            part = prepareFilePart("file", filePath, userProfileDataSet.getUserName() + ".jpg");
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("nameOfuser", nameEt.getText().toString());
        jsonObject.addProperty("address1", address1Et.getText().toString());
        jsonObject.addProperty("address2", address2Et.getText().toString());
        jsonObject.addProperty("email", emailEt.getText().toString());
        jsonObject.addProperty("mobileNumber", mobileNoEt.getText().toString());
        jsonObject.addProperty("username", usernameEt.getText().toString());
        jsonObject.addProperty("userId", userProfileDataSet.getUserId());
        jsonObject.addProperty("caption", captionEt.getText().toString());
        jsonObject.addProperty("country", countrySp.getSelectedItem().toString());

        Call<UserProfileDataSet> call;
        if (part == null)
            call = service.updateProfile(jsonObject);
        else
            call = service.updateProfile(part, jsonObject);
        showCustomProgressDialog();
        call.enqueue(new Callback<UserProfileDataSet>() {
            @Override
            public void onResponse(Call<UserProfileDataSet> call, Response<UserProfileDataSet> response) {

                try {
                    if (response.isSuccessful()) {
                        getUserProfile();
                        Toast.makeText(context,"Profile updated successfully!", Toast.LENGTH_LONG);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cancelProgressDialog();
            }

            @Override
            public void onFailure(Call<UserProfileDataSet> call, Throwable t) {
                cancelProgressDialog();
                call.cancel();
            }
        });
    }
    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String filePath, String fileName) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new File(filePath));
        return MultipartBody.Part.createFormData(partName, fileName, requestFile);
    }

    private void selectImage() {
        /*final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                requestStoragePermission(true);
            } else if (items[item].equals("Choose from Library")) {
                requestStoragePermission(false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();*/

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_image_selection_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView takePhotoBtn = (TextView) dialog.findViewById(R.id.take_photo_btn);
        TextView chooseBtn = (TextView) dialog.findViewById(R.id.choose_from_lib_btn);
        TextView cancelBtn = (TextView) dialog.findViewById(R.id.cancel_btn);
        takePhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestStoragePermission(true);
                dialog.cancel();
            }
        });
        chooseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestStoragePermission(false);
                dialog.cancel();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(this);
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent();
                            } else {
                                dispatchGalleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
//                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> Toast.makeText(context, "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    /**
     * Capture image from camera
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile(context);
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);

                mPhotoPath = photoFile.getPath();
                Bundle bundle = new Bundle();
                bundle.putString("uri", String.valueOf(photoURI));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO, bundle);

            }
        }
    }

    /**
     * Select image fro gallery
     */
    private void dispatchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_GALLERY_PHOTO:
                try {
                    Uri selectedImage = data.getData();
                    File filePhoto = mCompressor.compressToFile(new File(FileUtils.getRealPathFromUri(selectedImage, context)));

                    String path = filePhoto.getPath();
                    Log.d(TAG, "File Path: " + path);
                    String fileName = path.substring(path.lastIndexOf("/") + 1);

                    updateProfile(path, fileName);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e){}
                break;
            case REQUEST_TAKE_PHOTO:
                try {
                    File TempPhotoFile = new File(mPhotoPath);
                    TempPhotoFile = mCompressor.compressToFile(TempPhotoFile);
                    String path = TempPhotoFile.getPath();
                    String fileName = path.substring(path.lastIndexOf("/") + 1);

                    updateProfile(path, fileName);

                } catch (IOException e) {
                    e.printStackTrace();
                }catch (Exception e){}
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void showCustomProgressDialog() {
        progressCustomDialog = new Dialog(this);
        progressCustomDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressCustomDialog.setCancelable(false);
        progressCustomDialog.setContentView(R.layout.custom_progress_dialog);

        progressCustomDialog.show();
        progressCustomDialog.getWindow().setBackgroundDrawable(dialogBg);
        Window window = progressCustomDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    private void cancelProgressDialog(){
        if(progressCustomDialog!=null){
            progressCustomDialog.dismiss();
        }
    }
    private void captureBg() {
        Bitmap map = CommonApiUtils.takeScreenShot(this);
        Bitmap fast = CommonApiUtils.fastblur(map, 30);
        dialogBg = new BitmapDrawable(getResources(), fast);
    }
}