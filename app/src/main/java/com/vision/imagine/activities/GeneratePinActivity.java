package com.vision.imagine.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.JsonObject;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.RegTokenDataSet;
import com.vision.imagine.pojos.RegistrationDataSet;
import com.vision.imagine.pojos.SliderItem;
import com.vision.imagine.rvAdapters.SliderAdapter;
import com.vision.imagine.utils.ChangeBtnColor;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneratePinActivity extends AppCompatActivity {

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";
    APIService service = RetrofitClient.getClient().create(APIService.class);
    PinEntryEditText entryEditText;
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    int themeSelected;
    boolean showPin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefUtils.saveData(Constants.CURRENT_SCREEN, GeneratePinActivity.class.getCanonicalName());
        setContentView(R.layout.activity_generate_pin);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Login" + "</font>")));
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));
        entryEditText = findViewById(R.id.txt_pin_entry_new);
        slider();
        TextView btn_submit_pin = (TextView) findViewById(R.id.btn_submit_pin);
        ImageView viewPinBtn = findViewById(R.id.view_pin_btn);

        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelected));
        btn_submit_pin.setBackground(unwrappedDrawable);

        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_enter_otp);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, getResources().getColor(colorSelectedLight));
        entryEditText.setPinBackground(unwrappedDrawable1);


        entryEditText.setMask("*");
        viewPinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPin = !showPin;
                if (showPin) {
                    entryEditText.setMask("");
                }else {
                    entryEditText.setMask("*");
                }
                entryEditText.setText(entryEditText.getText());
            }
        });

        btn_submit_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = entryEditText.getText().toString();
                if (!code.isEmpty() || code.length() == 4) {
                    setPin();
                } else {
                    Toast.makeText(getApplicationContext(), Constants.PIN_MISSING, Toast.LENGTH_SHORT).show();
                }
            }
        });
        UpdateOtpGetToken();
    }

    private void UpdateOtpGetToken() {

        JsonObject jsonObject = new JsonObject();
        //jsonObject.addProperty("username", SharedPrefUtils.getStringData(Constants.USER_NAME));
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("mobileNumber", SharedPrefUtils.getStringData(Constants.PHONE));
        jsonObject.addProperty("email", SharedPrefUtils.getStringData(Constants.EMAIL));
        jsonObject.addProperty("otpFlag", true);

        Call<RegTokenDataSet> call = service.otpVerification(jsonObject);

        call.enqueue(new Callback<RegTokenDataSet>() {
            @Override
            public void onResponse(Call<RegTokenDataSet> call, Response<RegTokenDataSet> response) {

                try {
                    if (response.isSuccessful()) {
                        SharedPrefUtils.saveData(Constants.TOKEN, response.body().getToken());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegTokenDataSet> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void setPin() {

        JsonObject jsonObject = new JsonObject();
        //jsonObject.addProperty("username", SharedPrefUtils.getStringData(Constants.USER_NAME));
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));
        jsonObject.addProperty("pin", entryEditText.getText().toString());
        jsonObject.addProperty("action", "update");

        Call<RegistrationDataSet> call = service.updatePin(jsonObject);

        call.enqueue(new Callback<RegistrationDataSet>() {
            @Override
            public void onResponse(Call<RegistrationDataSet> call, Response<RegistrationDataSet> response) {

                try {
                    if (response.isSuccessful()) {
                        SharedPrefUtils.saveData(Constants.USER_PIN, entryEditText.getText().toString());
                        Intent intent = new Intent(GeneratePinActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), Constants.TECHNICAL_FAILURE, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), Constants.TECHNICAL_FAILURE, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDataSet> call, Throwable t) {
                call.cancel();
                Toast.makeText(getApplicationContext(), Constants.TECHNICAL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void slider() {

        SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapter sliderAdapter = new SliderAdapter(this);
        sliderView.setSliderAdapter(sliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();

        for (int i = 0; i < 2; i++) {
            SliderItem sliderItem = new SliderItem();
            if (i == 0) {
                sliderItem.setDescription("Life is what happens when you're busy making other plans.\" - John Lennon");
            } else {
                sliderItem.setDescription("The greatest glory in living lies not in never falling, but in rising every time we fall.\" -Nelson Mandela");
            }
//            sliderItem.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
            sliderAdapter.addItem(sliderItem);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Login" + "</font>")));
        getSupportActionBar().setElevation(0);
    }
}
