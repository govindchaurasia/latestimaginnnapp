package com.vision.imagine.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.pojos.SliderItem;
import com.vision.imagine.rvAdapters.SliderAdapter;
import com.vision.imagine.utils.ChangeBtnColor;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import akndmr.github.io.colorprefutil.ColorPrefUtil;

public class PinEntryActivity extends AppCompatActivity {


    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    int themeSelected;
    boolean showPin = false;
    PinEntryEditText entryEditText;
    TextView btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);
////        ColorPrefUtil.changeTintColorOfIcon(this, mImageViewIcon, colorSelected);
        ColorPrefUtil.changeThemeStyle(this, themeSelected);

        setContentView(R.layout.activity_pin_entry);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
            getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Login" + "</font>")));
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.white)));
        }catch (Exception e){
            e.printStackTrace();
        }
        slider();
        entryEditText = findViewById(R.id.txt_pin_entry);
        btn_submit = (TextView) findViewById(R.id.btn_submit);
        TextView forgot_pin = (TextView) findViewById(R.id.tv_forgot_pin);
        ImageView viewPinBtn = findViewById(R.id.view_pin_btn);

        //ChangeBtnColor changeBtnColor = new ChangeBtnColor();
        //changeBtnColor.changecolor(PinEntryActivity.this,btn_submit,colorSelected);
        forgot_pin.setTextColor(getResources().getColor(colorSelected));
        entryEditText.setMask("*");
        viewPinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPin = !showPin;
                if (showPin) {
                    entryEditText.setMask("");
                }else {
                    entryEditText.setMask("*");
                }
                entryEditText.setText(entryEditText.getText());
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = entryEditText.getText().toString();
                if(!code.isEmpty() || code.length()==4){
                    if (SharedPrefUtils.getStringData(Constants.USER_PIN).equalsIgnoreCase(code)) {
                        Intent intent = new Intent(PinEntryActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),Constants.INVALID_PIN,Toast.LENGTH_SHORT).show();
                    }
                }else
                    Toast.makeText(getApplicationContext(),Constants.PIN_MISSING,Toast.LENGTH_SHORT).show();
            }
        });

        forgot_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PinEntryActivity.this, OTPEntryActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        entryEditText.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                closeKeyboard();
                String code = str.toString();
                if(code.length() == 4){
                    if (SharedPrefUtils.getStringData(Constants.USER_PIN).equalsIgnoreCase(code)) {
                        Intent intent = new Intent(PinEntryActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),Constants.INVALID_PIN,Toast.LENGTH_SHORT).show();
                    }
                }else
                    Toast.makeText(getApplicationContext(),Constants.PIN_MISSING,Toast.LENGTH_SHORT).show();

            }
        });
        entryEditText.requestFocus();
        showKeyboard();

        setPinBackgroundColor();
        setButtonColor();
    }

    private void setButtonColor() {
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelectedLight));
        btn_submit.setBackgroundDrawable(unwrappedDrawable);
    }

    private void setPinBackgroundColor() {
        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_enter_otp);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelectedLight));
        entryEditText.setPinBackground(unwrappedDrawable);
    }

    private void slider() {

       SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapter sliderAdapter = new SliderAdapter(this);
        sliderView.setSliderAdapter(sliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();



        for (int i = 0; i < 2; i++) {
            SliderItem sliderItem = new SliderItem();
            if (i==0){
                sliderItem.setDescription("Life is what happens when you're busy making other plans.\n - John Lennon");
            }else {
                sliderItem.setDescription("The greatest glory in living lies not in never falling, but in rising every time we fall.\n -Nelson Mandela");
            }
//            sliderItem.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
            sliderAdapter.addItem(sliderItem);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        try{
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Login" + "</font>")));
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));

    }catch (Exception e){
        e.printStackTrace();
    }
    }

    public void showKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void closeKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }
}
