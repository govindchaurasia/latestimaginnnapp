package com.vision.imagine.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.vision.imagine.R;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.networking.request.ForgotPasswordRequest;
import com.vision.imagine.networking.response.ForgotPasswordResponse;
import com.vision.imagine.networking.response.MyFriendsResponse;
import com.vision.imagine.pojos.MyFriendListRequest;
import com.vision.imagine.pojos.SliderItem;
import com.vision.imagine.rvAdapters.SliderAdapter;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPinActivity extends AppCompatActivity {
    APIService service = RetrofitClient.getClient().create(APIService.class);
    TextView goBtn;
    EditText userNameEt;

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";
    int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    int themeSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pin);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + "Login" + "</font>")));
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));

        goBtn = findViewById(R.id.btn_done);
        userNameEt = findViewById(R.id.txt_name);
        slider();
        setData();
        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.ic_go_btn);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(colorSelected));
        goBtn.setBackground(unwrappedDrawable);
    }

    private void setData() {
        goBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!userNameEt.getText().toString().isEmpty()){
                    forgotPasswordAPICall(userNameEt.getText().toString());
                }
            }
        });
    }

    private void forgotPasswordAPICall(String userName) {
        ForgotPasswordRequest request = new ForgotPasswordRequest();
        request.setUserName(userName);
        Call<ForgotPasswordResponse> call = service.forgotPassword(request);
        call.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                try {
                    if (response != null && response.body() != null && response.isSuccessful()) {
                        if(response.body()!=null){
                            SharedPrefUtils.saveData(Constants.USER_NAME, userNameEt.getText().toString());
                            SharedPrefUtils.saveData(Constants.EMAIL, response.body().getEmail());
                            SharedPrefUtils.saveData(Constants.USER_ID, String.valueOf(response.body().getUserId()));
                            SharedPrefUtils.saveData(Constants.PHONE, String.valueOf(response.body().getMobile()));
                            Intent intent = new Intent(ForgotPinActivity.this, OTPEntryActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),"This user is not exist", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),"This user is not exist", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"This user is not exist", Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    private void slider() {
        SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapter sliderAdapter = new SliderAdapter(this);
        sliderView.setSliderAdapter(sliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();


        for (int i = 0; i < 2; i++) {
            SliderItem sliderItem = new SliderItem();
            if (i == 0) {
                sliderItem.setDescription("Life is what happens when you're busy making other plans.\n - John Lennon");
            } else {
                sliderItem.setDescription("The greatest glory in living lies not in never falling, but in rising every time we fall.\n -Nelson Mandela");
            }
//            sliderItem.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
            sliderAdapter.addItem(sliderItem);

        }

    }

}