package com.vision.imagine;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vision.imagine.Interface.FragmentChangeCallBacks;
import com.vision.imagine.activities.BaseActivity;
import com.vision.imagine.activities.ColorPrefActivity;
import com.vision.imagine.activities.LoginActivity;
import com.vision.imagine.activities.MyProfileActivity;
import com.vision.imagine.firebasenotifications.firebaseutils.NotificationUtils;
import com.vision.imagine.fragments.AuthorProfileFragment;
import com.vision.imagine.fragments.BottomNavigationFragment;
import com.vision.imagine.fragments.ChangePinFragment;
import com.vision.imagine.fragments.CommentReplyFragment;
import com.vision.imagine.fragments.ContactChatFragment;
import com.vision.imagine.fragments.ContactUsFragment;
import com.vision.imagine.fragments.CreateIdeasFragment;
import com.vision.imagine.fragments.DrawFragment;
import com.vision.imagine.fragments.ExploreIdeaFragment;
import com.vision.imagine.fragments.FaqsFragment;
import com.vision.imagine.fragments.FavoritesFragment;
import com.vision.imagine.fragments.IdeaCategorySelectorFragment;
import com.vision.imagine.fragments.IdeaCategoryUpdateFragment;
import com.vision.imagine.fragments.MyConnectionsFragment;
import com.vision.imagine.fragments.MyIdeaPublishPreviewFragment;
import com.vision.imagine.fragments.MyIdeaUpdatePreviewFragment;
import com.vision.imagine.fragments.MyIdeasFragment;
import com.vision.imagine.fragments.MyNotificationFragment;
import com.vision.imagine.fragments.PeopleFragment;
import com.vision.imagine.fragments.PreviewMyIdeaFragment;
import com.vision.imagine.fragments.PreviewOthersIdeaFragment;
import com.vision.imagine.fragments.ProfileFragment;
import com.vision.imagine.fragments.RatingFragment;
import com.vision.imagine.fragments.ResetPinFragment;
import com.vision.imagine.fragments.TermsAndConditionFragment;
import com.vision.imagine.fragments.UpdateIdeasFragment;
import com.vision.imagine.networking.APIService;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.networking.request.NotificationRequest;
import com.vision.imagine.networking.response.NotificationResponse;
import com.vision.imagine.pojos.AuthorProfileResponse;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.Notify;
import com.vision.imagine.pojos.ResponseGetConversation;
import com.vision.imagine.rvAdapters.NotifyAdapter;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import akndmr.github.io.colorprefutil.ColorPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, FragmentChangeCallBacks, FragmentManager.OnBackStackChangedListener {

    public static final String PREF_COLOR = "pref_color";
    public static final String COLOR_SELECTED = "color_selected";
    public static final String COLOR_LIGHT_SELECTED = "color_light_selected";
    public static final String THEME_SELECTED = "theme_selected";

    NavigationView navigationView;
    BottomNavigationView bottomNavigationView;
    LinearLayout vaultBtn, exploreBtn, contactBtn, favBtn, customBottomBar;
    ImageView vaultIv, exploreIv, contactTv, favTv, collapseBtn;
    private DrawerLayout drawer;
    public static int colorSelected, colorSelectedLight;
    SharedPreferences mSharedPreferences;
    public static int themeSelected;
    BottomNavigationFragment bottomNavigationFragment;
    public static boolean isCreateIdeaOpen = false;
    public static boolean isPreviewIdeaOpen = false;
    public static boolean isExploreIdeaOpen = false;
    public static boolean isMyIdeaOpen = false;
    public static boolean isPeopleOpen = false;
    public static boolean isFavIdeaOpen = false;
    public static int startElementCount = 0;
    public static int endElementCount = 0;
    ContactChatFragment objContactChatFragment;
    APIService service = RetrofitClient.getClient().create(APIService.class);
    List<Notify> notifyList = new ArrayList<>();
    NotifyAdapter notifyAdapter;
    Dialog notifyDialog;
    RecyclerView notifyRecyclerView;
    TextView noNotificationDataTv;
    Drawable dialogBg;
    PreviewOthersIdeaFragment currentFragment = null;
    CreateIdeasFragment createFragment = null;
    ImageView profileImageView;
    private BroadcastReceiver br;
    public static boolean exitWithDraft = false;
    public static boolean isDeleteIdea = false;
    public static ArrayList<MyIdeasModel> exploreIdeas = new ArrayList<>();
    public static ArrayList<MyIdeasModel> myIdeas = new ArrayList<>();
    public static ArrayList<MyIdeasModel> fevIdeas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getSharedPreferences(PREF_COLOR, MODE_PRIVATE);
        colorSelected = mSharedPreferences.getInt(COLOR_SELECTED, R.color.white);
        colorSelectedLight = mSharedPreferences.getInt(COLOR_LIGHT_SELECTED, R.color.white);
        themeSelected = mSharedPreferences.getInt(THEME_SELECTED, R.style.AppTheme);
        //ColorPrefUtil.changeTintColorOfIcon(this, mImageViewIcon, colorSelected);
        registerReceiver();

        ColorPrefUtil.changeThemeStyle(this, themeSelected);

        setContentView(R.layout.activity_main);
        SharedPrefUtils.saveData(Constants.CURRENT_SCREEN, MainActivity.class.getCanonicalName());
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));

        collapseBtn = (ImageView) findViewById(R.id.collapse_btn);
        customBottomBar = (LinearLayout) findViewById(R.id.custom_bottom_bar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        /*bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        bottomNavigationView.setItemIconTintList(null);
        bottomNavigationView.setBackgroundResource(R.drawable.bottom_bg);*/
        setBottomNavigation();

        navigationView.getMenu().getItem(0).setChecked(true);


        View headView = navigationView.getHeaderView(0);
        String first = "<font color='#096E9E'>Imagi</font>";
        String second = "<font color='#5795C1'>nn</font>";
        String next = "<font color='#82C7E8'>n</font>";
        LinearLayout logoBtn = (LinearLayout) headView.findViewById(R.id.logo_btn);
        ((TextView) headView.findViewById(R.id.tv_title_nav)).setText(Html.fromHtml(first + second + next));

        ((TextView) headView.findViewById(R.id.tv_name)).setText("" + SharedPrefUtils.getStringData(Constants.USER_NAME));
        profileImageView = (ImageView) headView.findViewById(R.id.iv_user);
        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
            }
        });
        logoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                FragmentManager fm = getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                goToExploreIdeaFragment(new Bundle(),false);
            }
        });

        askCheckPermission();
        goToExploreIdeaFragment(null, false);

        NotificationUtils.registerPushTokenIfMissing(this);

        //Notify list setup

        notifyAdapter = new NotifyAdapter(getApplicationContext(), notifyList);
        notifyDialog = new Dialog(MainActivity.this);
        notifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        notifyDialog.setCancelable(true);
        notifyDialog.setCanceledOnTouchOutside(true);
        notifyDialog.setContentView(R.layout.custom_dialog_notify_list);
        notifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        notifyRecyclerView = notifyDialog.findViewById(R.id.notify_recycler_view);
        noNotificationDataTv = notifyDialog.findViewById(R.id.no_data_tv);
        notifyRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        notifyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        notifyAdapter = new NotifyAdapter(this, notifyList);
        notifyRecyclerView.setAdapter(notifyAdapter);

        collapseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customBottomBar.setVisibility(View.VISIBLE);
                customBottomBar.animate()
                        .translationY(0)
                        .alpha(1.0f)
                        .setDuration(500)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                            }
                        });
                collapseBtn.setVisibility(View.GONE);
                currentFragment.setNormalMode();
            }
        });
        redirectUserOnNotificationClick(getIntent());
    }

    private void setBottomNavigation() {
        vaultBtn = (LinearLayout) findViewById(R.id.vault_btn);
        exploreBtn = (LinearLayout) findViewById(R.id.create_idea_btn);
        contactBtn = (LinearLayout) findViewById(R.id.contact_btn);
        favBtn = (LinearLayout) findViewById(R.id.fav_btn);
        vaultIv = (ImageView) findViewById(R.id.vault_iv);
        exploreIv = (ImageView) findViewById(R.id.create_idea_iv);
        contactTv = (ImageView) findViewById(R.id.contact_iv);
        favTv = (ImageView) findViewById(R.id.fav_iv);

        ImageViewCompat.setImageTintList(vaultIv, ColorStateList.valueOf(getResources().getColor(colorSelected)));
        ImageViewCompat.setImageTintList(exploreIv, ColorStateList.valueOf(getResources().getColor(colorSelected)));
        ImageViewCompat.setImageTintList(contactTv, ColorStateList.valueOf(getResources().getColor(colorSelected)));
        ImageViewCompat.setImageTintList(favTv, ColorStateList.valueOf(getResources().getColor(colorSelected)));
        ImageViewCompat.setImageTintList(collapseBtn, ColorStateList.valueOf(getResources().getColor(colorSelected)));
        vaultBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToMyIdeaFragment(null);
            }
        });
        exploreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToExploreIdeaFragment(null, false);
            }
        });
        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                goToPeopleFragment(null);
                goToMyConnectionFragment(null);
            }
        });
        favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFavoritesFragment(null);
            }
        });

        goToExploreIdeaFragment(null, false);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (isCreateIdeaOpen) {
            if(MainActivity.startElementCount == MainActivity.endElementCount){
                super.onBackPressed();
            }else {
                showDiscardIdeaAlert();
            }
        } else {
            if(isPreviewIdeaOpen) isCreateIdeaOpen = true;
            if(checkExploreIdeaOpen()) finish();
            if(checkFavIdeaOpen() || checkPeopleOpen() || checkMyIdeaOpen()) {
                goToExploreIdeaFragment(null, false);
            }else {
                super.onBackPressed();
            }
        }
        onResume();
    }

    public boolean checkFavIdeaOpen(){
        FavoritesFragment myFragment = (FavoritesFragment)getSupportFragmentManager().findFragmentByTag(getName(new FavoritesFragment()));
        return myFragment != null && myFragment.isVisible();
    }
    public boolean checkPeopleOpen(){
        MyConnectionsFragment myFragment = (MyConnectionsFragment)getSupportFragmentManager().findFragmentByTag(getName(new MyConnectionsFragment()));
        return myFragment != null && myFragment.isVisible();
    }
    public boolean checkMyIdeaOpen(){
        MyIdeasFragment myFragment = (MyIdeasFragment)getSupportFragmentManager().findFragmentByTag(getName(new MyIdeasFragment()));
        return myFragment != null && myFragment.isVisible();
    }
    public boolean checkExploreIdeaOpen(){
        ExploreIdeaFragment myFragment = (ExploreIdeaFragment)getSupportFragmentManager().findFragmentByTag(getName(new ExploreIdeaFragment()));
        return myFragment != null && myFragment.isVisible();
    }

    private void showDiscardIdeaAlert() {
       /* DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        //Yes button clicked
                        isCreateIdeaOpen = false;
                        FragmentManager fm = getSupportFragmentManager();
                        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();
                        }
                        goToMyIdeaFragment(new Bundle());

                        break;

                    case DialogInterface.BUTTON_POSITIVE:
                        isCreateIdeaOpen = false;
                        if(createFragment!=null){
                            exitWithDraft = true;
                            createFragment.ideaUpdate(false);

                        }
                        //onBackPressed();
                        break;
                        case DialogInterface.BUTTON_NEUTRAL:
                            //Do nothing
                            break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Exit?").setPositiveButton("Save as Draft", dialogClickListener)
                .setNegativeButton("Discard", dialogClickListener).setNeutralButton("Cancel", dialogClickListener).show();
*/
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_discard_idea_confirmation);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView saveBtn = (TextView) dialog.findViewById(R.id.save_btn);
        TextView discardBtn = (TextView) dialog.findViewById(R.id.discard_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCreateIdeaOpen = false;
                if(createFragment!=null){
                    exitWithDraft = true;
                    createFragment.ideaUpdate(false);

                }
                MainActivity.isDeleteIdea = true;
                dialog.cancel();
            }
        });
        discardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.isDeleteIdea = true;
                isCreateIdeaOpen = false;
                createFragment.deleteIdeaAPI();

                dialog.cancel();
            }
        });
        Bitmap map= CommonApiUtils.takeScreenShot(this);
        Bitmap fast=CommonApiUtils.fastblur(map, 25);
        final Drawable draw=new BitmapDrawable(getResources(),fast);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
    }

    private void askCheckPermission() {
        Dexter.withActivity(MainActivity.this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
          /*  Fragment bottomNavigationFragment = getSupportFragmentManager().findFragmentByTag(getName(new BottomNavigationFragment(this)));
            Fragment profileFragment = getSupportFragmentManager().findFragmentByTag(getName(new ProfileFragment()));
            if (bottomNavigationFragment != null)
                bottomNavigationFragment.onActivityResult(requestCode, resultCode, data);
            if (profileFragment != null)
                profileFragment.onActivityResult(requestCode, resultCode, data);*/
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
        }
    }

    protected String getName(final Fragment fragment) {
        return fragment.getClass().getSimpleName();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (id) {
           /* case R.id.nav_profile:
                ProfileFragment profileFragment = new ProfileFragment();
                profileFragment.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
                fragmentTransaction.replace(R.id.fragment_container, profileFragment, getName(profileFragment));
                fragmentTransaction.addToBackStack(getName(profileFragment));
                fragmentTransaction.commit();
                break;
*/
           /* case R.id.nav_settings:
                SettingsFragment settingsFragment = new SettingsFragment();
                fragmentTransaction.replace(R.id.fragment_container, settingsFragment, getName(settingsFragment));
                fragmentTransaction.addToBackStack(getName(settingsFragment));
                fragmentTransaction.commit();
                break;*/

            case R.id.nav_change_pin:
                ChangePinFragment changePinFragment = new ChangePinFragment();
                fragmentTransaction.replace(R.id.fragment_container, changePinFragment, getName(changePinFragment));
                fragmentTransaction.addToBackStack(getName(changePinFragment));
                fragmentTransaction.commit();
                break;

            case R.id.nav_terms:
                TermsAndConditionFragment termsAndConditionFragment = new TermsAndConditionFragment();
                fragmentTransaction.replace(R.id.fragment_container, termsAndConditionFragment, getName(termsAndConditionFragment));
                fragmentTransaction.addToBackStack(getName(termsAndConditionFragment));
                fragmentTransaction.commit();
                break;

            case R.id.nav_contact_us:
                ContactUsFragment contactUsFragment = new ContactUsFragment();
                fragmentTransaction.replace(R.id.fragment_container, contactUsFragment, getName(contactUsFragment));
                fragmentTransaction.addToBackStack(getName(contactUsFragment));
                fragmentTransaction.commit();
                break;

            case R.id.nav_rate:
                RatingFragment ratingFragment = new RatingFragment();
                fragmentTransaction.replace(R.id.fragment_container, ratingFragment, getName(ratingFragment));
                fragmentTransaction.addToBackStack(getName(ratingFragment));
                fragmentTransaction.commit();
                break;

            case R.id.nav_invite_friends:
                /*InviteFragment inviteFragment = new InviteFragment();
                fragmentTransaction.replace(R.id.fragment_container, inviteFragment, getName(inviteFragment));
                fragmentTransaction.addToBackStack(getName(inviteFragment));
                fragmentTransaction.commit();*/
                /*Create an ACTION_SEND Intent*/
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                /*This will be the actual content you wish you share.*/
                String shareBody = "Checkout this amazing application *Immaginnn* : https://play.google.com/store/apps/details?id=com.vision.imagine&hl=en_IN&gl=US";
                /*The type of the content is text, obviously.*/
                intent.setType("text/plain");
                /*Applying information Subject and Body.*/
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_app));
                intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                /*Fire!*/
                startActivity(Intent.createChooser(intent, "Invite"));
                break;

            /*case R.id.nav_upgrade:
                UpgradeFragment upgradeFragment = new UpgradeFragment();
                fragmentTransaction.replace(R.id.fragment_container, upgradeFragment, getName(upgradeFragment));
                fragmentTransaction.addToBackStack(getName(upgradeFragment));
                fragmentTransaction.commit();
                break;*/


            case R.id.nav_faqs:
                FaqsFragment faqsFragment = new FaqsFragment();
                fragmentTransaction.replace(R.id.fragment_container, faqsFragment, getName(faqsFragment));
                fragmentTransaction.addToBackStack(getName(faqsFragment));
                fragmentTransaction.commit();
                break;

            case R.id.nav_logout:
                SharedPrefUtils.clearData();
                Intent intent1 = new Intent(MainActivity.this, ColorPrefActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                break;
        }
        isCreateIdeaOpen = false;
        drawer.closeDrawer(GravityCompat.START);
        onResume();
        return true;
    }

    /*private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId()) {
                        case R.id.nav_explore:
                            goToMyIdeaFragment(null);
                            break;
                        case R.id.nav_ideas:
                            goToExploreIdeaFragment(null);
                            break;
                        case R.id.nav_my_connections:
                            goToMyConnectionFragment(null);
                        case R.id.nav_my_ideas:
                            goToPeopleFragment(null);
                            break;
                        case R.id.nav_favorites:
                            goToFavoritesFragment(null);
                            break;
                    }
                    return true;
                }
            };*/

    @Override
    public void fragmentName(String fragment, Bundle bundle) {
        isPreviewIdeaOpen = false;
        isExploreIdeaOpen = false; isMyIdeaOpen = false; isPeopleOpen = false; isFavIdeaOpen = false;
        if (fragment.equalsIgnoreCase("goToMyIdeaPublishPreviewFragment")) {
            isPreviewIdeaOpen = true;
            goToMyIdeaPublishPreviewFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToIdeaCommentReplyFragment")) {
            goToIdeaCommentReplyFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToOthersIdeaPreviewFragment")) {
            goToOthersIdeaPreviewFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToIdeaCategorySelectorFragment")) {
            goToIdeaIdeaCategorySelectorFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToDrawFragment")) {
            goToDrawFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToResetPinFragment")) {
            goToResetPinFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToMyIdeaPreviewFragment")) {
            goToMyIdeaPreviewFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToMyIdeasFragment")) {
            isMyIdeaOpen = true;
            goToMyIdeasFragment(bundle, false);
        } else if (fragment.equalsIgnoreCase("goToUpdateIdeasFragment")) {
            goToUpdateIdeasFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToMyIdeaUpdatePreviewFragment")) {
            goToMyIdeaUpdatePreviewFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToIdeaCategoryUpdateFragment")) {
            goToIdeaCategoryUpdateFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToCreateIdeaFragment")) {
            goToCreateIdeaFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToAuthorProfileFragment")) {
            goToAuthorProfileFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToEditIdeaFragment")) {
            goToEditIdeaFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToNotificationFragment")) {
            goToNotificationFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToContactChatFragment")) {
            goToContactChatFragment(bundle);
        } else if (fragment.equalsIgnoreCase("goToExploreIdeaFragment")) {
            isExploreIdeaOpen = true;
            goToExploreIdeaFragment(bundle, true);
        } else if (fragment.equalsIgnoreCase("goToMyIdeasFragmentFromChat")) {
            goToMyIdeasFragment(bundle, true);
        }
        onResume();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 500);

    }

    private void goToIdeaCategoryUpdateFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        IdeaCategoryUpdateFragment ideaCategoryUpdateFragment = new IdeaCategoryUpdateFragment();
        ideaCategoryUpdateFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, ideaCategoryUpdateFragment, getName(ideaCategoryUpdateFragment));
        fragmentTransaction.addToBackStack(getName(ideaCategoryUpdateFragment));
        fragmentTransaction.commit();
    }

    private void goToMyIdeaUpdatePreviewFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        MyIdeaUpdatePreviewFragment myIdeaUpdatePreviewFragment = new MyIdeaUpdatePreviewFragment();
        myIdeaUpdatePreviewFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, myIdeaUpdatePreviewFragment, getName(myIdeaUpdatePreviewFragment));
        fragmentTransaction.addToBackStack(getName(myIdeaUpdatePreviewFragment));
        fragmentTransaction.commit();
    }

    private void goToUpdateIdeasFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        UpdateIdeasFragment updateIdeasFragment = new UpdateIdeasFragment();
        updateIdeasFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, updateIdeasFragment, getName(updateIdeasFragment));
        fragmentTransaction.addToBackStack(getName(updateIdeasFragment));
        fragmentTransaction.commit();
    }

    private void goToDrawFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        DrawFragment drawFragment = new DrawFragment();
        drawFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, drawFragment, getName(drawFragment));
        fragmentTransaction.addToBackStack(getName(drawFragment));
        fragmentTransaction.commit();
    }

    private void goToMyIdeaPublishPreviewFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        MyIdeaPublishPreviewFragment myIdeaPublishPreviewFragment = new MyIdeaPublishPreviewFragment();
        myIdeaPublishPreviewFragment.setArguments(bundle);
        fragmentTransaction.add(R.id.fragment_container, myIdeaPublishPreviewFragment, getName(myIdeaPublishPreviewFragment));
        fragmentTransaction.addToBackStack(getName(myIdeaPublishPreviewFragment));
        fragmentTransaction.commit();
    }

    private void goToOthersIdeaPreviewFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        PreviewOthersIdeaFragment previewOthersIdeaFragment = new PreviewOthersIdeaFragment();
        previewOthersIdeaFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, previewOthersIdeaFragment, getName(previewOthersIdeaFragment));
        fragmentTransaction.addToBackStack(getName(previewOthersIdeaFragment));
        fragmentTransaction.commit();
        currentFragment = previewOthersIdeaFragment;
    }

    private void goToCreateIdeaFragment(Bundle bundle) {
        isCreateIdeaOpen= true;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        CreateIdeasFragment previewOthersIdeaFragment = new CreateIdeasFragment();
        previewOthersIdeaFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, previewOthersIdeaFragment, getName(previewOthersIdeaFragment));
        fragmentTransaction.addToBackStack(getName(previewOthersIdeaFragment));
        fragmentTransaction.commit();
        createFragment = previewOthersIdeaFragment;
    }

    private void goToEditIdeaFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        CreateIdeasFragment previewOthersIdeaFragment = new CreateIdeasFragment();
        previewOthersIdeaFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, previewOthersIdeaFragment, getName(previewOthersIdeaFragment));
        fragmentTransaction.addToBackStack(getName(previewOthersIdeaFragment));
        fragmentTransaction.commit();
        createFragment = previewOthersIdeaFragment;
    }

    private void goToAuthorProfileFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        AuthorProfileFragment authorProfileFragment = new AuthorProfileFragment();
        authorProfileFragment.setAuthorId(bundle.getString("userId"));
        authorProfileFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, authorProfileFragment, getName(authorProfileFragment));
        fragmentTransaction.addToBackStack(getName(authorProfileFragment));
        fragmentTransaction.commit();
    }

    private void goToNotificationFragment(Bundle bundle) {
        isCreateIdeaOpen= false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        CommonUtils.hideKeyboard(this, drawer.getWindowToken());
        MyNotificationFragment notificationFragment = new MyNotificationFragment();
        notificationFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, notificationFragment, getName(notificationFragment));
        fragmentTransaction.addToBackStack(getName(notificationFragment));
        fragmentTransaction.commit();
    }

    private void goToIdeaCommentReplyFragment(Bundle bundle) {
        isCreateIdeaOpen = false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        CommentReplyFragment commentReplyFragment = new CommentReplyFragment();
        commentReplyFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, commentReplyFragment, getName(commentReplyFragment));
        fragmentTransaction.addToBackStack(getName(commentReplyFragment));
        fragmentTransaction.commit();
    }

    private void goToMyIdeaPreviewFragment(Bundle bundle) {
        isCreateIdeaOpen = false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        PreviewMyIdeaFragment previewMyIdeaFragment = new PreviewMyIdeaFragment();
        previewMyIdeaFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, previewMyIdeaFragment, getName(previewMyIdeaFragment));
        fragmentTransaction.addToBackStack(getName(previewMyIdeaFragment));
        fragmentTransaction.commit();
    }

    private void goToIdeaIdeaCategorySelectorFragment(Bundle bundle) {
        isCreateIdeaOpen = false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        IdeaCategorySelectorFragment ideaCategorySelectorFragment = new IdeaCategorySelectorFragment();
        ideaCategorySelectorFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, ideaCategorySelectorFragment, getName(ideaCategorySelectorFragment));
        fragmentTransaction.addToBackStack(getName(ideaCategorySelectorFragment));
        fragmentTransaction.commit();
    }

    private void goToResetPinFragment(Bundle bundle) {
        isCreateIdeaOpen = false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        ResetPinFragment resetPinFragment = new ResetPinFragment();
        resetPinFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, resetPinFragment, getName(resetPinFragment));
        //fragmentTransaction.addToBackStack(getName(resetPinFragment));
        fragmentTransaction.commit();
    }

    private void goToExploreIdeaFragment(Bundle bundle, boolean addFragment) {
        isCreateIdeaOpen = false;isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = true;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        ExploreIdeaFragment exploreIdeaFragment;
        if (addFragment) {
            ExploreIdeaFragment.isCalledFromSharingActivity = true;
            exploreIdeaFragment = new ExploreIdeaFragment();
            exploreIdeaFragment.setArguments(bundle);
            fragmentTransaction.add(R.id.fragment_container, exploreIdeaFragment, getName(exploreIdeaFragment));
            exploreIdeaFragment.setCalledFromSharingActivity(true);
            exploreIdeaFragment.setObjContactChatFragment(objContactChatFragment);
        } else {
            ExploreIdeaFragment.isCalledFromSharingActivity = false;
            exploreIdeaFragment = new ExploreIdeaFragment();
            exploreIdeaFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.fragment_container, exploreIdeaFragment, getName(exploreIdeaFragment));
        }
//        fragmentTransaction.addToBackStack(getName(exploreIdeaFragment));
        fragmentTransaction.commit();
    }

    private void goToMyIdeaFragment(Bundle bundle) {
        isCreateIdeaOpen = false;
        isMyIdeaOpen = true;isPeopleOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        MyIdeasFragment myIdeasFragment = new MyIdeasFragment();
        myIdeasFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, myIdeasFragment, getName(myIdeasFragment));
        //fragmentTransaction.addToBackStack(getName(myIdeasFragment));
        fragmentTransaction.commit();
    }

    private void goToMyIdeasFragment(Bundle bundle, boolean addFragment) {
        isCreateIdeaOpen = false;
        isMyIdeaOpen = true;isPeopleOpen = false;isExploreIdeaOpen = false;isFavIdeaOpen=false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        MyIdeasFragment myIdeasFragment;
        if (addFragment) {
            MyIdeasFragment.isCalledFromSharingActivity = true;
            myIdeasFragment = new MyIdeasFragment();
            fragmentTransaction.add(R.id.fragment_container, myIdeasFragment, getName(myIdeasFragment));
            myIdeasFragment.setObjContactChatFragment(objContactChatFragment);
        } else {
            MyIdeasFragment.isCalledFromSharingActivity = false;
            myIdeasFragment = new MyIdeasFragment();
            fragmentTransaction.replace(R.id.fragment_container, myIdeasFragment, getName(myIdeasFragment));
            fragmentTransaction.addToBackStack(getName(myIdeasFragment));
        }
        fragmentTransaction.commit();
    }

    private void goToPeopleFragment(Bundle bundle) {
        isCreateIdeaOpen = false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        PeopleFragment peopleFragment = new PeopleFragment();
        peopleFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, peopleFragment, getName(peopleFragment));
        //fragmentTransaction.addToBackStack(getName(peopleFragment));
        fragmentTransaction.commit();
    }

    private void goToFavoritesFragment(Bundle bundle) {
        isCreateIdeaOpen = false;
        isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=true;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        FavoritesFragment favoritesFragment = new FavoritesFragment();
        favoritesFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, favoritesFragment, getName(favoritesFragment));
        //fragmentTransaction.addToBackStack(getName(favoritesFragment));
        fragmentTransaction.commit();
    }

    private void goToMyConnectionFragment(Bundle bundle) {
        isPeopleOpen = true;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        isCreateIdeaOpen = false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        MyConnectionsFragment objMyConnectionsFragment = new MyConnectionsFragment();
        objMyConnectionsFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, objMyConnectionsFragment, getName(objMyConnectionsFragment));
        //fragmentTransaction.addToBackStack(getName(myIdeasFragment));
        fragmentTransaction.commit();
    }

    private void goToContactChatFragment(Bundle bundle) {
        isPeopleOpen = false;isMyIdeaOpen = false; isExploreIdeaOpen = false;isFavIdeaOpen=false;
        isCreateIdeaOpen = false;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        objContactChatFragment = new ContactChatFragment();
//        objContactChatFragment.setAuthorId(bundle.getString("userId"));
        objContactChatFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_container, objContactChatFragment, getName(objContactChatFragment));
        fragmentTransaction.addToBackStack(getName(objContactChatFragment));
        fragmentTransaction.commit();
    }

    @Override
    public void onBackStackChanged() {
        ProfileFragment profileFragment = (ProfileFragment) getSupportFragmentManager().findFragmentByTag(getName(new ProfileFragment()));
        MyIdeaPublishPreviewFragment myIdeaPublishPreviewFragment = (MyIdeaPublishPreviewFragment) getSupportFragmentManager().findFragmentByTag(getName(new MyIdeaPublishPreviewFragment()));
        CommentReplyFragment commentReplyFragment = (CommentReplyFragment) getSupportFragmentManager().findFragmentByTag(getName(new CommentReplyFragment()));

        if (profileFragment != null && profileFragment.isVisible()) {
//            bottomNavigationView.setVisibility(View.GONE);
        } else if (myIdeaPublishPreviewFragment != null && myIdeaPublishPreviewFragment.isVisible()) {
//            bottomNavigationView.setVisibility(View.GONE);
        } else if (commentReplyFragment != null && commentReplyFragment.isVisible()) {
//            bottomNavigationView.setVisibility(View.GONE);
        } else {
//            bottomNavigationView.setVisibility(View.VISIBLE);
            getSupportActionBar().show();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_logo_icon_small);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.white)));
        getNotificationsAPICall();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 500);
        getUserProfile();
    }

    private void getNotificationsAPICall() {
        NotificationRequest request = new NotificationRequest();
        request.setOffsetCount("100");
        request.setStartCount("0");
        request.setUserId(SharedPrefUtils.getStringData(Constants.USER_ID));
        Call<NotificationResponse> call = service.getNotifications(request);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                try {
                    if (response != null && response.body() != null && response.isSuccessful()) {
                        if (response.body().getNotifyList() != null) {
                            notifyAdapter.notifyList = response.body().getNotifyList();
                            notifyAdapter.notifyDataSetChanged();
                            noNotificationDataTv.setVisibility(View.GONE);
                        }else {
                            noNotificationDataTv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "You don't have any notifications", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "You don't have any notifications", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                //Toast.makeText(getApplicationContext(),"This user is not exist", Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    public void showNotifyList() {
        notifyDialog.getWindow().setBackgroundDrawable(dialogBg);
        Window window = notifyDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        notifyDialog.show();
    }

    private void captureBg() {
        Bitmap map = CommonApiUtils.takeScreenShot(MainActivity.this);
        Bitmap fast = CommonApiUtils.fastblur(map, 30);
        dialogBg = new BitmapDrawable(getResources(), fast);
    }

    @Override
    protected void onStart() {
        super.onStart();
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                captureBg();
            }
        }, 500);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.item_notification:
                showNotifyList();
                return true;
        }
        return false;
    }

    public void setFullScreen() {
        customBottomBar.animate()
                .translationY(customBottomBar.getHeight())
                .alpha(0.0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        customBottomBar.setVisibility(View.GONE);
                    }
                });
        collapseBtn.setVisibility(View.VISIBLE);
    }

    private void getUserProfile() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", SharedPrefUtils.getStringData(Constants.USER_ID));

        Call<AuthorProfileResponse> call = service.userProfile(jsonObject);
        call.enqueue(new Callback<AuthorProfileResponse>() {
            @Override
            public void onResponse(Call<AuthorProfileResponse> call, Response<AuthorProfileResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        String localUrl = null;
                        if (response.body() != null) {
                            localUrl = "http://imaginnn.com:443/images/profilepic/" + response.body().getAuthorData().getProfilePicName();
                        }
                        Glide.with(getApplicationContext())
                                .load(localUrl)
                                .placeholder(getResources().getDrawable(R.drawable.ic_user))
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .circleCrop()
                                .into(profileImageView);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AuthorProfileResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void registerReceiver() {
        br = new MyBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.MESSAGE_RECEIVED);
        registerReceiver(br, filter);
    }

    @Override
    protected void onDestroy() {
        if (br != null)
            this.unregisterReceiver(br);
        super.onDestroy();
    }

    public class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null && intent.getAction().equals(Constants.MESSAGE_RECEIVED)) {
                    if (intent.hasExtra("messageJson")) {
                        ResponseGetConversation objResponseGetConversation = new Gson().fromJson(intent.getExtras().getString("messageJson", ""), ResponseGetConversation.class);
                        if (objContactChatFragment != null)
                            objContactChatFragment.setReceivedMessage(objResponseGetConversation);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        redirectUserOnNotificationClick(intent);
    }

    private void redirectUserOnNotificationClick(Intent intent) {
        try {
            if (intent != null && intent.hasExtra("notificationType")) {
                String notificationType = intent.getStringExtra("notificationType");
                if (notificationType.equals(Constants.NOTIFICATION_TYPE_FRIEND_REQUEST)) {
                    goToNotificationFragment(new Bundle());
                } else if (notificationType.equals(Constants.NOTIFICATION_TYPE_FRIEND_REQUEST_ACCEPTED)) {
                    goToMyConnectionFragment(null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onShowKeyboard(int keyboardHeight) {
        super.onShowKeyboard(keyboardHeight);

    }

    @Override
    protected void onHideKeyboard() {
        super.onHideKeyboard();

    }
}
