package com.vision.imagine.networking.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vision.imagine.pojos.Notify;

import java.util.List;

public class NotificationResponse {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("profileImagePath")
    @Expose
    private String profileImagePath;
    @SerializedName("notifyList")
    @Expose
    private List<Notify> notifyList = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfileImagePath() {
        return profileImagePath;
    }

    public void setProfileImagePath(String profileImagePath) {
        this.profileImagePath = profileImagePath;
    }

    public List<Notify> getNotifyList() {
        return notifyList;
    }

    public void setNotifyList(List<Notify> notifyList) {
        this.notifyList = notifyList;
    }
}
