package com.vision.imagine.networking.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationRequest {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("start_count")
    @Expose
    private String startCount;
    @SerializedName("offset_count")
    @Expose
    private String offsetCount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStartCount() {
        return startCount;
    }

    public void setStartCount(String startCount) {
        this.startCount = startCount;
    }

    public String getOffsetCount() {
        return offsetCount;
    }

    public void setOffsetCount(String offsetCount) {
        this.offsetCount = offsetCount;
    }
}
