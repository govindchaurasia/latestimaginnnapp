package com.vision.imagine.networking.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vision.imagine.pojos.MyFriend;

import java.io.Serializable;
import java.util.ArrayList;

public class MyFriendsResponse implements Serializable {
    @SerializedName("userId")
    @Expose
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserProfileURL() {
        return userProfileURL;
    }

    public void setUserProfileURL(String userProfileURL) {
        this.userProfileURL = userProfileURL;
    }

    public ArrayList<MyFriend> getFriendList() {
        return friendList;
    }

    public void setFriendList(ArrayList<MyFriend> friendList) {
        this.friendList = friendList;
    }

    @SerializedName("userProfileURL")
    @Expose
    private String userProfileURL;
    @SerializedName("friendList")
    @Expose
    private ArrayList<MyFriend> friendList = null;
}
