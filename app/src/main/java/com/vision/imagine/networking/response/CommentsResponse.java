package com.vision.imagine.networking.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vision.imagine.pojos.CommentsModel;

import java.io.Serializable;
import java.util.List;

public class CommentsResponse implements Serializable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("comment")
    @Expose
    private List<CommentsModel> comment = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<CommentsModel> getComment() {
        return comment;
    }

    public void setComment(List<CommentsModel> comment) {
        this.comment = comment;
    }
}
