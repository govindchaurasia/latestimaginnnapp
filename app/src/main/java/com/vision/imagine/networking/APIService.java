package com.vision.imagine.networking;

import com.google.gson.JsonObject;
import com.vision.imagine.firebasenotifications.models.fcmregistration.RequestFCMPushToken;
import com.vision.imagine.firebasenotifications.models.fcmregistration.ResponseFCMPushToken;
import com.vision.imagine.fragments.CommentReplyFragment;
import com.vision.imagine.networking.request.ForgotPasswordRequest;
import com.vision.imagine.networking.request.NotificationRequest;
import com.vision.imagine.networking.response.CommentsResponse;
import com.vision.imagine.networking.response.ForgotPasswordResponse;
import com.vision.imagine.networking.response.MyFriendsResponse;
import com.vision.imagine.networking.response.NotificationResponse;
import com.vision.imagine.pojos.AddComment;
import com.vision.imagine.pojos.AddViewResponse;
import com.vision.imagine.pojos.AuthorProfileResponse;
import com.vision.imagine.pojos.CategoryDataSet;
import com.vision.imagine.pojos.CommentListResponse;
import com.vision.imagine.pojos.CommentsModel;
import com.vision.imagine.pojos.CreateIdeaResponse;
import com.vision.imagine.pojos.DisableIdeaResponse;
import com.vision.imagine.pojos.IdeaDetailsDataSet;
import com.vision.imagine.pojos.IdeaListResponse;
import com.vision.imagine.pojos.LoginResponse;
import com.vision.imagine.pojos.MyFriendListRequest;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.pojos.RegTokenDataSet;
import com.vision.imagine.pojos.RegistrationDataSet;
import com.vision.imagine.pojos.RequestAddConnection;
import com.vision.imagine.pojos.RequestDeleteFriend;
import com.vision.imagine.pojos.RequestFilterUser;
import com.vision.imagine.pojos.RequestGetConversation;
import com.vision.imagine.pojos.RequestNotificationFriendRequest;
import com.vision.imagine.pojos.RequestSendMessage;
import com.vision.imagine.pojos.RequestShowMyConnections;
import com.vision.imagine.pojos.ResponseAddConnection;
import com.vision.imagine.pojos.ResponseDeleteFriend;
import com.vision.imagine.pojos.ResponseFilterUser;
import com.vision.imagine.pojos.ResponseGetConversation;
import com.vision.imagine.pojos.ResponseNotificationFriendRequest;
import com.vision.imagine.pojos.ResponseShowMyConnections;
import com.vision.imagine.pojos.SuccessFilterResponse;
import com.vision.imagine.pojos.SuccessResponse;
import com.vision.imagine.pojos.UserAvailabilityDataSet;
import com.vision.imagine.pojos.UserProfileDataSet;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIService {

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("username")
    Call<UserAvailabilityDataSet> checkUserNameAvailability(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("userReg")
    Call<RegistrationDataSet> userRegistration(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("userProfile")
    Call<AuthorProfileResponse> userProfile(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("OtpCheck")
    Call<RegTokenDataSet> otpVerification(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("pinUpdate")
    Call<RegistrationDataSet> updatePin(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("userLogin")
    Call<LoginResponse> loginUser(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("showIdeas")
    Call<IdeaListResponse> showIdeas(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("searchIdeas")
    Call<IdeaListResponse> searchIdeas(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("showIdeasForUser")
    Call<IdeaListResponse> showIdeasForUser(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("seletedIdeaDetail")
    Call<IdeaListResponse> showSelectedIdea(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("createIdea")
    Call<CreateIdeaResponse> createIdea(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("getAllfavorite")
    Call<IdeaListResponse> showFavIdea(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("ideaFavorite")
    Call<SuccessResponse> updateFav(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("comments/add")
    Call<AddComment> addComments(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("ideaView")
    Call<AddViewResponse> addViews(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("comments/get")
    Call<CommentListResponse> getComments(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("disableIdea")
    Call<DisableIdeaResponse> disableIdea(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("deleteIdea")
    Call<DisableIdeaResponse> deleteIdea(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("ideaLike")
    Call<SuccessResponse> ideaLike(@Body JsonObject params);

    @Multipart
    @POST("updateIdea")
    Call<CreateIdeaResponse> updateIdea(@Part List<MultipartBody.Part> files, @Part("ideas") JsonObject ideaData);

    @Multipart
    @POST("userProfileEdit")
    Call<UserProfileDataSet> updateProfile(@Part MultipartBody.Part files, @Part("user") JsonObject ideaData);

    @Multipart
    @POST("userProfileEdit")
    Call<UserProfileDataSet> updateProfile(@Part("user") JsonObject ideaData);

    @GET("displayIdeaCategory")
    Call<List<CategoryDataSet>> displayIdeaCategory();

    @POST("getDefaultFilter")
    Call<List<String>> getDefaultFilters(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("addDefaultFilter")
    Call<SuccessFilterResponse> addDefaultFilters(@Body JsonObject params);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("pushNotificaion/token")
    Call<ResponseFCMPushToken> callRegisterPushToken(@Body RequestFCMPushToken objRequestFCMPushToken);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("friends/list")
    Call<ResponseShowMyConnections> showMyConnections(@Body RequestShowMyConnections objRequestShowMyConnections);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("friends/add")
    Call<ResponseAddConnection> addConnection(@Body RequestAddConnection objRequestAddConnection);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("friends/accept")
    Call<ResponseAddConnection> acceptConnectionsRequest(@Body RequestAddConnection objRequestAddConnection);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("friends/reject")
    Call<ResponseAddConnection> rejectConnectionsRequest(@Body RequestAddConnection objRequestAddConnection);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("filter/username")
    Call<ResponseFilterUser> filterUserApi(@Body RequestFilterUser objRequestFilterUser);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("notifications/friend/show")
    Call<ResponseNotificationFriendRequest> getFriendRequestsApi(@Body RequestNotificationFriendRequest objRequestNotificationFriendRequest);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("getAccepted/friends/list")
    Call<MyFriendsResponse> getFriendsApi(@Body MyFriendListRequest request);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("forgot/password")
    Call<ForgotPasswordResponse> forgotPassword(@Body ForgotPasswordRequest request);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("friends/delete")
    Call<ResponseDeleteFriend> deleteFriendApi(@Body RequestDeleteFriend objRequestDeleteFriend);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("notifications/shows")
    Call<NotificationResponse> getNotifications(@Body NotificationRequest request);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("send/message")
    Call<ResponseDeleteFriend> sendMessageApi(@Body RequestSendMessage objRequestSendMessage);

    @Headers({"Content-type: application/json; charset=utf-8"})
    @POST("get/conversation")
    Call<ResponseGetConversation> getConversationApi(@Body RequestGetConversation objRequestGetConversation);

}
