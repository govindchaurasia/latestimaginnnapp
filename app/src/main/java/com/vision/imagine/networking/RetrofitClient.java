package com.vision.imagine.networking;

import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static final String LOG_TAG = RetrofitClient.class.getName();
    public static String BASE_URL = "http://imaginnn.com:8833/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit != null)
            return retrofit;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                String headerStr = "Bearer "+SharedPrefUtils.getStringData(Constants.TOKEN)+":"+SharedPrefUtils.getStringData(Constants.USER_ID);
                Request request = chain.request().newBuilder()
                        //.addHeader("Authorization", "Bearer 58f56ff4-53b5-4768-b3d3-b29f94391765:10")
                        //.addHeader("id", SharedPrefUtils.getStringData(Constants.USER_ID))
                        .addHeader("Authorization", headerStr)
                        .build();
                return chain.proceed(request);
            }
        }).connectTimeout(300, TimeUnit.SECONDS).readTimeout(300, TimeUnit.SECONDS).writeTimeout(300, TimeUnit.SECONDS).build();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(interceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return retrofit;
    }
}
