package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.vision.imagine.R;
import com.vision.imagine.pojos.SliderItem;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

        private Context context;
//    private List<SliderItem> mSliderItems = new ArrayList<>();
    private List<SliderItem> mSliderItems = new ArrayList<SliderItem>();

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public void renewItems(List<SliderItem> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mSliderItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(SliderItem sliderItem) {
        this.mSliderItems.add(sliderItem);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapter.SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_single_row, null);
        return new SliderAdapterVH(inflate);    }

    @Override
    public void onBindViewHolder(SliderAdapter.SliderAdapterVH viewHolder, int position) {

                SliderItem sliderItem = mSliderItems.get(position);

        viewHolder.tv_description.setText(sliderItem.getDescription());


//        Glide.with(viewHolder.itemView.getContext())
//                .load(sliderItem.getImageUrl())
//                .fitCenter()
//                .into(viewHolder.imageViewBackground);

//        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "This is item in position " + position, Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    @Override
    public int getCount() {
        return mSliderItems.size();
    }

    public static class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        TextView tv_description;
        ImageView imageViewBackground;

        public SliderAdapterVH(View itemView) {
            super(itemView);
//            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);

            tv_description = itemView.findViewById(R.id.tv_description);


        }
    }
}
