package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.vision.imagine.R;
import com.vision.imagine.pojos.Message;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;
import com.vision.imagine.utils.TimeUtil;

import java.util.List;

public class AdapterChatSharedIdea extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Message> messageArrayList;
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    public static String profilePath = "";
    ColorGenerator generator = ColorGenerator.MATERIAL;

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterChatSharedIdea(Context context, List<Message> messageArrayList) {
        ctx = context;
        profilePath = "http://imaginnn.com:443/images";
        this.messageArrayList = messageArrayList;
    }

    public class MessageItemViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView text_content;
        public TextView text_time;
        public View lyt_parent;

        public MessageItemViewHolder(View v) {
            super(v);
            text_content = v.findViewById(R.id.text_content);
            text_time = v.findViewById(R.id.text_time);
            lyt_parent = v.findViewById(R.id.lyt_parent);
        }

        private void setValues(Message objMessage, int position) {
            text_content.setText(objMessage.getMessageText());
//            text_time.setText(objMessage.getCreationTime());
            text_time.setText(TimeUtil.calculateAge2(objMessage.getCreationTime()));

            lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, objMessage, position);
                    }
                }
            });

//            btnAction.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mOnItemClickListener != null) {
//                        mOnItemClickListener.onButtonClick(view, objMessage, position, text_content);
//                    }
//                }
//            });
        }
    }

    public class IdeaItemViewHolder extends RecyclerView.ViewHolder {
        public TextView title, tv_descrption, tv_like_count, tv_author, category, tv_ago, view_count;
        ImageView thumbnail, iv_fav;
        public View lyt_parent;

        public IdeaItemViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            tv_descrption = view.findViewById(R.id.tv_descrption);
            tv_like_count = view.findViewById(R.id.tv_like_count);
            tv_author = view.findViewById(R.id.tv_author);
            category = view.findViewById(R.id.category);
            thumbnail = view.findViewById(R.id.thumbnail);
            iv_fav = view.findViewById(R.id.iv_fav);
            tv_ago = view.findViewById(R.id.tv_ago);
            view_count = view.findViewById(R.id.tv_views_count);
            lyt_parent = view.findViewById(R.id.lyt_parent);
        }

        private void setValues(Message objMessage) {
            MyIdeasModel sharedIdea = objMessage.getSharedIdea();

            title.setText(sharedIdea.getTitle());
            tv_descrption.setText(sharedIdea.getCaption());
            if(sharedIdea.getLikes() != null){tv_like_count.setText(sharedIdea.getLikes() + "");
            }else {
                tv_like_count.setText("0");
            }
            tv_author.setText("Author: " + sharedIdea.getAuthor());
            if (sharedIdea.getViews() != null) {
                view_count.setText("" + sharedIdea.getViews());
            } else {
                view_count.setText("0");
            }
            if (sharedIdea.getCategory() != null && sharedIdea.getCategory().toString().contains(",")) {
                String[] cat = sharedIdea.getCategory().toString().split(",");
                String appFilters = SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS_NAMES);
                if (!appFilters.equals("") && appFilters.contains(",")) {
                    String[] appCat = appFilters.split(",");
                    category.setText("" + cat[0]);
                    for (String cat_ : cat) {
                        for (String appliedCat : appCat) {
                            if (cat_.toLowerCase().equals(appliedCat.toLowerCase())) {
                                category.setText("" + appliedCat);
                                break;
                            }
                        }
                    }
                } else {
                    category.setText("" + cat[0]);
                    for (String cat_ : cat) {
                        if (cat_.toLowerCase().equals(appFilters.toLowerCase())) {
                            category.setText("" + cat_);
                            break;
                        }
                    }
                }
            } else {
                category.setText("" + sharedIdea.getCategory().toString());
            }

            try {
                tv_ago.setText(TimeUtil.calculateAge2(sharedIdea.getIdeaCreationDate()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            String userId = SharedPrefUtils.getStringData(Constants.USER_ID);
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .withBorder(4) /* thickness in px */
                    .endConfig()
                    .buildRoundRect(String.valueOf(sharedIdea.getTitle().charAt(0)).toUpperCase(), color, 10);


            String path = profilePath + "/" + sharedIdea.getUserId() + "/" + sharedIdea.getIdeaId() + "/" + sharedIdea.getIdeaImage();
            Glide.with(ctx).load(path)
                    .placeholder(R.drawable.ic_default)
                    .error(R.drawable.ic_default).into(thumbnail);

            lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, objMessage, -1);
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == Integer.parseInt(Constants.MESSAGE_FROM_ME)) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_me, parent, false);
            viewHolder = new MessageItemViewHolder(v);
        } else if (viewType == Integer.parseInt(Constants.MESSAGE_FROM_YOU)) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_you, parent, false);
            viewHolder = new MessageItemViewHolder(v);
        } else if (viewType == Integer.parseInt(Constants.IDEA_FROM_ME)) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content_me, parent, false);
            viewHolder = new IdeaItemViewHolder(v);
        } else if (viewType == Integer.parseInt(Constants.IDEA_FROM_YOU)) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content_you, parent, false);
            viewHolder = new IdeaItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_me, parent, false);
            viewHolder = new MessageItemViewHolder(v);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Message objMessage = messageArrayList.get(position);

        try {
            Integer.parseInt(objMessage.getViewType());
        } catch (NumberFormatException ex) {
            objMessage.setViewType(Constants.MESSAGE_FROM_ME);
        }

        switch (objMessage.getViewType()) {
            case Constants.MESSAGE_FROM_ME:
            case Constants.MESSAGE_FROM_YOU: {
                if (holder instanceof MessageItemViewHolder) {
                    MessageItemViewHolder messageItemViewHolder = (MessageItemViewHolder) holder;
                    messageItemViewHolder.setValues(objMessage, position);
                }
            }
            break;
            case Constants.IDEA_FROM_ME:
            case Constants.IDEA_FROM_YOU: {
                if (holder instanceof IdeaItemViewHolder) {
                    IdeaItemViewHolder ideaItemViewHolder = (IdeaItemViewHolder) holder;
                    ideaItemViewHolder.setValues(objMessage);
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        try {
            return Integer.parseInt(this.messageArrayList.get(position).getViewType());
        } catch (NumberFormatException e) {
            return Integer.parseInt(Constants.MESSAGE_FROM_ME);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Message obj, int position);

        void onButtonClick(View view, Message obj, int position, TextView text_content);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void insertItem(Message item) {
        this.messageArrayList.add(item);
        notifyItemInserted(getItemCount());
    }

    public List<Message> getMessageArrayList() {
        return messageArrayList;
    }

    public void setMessageArrayList(List<Message> messageArrayList, String profilePath) {
        this.messageArrayList = messageArrayList;
        this.profilePath = profilePath;
    }
}