package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnReplyCommentClickedListener;
import com.vision.imagine.pojos.CommentList;
import com.vision.imagine.pojos.CommentsModel;
import com.vision.imagine.utils.TimeUtil;

import java.util.ArrayList;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;

public class CommentReplyAdapter extends RecyclerView.Adapter<CommentReplyAdapter.MyViewHolder> {
    Context context;
    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
    private List<CommentList> commentsModelArrayList;
    OnReplyCommentClickedListener replyCallback;
    String path;

    public CommentReplyAdapter(ArrayList<CommentList> commentsModelArrayList, OnReplyCommentClickedListener replyCallback, Context context, String path) {
        this.commentsModelArrayList = commentsModelArrayList;
        this.replyCallback = replyCallback;
        this.context = context;
        this.path = path;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_reply_single_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CommentList commentsModel = commentsModelArrayList.get(position);
        holder.expandableTextView.setText(commentsModel.getMessage());
        holder.text_view_message_item_layout_chat_user.setText(commentsModel.getUsername());
        holder.text_view_date_time_item_layout_chat_user.setText(TimeUtil.calculateCommentTime(commentsModel.getCreationTime()));
//        int color = generator.getRandomColor();
//        TextDrawable drawable = TextDrawable.builder()
//                .beginConfig()
//                .withBorder(4) /* thickness in px */
//                .endConfig()
//                .buildRoundRect(String.valueOf(commentsModel.getUsername().charAt(0)).toUpperCase(), color, 10);

//        holder.image_view_icon_user_item_layout_chat_user.setBackground(drawable);

        /*if (commentsModel.getMessage().length() < 60)
            holder.tv_view_more.setVisibility(View.INVISIBLE);
        else
            holder.tv_view_more.setVisibility(View.VISIBLE);*/
        Glide.with(context).load(""+path+"/"+commentsModel.getProfileImage())
                .placeholder(context.getResources().getDrawable(R.drawable.ic_default_user))
                .into(holder.image_view_icon_user_item_layout_chat_user);
        holder.tv_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.expandableTextView.expand();
                if (holder.expandableTextView.isExpanded()) {
                    holder.expandableTextView.collapse();
                    holder.tv_view_more.setText("View More");
                } else {
                    holder.expandableTextView.expand();
                    holder.tv_view_more.setText("View Less");
                }
            }
        });

        holder.replyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle data = new Bundle();
                data.putString("comment",commentsModel.getMessage());
                data.putString("comment_id",commentsModel.getId());
                replyCallback.clickedReply(data);
            }
        });
        holder.replyListLayout.removeAllViews();
        holder.replyListLayout.setVisibility(View.GONE);
        holder.replyCountBtn.setVisibility(View.GONE);
        if(commentsModel.getReplyList()!=null && commentsModel.getReplyList().size()>0){
            holder.replyCountBtn.setVisibility(View.VISIBLE);
            holder.replyCountBtn.setText(""+commentsModel.getReplyList().size()+" Reply");
            holder.replyCountBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(holder.replyListLayout.getVisibility() == View.VISIBLE){
                        holder.replyListLayout.setVisibility(View.GONE);
                    }else {
                        holder.replyListLayout.setVisibility(View.VISIBLE);
                    }
                }
            });
            for(CommentList item : commentsModel.getReplyList()) {
                View layout = LayoutInflater.from(context).inflate(R.layout.single_reply_layout, holder.replyListLayout, false);
                TextView commentTv = layout.findViewById(R.id.reply_msg_tv);
                TextView userNameTv = layout.findViewById(R.id.user_name_tv);
                TextView timeTv = layout.findViewById(R.id.reply_time);
                ImageView profileIv = layout.findViewById(R.id.user_profile_iv);
                commentTv.setText(item.getMessage());
                userNameTv.setText(item.getUsername());
                timeTv.setText(TimeUtil.calculateCommentTime(item.getCreationTime()));
                Glide.with(context).load(""+path+"/"+item.getProfileImage())
                        .placeholder(context.getResources().getDrawable(R.drawable.ic_default_user))
                        .into(profileIv);
                holder.replyListLayout.addView(layout);
            }
        }else {
            holder.replyListLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (commentsModelArrayList == null) {
            return 0;
        }
        return commentsModelArrayList.size();
    }

    public void resetData() {
        commentsModelArrayList.clear();
        notifyDataSetChanged();
    }

    public void swapData(List<CommentList> items) {
        if (items != null) {
            commentsModelArrayList.addAll(items);
            notifyDataSetChanged();
        } else {
            commentsModelArrayList = null;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, tv_view_more, text_view_message_item_layout_chat_user, text_view_date_time_item_layout_chat_user, replyCountBtn;
        CircularImageView image_view_icon_user_item_layout_chat_user;
        ImageView replyBtn;
        LinearLayout replyListLayout;
        private ExpandableTextView expandableTextView;

        public MyViewHolder(View view) {
            super(view);
            expandableTextView = (ExpandableTextView) view.findViewById(R.id.expandableTextView);
            tv_view_more = (TextView) view.findViewById(R.id.tv_view_more);
            text_view_message_item_layout_chat_user = (TextView) view.findViewById(R.id.text_view_message_item_layout_chat_user);
            text_view_date_time_item_layout_chat_user = (TextView) view.findViewById(R.id.text_view_date_time_item_layout_chat_user);
            image_view_icon_user_item_layout_chat_user = (CircularImageView) view.findViewById(R.id.image_view_icon_user_item_layout_chat_user);
            replyBtn = (ImageView) view.findViewById(R.id.reply_btn);
            replyListLayout = (LinearLayout) view.findViewById(R.id.reply_layout);
            replyCountBtn = (TextView) view.findViewById(R.id.reply_count_btn);
        }
    }
}