package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Api;
import com.google.api.Distribution;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.db.DatabaseManager;
import com.vision.imagine.fragments.ExploreIdeaFragment;
import com.vision.imagine.fragments.MyIdeasFragment;
import com.vision.imagine.networking.RetrofitClient;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;
import com.vision.imagine.utils.TimeUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MyIdeaAdapter extends RecyclerView.Adapter<MyIdeaAdapter.MyViewHolder> {
    Context context;
    public static String profilePath = RetrofitClient.BASE_URL + "getImage/";
    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
    OnItemClickedListener mCallback;
    public List<MyIdeasModel> IdeasModelsList;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public MyIdeaAdapter(List<MyIdeasModel> IdeasModelsList, OnItemClickedListener mCallback, Context context) {
        this.IdeasModelsList = IdeasModelsList;
        this.mCallback = mCallback;
        this.context = context;
    }

    public ArrayList<MyIdeasModel> getIdeas(){
        return (ArrayList<MyIdeasModel>) this.IdeasModelsList;
    }

    @Override
    public MyIdeaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ideas_single_row, parent, false);
        return new MyIdeaAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyIdeaAdapter.MyViewHolder holder, int position) {
        MyIdeasModel ideasModel = IdeasModelsList.get(position);
        holder.title.setText(ideasModel.getTitle());
        holder.tv_descrption.setText(ideasModel.getCaption());

        //holder.tv_author.setText("Author: " + ideasModel.getAuthor());

        if(ideasModel.getCategory()!=null && ideasModel.getCategory().toString().contains(",")){
            String[] cat = ideasModel.getCategory().toString().split(",");
            String appFilters = SharedPrefUtils.getStringData(Constants.APPLIED_FILTERS_NAMES);
            if(!appFilters.equals("") && appFilters.contains(",")){
                String[] appCat = appFilters.split(",");
                holder.category.setText("" + cat[0]);
                for(String cat_ : cat){
                    for(String appliedCat : appCat){
                        if(cat_.toLowerCase().equals(appliedCat.toLowerCase())){
                            holder.category.setText("" + appliedCat);
                            break;
                        }
                    }
                }
            }else {
                holder.category.setText("" + cat[0]);
                for(String cat_ : cat){
                    if(cat_.toLowerCase().equals(appFilters.toLowerCase())){
                        holder.category.setText("" + cat_);
                        break;
                    }
                }
            }
        }else {
            holder.category.setText("" + ideasModel.getCategory().toString());
        }
        holder.tv_ago.setVisibility(View.VISIBLE);
        try {
            holder.tv_ago.setText(TimeUtil.calculateAge2(ideasModel.getIdeaCreationDate()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.tv_author.setVisibility(View.VISIBLE);
        if(ideasModel.getAccessTo()==null || ideasModel.getAccessTo().equals("")){
            holder.tv_ago.setText("Unpublished");
            holder.tv_ago.setVisibility(View.VISIBLE);
            holder.likeViewPanel.setVisibility(View.INVISIBLE);
        }else if(ideasModel.getAccessTo().equals("public")){
            holder.tv_author.setText("Published - Public");
            holder.tv_like_count.setText(ideasModel.getLikeCount() + "");
            if (ideasModel.getViews() != null) {
                holder.view_count.setText("" + ideasModel.getViews());
            } else {
                holder.view_count.setText("0");
            }
            holder.likeViewPanel.setVisibility(View.VISIBLE);
        }else if(ideasModel.getAccessTo().equals("private")){
            holder.tv_author.setText("Published - Private");
            holder.likeViewPanel.setVisibility(View.INVISIBLE);
        }else if(ideasModel.getAccessTo().equals("unpublished")){
            holder.tv_ago.setText("Unpublished");
            holder.tv_ago.setVisibility(View.VISIBLE);
            holder.likeViewPanel.setVisibility(View.INVISIBLE);
            holder.tv_author.setVisibility(View.INVISIBLE);
            Glide.with(context).load(ideasModel.getIdeaImage())
                    .placeholder(R.drawable.ic_default).into(holder.thumbnail);
        }else {
            holder.tv_ago.setVisibility(View.VISIBLE);
        }

        String userId = SharedPrefUtils.getStringData(Constants.USER_ID);
        int color = generator.getRandomColor();
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .withBorder(4) /* thickness in px */
                .endConfig()
                .buildRoundRect(String.valueOf(ideasModel.getTitle().charAt(0)).toUpperCase(), color, 10);
        //if(ideasModel.getIdeaImage()!=null && !ideasModel.getIdeaImage().equals("")){
        String path = profilePath + ideasModel.getUserId() + "/" + ideasModel.getIdeaId() + "/" + ideasModel.getIdeaImage();
        Glide.with(context).load(path)
                .placeholder(R.drawable.ic_default).into(holder.thumbnail);
        //}else {
        //    holder.thumbnail.setBackground(drawable);
        //}
        /*if (ideasModel.getFavCount() == 1) {
            holder.iv_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            holder.iv_fav.setImageResource(R.drawable.ic_favorite_border_24px);
        }*/

        holder.iv_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ideasModel.getFavCount() == 1) {
                    holder.iv_fav.setImageResource(R.drawable.ic_favorite_border_24px);
                    CommonApiUtils.updateFav(false, ideasModel.getCreatedBy(), ideasModel.getIdeaId());
                } else {
                    holder.iv_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
                    CommonApiUtils.updateFav(true, ideasModel.getCreatedBy(), ideasModel.getIdeaId());
                }
            }
        });
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ideasModel.getAccessTo().equals("unpublished")){
                    ideasModel.setDraft(true);
                   // Constants.currentIdeaId = ideasModel.getIdeaId();
                    //Constants.postIdeaModel = DatabaseManager.getInstance(context).getIdeaById(String.valueOf(ideasModel.getIdeaId()));
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", ideasModel);
                bundle.putSerializable("position", position);
                mCallback.clickedItem(bundle);
            }
        });
        if(MyIdeasFragment.isCalledFromSharingActivity && ideasModel.isSelected()){
            holder.card_view.setCardBackgroundColor(context.getResources().getColor(R.color.quotes_box));
        }else {
            holder.card_view.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        }
        /*if(ideasModel.isDraft()) {
            holder.tv_author.setText("Draft");
            holder.likeViewPanel.setVisibility(View.INVISIBLE);
            holder.tv_ago.setVisibility(View.GONE);
            Glide.with(context).load(ideasModel.getIdeaImage())
                    .placeholder(R.drawable.ic_default).into(holder.thumbnail);
        }else{
            holder.tv_ago.setVisibility(View.VISIBLE);
        }*/

    }

    @Override
    public int getItemCount() {
        if (IdeasModelsList == null) {
            return 0;
        }
        return IdeasModelsList.size();
    }

    public void resetData() {
        IdeasModelsList.clear();
       // notifyDataSetChanged();
    }

    public void sortByName(boolean isAsc) {
        Collections.sort(IdeasModelsList, new Comparator<MyIdeasModel>() {
            public int compare(MyIdeasModel obj1, MyIdeasModel obj2) {
                if (isAsc) {
                    // ## Ascending order
                    return obj1.getTitle().compareToIgnoreCase(obj2.getTitle()); // To compare string values
                    // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                } else {
                    // ## Descending order
                    return obj2.getTitle().compareToIgnoreCase(obj1.getTitle()); // To compare string values
                    // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
                }
            }
        });
        notifyDataSetChanged();
    }

    public void sortByDate(boolean isAsc) {
        Collections.sort(IdeasModelsList, new Comparator<MyIdeasModel>() {
            public int compare(MyIdeasModel obj1, MyIdeasModel obj2) {
                try {
                    Date date1 = format.parse(obj1.getIdeaCreationDate());
                    Date date2 = format.parse(obj2.getIdeaCreationDate());
                    Long d1 = date1.getTime();
                    Long d2 = date2.getTime();
                    if (isAsc) {
                        // ## Ascending order
                        return d1.compareTo(d2);
                    } else {
                        // ## Descending order
                        return d2.compareTo(d1);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        notifyDataSetChanged();
    }

    public void swapData(List<MyIdeasModel> items) {
        if (items != null) {
            List<MyIdeasModel> listTwoCopy = new ArrayList<>(items);
            listTwoCopy.removeAll(IdeasModelsList);
            IdeasModelsList.addAll(listTwoCopy);
            notifyDataSetChanged();
        } else {
            //IdeasModelsList = null;
        }
    }
    public void swapFilterData(List<MyIdeasModel> items) {
        if (items != null) {
            if(IdeasModelsList!=null) IdeasModelsList.clear();
            IdeasModelsList.addAll(items);
            notifyDataSetChanged();

        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, tv_descrption, tv_like_count, tv_author, category, tv_ago, view_count;
        ImageView thumbnail, iv_fav;
        CardView card_view;
        LinearLayout likeViewPanel;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            tv_descrption = (TextView) view.findViewById(R.id.tv_descrption);
            tv_like_count = (TextView) view.findViewById(R.id.tv_like_count);
            tv_author = (TextView) view.findViewById(R.id.tv_author);
            category = (TextView) view.findViewById(R.id.category);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            iv_fav = (ImageView) view.findViewById(R.id.iv_fav);
            tv_ago = (TextView) view.findViewById(R.id.tv_ago);
            view_count = (TextView) view.findViewById(R.id.tv_views_count);
            card_view = (CardView) view.findViewById(R.id.card_view);
            likeViewPanel = (LinearLayout) view.findViewById(R.id.like_view_panel);
        }
    }
}
