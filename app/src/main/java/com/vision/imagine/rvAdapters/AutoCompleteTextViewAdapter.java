package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.pojos.MyIdeasModel;

import java.util.List;

public class AutoCompleteTextViewAdapter extends RecyclerView.Adapter<AutoCompleteTextViewAdapter.MyViewHolder> {
    Context context;
    OnItemClickedListener mCallback;
    private List<MyIdeasModel> IdeasModelsList;

    public AutoCompleteTextViewAdapter(List<MyIdeasModel> IdeasModelsList, OnItemClickedListener mCallback, Context context) {
        this.IdeasModelsList = IdeasModelsList;
        this.mCallback = mCallback;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_auto_complete_single_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyIdeasModel ideasModel = IdeasModelsList.get(position);

        holder.txtUserName.setText(ideasModel.getTitle());
        String path = ideasModel.getUserId() + "/" + ideasModel.getIdeaId() + "/" + ideasModel.getIdeaImage();
        Glide.with(context).load(path)
                .placeholder(R.drawable.ic_default_user).error(R.drawable.ic_default_user).into(holder.imgUserProfile);

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", ideasModel);
                mCallback.clickedItem(bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (IdeasModelsList == null) {
            return 0;
        }
        return IdeasModelsList.size();
    }

    public void resetData() {
        IdeasModelsList.clear();
        notifyDataSetChanged();
    }

    public void swapData(List<MyIdeasModel> items) {
        if (items != null) {
            IdeasModelsList.addAll(items);
            notifyDataSetChanged();
        } else {
            IdeasModelsList = null;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtUserName;
        CircularImageView imgUserProfile;
        LinearLayout container;

        public MyViewHolder(View view) {
            super(view);
            txtUserName = (TextView) view.findViewById(R.id.txtNotificationTitle);
            imgUserProfile = (CircularImageView) view.findViewById(R.id.imgUserProfile);
            container = (LinearLayout) view.findViewById(R.id.container);
        }
    }
}