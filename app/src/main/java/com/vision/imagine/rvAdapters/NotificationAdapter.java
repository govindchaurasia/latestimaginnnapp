package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.pojos.FriendNotifyListItem;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    Context context;
    public static String profilePath = "";
    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
    OnItemClickedListener mCallback;
    private List<FriendNotifyListItem> friendsItemList;

    public NotificationAdapter(List<FriendNotifyListItem> friendsItemList, OnItemClickedListener mCallback, Context context) {
        this.friendsItemList = friendsItemList;
        this.mCallback = mCallback;
        this.context = context;
//        profilePath = "http://imaginnn.com:443/images/profilepic/";
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_notification, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FriendNotifyListItem friendNotifyListItem = friendsItemList.get(position);
        String userId = SharedPrefUtils.getStringData(Constants.USER_ID);
//        String path = profilePath + ideasModel.getUserId()+"/"+ideasModel.getIdeaId()+"/"+ ideasModel.getIdeaImage();
        String path = profilePath + friendNotifyListItem.getSenderProfileImage();

        holder.txtNotificationTitle.setText(friendNotifyListItem.getSenderName());
        Glide.with(context).load(path)
                .placeholder(R.drawable.profile_pic_place_holder).into(holder.imgUserProfile);

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("friendNotifyListItem", friendNotifyListItem);
                mCallback.clickedItem(bundle);
            }
        });

        if (!friendNotifyListItem.getUserId().isEmpty()) {
            holder.containerAcceptRejectButton.setVisibility(View.VISIBLE);

            holder.btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("actionType", Constants.ACTION_TYPE_ACCEPT_REQUEST);
                    bundle.putInt("position", position);
                    bundle.putSerializable("friendNotifyListItem", friendNotifyListItem);
                    mCallback.clickedItem(bundle);
                }
            });

            holder.btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("actionType", Constants.ACTION_TYPE_REJECT_REQUEST);
                    bundle.putInt("position", position);
                    bundle.putSerializable("friendNotifyListItem", friendNotifyListItem);
                    mCallback.clickedItem(bundle);
                }
            });
        } else {
            holder.containerAcceptRejectButton.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (friendsItemList == null) {
            return 0;
        }
        return friendsItemList.size();
    }

    public void resetData() {
        friendsItemList.clear();
        notifyDataSetChanged();
    }

    public void swapData(List<FriendNotifyListItem> items, String profilePath) {
        this.profilePath = profilePath + "/";

        if (items != null) {
            friendsItemList.addAll(items);
            notifyDataSetChanged();
        } else {
            friendsItemList = null;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNotificationTitle;
        CircularImageView imgUserProfile;
        RelativeLayout container;
        LinearLayout containerAcceptRejectButton;
        TextView btnAccept, btnReject;

        public MyViewHolder(View view) {
            super(view);
            txtNotificationTitle = view.findViewById(R.id.txtNotificationTitle);
            imgUserProfile = view.findViewById(R.id.imgUserProfile);
            container = view.findViewById(R.id.container);
            containerAcceptRejectButton = view.findViewById(R.id.containerAcceptRejectButton);
            btnAccept = view.findViewById(R.id.btnAccept);
            btnReject = view.findViewById(R.id.btnReject);
        }
    }
}