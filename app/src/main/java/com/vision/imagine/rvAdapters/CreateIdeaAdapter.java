package com.vision.imagine.rvAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.pojos.CreateIdeaDataSet;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.ItemMoveCallback;
import com.vision.imagine.utils.Navigator;
import com.vision.imagine.callbacks.StartDragListener;

import java.util.ArrayList;
import java.util.Collections;

public class CreateIdeaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemMoveCallback.ItemTouchHelperContract {

    private final StartDragListener mStartDragListener;
    public OnItemClickedListener onItemClickedListener;
    private Context context;
    private ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList_;
    private Navigator navigator;

    public CreateIdeaAdapter(ArrayList<CreateIdeaDataSet> createIdeaDataSetArrayList, Context context, StartDragListener startDragListener,  OnItemClickedListener onItemClickedListener) {
        this.createIdeaDataSetArrayList_ = createIdeaDataSetArrayList;
        mStartDragListener = startDragListener;
        this.context = context;
        this.navigator = navigator;
        this.onItemClickedListener = onItemClickedListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Constants.UPLOAD:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_single_item, parent, false);
                return new CreateIdeaAdapter.UploadViewHolder(view);

            case Constants.CAPTURE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.capture_single_item, parent, false);
                return new CreateIdeaAdapter.CaptureViewHolder(view);

            case Constants.RECORD:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.record_single_item, parent, false);
                return new CreateIdeaAdapter.RecordViewHolder(view);

            case Constants.WRITE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.write_single_item, parent, false);
                return new CreateIdeaAdapter.WriteViewHolder(view);

            case Constants.DRAW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.draw_single_item, parent, false);
                return new CreateIdeaAdapter.WriteViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        CreateIdeaDataSet categories = createIdeaDataSetArrayList_.get(position);
        View rowView = null;
        View delete = null;
        View textView = null;
        if (holder instanceof UploadViewHolder) {
            //do nothing since its a static data
            ((UploadViewHolder) holder).setData(categories);
            rowView = ((UploadViewHolder) holder).upload_container;
            delete = ((UploadViewHolder) holder).iv_delete;
        } else if (holder instanceof CaptureViewHolder) {
            ((CaptureViewHolder) holder).setData(categories);
            rowView = ((CaptureViewHolder) holder).upload_container;
            delete = ((CaptureViewHolder) holder).iv_delete;
        } else if (holder instanceof CreateViewHolder) {
            ((CreateViewHolder) holder).setData(categories);
            rowView = ((CreateViewHolder) holder).upload_container;
            delete = ((CreateViewHolder) holder).iv_delete;
        } else if (holder instanceof RecordViewHolder) {
            ((RecordViewHolder) holder).setData(categories);
            rowView = ((RecordViewHolder) holder).upload_container;
            delete = ((RecordViewHolder) holder).iv_delete;
        } else if (holder instanceof WriteViewHolder) {
            ((WriteViewHolder) holder).setData(categories);
            rowView = ((WriteViewHolder) holder).upload_container;
            delete = ((WriteViewHolder) holder).iv_delete;
            textView = ((WriteViewHolder) holder).uploaded_file_name;
        } else if (holder instanceof DrawViewHolder) {
            ((DrawViewHolder) holder).setData(categories);
            rowView = ((DrawViewHolder) holder).upload_container;
            delete = ((DrawViewHolder) holder).iv_delete;
        }

        if (rowView != null)
            rowView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() ==
                            MotionEvent.ACTION_DOWN) {
                        mStartDragListener.requestDrag(holder);
                    }
                    return false;
                }
            });
        if (delete != null)
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Do your Yes progress
                                    removeAt(position);
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    //Do your No progress
                                    break;
                            }
                        }
                    };
                    AlertDialog.Builder ab = new AlertDialog.Builder(context);
                    ab.setMessage("Do you wish to delete this block?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();

                }
            });
        if(textView!=null){
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle data = new Bundle();
                    data.putSerializable("textData", categories);
                    data.putInt("position", position);
                    onItemClickedListener.clickedItem(data);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        int count = createIdeaDataSetArrayList_ == null ? 0 : createIdeaDataSetArrayList_.size();
        MainActivity.endElementCount = count;
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        CreateIdeaDataSet createIdeaDataSet = createIdeaDataSetArrayList_.get(position);
        int type = createIdeaDataSet.getType();
        if (type == Constants.CAPTURE) {
            return Constants.CAPTURE;
        } else if (type == Constants.RECORD) {
            return Constants.RECORD;
        } else if (type == Constants.UPLOAD) {
            return Constants.UPLOAD;
        } else if (type == Constants.WRITE) {
            return Constants.WRITE;
        } else if (type == Constants.DRAW) {
            return Constants.DRAW;
        } else {
            return -1;
        }
    }

    public void setData(ArrayList<CreateIdeaDataSet> data) {
        createIdeaDataSetArrayList_ = data;
        notifyDataSetChanged();
    }

    public void reSetData() {
        createIdeaDataSetArrayList_.clear();
        //notifyDataSetChanged();
    }

    public void removeAt(int position) {
        createIdeaDataSetArrayList_.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, createIdeaDataSetArrayList_.size());
    }

    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(createIdeaDataSetArrayList_, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(createIdeaDataSetArrayList_, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        }, 300);

    }

    public ArrayList<CreateIdeaDataSet> getOrderedList() {
        return createIdeaDataSetArrayList_;
    }

    @Override
    public void onRowSelected(RecyclerView.ViewHolder holder) {
        holder.itemView.setBackgroundColor(Color.GRAY);
    }

    @Override
    public void onRowClear(RecyclerView.ViewHolder holder) {
        holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_blue));
    }

    public static class UploadViewHolder extends RecyclerView.ViewHolder {
        View rowView;
        RelativeLayout upload_container;
        AppCompatImageView iv_delete;
        TextView uploaded_file_name;
        AppCompatImageView uploadIv;
        private Context context;

        public UploadViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            context = itemView.getContext();
            upload_container = itemView.findViewById(R.id.upload_container);
            iv_delete = itemView.findViewById(R.id.iv_delete_image_add_design_idea);
            uploaded_file_name = itemView.findViewById(R.id.uploaded_file_name);
            uploadIv = itemView.findViewById(R.id.upload);
        }

        public void setData(final CreateIdeaDataSet categories) {
            uploaded_file_name.setText(categories.getFileName());
            if(categories.getFile()!=null) {
                Glide.with(context).load(categories.getFileUri()).placeholder(context.getResources().getDrawable(R.drawable.ic_upload_2)).into(uploadIv);
            }else if(categories.getFileUrl()!=null){
                Glide.with(context).load(categories.getFileUrl()).placeholder(context.getResources().getDrawable(R.drawable.ic_upload_2)).into(uploadIv);
            }else {
                Glide.with(context).load(categories.getFileUrl()).placeholder(context.getResources().getDrawable(R.drawable.ic_upload_2)).into(uploadIv);
            }
        }
    }

    public static class CaptureViewHolder extends RecyclerView.ViewHolder {
        View rowView;
        RelativeLayout upload_container;
        AppCompatImageView iv_delete;
        TextView uploaded_file_name;
        AppCompatImageView uploadIv;
        private Context context;

        public CaptureViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            context = itemView.getContext();
            upload_container = itemView.findViewById(R.id.upload_container);
            iv_delete = itemView.findViewById(R.id.iv_delete_image_add_design_idea);
            uploaded_file_name = itemView.findViewById(R.id.uploaded_file_name);
            uploadIv = itemView.findViewById(R.id.upload);
        }

        public void setData(final CreateIdeaDataSet categories) {
            uploaded_file_name.setText(categories.getFileName());
            if(categories.getFileUri()!=null) {
                Glide.with(context).load(categories.getFileUri()).placeholder(context.getResources().getDrawable(R.drawable.ic_upload_2)).into(uploadIv);
            }else if(categories.getFileUrl()!=null){
                Glide.with(context).load(categories.getFileUrl()).placeholder(context.getResources().getDrawable(R.drawable.ic_upload_2)).into(uploadIv);
            }else {
                Glide.with(context).load(Uri.fromFile(categories.getFile())).placeholder(context.getResources().getDrawable(R.drawable.ic_upload_2)).into(uploadIv);
            }
        }
    }

    public static class RecordViewHolder extends RecyclerView.ViewHolder {
        View rowView;
        RelativeLayout upload_container;
        AppCompatImageView iv_delete;
        TextView uploaded_file_name;
        private Context context;

        public RecordViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            context = itemView.getContext();
            upload_container = itemView.findViewById(R.id.upload_container);
            iv_delete = itemView.findViewById(R.id.iv_delete_image_add_design_idea);
            uploaded_file_name = itemView.findViewById(R.id.uploaded_file_name);
        }

        public void setData(final CreateIdeaDataSet categories) {
            uploaded_file_name.setText(categories.getFileName());
        }
    }

    public static class WriteViewHolder extends RecyclerView.ViewHolder {
        View rowView;
        RelativeLayout upload_container;
        AppCompatImageView iv_delete;
        TextView uploaded_file_name;
        private Context context;

        public WriteViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            context = itemView.getContext();
            upload_container = itemView.findViewById(R.id.upload_container);
            iv_delete = itemView.findViewById(R.id.iv_delete_image_add_design_idea);
            uploaded_file_name = itemView.findViewById(R.id.uploaded_file_name);
        }

        public void setData(final CreateIdeaDataSet categories) {
            uploaded_file_name.setText(categories.getFileName());
        }
    }

    public static class CreateViewHolder extends RecyclerView.ViewHolder {
        View rowView;
        RelativeLayout upload_container;
        AppCompatImageView iv_delete;
        TextView uploaded_file_name;
        private Context context;

        public CreateViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            context = itemView.getContext();
            upload_container = itemView.findViewById(R.id.upload_container);
            iv_delete = itemView.findViewById(R.id.iv_delete_image_add_design_idea);
            uploaded_file_name = itemView.findViewById(R.id.uploaded_file_name);
        }

        public void setData(final CreateIdeaDataSet categories) {
            uploaded_file_name.setText(categories.getFileName());
        }
    }

    public static class DrawViewHolder extends RecyclerView.ViewHolder {
        View rowView;
        RelativeLayout upload_container;
        AppCompatImageView iv_delete;
        TextView uploaded_file_name;
        private Context context;

        public DrawViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            context = itemView.getContext();
            upload_container = itemView.findViewById(R.id.upload_container);
            iv_delete = itemView.findViewById(R.id.iv_delete_image_add_design_idea);
            uploaded_file_name = itemView.findViewById(R.id.uploaded_file_name);
        }

        public void setData(final CreateIdeaDataSet categories) {
            uploaded_file_name.setText(categories.getFileName());
        }
    }
}
