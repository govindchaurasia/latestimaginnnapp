package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.pojos.FilterUserItem;
import com.vision.imagine.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter {
    private ArrayList<FilterUserItem> filterUserItemArrayList;
    private Context mContext;
    private int itemLayout;
    private ListFilter listFilter = new ListFilter();
    private List<FilterUserItem> filterUserItemArrayList2;
    private OnItemClickedListener mCallback;
    private Drawable placeHolderDrawable;
    public static String profilePath = "";

    /**
     * @param context      Context of your Activity
     * @param resource     Single Item Layout Id
     * @param storeDataLst Data List
     */
    public CustomArrayAdapter(Context context, int resource, ArrayList<FilterUserItem> storeDataLst, OnItemClickedListener mCallback, String profilePath) {
        super(context, resource, storeDataLst);
        mContext = context;
        filterUserItemArrayList = storeDataLst;
        itemLayout = resource;
        this.mCallback = mCallback;
        if (profilePath != null && !profilePath.isEmpty())
            this.profilePath = profilePath;
        else
            this.profilePath = "http://imaginnn.com:443/images/profilepic/";
        placeHolderDrawable = CommonUtils.tintMyDrawable(context, context.getResources().getDrawable(R.drawable.profile_pic_place_holder));
    }

    /**
     * Handle your view counting or size based of list size
     *
     * @return return total item size
     */
    @Override
    public int getCount() {
        return filterUserItemArrayList.size();
    }

    /**
     * return item based on click position
     *
     * @param position item position in view
     * @return item
     */
    @Override
    public FilterUserItem getItem(int position) {
        return filterUserItemArrayList.get(position);
    }

    /**
     * Used in filter operation
     *
     * @return listFilter
     */
    @NonNull
    @Override
    public Filter getFilter() {
        return listFilter;
    }

    /**
     * View customization perform here with your custom view and there elements
     *
     * @param position Position of current item
     * @param view     View object
     * @param parent   ViewGroup
     * @return View Object
     */
    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(itemLayout, parent, false);
        }

        FilterUserItem filterUserItem = filterUserItemArrayList.get(position);

        ((TextView) view.findViewById(R.id.txtNotificationTitle)).setText(filterUserItem.getUsername());

        String path = profilePath + "/" + filterUserItem.getUserProfileImage();
//        Glide.with(mContext).load(path)
//                .placeholder(R.color.lightGrey).error(R.drawable.profile_pic_place_holder).into(((CircularImageView) view.findViewById(R.id.imgUserProfile)));
        Glide.with(mContext).load(path)
                .placeholder(R.color.lightGrey).error(placeHolderDrawable).into(((CircularImageView) view.findViewById(R.id.imgUserProfile)));

        view.findViewById(R.id.container).setTag(filterUserItem.getUserId());
        view.findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = (String) view.getTag();
                Bundle bundle = new Bundle();
                bundle.putString("userId", userId);
                mCallback.clickedItem(bundle);
            }
        });

        return view;
    }

    public class ListFilter extends Filter {
        private Object lock = new Object();

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();
            if (filterUserItemArrayList2 == null) {
                synchronized (lock) {
                    filterUserItemArrayList2 = new ArrayList<>(filterUserItemArrayList);
                }
            }
            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    results.values = filterUserItemArrayList2;
                    results.count = filterUserItemArrayList2.size();
                }
            } else {
                final String searchStrLowerCase = prefix.toString().toLowerCase();
                ArrayList<FilterUserItem> matchValues = new ArrayList<FilterUserItem>();
                for (FilterUserItem dataItem : filterUserItemArrayList2) {
                    if (dataItem.getUsername().toLowerCase().startsWith(searchStrLowerCase)) {
                        matchValues.add(dataItem);
                    }
                }
                results.values = matchValues;
                results.count = matchValues.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                filterUserItemArrayList = (ArrayList<FilterUserItem>) results.values;
            } else {
                filterUserItemArrayList = null;
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}