package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.ItemKeyProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.pojos.FriendsItem;
import com.vision.imagine.utils.CommonUtils;
import com.vision.imagine.utils.Constants;
import com.vision.imagine.utils.SharedPrefUtils;

import java.util.List;

public class MyConnectionsAdapter extends RecyclerView.Adapter<MyConnectionsAdapter.MyViewHolder> {
    Context context;
    public static String profilePath = "";
    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
    OnItemClickedListener mCallback;
    private List<FriendsItem> friendsItemList;
    private Drawable placeHolderDrawable;
    private SelectionTracker<Long> selectionTracker;
    private int selectedPosition;

    public MyConnectionsAdapter(List<FriendsItem> friendsItemList, OnItemClickedListener mCallback, Context context) {
        this.friendsItemList = friendsItemList;
        this.mCallback = mCallback;
        this.context = context;
        profilePath = "http://imaginnn.com:443/images/";
        placeHolderDrawable = CommonUtils.tintMyDrawable(context, context.getResources().getDrawable(R.drawable.profile_pic_place_holder));
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_auto_complete_single_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FriendsItem friendsItem = friendsItemList.get(position);
        String userId = SharedPrefUtils.getStringData(Constants.USER_ID);
//        String path = profilePath + ideasModel.getUserId()+"/"+ideasModel.getIdeaId()+"/"+ ideasModel.getIdeaImage();
        String path = profilePath + "/" + friendsItem.getProfileImageName();

        holder.txtUserName.setText(friendsItem.getUsername());
        Glide.with(context).load(path)
                .placeholder(placeHolderDrawable).into(holder.imgUserProfile);

        if (friendsItem.getStatus() != null && friendsItem.getStatus().trim().equals("1")) {
            holder.txtUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            holder.container.setTag(true);
            holder.mView.setTag(true);
        } else {
            holder.txtUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_pending_round, 0);
            holder.container.setTag(false);
            holder.mView.setTag(false);
        }

        holder.imgUserProfile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                selectedPosition = holder.details.getPosition();
                return true;
            }
        });

        holder.details.position = position;
        if (selectionTracker != null) {
            if (selectionTracker.isSelected(holder.details.getSelectionKey())) {
                holder.imgCheck.setVisibility(View.VISIBLE);
                holder.mView.setActivated(true);
            } else {
                holder.imgCheck.setVisibility(View.GONE);
                holder.mView.setActivated(false);
            }
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                boolean isRequestApproved = (boolean) view.getTag();
                if (isRequestApproved) {
//              OPEN CHAT SCREEN WITH THIS USER
                    bundle.putSerializable("friendsItem", friendsItem);
                } else {
//              OPEN AUTHOR PROFILE PAGE
                    bundle.putString("userId", friendsItem.getUserId());
                }
                mCallback.clickedItem(bundle);
            }
        });
    }

    public int getCurrentlySelectedItemPosition() {
        return selectedPosition;
    }

    @Override
    public int getItemCount() {
        if (friendsItemList == null) {
            return 0;
        }
        return friendsItemList.size();
    }

    public void resetData() {
        friendsItemList.clear();
        notifyDataSetChanged();
    }

    public void swapData(List<FriendsItem> items, String profilePath) {
        this.profilePath = profilePath;

        if (items != null) {
            friendsItemList.addAll(items);
            notifyDataSetChanged();
        } else {
            friendsItemList = null;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView imgCheck;
        public TextView txtUserName;
        CircularImageView imgUserProfile;
        LinearLayout container;
        private Details details;

        public MyViewHolder(View view) {
            super(view);
            mView = view;
            txtUserName = view.findViewById(R.id.txtNotificationTitle);
            imgUserProfile = view.findViewById(R.id.imgUserProfile);
            container = view.findViewById(R.id.container);
            imgCheck = view.findViewById(R.id.imgCheck);
            details = new Details();

//            CommonUtils.tintMyDrawable(context, imgCheck.getDrawable());

            RelativeLayout.LayoutParams parameter = (RelativeLayout.LayoutParams) imgUserProfile.getLayoutParams();
            parameter.setMargins(0, parameter.topMargin, parameter.rightMargin, parameter.bottomMargin); // left, top, right, bottom
            imgUserProfile.setLayoutParams(parameter);

            LinearLayout.LayoutParams parameter2 = (LinearLayout.LayoutParams) txtUserName.getLayoutParams();
            parameter2.setMargins(parameter2.leftMargin, parameter2.topMargin, 0, parameter2.bottomMargin); // left, top, right, bottom
            txtUserName.setLayoutParams(parameter2);
        }

        Details getItemDetails() {
            return details;
        }
    }

    public void setSelectionTracker(SelectionTracker<Long> selectionTracker) {
        this.selectionTracker = selectionTracker;
    }

    public Selection<Long> getAllSelectionTrackerItems() {
        return this.selectionTracker.getSelection();
    }

    public static class Details extends ItemDetailsLookup.ItemDetails<Long> {

        long position;

        public Details() {
        }

        @Override
        public int getPosition() {
            return (int) position;
        }

        @Nullable
        @Override
        public Long getSelectionKey() {
            return position;
        }

        @Override
        public boolean inSelectionHotspot(@NonNull MotionEvent e) {
            return false;
        }
    }

    public static class KeyProvider extends ItemKeyProvider<Long> {

        public KeyProvider(RecyclerView.Adapter adapter) {
            super(ItemKeyProvider.SCOPE_MAPPED);
        }

        @Nullable
        @Override
        public Long getKey(int position) {
            return (long) position;
        }

        @Override
        public int getPosition(@NonNull Long key) {
            long value = key;
            return (int) value;
        }
    }

    public static class DetailsLookup extends ItemDetailsLookup<Long> {

        private RecyclerView recyclerView;

        public DetailsLookup(RecyclerView recyclerView) {
            this.recyclerView = recyclerView;
        }

        @Nullable
        @Override
        public ItemDetails<Long> getItemDetails(@NonNull MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(view);
                if (viewHolder instanceof RecyclerView.ViewHolder) {
                    return ((MyViewHolder) viewHolder).getItemDetails();
                }
            }
            return null;
        }
    }

    public static class Predicate extends SelectionTracker.SelectionPredicate<Long> {

        @Override
        public boolean canSetStateForKey(@NonNull Long key, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSetStateAtPosition(int position, boolean nextState) {
            return true;
        }

        @Override
        public boolean canSelectMultiple() {
            return true;
        }
    }
}