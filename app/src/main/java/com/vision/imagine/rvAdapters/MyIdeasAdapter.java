package com.vision.imagine.rvAdapters;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.vision.imagine.R;
import com.vision.imagine.callbacks.OnItemClickedListener;
import com.vision.imagine.pojos.MyIdeasModel;
import com.vision.imagine.utils.CommonApiUtils;
import com.vision.imagine.utils.TimeUtil;

import java.util.List;

public class MyIdeasAdapter extends RecyclerView.Adapter<MyIdeasAdapter.MyViewHolder> {

    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
    OnItemClickedListener mCallback;
    private List<MyIdeasModel> IdeasModelsList;

    public MyIdeasAdapter(List<MyIdeasModel> IdeasModelsList, OnItemClickedListener mCallback) {
        this.IdeasModelsList = IdeasModelsList;
        this.mCallback = mCallback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ideas_single_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyIdeasModel ideasModel = IdeasModelsList.get(position);
        holder.title.setText(ideasModel.getTitle());
        holder.tv_descrption.setText(ideasModel.getCaption());
        holder.tv_like_count.setText(ideasModel.getLikeCount() + "");
        holder.tv_author.setText("Author: "+ideasModel.getCreatedBy() + "");

        try {
            holder.tv_ago.setText(TimeUtil.calculateAge2(ideasModel.getIdeaCreationDate()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        int color = generator.getRandomColor();
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .withBorder(4) /* thickness in px */
                .endConfig()
                .buildRoundRect(String.valueOf(ideasModel.getTitle().charAt(0)).toUpperCase(), color, 10);

        holder.thumbnail.setBackground(drawable);

        if (ideasModel.getFavCount() == 1) {
            holder.iv_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            holder.iv_fav.setImageResource(R.drawable.ic_favorite_border_24px);
        }

        holder.iv_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ideasModel.getFavCount() == 1) {
                    holder.iv_fav.setImageResource(R.drawable.ic_favorite_border_24px);
                    CommonApiUtils.updateFav(false, ideasModel.getCreatedBy(), ideasModel.getIdeaId());
                } else {
                    holder.iv_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
                    CommonApiUtils.updateFav(true, ideasModel.getCreatedBy(), ideasModel.getIdeaId());
                }
            }
        });
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("ideaObj", ideasModel);
                mCallback.clickedItem(bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (IdeasModelsList == null) {
            return 0;
        }
        return IdeasModelsList.size();
    }

    public void resetData() {
        IdeasModelsList.clear();
        notifyDataSetChanged();
    }

    public void swapData(List<MyIdeasModel> items) {
        if (items != null) {
            IdeasModelsList.addAll(items);
            notifyDataSetChanged();

        } else {
            IdeasModelsList = null;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, tv_descrption, tv_like_count, tv_author, category, tv_ago;
        ImageView thumbnail, iv_fav;
        CardView card_view;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            tv_descrption = (TextView) view.findViewById(R.id.tv_descrption);
            tv_like_count = (TextView) view.findViewById(R.id.tv_like_count);
            tv_author = (TextView) view.findViewById(R.id.tv_author);
            category = (TextView) view.findViewById(R.id.category);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            iv_fav = (ImageView) view.findViewById(R.id.iv_fav);
            tv_ago = (TextView) view.findViewById(R.id.tv_ago);
            card_view = (CardView) view.findViewById(R.id.card_view);
        }
    }
}