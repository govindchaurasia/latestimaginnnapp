package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.pojos.CategorySelectionData;
import com.vision.imagine.pojos.Notify;
import com.vision.imagine.utils.TimeUtil;

import java.util.ArrayList;
import java.util.List;

public class NotifyAdapter extends RecyclerView.Adapter<NotifyAdapter.NotifyViewHolder>{
    Context context;
    public List<Notify> notifyList;

    public NotifyAdapter(Context context, List<Notify> notifyList){
        this.context = context;
        this.notifyList = notifyList;
    }

    @NonNull
    @Override
    public NotifyAdapter.NotifyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflater != null) {
            view = inflater.inflate(R.layout.row_notify_list_item, parent, false);
        }
        return new NotifyAdapter.NotifyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotifyAdapter.NotifyViewHolder holder, int position) {
        Notify notify = notifyList.get(position);
        if(notify!=null){
            holder.notifyTv.setText(notify.getMessage());
            holder.timeTv.setText(TimeUtil.calculateAge2(notify.getTimestamp()));
        }
    }

    @Override
    public int getItemCount() {
        return notifyList.size();
    }

    public class NotifyViewHolder extends RecyclerView.ViewHolder {
        public TextView notifyTv,timeTv;
        public NotifyViewHolder(View view) {
            super(view);
            notifyTv = (TextView) view.findViewById(R.id.notify_tv);
            timeTv = (TextView) view.findViewById(R.id.time_tv);
        }
    }

}
