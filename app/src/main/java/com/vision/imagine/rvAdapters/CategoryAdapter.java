package com.vision.imagine.rvAdapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.vision.imagine.MainActivity;
import com.vision.imagine.R;
import com.vision.imagine.pojos.CategoryDataSet;
import com.vision.imagine.pojos.CategorySelectionData;

import java.util.ArrayList;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;

public class CategoryAdapter  extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    Context context;
    public List<CategorySelectionData> categories;
    Drawable selectedIc,deselectIc;
    boolean isFilter = false;

    public CategoryAdapter(Context context, List<CategorySelectionData> categories, boolean isFilter){
        this.context = context;
        this.categories = categories;
        this.isFilter = isFilter;

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(context, R.drawable.selected_ic);
        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
        DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(MainActivity.colorSelected));
        selectedIc = unwrappedDrawable;
        Drawable unwrappedDrawable1 = AppCompatResources.getDrawable(context, R.drawable.deselect_ic);
        Drawable wrappedDrawable1 = DrawableCompat.wrap(unwrappedDrawable1);
        DrawableCompat.setTint(wrappedDrawable1, context.getResources().getColor(MainActivity.colorSelectedLight));
        deselectIc = unwrappedDrawable1;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflater != null) {
            view = inflater.inflate(R.layout.row_category_list_item, parent, false);
        }
        return new CategoryAdapter.CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        CategorySelectionData category = categories.get(position);
        if (category!=null){
            holder.catName.setText(category.getCatName());
            if(category.getIsSelected()==0){
                holder.checkBox.setImageDrawable(deselectIc);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   // holder.checkBox.setForeground(context.getResources().getDrawable(R.drawable.transparent_bg));
                }
            }else {
                holder.checkBox.setImageDrawable(selectedIc);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //holder.checkBox.setForeground(context.getResources().getDrawable(R.drawable.white_border));
                }
            }

            holder.mainPanel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(categories.get(position).getIsSelected() == 1) {
                        categories.get(position).setIsSelected(0);
                    }else if(!isMaxSelected()){
                        categories.get(position).setIsSelected(1);
                    } else {
                        Toast.makeText(context,"You can select maximum 3 fields", Toast.LENGTH_LONG).show();
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    private boolean isMaxSelected(){
        if(!isFilter) {
            int i = 0;
            for (CategorySelectionData data : categories) {
                if (data.getIsSelected() == 1) {
                    i++;
                }
            }
            if (i >= 3) {
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }

    public List<CategorySelectionData> getSelectedCategories(){
        List<CategorySelectionData> selected = new ArrayList<>();
        for(CategorySelectionData data : categories){
            if(data.getIsSelected() == 1){
                selected.add(data);
            }
        }
        return selected;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView catName;
        ImageView checkBox;
        LinearLayout mainPanel;

        public CategoryViewHolder(View view) {
            super(view);
            catName = (TextView) view.findViewById(R.id.cat_name_tv);
            checkBox = (ImageView) view.findViewById(R.id.check_iv);
            mainPanel = (LinearLayout) view.findViewById(R.id.main_panel);
        }
    }
}
